import Vue from "vue";
import VueRouter from "vue-router";

import TypeCalendar from "./../components/createCalendar/TypeCalendar.vue";
import AddTypeCalendar from "./../components/createCalendar/AddTypeCalendar.vue";
import EditTypeCalendar from "./../components/createCalendar/EditTypeCalendar.vue";
import AddTypeWorkerCalendar from "./../components/createCalendar/AddTypeWorkerCalendar.vue";
import UserCalendar from "./../components/createCalendar/UserCalendar.vue";
import WorkerCalendar from "./../components/createCalendar/WorkerCalendar.vue";
import AddWorkerCalendar from "./../components/createCalendar/AddWorkerCalendar.vue";
import EditWorkerCalendar from "./../components/createCalendar/EditWorkerCalendar.vue";
import SchedulesCalendar from "./../components/createCalendar/SchedulesCalendar.vue";
import WorkingDay from "./../components/createCalendar/WorkingDay.vue";
import LeaveDay from "./../components/createCalendar/LeaveDay.vue";
import AddLeaveDay from "./../components/createCalendar/AddLeaveDay.vue";
import EditLeaveDay from "./../components/createCalendar/EditLeaveDay.vue";
import ListingCalendar from "./../components/createCalendar/ListingCalendar.vue";
import CalendarView from "./../components/createCalendar/CalendarView.vue";
import ListView from "./../components/createCalendar/ListView.vue";
import SettingsCalendar from "./../components/createCalendar/SettingsCalendar.vue";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        redirect: { name: "listingCalendar.calendarView" },
        component: SchedulesCalendar,
        name: "createCalendar"
    },
    {
        path: "/type",
        component: TypeCalendar,
        name: "typeCalendar",
        props: true
        // children: [
        //     {
        //         path: "/add",
        //         component: AddTypeCalendar,
        //         name: "addTypeCalenar",
        //         props: true
        //     }
        // ]
    },
    {
        path: "/settings",
        component: SettingsCalendar,
        name: "settingsCalendar",
        props: true
    },
    {
        path: "/type/add",
        component: AddTypeCalendar,
        name: "addTypeCalendar",
        props: true
    },
    {
        path: "/type/:id/edit",
        component: EditTypeCalendar,
        name: "editTypeCalendar",
        props: true
    },
    {
        path: "/type/:id/worker",
        component: AddTypeWorkerCalendar,
        name: "addTypeWorkerCalendar",
        props: true
    },
    {
        path: "/user",
        component: UserCalendar,
        name: "userCalendar",
        props: true
    },
    {
        path: "/worker",
        component: WorkerCalendar,
        name: "workerCalendar",
        props: true
    },
    {
        path: "/worker/add",
        component: AddWorkerCalendar,
        name: "addWorkerCalendar",
        props: true
    },
    {
        path: "/worker/:id/edit",
        component: EditWorkerCalendar,
        name: "editWorkerCalendar",
        props: true
    },
    {
        path: "/schedules",
        component: SchedulesCalendar,
        name: "schedulesCalendar",
        redirect: { name: "schedulesCalendar.workingDay" },
        props: true,
        children: [
            {
                path: "working-day",
                components: {
                    schedules: WorkingDay
                },
                name: "schedulesCalendar.workingDay",
                props: true
            },
            {
                path: "leave-day",
                components: {
                    schedules: LeaveDay
                },
                name: "schedulesCalendar.leaveDay",
                props: true
            },
            {
                path: "leave-day/add",
                components: {
                    schedules: AddLeaveDay
                },
                name: "schedulesCalendar.addLeaveDay",
                props: true
            },
            {
                path: "leave-day/:id/add",
                components: {
                    schedules: EditLeaveDay
                },
                name: "schedulesCalendar.editLeaveDay",
                props: true
            }
        ]
    },
    {
        path: "/listing",
        component: ListingCalendar,
        name: "listingCalendar",
        redirect: { name: "listingCalendar.calendarView" },
        props: true,
        children: [
            {
                path: "calendar-view",
                components: {
                    listing: CalendarView
                },
                name: "listingCalendar.calendarView",
                props: true
            },
            {
                path: "list-view",
                components: {
                    listing: ListView
                },
                name: "listingCalendar.listView",
                props: true
            }
            // {
            //     path: "leave-day/add",
            //     components: {
            //         schedules: AddLeaveDay
            //     },
            //     name: "schedulesCalendar.addLeaveDay",
            //     props: true
            // },
            // {
            //     path: "leave-day/:id/add",
            //     components: {
            //         schedules: EditLeaveDay
            //     },
            //     name: "schedulesCalendar.editLeaveDay",
            //     props: true
            // }
        ]
    },
    {
        path: "*",
        redirect: { name: "listingCalendar.calendarView" },
        component: SchedulesCalendar
    }
];
const router = new VueRouter({
    base: "user/calendar/create",
    routes,
    hashbang: false,
    mode: "history"
});

export default router;
