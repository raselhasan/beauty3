<?php
return [
  //header-blade	
  'home' => 'Home',
  'services' => 'Services',
  'sell_services' => 'Sell Services',
  'buy_services' => 'buy Services',
  'offers' => 'Offers',
  'users' => 'Users',
  'models' => 'Models',
  'appointment' => 'Appointment',
  'faq' => 'FAQ',
  'contact' => 'Contact',
  'english' => 'English',
  'lithuania' => 'Lithuania',
  'rashiya' => 'Rashiya',
  'profile' => 'Profile',
  'create_calendar' => 'Create Calendar',
  'payment_history' => 'Payment-history',
  'log_out' => 'Log out',
  'log_in' => 'Log in',
  'register' => 'Register',
  'live_zone' => 'Live Zone',
  'booking_list' => 'Booking List',
  'service_payment' => 'Service payment',
  'remove_adds' => 'Remove Adds',
  'filtruoti' => 'Filtruoti',
  '' => '',
  '' => '',
  '' => '',
  '' => '',

  //header-filter.blade
  'volksagen' => 'volksagen',
  'men' => 'men',

  //header-filter-new.blade
  'meistrai' => 'MEISTRAI',
  'paslaugos' => 'PASLAUGOS',
  'km' => 'KM',
  'min' => 'min',
  'max' => 'max',
  'c' => 'C',
  'anuliuoti' => 'ANULIUOTI',

  //login
  'login' => 'Login',
  'login_with_facebook' => 'Login with Facebook',
  'login_with_google' => 'Login with Google',
  'password' => 'Password',

  //Registration
  'registration' => 'Registration',
  'nickname' => 'Nickname',
  'company_name' => 'Company name',

  //service-filter
  'reset_filter' => 'Reset filter',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',


  //index
  'third_slider_lavel' => 'Third slide label',
  'praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur' => 'Praesent commodo cursus magna, vel scelerisque nisl consectetur',
  'previous' => 'Previous',
  'next' => 'Next',
  'category_and_sub_category' => 'CATEGORY AND SUB CATEGORY',
  'pages' => 'Pages',
  'vip_paslaugos' => 'VIP PASLAUGOS',
  'fotografe_vilma' => 'FOTOGRAFĖ VILMA',

  //index-filter
  'salons' => 'Salons',
  'personal' => 'Personal',
  'atstuumas' => 'atstuumas',
  'search' => 'Search',
  'reset' => 'Reset',


  //footer
  'footer_text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make',
  'navigation' => 'Navigation',
  'ypatingi_pasiulymai' => 'YPATINGI PASIŪLYMAI',
  'prenumeruoti_naujienas' => 'PRENUMERUOTI NAUJIENAS',
  'reserved' => ' | All rights reserved | Grožio Kalviai, 2020',
  'susisiekti' => 'SUSISIEKTI',
  'terms_and_conditions' => 'Terms and Conditions',
  '' => '',



  // about model
  'cetagory' => 'Cetagory',
  'sub_cetagory' => 'Sub Cetagory',
  'inner_cetagory' => 'Inner Cetagory',
  'available_apointment' => 'Available Apointment:',
  'unavailable' => 'Unavailable',
  'book_available' => 'Book available',
  'time1' => '8:00am-8:00pm',
  'time2' => '0 time slot available',
  'send' => 'Send',
  'clear' => 'Clear',
  'populer_masters' => 'Populer Masters',
  'demo1' => 'Lorem Ipsum is simply dummy text of the printing to make',
  'replay_comment' => 'Replay Comment',
  'cancel' => 'Canel',
  'laisvo_laiko_rezervavimas' => 'Laisvo laiko rezervavimas',
  'sutaupysite' => 'Sutaupysite',
  'add_cart' => 'add cart',
  'comments' => 'Comments',
  'laisvo_laiko_rezervavimas' => 'Laisvo laiko rezervavimas',
  'covered' => 'During this make-up, the skin is perfectly covered and the face is contoured, eyes, lips are highlighted, and artificial lashes are glued. The goal of makeup is to turn you into a real party star.',
  'duration' => 'Duration',
  'validity' => 'Validity',
  'pre-registration_required' => 'Pre-registration required',
  'provider' => 'The details of the service provider will be visible when you purchase the e-mail.check',
  'checke' => 'This check is multi-type',
  'vouchers' => 'It is possible to pay with multi-type gift vouchers and use the services of other partners. More',
  'add_to_cart' => 'Add to cart',
  '1p.m.20min' => '1 p.m. 20 min.',
  '6months' => '6 months',
  'evening_makeup' => 'Evening makeup',
  'type' => 'Type',
  'price' => 'Price',
  'all' => 'All',



  //add control
  'add_controll_payment' => 'Add controll payment',
  'choose_duration' => 'choose duration',
  'paysera' => 'Paysera',
  'card_payment' => 'Card Payment',
  'pay_now' => 'Pay now',
  'plaukai' => 'PLAUKAI',

  //blog
  'palaugos_salia_taves' => 'palaugos salia taves',
  'see_more' => 'See More',
  'visus_ancitos' => 'Visus ancitos',

  //blog.blade
  'rikiavimas' => 'Rikiavimas',
  'naujausi_viršuje' => 'Naujausi viršuje',
  'see_more' => 'See more',
  'yra_vietu' => 'YRA VIETŲ',
  'visos_paslaugos' => 'Visos paslaugos',
  'you_have_no_favarite_items' => 'You have no favarite items',
  'skelbimai' => 'SKELBIMAI',
  'kūno_formavimas' => 'Kūno formavimas',
  'daugiau' => 'Daugiau',

  //booking-list.blade
  'booking_List' => 'Booking List',
  'service' => 'Service',
  'worker' => 'Worker',
  'date' => 'Date',
  'time' => 'Time',
  'action' => 'Action',
  'complete' => 'Complete',
  'cancle' => 'Cancle',
  'pending' => 'Pending',
  'profle' => 'Profle',
  'populer_masters' => 'Populer Masters',
  'simply' => 'Lorem Ipsum is simply dummy text of the printing to make',
  'third_slide_label' => 'Third slide label',
  'cursus' => 'Praesent commodo cursus magna, vel scelerisque nisl consectetur.',
  'are_you_sure?' => 'Are you sure?',
  'no' => 'No',
  'yes' => 'Yes',


  //comment
  'replay' => 'Replay',

  //my-service
  'active_service' => 'Active Service',
  'choose' => 'choose...',
  'address' => 'Address',
  'city' => 'City',
  'more_show' => 'More show',
  'comments' => 'Comments',
  'jhon' => 'Jhon',
  'time3' => '03-11-2019 at 10:00am',
  'edit' => 'Edit',
  'type' => 'Type',
  'price' => 'Price',
  'populer_masters' => 'Populer Masters',
  'update_your_profile_info' => 'Update your profile info',
  'title' => 'Title',
  'description' => 'Description',
  'save' => 'Save',
  'add_gallery_tab' => 'Add Gallery Tab',
  'tab_name' => 'Tab Name',
  'add' => 'Add',
  'service_image' => 'Service Image',
  'upload_service_image' => 'Upload Service Image',
  'update' => 'Update',
  'gallery_image' => 'Gallery Image',
  'add_gallery_image' => 'Add Gallery Image',
  'crop' => 'Crop',
  'update_service_image' => 'Update service image',
  'crop_the_image' => 'Crop the image',
  '' => '',
  '' => '',
  '' => '',
  '' => '',

  //payment-history
  'service_payment' => 'Service Payment',
  'profile_payment' => 'Profile Payment',
  'add_controll_payment' => 'Add controll paymentAdd controll payment',
  'you_have_no_payment_history' => 'You have no payment history',

  //payment-list
  'paid_services' => 'Paid Services',
  'update_service' => 'Update Service',
  'service_histories' => 'Service Histories',
  'service' => 'Service',
  'package' => 'Package',
  'star' => 'Star',
  'start_date' => 'Start Date',
  'end_date' => 'End Date',
  'day_left' => 'Day Left',
  'status' => 'Status',
  'add_controlls_histories' => 'Add Controlls Histories',
  'package_name' => 'Package Name',
  'profile_histories' => 'Profile Histories',
  'upgrade' => 'Upgrade',

  //profile-list.blade
  'found' => 'Sorry, We not found any data with the name of',

  //public-profile
  'share' => 'Share',
  'bio' => 'Bio',
  'live_zona' => 'Live Zona',
  'lorem_ipsum' => 'Lorem Ipsum',
  '' => '',

  //select-plan
  'select_service' => 'Select service',
  'checkout' => 'Checkout',
  'your_order' => 'Your order',
  'select_payment_method' => 'Select payment method',
  'active_without_payment' => 'Active without Payment',
  'more_info' => 'More info',
  'additional_paymen' => 'Additional Paymen',


  //services
  'see_more' => 'See more',
  'inner_category' => 'Inner Category:',
  'dummy' => 'Lorem Ipsum is simply dummy text of the printing',
  'chose' => 'chose',

  //services-date
  '1min' => '1 min',
  '' => '',
  '' => '',
  '' => '',

  //user-profile
  'add_live_zone' => 'Add Live Zone',
  'gallery' => 'Gallery',
  'add_picture' => 'Add picture',
  'my_services' => 'My Services',
  'add_my_services' => 'Add my services',
  'crop_the_image' => 'Crop the image',
  'profile_picture' => 'Profile Picture',
  'upload_profile_picture' => 'Upload Profile Picture',
  'update_your_picture' => 'update your picture',
  'add_image' => 'Add Image',
  'embade_a_video' => 'Embade a Video',
  'embade_link' => 'Embade Link',
  'upload_image' => 'Upload Image',
  'update_your_profile_info' => 'Update your profile info',
  'account_type' => 'Account Type',
  'personal' => 'Personal',
  'name' => 'Name',
  'usurname' => 'Usurname',
  'nick_name' => 'Nick Name',
  'email' => 'Email',
  'phone' => 'Phone',
  'show_on_profile' => 'Show on profile',
  'social_info' => 'Social Info',
  'or' => 'or',
  'facebook_link' => 'Facebook-link',
  'twitter_link' => 'Twitter-link',
  'gmail_link' => 'Gmail-link',
  'linkedin_link' => 'Linkedin-link',
  'pinterest_link' => 'Pinterest-link',
  'short_bio' => 'Short Bio',
  'update_your_cover_photos' => 'Update your cover photos',
  'cover_photos' => 'Cover Photos',
  'upload_cover_photos' => 'Upload Cover Photos',
  'make_your_profile_vip' => 'Make your profile VIP',
  'payment_method' => 'Payment Method',
  'make_vip' => 'Make vip',
  'upgrade_vip' => 'Upgrade VIP',
  'services' => 'Services',
  '5comments' => '5 comments',
  'like' => 'Like',
  'comment' => 'Comment',
  'view_8_comments' => 'View 8 comments',
  '3of12' => '3 of 12',
  'tomal' => 'Tomal',
  'pabaigos' => 'Nuostabi meistre! Makiažas atlaike iki pat pabaigos, meistre labai kruopšti, nekeicia žmogaus, o išryškina jo gražiausius bruožus',
  'view_more_7_replies' => 'View more 7 replies',
  'write_your_comment' => 'Write your comment',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',

  //vip-profile
  '' => '',
  'vip_profile' => 'VIP PROFILES',
  'vip_services' => 'VIP SERVICES',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  //calender
  'document' => 'Document',
  '' => '',
  //app.blade
  'some_text_some_message' => 'Some text some message',
  '' => '',
  '' => '',
  //add-contro-payment.blade
  'total' => 'Total',
  '' => '',
  '' => '',
  //payment.pdf.blade
  'duration_price' => 'Duration Price',
  'star_price' => 'Star Price',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',


  //new fariha
  //select-plan
  'hello' => 'Hello,',
  'we_are_giving' => 'we are giving to you 6 month free for your service. If you need additional advantage for your service please select item form below box. You can active your sevice without payment for 6 month.',

  //services-date
  'sugar' => 'Sugar Depilation (full bikini for women)',
  '1.2km' => '1.2km',
  '35.00_€' => '35.00 €',
  '5.00' => '5.00',
  '5' => '(5)',
  'no_result_found' => 'Sorry! No result found. Please try another.',

  //index.blade
  'būtent_tai' => 'Atraskite būtent tai ko jums reikia ',
  'asmeninė_fotosesija' => 'Asmeninė fotosesija',
  'nuo_50_€' => 'nuo 50 €',
  'laisvas' => 'LAISVAS',
  'daugiau' => 'DAUGIAU',
  'vip_meistrai' => 'VIP MEISTRAI',
  'visos_paslaugos' => 'VISOS PASLAUGOS',
  '4/5' => '4/5',
  'kategorijos' => 'KATEGORIJOS',







];
