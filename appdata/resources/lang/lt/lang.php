<?php
return [
  //header-blade	
  'home' => 'Home',
  'services' => 'Paslaugos',
  'sell_services' => 'Pardavimai',
  'buy_services' => 'Užsąkymai',
  'offers' => 'Nuolaidos',
  'users' => 'Nariai',
  'models' => 'Models',
  'appointment' => 'Appointment',
  'faq' => 'FAQ',
  'contact' => 'Contaktai',
  'english' => 'English',
  'lithuania' => 'Lithuania',
  'rashiya' => 'Russian',
  'profile' => 'Profilis',
  'create_calendar' => 'Rezervacijų valdymas',
  'payment_history' => 'Mokėjimų istorija',
  'log_out' => 'Atsijungti',
  'log_in' => 'Prisijungti',
  'register' => 'Registruotis',
  'live_zone' => 'Live Zona',
  'booking_list' => 'Užsąkymų sąršas',
  'service_payment' => 'Paslaugų mokėjimai',
  'remove_adds' => 'Pašalinti reklamas',
  'filtruoti' => 'Filtruoti',
  '' => '',
  '' => '',
  '' => '',
  '' => '',

  //header-filter.blade
  'volksagen' => 'volksagen',
  'men' => 'men',

  //header-filter-new.blade
  'meistrai' => 'SALONAI',
  'paslaugos' => 'PRIVATŪS',
  'km' => 'Km',
  'min' => 'Min',
  'max' => 'Max',
  'c' => '€',
  'anuliuoti' => 'IŠVALYTI',

  //login
  'login' => 'Prisijungti',
  'login_with_facebook' => 'Prisijungti su Facebook',
  'login_with_google' => 'Prisijungti su Google',
  'password' => 'Pasvordas',

  //Registration
  'registration' => 'Registracija',
  'nickname' => 'Slapyvardis',
  'company_name' => 'Salono ar įmonės pavadinimas',

  //service-filter
  'reset_filter' => 'Resetinti',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',


  //index
  'third_slider_lavel' => 'Third slide label',
  'praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur' => 'Praesent commodo cursus magna, vel scelerisque nisl consectetur',
  'previous' => 'Prieš tai',
  'next' => 'Sekantis',
  'category_and_sub_category' => 'Kategorija ir subkategorija',
  'pages' => 'Puslapiai',
  'vip_paslaugos' => 'VIP PASLAUGOS',
  'fotografe_vilma' => 'FOTOGRAFĖ VILMA',

  //index-filter
  'salons' => 'Salonai',
  'personal' => 'Privatūs',
  'atstuumas' => 'Atstumas',
  'search' => 'Ieškoti',
  'reset' => 'Reset',


  //footer
  'footer_text' => 'Grižio meistrai ir grožio paslaugos vienoje vietoje. Plaukų kirpimas, masažai, veido proceduros, nagų tvarkymas, kuno prieziura, SPA procedūros, depiliacija ir kita.',
  'navigation' => 'Navigation',
  'ypatingi_pasiulymai' => 'YPATINGI PASIŪLYMAI',
  'prenumeruoti_naujienas' => 'PRENUMERUOTI NAUJIENAS',
  'reserved' => ' | Visos teisės saugomos | Grožiokalviai.lt © 2020',
  'susisiekti' => 'SUSISIEKTI',
  'terms_and_conditions' => 'Taisyklės / Privartumas',
  '' => '',



  // about model
  'cetagory' => 'Kategorijos',
  'sub_cetagory' => 'Sub kategorijos',
  'inner_cetagory' => 'Vidinės kategorijos',
  'available_apointment' => 'Laisvos vietos:',
  'unavailable' => 'Užimtos vietos',
  'book_available' => 'Galima užsakyti',
  'time1' => '8:00am-8:00pm',
  'time2' => '0 time slot available',
  'send' => 'Siūsti',
  'clear' => 'Išvalyti',
  'populer_masters' => 'Populiarūs meistrai',
  'demo1' => 'Tai yra demo textas',
  'replay_comment' => 'Atsakyti į komentara',
  'cancel' => 'Baigti',
  'laisvo_laiko_rezervavimas' => 'Laisvo laiko rezervavimas',
  'sutaupysite' => 'Sutaupysite',
  'add_cart' => 'Į krepšelį',
  'comments' => 'KOmentarai',
  'laisvo_laiko_rezervavimas' => 'Laisvo laiko rezervavimas',
  'covered' => 'Demo textras',
  'duration' => 'Trukmė',
  'validity' => 'Galioje iki',
  'pre-registration_required' => 'Reikalinga registracija',
  'provider' => 'Išsami informacija apie paslaugų teikėją bus matoma įsigijus i E-mail.patikrinkit',
  'checke' => 'Šis patikrinimas yra kelių tipų',
  'vouchers' => 'Galima atsiskaityti kelių rūšių dovanų kuponais ir naudotis kitų partnerių paslaugomis. Daugiau',
  'add_to_cart' => 'Idėti į krepšelį',
  '1p.m.20min' => '1 p.m. 20 min.',
  '6months' => '6 mėnesiai',
  'evening_makeup' => 'Vakaro makiažas',
  'type' => 'Tipas',
  'price' => 'Kaina',
  'all' => 'Visi',



  //add control
  'add_controll_payment' => 'Reklamų pašalinimas skelbime ir profilyje',
  'choose_duration' => 'Pasirinkti laikotarpį',
  'paysera' => 'Paysera',
  'card_payment' => 'Mokejimas kortele',
  'pay_now' => 'Moketi dabar',
  'plaukai' => 'PLAUKAI',

  //blog
  'palaugos_salia_taves' => 'Paslaugos šalia tavęs',
  'see_more' => 'Žiūrėti daugiau',
  'visus_ancitos' => 'Visus',

  //blog.blade
  'rikiavimas' => 'Rikiavimas',
  'naujausi_viršuje' => 'Naujausi viršuje',
  'see_more' => 'Žiūrėti daugiau',
  'yra_vietu' => 'YRA VIETŲ',
  'visos_paslaugos' => 'Visos paslaugos',
  'you_have_no_favarite_items' => 'Jūs dar neturite mėgstamų skelbimų',
  'skelbimai' => 'SKELBIMAI',
  'kūno_formavimas' => 'Kūno formavimas',
  'daugiau' => 'Daugiau',

  //booking-list.blade
  'booking_List' => 'Užsąkymų sąrašas',
  'service' => 'Pąslaugos',
  'worker' => 'Darbuotojei',
  'date' => 'Data',
  'time' => 'Laikas',
  'action' => 'Veiksmas',
  'complete' => 'Užbaigta',
  'cancle' => 'Baigta',
  'pending' => 'Laukiama',
  'profle' => 'Profilis',
  'populer_masters' => 'Populiarūs meistrai',
  'simply' => 'Demo textas',
  'third_slide_label' => 'Trečioji skaidrių etiketė',
  'cursus' => 'Demo text.',
  'are_you_sure?' => 'Ar Jūs tikras?',
  'no' => 'Ne',
  'yes' => 'Taip',


  //comment
  'replay' => 'Atraskyti',

  //my-service
  'active_service' => 'Aktyvuoti paslaugą',
  'choose' => 'Uždaryti...',
  'address' => 'Adresas',
  'city' => 'City',
  'more_show' => 'Rodyti daugiau',
  'comments' => 'Atsiliepimai',
  'jhon' => 'Jhon',
  'time3' => '03-11-2019 at 10:00am',
  'edit' => 'Redaguoti',
  'type' => 'Rašyti',
  'price' => 'Kaina',
  'populer_masters' => 'Populiarūs meistrai',
  'update_your_profile_info' => 'Atnaujinti Jūsų profilio informaciją',
  'title' => 'Pavadinimas',
  'description' => 'Aprašymas',
  'save' => 'Išsaugoti',
  'add_gallery_tab' => 'Pridėti galerijos menių',
  'tab_name' => 'Menių pavadinimas',
  'add' => 'Skelbimas',
  'service_image' => 'Paslaugos paveiksliukas',
  'upload_service_image' => 'Įkelti nuotrauką',
  'update' => 'Atnaujinti',
  'gallery_image' => 'Galerijos nuotraukos',
  'add_gallery_image' => 'Pridėti nuotraukų į galeriją',
  'crop' => 'Kirpti',
  'update_service_image' => 'Atnaujinti nuotrauką',
  'crop_the_image' => 'Kirpti nuotrauką',
  '' => '',
  '' => '',
  '' => '',
  '' => '',

  //payment-history
  'service_payment' => 'Paslaugos mokėjimas',
  'profile_payment' => 'Profilio nuorauka',
  'add_controll_payment' => 'Reklamų pašalinimas skelbime ir profilyje',
  'you_have_no_payment_history' => 'Jūs neturite mokėjimų',

  //payment-list
  'paid_services' => 'Mokamos paslaugos',
  'update_service' => 'Atnaujinti paslaugą',
  'service_histories' => 'Paslaugų istorija',
  'service' => 'Paslaugos',
  'package' => 'Pakuotė',
  'star' => 'Žvaigždutės',
  'start_date' => 'Pradžia',
  'end_date' => 'Pabaiga',
  'day_left' => 'Likurios dienos',
  'status' => 'Statusas',
  'add_controlls_histories' => 'Reklamų išjungimas Jūsų ervėse',
  'package_name' => 'Paketų pavadinimai',
  'profile_histories' => 'Profiio istorijos',
  'upgrade' => 'Atnaujinti',

  //profile-list.blade
  'found' => 'Atsiprašome, neradome jokių duomenų su šiuo vardu',

  //public-profile
  'share' => 'Dalintis',
  'bio' => 'BIO',
  'live_zona' => 'Live Zona',
  'lorem_ipsum' => 'Demo textas',
  '' => '',

  //select-plan
  'select_service' => 'Pasirinkti paslaugą',
  'checkout' => 'Apmokėti',
  'your_order' => 'Jūsų užsakymas',
  'select_payment_method' => 'Pasirinkti mokėjimų būdą',
  'active_without_payment' => 'Aktyvuoti be mokėjimo',
  'more_info' => 'Daugiau informacijos',
  'additional_paymen' => 'Papildomi mokėjimai',


  //services
  'see_more' => 'Žiūrėti daugiau',
  'inner_category' => 'Vidinė kategorija:',
  'dummy' => 'Paprastas demo textas',
  'chose' => 'Pasirinkti',

  //services-date
  '1min' => '1 min',
  '' => '',
  '' => '',
  '' => '',

  //user-profile
  'add_live_zone' => 'Pridėti Live įrašą',
  'gallery' => 'Galerija',
  'add_picture' => 'Pridėti nuotrauką',
  'my_services' => 'Mano paslaugos',
  'add_my_services' => 'Pridėti paslaugą',
  'crop_the_image' => 'Kirpti nuotrauką',
  'profile_picture' => 'Profilio nuotrauka',
  'upload_profile_picture' => 'Ikelti profilio nuotrauką',
  'update_your_picture' => 'Atnaujinti nuotrauką',
  'add_image' => 'Pridėti nuotrauką',
  'embade_a_video' => 'Youtube nuoroda URL',
  'embade_link' => 'Youtube URL',
  'upload_image' => 'Ikelti nuotrauką',
  'update_your_profile_info' => 'Atnaujinti profilio informaciją',
  'account_type' => 'Profilio tipas',
  'personal' => 'Asmeninis',
  'name' => 'Vardas',
  'usurname' => 'Pavardė',
  'nick_name' => 'Slapyvardis',
  'email' => 'E-mail.',
  'phone' => 'Telefonas',
  'show_on_profile' => 'Rodyti profilyje',
  'social_info' => 'Socialiniai  tinklai',
  'or' => 'arba',
  'facebook_link' => 'Facebook-linkas',
  'twitter_link' => 'Twitter-linkas',
  'gmail_link' => 'Gmail-linkas',
  'linkedin_link' => 'Linkedin-linkas',
  'pinterest_link' => 'Pinterest-linkas',
  'short_bio' => 'Rodyti BIO',
  'update_your_cover_photos' => 'Atnaujinti koverio nuotrauką',
  'cover_photos' => 'Koverio nuotrauka',
  'upload_cover_photos' => 'Ikelti koverio nuotrauką',
  'make_your_profile_vip' => 'Padaryk savo profilį VIP',
  'payment_method' => 'Mokėjimo būdas',
  'make_vip' => 'Padaryk VIP',
  'upgrade_vip' => 'Atnaujinti VIP',
  'services' => 'Paslaugos',
  '5comments' => 'Komentarai',
  'like' => 'Patinka',
  'comment' => 'Komentaras',
  'view_8_comments' => 'Žiūrėti komentarus',
  '3of12' => 'iš',
  'tomal' => 'Vardas',
  'pabaigos' => 'Nuostabi meistre! Makiažas išlaikė visą vakarą, nuostabi meistrė, nekeičia žmogaus, o išryškina jo gražiausius bruožus',
  'view_more_7_replies' => 'Peržiūrėti daugiau komentarų',
  'write_your_comment' => 'Rašyti komentarą',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',

  //vip-profile
  '' => '',
  'vip_profile' => 'VIP PROFILIS',
  'vip_services' => 'VIP PASLAUGA',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  //calender
  'document' => 'Dokumentas',
  '' => '',
  //app.blade
  'some_text_some_message' => 'Demo textas',
  '' => '',
  '' => '',
  //add-contro-payment.blade
  'total' => 'Iš viso',
  '' => '',
  '' => '',
  //payment.pdf.blade
  'duration_price' => 'Trukmės kaina',
  'star_price' => 'Pradinė kaina',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',


  //new fariha
  //select-plan
  'hello' => 'Sveiki,',
  'we_are_giving' => 'Dovanojeme Jums 6 mėnesius nemokama paslaugos publikaciją. Jei jums reikia papildomų pranašumų, pasirinkite žemiau esančią papildomą paslaugą. O paslaugą 6 mėnesius aktyvuokite nemokėdami.',

  //services-date
  'sugar' => 'Cukraus depilecija (full bikini for women)',
  '1.2km' => '1.2km',
  '35.00_€' => '35.00 €',
  '5.00' => '5.00',
  '5' => '(5)',
  'no_result_found' => 'Atsiprašome! Neradome jokių rezultatų. Mėginkite iš naujo.',

  //index.blade
  'būtent_tai' => 'Geriausi meistrai Jūsų paslaugoms ',
  'asmeninė_fotosesija' => 'Asmeninė fotosesija',
  'nuo_50_€' => 'nuo 50 €',
  'laisvas' => 'LAISVAS',
  'daugiau' => 'DAUGIAU',
  'vip_meistrai' => 'VIP MEISTRAI',
  'visos_paslaugos' => 'VISOS PASLAUGOS',
  '4/5' => '4/5',
  'kategorijos' => 'KATEGORIJOS',
  'nuo' => 'nuo',
  'euro' => '€',
  'km' => 'km',







];
