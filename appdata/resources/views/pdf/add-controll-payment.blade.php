<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table{
			border-collapse: collapse;
      		width: 100%;
      		font-weight: normal;
		}
		th,td{
			padding: 8px;
		    text-align: left;
		    border-bottom: 1px solid #ddd;
		    font-weight: normal;
		}
		th{
			font-weight: bold;
		}
		p{
			margin: 6px;
		}

	</style>
</head>
<body>
	<div style="text-align: center;">
		<h1>GrožioKalviai</h1>
		<p>Date: {{date('Y-m-d',strtotime($payments['created_at']))}}</p>
		<p>Order: # {{$payments['order_id']}}</p>
	</div>
	<div style="margin-top: 50px;">
		<p>{{$user->name}} @if($user->surname) {{$user->username}} @endif @if($user->nickname)({{$user->nickname}})@endif</p>
		@if($user->address)
			<p>{{$user->address}}</p>
		@endif
		@if($user->phone)
			<p>Phone: {{$user->phone}}</p>
		@endif
		@if($user->email)
			<p>Email: {{$user->email}}</p>
		@endif
	</div>
	<div style="width: 100%; margin-top: 20px;">
		<table>
			<thead>
				<tr>
					<th>@lang('lang.package')</th>
					<th>@lang('lang.duration')</th>
					<th>@lang('lang.start_date')</th>
					<th>@lang('lang.end_date')</th>
					<th>@lang('lang.total')</th>
				</tr>
			</thead>
			<tbody>
				
				<tr>
					<td>{{$payments['add_showing_package']['name']}}</td>
					<td>{{$payments['add_showing_package']['duration']}}</td>
					<td>{{date('Y-m-d',strtotime($payments['start_date']))}}</td>
					<td>{{date('Y-m-d',strtotime($payments['end_date']))}}</td>
					<td>{{number_format($payments['price'],2)}}</td>
				</tr>
					
			</tbody>	
		</table>
	</div>
	<div style="text-align:right; margin-right:10px">
		<p><b>@lang('lang.total'): {{number_format($payments['price'],2)}}</b></p>
	</div>		

</body>
</html>