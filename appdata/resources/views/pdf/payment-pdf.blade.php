<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title></title>
	<style type="text/css">
		p{
			margin:1px;
			padding: 1px;
		}
		table{
			border-collapse: collapse;
      		width: 100%;
      		font-weight: normal;
		}
		th,td{
			padding: 8px;
		    text-align: left;
		    border-bottom: 1px solid #ddd;
		    font-weight: normal;
		}
		h2{
			margin:0px;
			padding: 0px;
		}
	</style>
</head>
<body>
	<div style="width: 100%; text-align: center">
		<h2>{{ $payments[0]['service_name']}} </h2>
		<p>Order id: # {{ $payments[0]['order_id']}}</p>
		<p>Date: {{ date('Y-m-d',strtotime($payments[0]['created_at']))}}</p>
	</div>
	<div style="width: 100%; margin-top:70px;">
		<p>{{ Auth()->user()->name }} {{ Auth()->user()->surname }}</p>
		<p>{{ Auth()->user()->address }}</p>
		<p>Email: {{ Auth()->user()->email }}</p>
		<p>Phone: {{ Auth()->user()->phone }}</p>
	</div>
	@php
		$col = 0;
	@endphp
	@foreach ($payments as $p_col)
		@if($p_col['star_number'])
			@php
				$col = 1;
			@endphp
		@endif
	@endforeach
	<div style="width: 100%; margin-top: 20px;">
		<table>
			<tr>
				<th><b>@lang('lang.package_name')</b></th>
				<th><b>@lang('lang.duration')</b></th>
				<th><b>@lang('lang.duration_price')</b></th>
				@if($col == 1)
					<th><b>@lang('lang.star')</b></th>
					<th><b>@lang('lang.star_price')</b></th>
				@endif
				<th><b><th>@lang('lang.total')</b></th>
			</tr>
			@php
				$total = 0;
			@endphp
			@foreach ($payments as $payment)
			<tr>
				<th>{{ $payment['package_name']}}</th>
				<th>{{ $payment['duration_name']}}</th>
				<th>
					@if($col == 1)
						@if($payment['star_price'])
							€{{ number_format($payment['paid_amount']-$payment['star_price'],2) }}
						@else 
							€{{ number_format($payment['paid_amount'],2) }}
						@endif
					@else 
						€{{ number_format($payment['paid_amount'],2) }}
					@endif
					
				</th>
				@if($col == 1)
					<th> {{$payment['star_number']}} </th>
					<th>
						@if($payment['star_price'])
							€{{number_format($payment['star_price'],2)}} 
						@endif
					</th>
				@endif
				<th>€{{ number_format($payment['paid_amount'],2) }}</th>
				@php
					$total = $total + $payment['paid_amount'];
				@endphp
			</tr>
			@endforeach
		</table>
		@if($col == 1)
			<div style="float: right; text-align:right;margin-right: 30px; margin-top: 10px;"><b><th>@lang('lang.total'): €{{number_format($total,2)}}</b></div>
		@else
			<div style="float: right; text-align:right;margin-right: 55px; margin-top: 10px;"><b><th>@lang('lang.total'): €{{number_format($total,2)}}</b></div>
		@endif
	</div>
</body>
</html>