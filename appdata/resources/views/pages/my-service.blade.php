@extends('layout.app')
@section('title')
{{@$title}} | GrožioKalviai
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fancybox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cropper.css') }}">
<style type="text/css">
	.gm-style-iw {
		max-width: 300px !important;
		max-height: 300px !important;
	}

	.gm-style-iw-d {
		max-width: 300px !important;
		max-height: 300px !important;
	}
</style>
@endsection
@section('content')
@include('inc.header-filter-new')
<section class="about-section">
	<div class="max-fix-width">
		@if($service->active == 1)
		<a href="{{url('select-plan/'.$service->id)}}" class="btn active-btn">Upgrade Service</a>
		@else
		<a href="{{url('select-plan/'.$service->id)}}" class="btn active-btn">Active Service</a>
		@endif
		<div class="link-menu">
			<span class="arrow-l">
				<i class="fas fa-arrow-left"></i>
			</span>
			<span>
				@if($service->category)
				<i class="fas fa-caret-right"></i><a href="#">{{$service->category->name_lt}}</a>
				@endif
				<i class="fas fa-caret-right"></i>
			</span>
			<span class="l-sub-cat service-title">{{$service->title}}</span>
		</div>
		<div class="about-top-tlt ">
			<div class="service-logo">
				<img src="{{asset('assets/img/services/' . ((@$service->image) ? $service->image : 'no_service.png'))}}" alt="img">
			</div>
			<h1 class="service-title">{{$service->title}}</h1>
			<div class="about-star d-none d-md-block">
				<div class="">
					<span>
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/half-star.png')}}" alt="img">
					</span>
					<span class="font-weight-bold">4.5/5</span>
				</div>
				<div class="">21 klientų įvertinimai</div>
			</div>
		</div>
		<div class="about-top-tlt border-none d-block d-md-none ml-3">
			<div class="about-star d-flex">
				<div class="">21 klientų įvertinimai</div>
				<div class="pl-2">
					<span>
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/half-star.png')}}" alt="img">
					</span>
					<span class="font-weight-bold">4.5/5</span>
				</div>
			</div>
		</div>
		<div class="row mar-0">
			<div class="col-12 col-lg-9">
				<div class="about-cnt mar-b-30">
					<div class="about-tlt">
						<input type="hidden" name="service_id_h" value="{{ $service->id }}">
						<div class="edit-info-btn btn-ctm sty-black" onclick="serviceTD(event)">
							<i class="fa fa-edit" title="edit image"></i>
						</div>
					</div>
					<div class="about-txt">
						<p>
							<span><i class="fas fa-exclamation-circle"></i></span>
						</p>
						<span class="service-description"> {!! $service->description !!}</span>
						<p>
							<span class="read-more">read more</span>
						</p>
					</div>
				</div>
				<div class="category-area">
					<div class="cat-slt">
						<div class="row">
							<div class="col-12 col-md-6">
								<div class="form-group">
									<label for="name">@lang('lang.cetagory')</label>
									<div class="ctm-select" ctm-slt-n="cat_name">
										<div class="ctm-select-txt pad-l-10" onclick="event.stopPropagation();">
											<span class="select-txt noselect" id="category">
												{{ (@count($service->category_id)) ? $service->category->name_lt : __('lang.chose') . '...' }}
											</span>
											<span class="select-arr"><i class="fas fa-caret-down"></i></span>
										</div>
										<div class="ctm-option-box noselect">
											<div class="ctm-option noselect">@lang('lang.choose')</div>
											@foreach($categories as $cat)
											<div class="ctm-option noselect" {{ ($cat->id == $service->category_id) ? 'ctm-select="true"' : '' }} ctm-otn-v="{{ $cat->id }}">{{ $cat->name_lt }}</div>
											@endforeach
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<label for="name">@lang('lang.sub_cetagory')</label>
									<div class="ctm-select" ctm-slt-n="sub_cat_name">
										<div class="ctm-select-txt pad-l-10" onclick="event.stopPropagation();">
											<span class="select-txt noselect" id="subCategory">

												{{ (@count($service->sub_category_id)) ? $service->subCategory->name_lt : __('lang.chose') . '...' }}

											</span>
											<span class="select-arr"><i class="fas fa-caret-down"></i></span>
										</div>
										<div class="ctm-option-box noselect">
											<div class="ctm-option noselect">@lang('lang.choose')</div>
											@foreach($sub_categories as $cat)
											<div class="ctm-option noselect" {{ ($cat->id == $service->sub_category_id) ? 'ctm-select="true"' : '' }} ctm-otn-v="{{ $cat->id }}">{{ $cat->name_lt }}</div>
											@endforeach
										</div>
									</div>
								</div>
							</div>

							<div class="col-12 col-md-6">
								<div class="form-group">
									<label for="name">@lang('lang.inner_cetagory')</label>
									<div class="ctm-select" ctm-slt-n="inner_cat_name">

										<div class="ctm-select-txt pad-l-10" onclick="event.stopPropagation();">
											<span class="select-txt noselect" id="category">@lang('lang.choose')</span>
											<span class="select-arr"><i class="fas fa-caret-down"></i></span>
										</div>
										<div class="ctm-option-box">
											<div class="ctm-option-ch">
												<label class="ctm-con mar-0 noselect">
													@lang('lang.choose')
												</label>
											</div>
											@foreach($inner_categories as $inner_cat)
											<div class="ctm-option-ch">
												<label class="ctm-con mar-0 noselect">
													{{ $inner_cat->name_lt }}
													<input type="checkbox" class="srvc-inner-cat" name="srvc_inner_cat[]" value="{{ $inner_cat->id }}" {{ (in_array($inner_cat->id, array_column($service->takeInnerCategory, 'id'))) ? 'checked' : '' }}>
													<span class="checkmark"></span>
												</label>
											</div>
											@endforeach
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-6">
								<div class="form-group">
									<?php
									$city_arr = ['Vilnius', 'Kaunas', 'Klaipėda', 'Panevėžys', 'Šiauliai'];
									?>
									<label for="city">@lang('lang.city')</label>
									<div class="input-group">
										<div class="ctm-select" ctm-slt-n="city_name">
											<div class="ctm-select-txt pad-l-10" onclick="event.stopPropagation();">
												<span class="select-txt noselect" id="city_name">
													{{ @$service->city? : __('lang.chose') . '...' }}
												</span>
												<span class="select-arr"><i class="fas fa-caret-down"></i></span>
											</div>
											<div class="ctm-option-box noselect">
												<div class="ctm-option noselect">
													@lang('lang.chose')...
												</div>
												@foreach($city_arr as $city)
												<div class="ctm-option noselect" {{ ($city == $service->city) ? 'ctm-select="true"' : '' }} ctm-otn-v="{{ $city }}">{{ $city }}</div>
												@endforeach
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							{{-- google address --}}
							<div class="col-md-6">
								<div class="form-group">
									<label for="exampleInputEmail1">@lang('lang.address')</label>
									<input type="text" class="form-control" id="autoAddress" aria-describedby="emailHelp" placeholder="Type address" autocomplete="off" value="{{$service->address}}">
								</div>
							</div>
							{{-- end google address --}}

						</div>
					</div>
					<div class="save-cat-btn">
						<div class="edit-info-btn btn-ctm sty-black" onclick="editServiceCategory(event)">
							<i class="fa fa-edit" title="edit categories"></i>
						</div>
					</div>
				</div>
				<div class="apointment-area">
					<view-calendar :own="1"></view-calendar>
				</div>
				<div class="about-nav service-gallery-area">
					@include('pages.myServiceInc.gallery-area')
					{{-- <div class="more"><button class="btn more-btn">@lang('lang.more_show')</button></div> --}}
				</div>
				{{-- <div class="comment-s">
					<div class="com-tlt">
						<h1><img src="{{asset('assets/img/services/com-i.png')}}" alt="img"> Comments(<span id="total_comment">10</span>)</h1>
				<div class="short-select">
					<span class="label-txt">Rikiavimas:</span>
					<div class="ctm-select" ctm-slt-n="cat_name">
						<div class="ctm-select-txt pad-l-10">
							<span class="select-txt" id="category">Naujausi viršuje</span>
							<span class="select-arr"><img src="{{asset('assets/img/services/triangle-d.png')}}" alt="img"></span>
						</div>
						<div class="ctm-option-box">
							<div class="ctm-option">Naujausi viršuje</div>
							<div class="ctm-option">Naujausi viršuje</div>
						</div>
					</div>
				</div>
			</div>
			<div class="com-text-ar">
				<div class="com-text-in">
					<div class="com-img"><img src="{{asset('assets/img/services/demo.jpg')}}" alt="img"></div>
					<div class="form-group">
						<label><span>Įvertinkite:</span>
							<span class="com-rew">
								<img class="star-n" src="{{asset('assets/img/services/star-w.png')}}" alt="img">
								<img class="star-active" src="{{asset('assets/img/services/star.png')}}" alt="img">
							</span>
							<span class="com-rew">
								<img class="star-n" src="{{asset('assets/img/services/star-w.png')}}" alt="img">
								<img class="star-active" src="{{asset('assets/img/services/star.png')}}" alt="img">
							</span>
							<span class="com-rew">
								<img class="star-n" src="{{asset('assets/img/services/star-w.png')}}" alt="img">
								<img class="star-active" src="{{asset('assets/img/services/star.png')}}" alt="img">
							</span>
							<span class="com-rew">
								<img class="star-n" src="{{asset('assets/img/services/star-w.png')}}" alt="img">
								<img class="star-active" src="{{asset('assets/img/services/star.png')}}" alt="img">
							</span>
							<span class="com-rew">
								<img class="star-n" src="{{asset('assets/img/services/star-w.png')}}" alt="img">
								<img class="star-active" src="{{asset('assets/img/services/star.png')}}" alt="img">
							</span>
						</label>
						<input placeholder="Rašykite komentarą..." class="form-control" id="user_comment">
					</div>
				</div>
				<div class="form-group com-btn">
					<button class="p-btn cansel btn" onclick="textClear()">@lang('lang.clear')</button>
					<button class="p-btn btn send" onclick="addComment()">@lang('lang.send')</button>
				</div>
			</div>
			<div class="com-reply">
				<div class="comment" id="main-cmt">
					<div class="row">
						<div class="user-img-c">
							<img src="{{asset('assets/img/services/demo.jpg')}}" alt="user-img">
						</div>
						<div class="col-12 pad-0">
							<div class="comment-cnt">
								<div class="user-time">
									<span class="com-rew-s">
										<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
										<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
										<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
										<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
										<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
									</span>
									<span class="user-n">tamal</span>
									<span class="com-t">2 Hours ago</span>
								</div>
								<p>Puiki paslaugų kokybė, visiems rekomenduoju!</p>
								<div class="edit-com"><span>Reply</span></div>
							</div>
							<div class="cmt-replay">
								<div class="reply">
									<div class="row">
										<div class="user-img-c">
											<img src="{{asset('assets/img/services/demo.jpg')}}" alt="user-img">
										</div>
										<div class="col-12 pad-0">
											<div class="comment-cnt">
												<div class="user-time">
													<span class="user-n">tamal</span>
													<span class="com-t">2 Hours ago</span>
												</div>
												<p>Puiki paslaugų kokybė, visiems rekomenduoju!</p>
												<div class="edit-com"><span>Reply</span></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> --}}
	</div>
	<div class="col-12 col-lg-3">
		<div class="model-tlt">
			@if(@$service->user->profile_image)
			<img src="{{asset('userImage/fixPic/' . $service->user->profile_image)}}" alt="img">
			@else
			<img src="{{asset('img/services/profile-img.png')}}" alt="img">
			@endif
			<div class="pro-btn">
				<a href="{{ route('user.profile') }}">
					<button class="p-btn btn">{{$service->user->name}} {{$service->user->surname}} - Profle</button>
				</a>
			</div>
		</div>
		<div class="model-bg-img" onclick="changeServiceImage(event)">
			<div class="model-s-img">
				@if($service['image'])
				<img src="{{asset('assets/img/services/'. $service['image'])}}" class="wth-100-p" alt="service-img">
				@else
				<img src="{{asset('assets/img/services/no_service.png')}}" class="wth-100-p" alt="service-img">
				@endif
			</div>
		</div>
		<div class="pro-box">
			<div class="pro-cnt">
				<!-- <div class="pro-name">{{$service->user->name}} {{$service->user->surname}}</div> -->
				<div class="pro-m-info">
					<div class="pro-tlt">Susisiekite:</div>
					<div class="pro-email"><i class="fa fa-envelope"></i>{{$service->user->email}}</div>
					<div class="pro-phn"><i class="fas fa-mobile-alt"></i>{{$service->user->phone}}</div>
					<div class="pro-cn"><i class="fas fa-map-marker-alt"></i>{{$service->address}}</div>
				</div>
				<div class="pro-m-info">
					<div class="pro-tlt">Darbo valandos:</div>
					<div class="pro-m-time">
						@if(@Auth::user()->businessHours()->first())
						<div class="pro-m-time1 pro-t">
							@foreach(Auth::user()->businessHours()->first()->runningHours()->orderBy('day_no', 'asc')->get() as $hour)
							<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> {{($hour->start) ? Carbon\Carbon::parse($hour->start)->format('H:i') : ''}} - {{($hour->end) ? Carbon\Carbon::parse($hour->end)->format('H:i') : ''}}</div>
							@endforeach
						</div>
						<div class="pro-m-time2 pro-t">
							@foreach(Auth::user()->businessHours()->first()->breakHours()->orderBy('day_no', 'asc')->get() as $hour)
							<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> {{($hour->start) ? Carbon\Carbon::parse($hour->start)->format('H:i') : ''}} - {{($hour->end) ? Carbon\Carbon::parse($hour->end)->format('H:i') : ''}}</div>
							@endforeach
						</div>
						@else
						<div class="pro-m-time1 pro-t">
							<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
						</div>
						<div class="pro-m-time2 pro-t">
							<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
							<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
						</div>
						@endif
					</div>
				</div>
				<div class="social-icon">
					<ul>
						<li class="{{ (Auth::user()->facebook) ? '' : 'd-none' }}"><a class="fb" href="{{ Auth::user()->facebook }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
						<li class="{{ (Auth::user()->twitter) ? '' : 'd-none' }}"><a class="tw" href="{{ Auth::user()->twitter }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
						<li class="{{ (Auth::user()->gmail) ? '' : 'd-none' }}"><a class="go" href="{{ Auth::user()->gmail }}" target="_blank"><i class="fab fa-goodreads-g"></i></a></li>
						<li class="{{ (Auth::user()->linkedin) ? '' : 'd-none' }}"><a class="lin" href="{{ Auth::user()->linkedin }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
						<li class="{{ (Auth::user()->pinterest) ? '' : 'd-none' }}"><a class="pin" href="{{ Auth::user()->pinterest }}" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
					</ul>
				</div>
				@php
				$full_name = '';
				if(@$service->user->name){
				$full_name .= @$service->user->name.' ';
				}
				if(@$service->user->surname){
				$full_name .= @$service->user->surname .' ';
				}
				if(@$service->user->nickname){
				$full_name .= '('.@$service->user->nickname.')';
				}
				$full_name = preg_replace('!\s+!', ' ', $full_name);
				$full_name = str_replace(' ', '-', $full_name);
				@endphp
			</div>
		</div>
		<div class="g-map">
			<div class="mapouter">
				<div class="gmap_canvas" id="map">

				</div>
			</div>
		</div>

		@if(count($allVip)>0)
		<div class="side-blog">
			<section class="blog-section">
				<div class="blog-sidbar">
					<div class="side-tlt">
						<div class="side-txt">SKELBIMAI</div>
						<img src="{{asset('assets/img/services/vip-sm.png')}}" alt="img">
					</div>

					<div class="row m-0">
						@foreach ($allVip as $vip)
						@if(property_exists($vip,'title'))
						@php
						$page_title = $vip->title;
						$page_title = preg_replace('!\s+!', ' ', $page_title);
						$page_title = str_replace(' ', '-', $page_title);
						$see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($vip->title, $vip->service_main_id);
						@endphp
						<div class="co-12 col-sm-6 col-lg-12 p-0">
							<div class="single-content">
								<div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
								<div class="content-img-p">
									<div class="content-img">
										@if($vip->image)
										<img src="{{asset('assets/img/services/'.$vip->image)}}" alt="img">
										@else
										<img src="{{asset('assets/img/services/no_service.png')}}" alt="img">
										@endif
									</div>
								</div>
								<div class="content-des">
									<div class="des-itm">
										<div class="itm-name-all">
											@php
											$inners = App\Http\Controllers\SearchServiceController::serviceAndProfileCategory($vip->inner_category,$vip->sub_category_id);
											@endphp
											@if(count($inners['category']) > 0)
											@foreach ($inners['category'] as $inner)
											<div class="itm-name">{{$inner->name_lt}}</div>
											@endforeach
											@endif
										</div>
										<div class="itm-dis">
											<div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance, 2)}} km</div>
											<div class="dis-txt"><span><a href="{{$see_more}}">Daugiau</a></span></div>
										</div>
									</div>
								</div>
								<div class="content-tlt">
									<a href="{{$see_more}}" onclick="recentlyView('{{$vip->service_main_id}}')">{{ $vip->title }}</a>
								</div>
							</div>
						</div>
						@else
						@php
						$userCats = App\Http\Controllers\SearchServiceController::ProfileCategory($vip->user_main_id);
						@endphp
						<div class="co-12 col-sm-6 col-lg-12 p-0">
							<div class="single-content">
								<div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
								<div class="content-img-p">
									<div class="content-img">
										@if($userCats['cover_img'])
										<img src="{{asset('userImage/coverPic/'.$userCats['cover_img']->img)}}" alt="img">
										@else
										<img src="{{asset('assets/img/default_cover.png')}}" alt="img">
										@endif
									</div>
									<div class="s-pro-img">
										@if($vip->profile_image)
										<img src="{{asset('userImage/fixPic/'.$vip->profile_image)}}" alt="img">
										@else
										<img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
										@endif
									</div>
								</div>
								@php
								$full_name = '';
								if(@$vip->name){
								$full_name .= @$vip->name.' ';
								}
								if(@$vip->surname){
								$full_name .= @$vip->surname.' ';
								}
								if(@$vip->nickname){
								$full_name .= '('.@$vip->nickname.')';
								}
								$full_name = preg_replace('!\s+!', ' ', $full_name);
								$full_name = str_replace(' ', '-', $full_name);

								@endphp
								<div class="content-des">
									<div class="des-itm">
										<div class="itm-name-all">
											@if(count($userCats['cats']) > 0)
											@foreach($userCats['cats'] as $cat)
											<div class="itm-name">{{$cat->name_lt}}</div>
											@endforeach
											@endif

										</div>
										<div class="itm-dis">
											<div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance,2)}} km</div>
											<div class="dis-txt"><span><a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">Daugiau</a></span></div>
										</div>
									</div>
								</div>
								<div class="content-tlt">
									<a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">{{ $vip->surname }}</a>
								</div>
							</div>
						</div>

						@endif
						@endforeach
					</div>
				</div>
			</section>
		</div>
		@endif

	</div>
	</div>
	</div>
</section>
<!-- <section class="slider-section bg-dark-sec">
	<div class="max-fix-width">
		<div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="{{asset('assets/img/slider1.jpg')}}">
					<div class="carousel-caption">
						<h1>Third slide label</h1>
						<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
					</div>
				</div>
				<div class="carousel-item">
					<img src="{{asset('assets/img/slider2.jpg')}}">
					<div class="carousel-caption">
						<h1>@lang('lang.third_slider_lavel')</h1>
						<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
					</div>
				</div>
				<div class="carousel-item">
					<img src="{{asset('assets/img/slider3.jpg')}}">
					<div class="carousel-caption">
						<h1>@lang('lang.third_slider_lavel')</h1>
						<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
					</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">>@lang('lang.previous')</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">@lang('lang.next')</span>
			</a>
		</div>
	</div>
</section> -->
<div class="modal fade pro-i-m" id="serviceTD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="text-center">@lang('lang.update_your_profile_info')</h3>
					<form autocomplete="off" method="post" action="" id="change_service_details" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="id" value="{{ $service['id'] }}">
						<div class="col-12 pad-0">
							<div class="form-group">
								<label for="serviceTitle">@lang('lang.title')<span class="c-red">*</span></label>
								<input type="text" id="serviceTitle" class="form-control" placeholder="Service Title" value="{{ $service['title'] }}" aria-label="service_title" name="title">
							</div>
						</div>
						<div class="col-12 pad-0">
							<div class="form-group">
								<label for="serviceDescription">@lang('lang.description')</label>
								<textarea id="serviceDescription" class="form-control" placeholder="Service Description" name="description">{!! $service['description'] !!}</textarea>
							</div>
						</div>
						<button class="btn btn-success float-right">@lang('lang.save')</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade pro-i-m" id="serviceGlryTab" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="text-center">@lang('lang.add_gallery_tab')</h3>
					<form autocomplete="off" method="post" action="" id="change_service_glry_tab" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="service_id" value="{{ $service['id'] }}">
						<div class="col-12 pad-0">
							<div class="form-group">
								<label for="glryTabName">@lang('lang.tab_name')<span class="c-red">*</span></label>
								<input type="text" id="glryTabName" class="form-control" placeholder="Tab Name" name="tab">
							</div>
						</div>
						<button class="btn btn-success float-right">@lang('lang.add')</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="serviceImg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box text-center">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="">Update service image</h3>
					<div class="col-md-12" style="padding-bottom:13px">
						<div id="service_files">
							<?php
							if (Session::has('service_img')) {
								$image = Session::get('service_img');
								$html = '';
								$html .= '<span class="pip">';
								$html .= '<img class="imageThumb" src="' . asset('assets/img/services/tmpImg/' . Auth()->user()->id . '/' . $image) . '">';
								$html .= '<br/>';
								$html .= '<span class="service-remove" id="' . $image . '">✖</span>';
								$html .= '</span>';
								echo $html;
							}
							?>
						</div>
					</div>

					<label class="form-label text-dark"><b>@lang('lang.service_image')</b></label>
					<div class="controls m-b-10">
						<label class="btn btn-primary btn-file">
							<i class="fa fa-upload"></i> @lang('lang.upload_service_image')
							<input type="file" style="display: none;" name="server_img_u" id="server_img_u">
						</label>
					</div>


					<form method="post" action="{{url('user/update-service-image')}}" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="id" value="{{ $service['id'] }}">
						<button class="btn btn-success">@lang('lang.update')</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="serviceGalleryImg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box text-center">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="">Add Gallery image</h3>
					<div class="col-md-12" style="padding-bottom:13px">
						<div class="input-group">
							<div class="ctm-select" ctm-slt-n="service_gallery_tab">
								<div class="ctm-select-txt pad-l-10">
									<span class="select-txt noselect" id="service_gallery_tab_a">@lang('lang.choose')</span>
									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
								</div>
								<div class="ctm-option-box noselect">
									<div class="ctm-option noselect">@lang('lang.choose')</div>
									@foreach($service_glry_tab as $tab)
									<div class="ctm-option noselect" ctm-otn-v="{{ $tab->id }}">{{ $tab->tab }}</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12" style="padding-bottom:13px">
						<div id="service_gallery">

						</div>
					</div>

					<label class="form-label text-dark"><b>@lang('lang.gallery_image')</b></label>
					<div class="controls m-b-10">
						<label class="btn btn-primary btn-file">
							<i class="fa fa-upload"></i> @lang('lang.add_gallery_image')
							<input type="file" style="display: none;" name="gallery_img_u" id="gallery_img_u">
						</label>
					</div>


					<form method="post" action="" id="upload_service_gallery_info" enctype="multipart/form-data">
						@csrf
						<button class="btn btn-success">@lang('lang.add')</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="serviceGlryTabDel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box text-center">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="">Delete Gallery tab</h3>
					<div class="col-md-12" style="padding-bottom:13px">
						<div class="input-group">
							<div class="ctm-select" ctm-slt-n="service_gallery_tab2">
								<div class="ctm-select-txt pad-l-10">
									<span class="select-txt noselect" id="service_gallery_tab_a2">@lang('lang.choose')</span>
									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
								</div>
								<div class="ctm-option-box noselect">
									<div class="ctm-option noselect">@lang('lang.choose')</div>
									@foreach($service_glry_tab as $tab)
									<div class="ctm-option noselect" ctm-otn-v="{{ $tab->id }}">{{ $tab->tab }}</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<button class="btn btn-success" onclick="delServiceGalleryD()">Del</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="serviceGlryTabEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box text-center">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="">Edit Gallery tab</h3>
					<div class="col-md-12" style="padding-bottom:13px">
						<div class="input-group">
							<div class="ctm-select" ctm-slt-n="service_gallery_tab3">
								<div class="ctm-select-txt pad-l-10">
									<span class="select-txt noselect" id="service_gallery_tab_a3">@lang('lang.choose')</span>
									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
								</div>
								<div class="ctm-option-box noselect">
									<div class="ctm-option noselect">@lang('lang.choose')</div>
									@foreach($service_glry_tab as $tab)
									<div class="ctm-option noselect" ctm-otn-v="{{ $tab->id }}">{{ $tab->tab }}</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12" style="padding-bottom:13px">
						<input type="text" class="edit-tab-txt form-control">
					</div>
					<button class="btn btn-success" onclick="updateServiceGallery()">Update</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade cr-modal" id="serverUploadImgM" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Crop the image</h4>
			</div>
			<div class="modal-body" style="width: 100%; overflow: hidden;">
				<div class="img-container">
					<img id="server_image" src="https://avatars0.githubusercontent.com/u/3456749" style="width: 100%">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
				<button type="button" class="btn" id="crop_service_img">@lang('lang.crop')</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade cr-modal" id="serviceGalleryImgU" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Crop the image</h4>
			</div>
			<div class="modal-body" style="width: 100%; overflow: hidden;">
				<div class="img-container">
					<img id="service_gallery_image" src="https://avatars0.githubusercontent.com/u/3456749" style="width: 100%">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
				<button type="button" class="btn" id="crop_service_gallery_img">@lang('lang.crop')</button>
			</div>
		</div>
	</div>
</div>
<div class="modal p-0" id="ser-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-h">
				<h5 class="modal-title" id="exampleModalLongTitle">Evening makeup</h5>
				<div class="ser-s">
					<span class="r-star">
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
						<i class="fa fa-star"></i>
					</span>
					<span class="p-txt">5.00</span>
					<span class="r-txt">(1)</span>
				</div>
				<div class="ser-d"><i class="fa fa-calendar-plus"></i>Laisvo laiko rezervavimas</div>
				<span class="close-m" data-dismiss="modal" aria-label="Close">&times;</span>
			</div>
			<div class="modal-body">
				<p class="ser-des">During this make-up, the skin is perfectly covered and the face is contoured, eyes, lips are highlighted, and artificial lashes are glued. The goal of makeup is to turn you into a real party star.</p>
				<div class="row">
					<div class="col-12 col-md-2 pr-0">
						<span class="s-t-img"><img src="{{asset('assets/img/duration.svg')}}"></span>
						<div class="ser-txt-a">
							<div class="ser-s-h">Duration</div>
							<div class="ser-s-txt">1 p.m. 20 min.</div>
						</div>
					</div>
					<div class="col-12 col-md-2 pr-0">
						<span class="s-t-img"><img src="{{asset('assets/img/valid.svg')}}"></span>
						<div class="ser-txt-a">
							<div class="ser-s-h">Validity</div>
							<div class="ser-s-txt">6 months</div>
						</div>
					</div>
					<div class="col-12 col-md-4 pr-0">
						<span class="s-t-img"><img src="{{asset('assets/img/registration.svg')}}"></span>
						<div class="ser-txt-a">
							<div class="ser-s-h">Pre-registration required</div>
							<div class="ser-s-txt">The details of the service provider will be visible when you purchase the e-mail. check</div>
						</div>
					</div>
					<div class="col-12 col-md-4 pr-0">
						<span class="s-t-img"><img src="{{asset('assets/img/multifunctional.svg')}}"></span>
						<div class="ser-txt-a">
							<div class="ser-s-h">This check is multi-type</div>
							<div class="ser-s-txt">It is possible to pay with multi-type gift vouchers and use the services of other partners. More</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="s-per">-14%</div>
				<div class="">
					<div class="o-p"><del>45.00 €</del></div>
					<div class="n-p">45.00 €</div>
				</div>
				<button type="button" class="btn add-cart ">Add to cart</button>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
<script src="{{ asset('assets/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('assets/js/cropper.js')}}"></script>
<script src="{{ asset('assets/js/maugallery.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>
<script>
	$(function() {
		$('.gallery').mauGallery({
			columns: {
				xs: 1,
				sm: 2,
				md: 3,
				lg: 4,
				xl: 6
			},
			lightBox: true,
			lightboxId: 'myAwesomeLightbox',
			showTags: true,
			tagsPosition: 'top'
		});
	});
	window.change_service_details = "<?= route('user.changeServiceDetails')  ?>";
	window.service_tmp_img = "<?= route('user.serviceTmpImg')  ?>";
	window.service_single_tmp_remove = "<?= route('user.serviceTmpImgRemove')  ?>";
	window.change_service_sub_category = "<?= route('user.changeServiceSubCategory')  ?>";
	window.change_service_inner_category = "<?= route('user.changeServiceInnerCategory')  ?>";
	window.save_service_category = "<?= route('user.saveServiceCategory')  ?>";
	window.change_service_glry_tab = "<?= route('user.saveServiceGlryTab')  ?>";
	window.del_gallery_tab = "<?= route('user.delServiceGlryTab')  ?>";
	window.update_gallery_tab = "<?= route('user.updateServiceGlryTab')  ?>";
	window.get_service_glry_tab = "<?= route('user.getServiceGlryTab')  ?>";
	window.service_gallery_img_upload = "<?= route('user.serviceGalleryImgUpload')  ?>";
	window.delete_service_gallery_image = "<?= route('user.serviceGalleryImgDelete')  ?>";
	window.service_gallery_single_tmp_remove = "<?= route('user.serviceGalleryTmpImgRemove')  ?>";
	window.add_service_gallery_image = "<?= url('user/add-service-gallery-image') ?>";
	window.base_url = "<?= url('/') ?>";
</script>
<script>
	function goActive() {
		// add filtering code here
		var url = '<?= url("select-plan/" . $service["id"]) ?>';
		window.location.href = url;
	}
	$(function() {
		var autocomplete;
		autocomplete = new google.maps.places.Autocomplete((document.getElementById('autoAddress')), {
			types: ['geocode'],
		});

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var near_place = autocomplete.getPlace();
			var data = new FormData();
			data.append('address', $('#autoAddress').val());
			data.append('lat', near_place.geometry.location.lat());
			data.append('lng', near_place.geometry.location.lng());
			data.append('service_id', "{{$service['id']}}");
			var city = '';
			for (var i = 0; i < near_place.address_components.length; i++) {
				for (var j = 0; j < near_place.address_components[i].types.length; j++) {
					if (near_place.address_components[i].types[j] == 'administrative_area_level_2') {
						city = near_place.address_components[i].long_name;
					}
				}
			}
			data.append('city', city);
			$.ajax({
				processData: false,
				contentType: false,
				data: data,
				type: 'POST',
				url: '{{url("user/service-address")}}',
				success: function(response) {
					serviceMap(near_place.geometry.location.lat(), near_place.geometry.location.lng());
					$('.set-address').text($('#autoAddress').val());
					//console.log(response);
				}

			});
		});

	});
</script>
<script>
	$(function() {
		var lat = '{{$service["lat"]}}';
		var lng = '{{$service["lng"]}}';
		serviceMap(lat, lng);

	});

	function serviceMap(lat, lng) {
		lat = parseFloat(lat);
		lng = parseFloat(lng);
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {
				lat: lat,
				lng: lng
			},
			zoom: 8
		});
		var marker = new google.maps.Marker({
			position: {
				lat: lat,
				lng: lng
			},
			map: map,
			title: 'My Location'
		});

		var mainContent = '';
		mainContent += '<div style="color:#1014a0;font-size:17px; text-align:center;margin-bottom:5px"><?= $service['title'] ?></div>';
		mainContent += '<div style="color:#1014a0;font-size:17px; text-align:center;margin-bottom:5px"><?= Auth::user()->name ?>&nbsp;<?= Auth::user()->surname ?>&nbsp;<?= (Auth::user()->nickname) ? '(' . Auth::user()->nickname . ')' : '' ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fa fa-envelope" style="margin-right:6px;"></i> <?= Auth::user()->email ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fas fa-mobile-alt" style="margin-right:6px;"></i><?= Auth::user()->phone ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fas fa-map-marker-alt" style="margin-right:6px;"></i>' + $('#autoAddress').val() + '</div>';

		var infowindow = new google.maps.InfoWindow();

		marker.addListener('click', function() {
			infowindow.setContent(mainContent);
			infowindow.open(map, marker);
		});
	}
</script>
@endsection
@section('wondowv')
<script>
	window.base_url = "<?= url('/') ?>";
	window.c_user_id = <?= (@$service->user->id) ? @$service->user->id : 0 ?>;
	window.c_user = <?= (@$service->user) ? @$service->user : 0 ?>;
	window.service = <?= json_encode($service) ?>;
	window.window.s_id = <?= $s_id ?>;
</script>
@endsection