 @extends('layout.app')
 @section('title')
Payment List | GrožioKalviai
@endsection
 @section('style')
 <style>
 	.service-img img {
 		height: 96px;
 	}

 	.action-head {
 		text-align: center;
 	}

 	.title-td {
 		vertical-align: middle !important;
 		font-size: 17px !important;
 	}

 	.action-td {
 		vertical-align: middle !important;
 		text-align: center !important;
 	}
 </style>
 @endsection
 @section('content')
 @include('inc.header-filter-new')
 <section class="select-panel-section">
 	<div class="max-fix-width">
 		@if(count($services) >0)
 		<div class="section-title">@lang('lang.service_histories')</div>
 		<table class="table table-striped table-dark">
 			<thead>
 				<tr>
 					<th scope="col">@lang('lang.service')</th>
 					<th scope="col">@lang('lang.package')</th>
 					<th scope="col">@lang('lang.duration')</th>
 					<th scope="col">@lang('lang.star')</th>
 					<th scope="col">@lang('lang.start_date')</th>
 					<th scope="col">@lang('lang.end_date')</th>
 					<th scope="col">@lang('lang.day_left')</th>
 					<th scope="col">@lang('lang.status')</th>
 					<th scope="col" class="action-head">@lang('lang.action')</th>
 				</tr>
 			</thead>
 			<tbody>
 				@foreach($services as $service)
 				<tr>
 					<td rowspan="{{count($service['payment'])+1}}" class="title-td">{{$service['title']}}</td>
 					<td>6 Month free package</td>
 					<td>6 Month</td>
 					<td></td>
 					<td>{{date('Y-m-d',strtotime($service['s_start_date']))}}</td>
 					<td>{{date('Y-m-d',strtotime($service['s_end_date']))}}</td>
 					<td>
 						@php
 						$status = 0;
 						$from = \Carbon\Carbon::parse(date('Y-m-d H:i:s'));
 						$to = \Carbon\Carbon::parse($service['s_end_date']);
 						$left = $to->diffInDays($from);
 						if($left>0){
 						$status = 1; //active
 						echo $left.' days left';
 						}else{
 						$status = 2; // date expire
 						echo 'Expired';

 						}
 						@endphp
 					</td>
 					<td>
 						@if($status == 1) Active @endif @if($status == 2) Expired @endif
 					</td>
 					<td rowspan="{{count($service['payment'])+1}}" class="action-td">
 						<a href="{{url('user/my-service/'.$service['title'].'/'.$service['id'])}}" class="btn btn-primary">Upgrade</a>
 					</td>
 				</tr>
 				@if(count($service['payment']) >0 )
 				@foreach ($service['payment'] as $payment)
 				<tr>
 					<td>{{$payment['package_name']}}</td>
 					<td>{{$payment['duration_name']}}</td>
 					<td>{{$payment['star_number']}}</td>
 					<td>{{date('Y-m-d',strtotime($payment['start_date']))}}</td>
 					<td>{{date('Y-m-d',strtotime($payment['end_date']))}}</td>
 					<td>
 						@php
 						$p_days_left = '';
 						$p_status = 0;
 						if($status == 1){

 						$from1 = \Carbon\Carbon::parse(date('Y-m-d H:i:s'));
 						$to1 = \Carbon\Carbon::parse($payment['end_date']);
 						$left1 = $to1->diffInDays($from1);
 						if($left1>0){
 						$p_days_left = $left1.' days left';
 						$p_status = 1;
 						}else{
 						$p_days_left = 'Expired';
 						$p_status = 2;
 						}

 						if(strtotime($payment['start_date']) > strtotime(date('Y-m-d H:i:s')) && strtotime($payment['end_date']) > strtotime(date('Y-m-d H:i:s'))){
 						$from1 = \Carbon\Carbon::parse(date('Y-m-d H:i:s',strtotime($payment['start_date'])));
 						$left1 = $to1->diffInDays($from1);
 						$p_days_left = $left1.' days left';
 						$p_status = 3;

 						}

 						}else{
 						$p_days_left = 'Expired';
 						$p_status = 2;
 						}
 						echo $p_days_left;
 						@endphp
 					</td>
 					<td>@if($p_status == 1) Active @endif @if($p_status == 2) Expired @endif @if($p_status == 3) Pending @endif</td>
 				</tr>
 				@endforeach
 				@endif
 				@endforeach
 			</tbody>
 		</table>
 		@endif
 		@if($profile_payments)
 		<div class="section-title">@lang('lang.profile_histories')</div>
 		<table class="table table-striped table-dark">
 			<thead>
 				<tr>
 					<th scope="col">@lang('lang.package_name')</th>
 					<th scope="col">@lang('lang.start_date')</th>
 					<th scope="col">@lang('lang.end_date')</th>
 					<th scope="col">@lang('lang.day_left')</th>
 					<th scope="col">@lang('lang.status')</th>
 					<th scope="col" class="action-head">@lang('lang.action')</th>
 				</tr>
 			</thead>
 			<tbody>
 				<tr>
 					<td>{{$profile_payments['vip_package']['pkg_title']}}</td>
 					<td>{{date('Y-m-d',strtotime($profile_payments['start_date']))}}</td>
 					<td>{{date('Y-m-d',strtotime($profile_payments['end_date']))}}</td>
 					<td>
 						@php
 						$status = 0;
 						$from1 = \Carbon\Carbon::parse(date('Y-m-d H:i:s'));
 						$to1 = \Carbon\Carbon::parse($profile_payments['end_date']);
 						$left1 = $to1->diffInDays($from1);
 						if($left1>0){
 						$status = 0;
 						echo $left1.' days left';
 						}else{
 						$status = 1;
 						echo 'Expired';
 						}

 						@endphp
 					</td>
 					<td>@if($status == 0) Active @endif @if($status == 1) Expired @endif</td>
 					<td class="action-td"><a href="{{url('user/profile')}}" class="btn btn-primary">Upgrade</a></td>
 				</tr>
 			</tbody>
 		</table>
 		@endif

 		@if($add_controll)
 		<div class="section-title">@lang('lang.add_controlls_histories')</div>
 		<table class="table table-striped table-dark">
 			<thead>
 				<tr>
 					<th scope="col">@lang('lang.package_name')</th>
 					<th scope="col">@lang('lang.start_date')</th>
 					<th scope="col">@lang('lang.end_date')</th>
 					<th scope="col">@lang('lang.day_left')</th>
 					<th scope="col">@lang('lang.status')</th>
 					<th scope="col" class="action-head">@lang('lang.action')</th>
 				</tr>
 			</thead>
 			<tbody>
 				<tr>
 					<td>{{$add_controll['add_showing_package']['name']}}</td>
 					<td>{{date('Y-m-d',strtotime($add_controll['start_date']))}}</td>
 					<td>{{date('Y-m-d',strtotime($add_controll['end_date']))}}</td>
 					<td>
 						@php
 						$status = 0;
 						$from1 = \Carbon\Carbon::parse(date('Y-m-d H:i:s'));
 						$to1 = \Carbon\Carbon::parse($add_controll['end_date']);
 						$left1 = $to1->diffInDays($from1);
 						if($left1>0){
 						$status = 0;
 						echo $left1.' days left';
 						}else{
 						$status = 1;
 						echo 'Expired';
 						}

 						@endphp
 					</td>
 					<td>{{($status == 0)? 'Active' : 'Expired' }}</td>
 					<td class="action-td"><a href="{{url('add-control-payment')}}" class="btn btn-primary">@lang('lang.upgrade')</a></td>
 				</tr>
 			</tbody>
 		</table>
 		@endif
 	</div>
 </section>
 @endsection
 @section('script')

 @endsection