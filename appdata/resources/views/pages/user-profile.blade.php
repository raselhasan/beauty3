@extends('layout.app')
@section('title')
{{@$makeName}} | GrožioKalviai
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fancybox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cropper.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}"> --}}
<style type="text/css">
.add-live-zone {
  margin-bottom: 10px; 
  cursor: pointer;
}

.add_a_img {
  color: #2a9e63;
  text-decoration: underline;
  margin-right: 10px;
}

.embade_video {
  color: #E25B37;
  text-decoration: underline;
  margin-right: 10px;
}

#link-area,
#img-area {
  display: none;
}

.imgVideo {
  display: none;
}

.imgVideoBlock {
  display: block;
}

.gm-style-iw {
  max-width: 300px !important;
  max-height: 300px !important;
}

.pac-container {
  z-index: 10000 !important;
}

.gm-style-iw-d {
  max-width: 300px !important;
  max-height: 300px !important;
}

.date a {
  color: #E25B37;
}

.url-color {
  color: #E25B37;
}

.fa-trash {
  margin-left: 10px;
}

.image-error {
  display: none;
}

.uploaded_image_file {
  border-bottom: 1px solid #704923;
}

.uploaded_image_file1 img {
  height: 315px;
  width: 840px;
  padding-left: 20px;
  margin-top: 5px;
  margin-bottom: 5px;
}

.f-avitiy {
  margin-top: 20px;
}

.see-more {
  margin-left: 19px;
}

.v-f-s {
  font-size: 12px !important;
  text-decoration: underline;
}

.l-edit {
  font-size: 12px !important;
  text-decoration: underline;
  color: blue;
}

.l-delete {
  font-size: 12px !important;
  text-decoration: underline;
  color: #e67a3b;
}

#editModal .modal-content {
  background-color: #121212 !important;
}

.video-link {
  display: none;
}

.upload-image {
  display: none;
}

.u-i-tab {
  background: #352e2e;
  padding: 5px;
  cursor: pointer;
  font-size: 12px;
  margin-right: 14px;
}

.u-v-tab {
  background: #352e2e;
  padding: 5px;
  cursor: pointer;
  font-size: 12px;
  margin-right: 14px;
}

.f-u-tab {
  margin-top: 20px;
}

#s_show {
  display: none;
}

#s_see_more {
  color: orange;
  cursor: pointer;
}

.seeMore {
  color: #e25b37;
}
</style>
@endsection
@section('content')
@include('inc.header-filter-new')
<section class="about-section userPro-section live-zona-section">
  <div class="max-fix-width">
    <div class="row mar-0">
      <div class="col-12 col-lg-9 px-0 px-lg-3">
        <div class="userPro-cnt">
          <div class="user-slider">
            <span class="cover-btn" onclick="changeCoverImage(event)"><i class="fas fa-camera" title="edit image"></i> Update cover photo</span>
            <div class="swiper-container ctm-container-4">
              <div class="swiper-wrapper">
                @if($UserCover->count())
                @foreach ($UserCover as $uc)
                <div class="swiper-slide">
                  <div class="single-content wh-100">
                    <div class="content-img-p">
                      <div class="content-img">
                        <img src="{{ asset('userImage/coverPic/' . $uc->img) }}" alt="cover-pic">
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
                @else
                <div class="swiper-slide">
                  <div class="single-content wh-100">
                    <div class="content-img-p">
                      <div class="content-img">
                        <img src="{{asset('assets/img/default_cover.png')}}" alt="user-img">
                      </div>
                    </div>
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>
          <div class="userPro-bio">
             <div class="col-12">
                @if($userInfo['profile_payment'])
                  VIP profile
                @else
                <a href="javascript:;"  data-toggle="modal" data-target="#profile-payment-modal">Make your profile VIP</a>
                @endif
              </div>
            <div class="row mar-0">
              <div class="userPro-imgM">
                @if(Auth()->user()->profile_image)
                <img src="{{asset('userImage/fixPic/'. Auth()->user()->profile_image)}}" alt="user-img">
                @else
                <img src="{{asset('assets/img/services/profile-img.png')}}" alt="user-img">
                @endif
                <div class="overShade s-overShade" onclick="showPicEditBox()">
                  <span><i class="fas fa-camera" title="edit image"></i> Update</span>
                </div>
                <div class="overShade m-overShade d-inline-block d-md-none" onclick="showPicEditBox()">
                  <span><i class="fas fa-camera" title="edit image"></i></span>
                </div>
              </div>

              <div class="col-12 px-2">
                <div class="userPro-txt">
                  <div class="edit-info-btn d-none d-md-block" onclick="proInfoC(event)">
                    <i class="fa fa-edit" title="edit image"></i>
                  </div>
                  <div class="userPro-tlt">
                    <h3 class="setName">{{ $makeName }}</h3>
                    <div class="about-star d-inline-block d-md-none">
                      <span class="d-inline-flex" style="align-items: center;">
                        <span>
                          <img src="{{asset('assets/img/services/star.png')}}" alt="img">
                          <img src="{{asset('assets/img/services/star.png')}}" alt="img">
                          <img src="{{asset('assets/img/services/star.png')}}" alt="img">
                          <img src="{{asset('assets/img/services/star.png')}}" alt="img">
                          <img src="{{asset('assets/img/services/half-star.png')}}" alt="img">
                        </span>
                        <span class="font-weight-bold pl-1">4.5/5</span>
                      </span>
                      <span class="pl-2"><span> (21 klientų įvertinimai) </span>
                    </span>
                  </div>
                  <button class="love-btn"><i class="fas fa-heart"></i> ĮSIMINTI</button>
                </div>
                <div class="bio-txt d-none d-md-block">
                  <span><i class="fas fa-exclamation-circle"></i></span>
                  <p class="bio-txtP">
                    {!! Auth::user()->bio !!}
                  </p>
                </div>
              </div>
              {{-- <div class="chat-icon"><i class="fas fa-comments"></i></div> --}}
            </div>
            <div class="col-12 d-block d-md-none  mobile-col p-0">
              <div class="userPro-txt">
                <div class="edit-info-btn" onclick="proInfoC(event)">
                  <i class="fa fa-edit" title="edit image"></i>
                </div>
                <div class="bio-txt p-0">
                  <div class="bio-tlt"><span class="position-relative "><i class="fas fa-exclamation-circle"></i></span> Apie:</div>
                  <p class="bio-txtP">
                    {!! Auth::user()->bio !!}
                  </p>
                </div>
                <div class="text-center more-bio p-1"> <span>- Redaguoti -</span></div>
              </div>
              {{-- <div class="chat-icon"><i class="fas fa-comments"></i></div> --}}
            </div>
          </div>
        </div>
      </div>
      <div class="profile-nav-div">
        <ul class="nav profile-nav-tab nav-pills mb-3" id="pills-tab" role="tablist">
          <li class="nav-item  service-nav">
            <a class="nav-link active " id="nav-live-zone-tab" data-toggle="pill" href="#nav-live-zone" role="tab" aria-controls="pills-home" aria-selected="true">Live Zone</a>
          </li>
          <li class="nav-item df-nav">
            <a class="nav-link "><i class="fas fa-plus"></i></a>
          </li>
          <li class="nav-item service-nav">
            <a class="nav-link" id="nav-services-tab" data-toggle="pill" href="#nav-services" role="tab" aria-controls="pills-contact" aria-selected="false">Services</a>
          </li>
          <li class="nav-item df-nav">
            <a class="nav-link " onclick="Confirm('Create New Service', 'Are you sure you want to create new service', 'Yes', 'Cancel', '<?= route('user.addMyService') ?>');"><i class="fas fa-plus"></i></a>
          </li>
        </ul>
        <div class="profile-tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-live-zone" role="tabpanel" aria-labelledby="nav-live-zone-tab">
            <div class="live-zona">
              <!-- <div class="section-title">Live Zona</div> -->
                <!-- <div class="add-live-zone text-right">
                <span onclick="openLiveZone1()"><i class="fa fa-plus"></i> @lang('lang.add_live_zone')</span>
              </div> -->
              {{-- <div class="live-zona-cnt mylz-content">
                <div class="row mar-0">
                  <div class="col-12 col-md-7 col-lg-8 p-0">
                    <div class="single-content img-v" id="del-live-zone-1">
                      <div class="content-img-p d-none ">
                        <div class="content-img">
                          <img id="lz-img-1" src="{{asset('assets/img/services/demo.jpg')}}">
                        </div>
                      </div>
                      <div class="content-img-p video">
                        <div class="content-img">
                          <iframe id="lz-video-1" src="https://www.youtube.com/embed/AgX2II9si7w?list=RDAgX2II9si7w">
                          </iframe>
                        </div>
                      </div>

                      <div class="like-com-btn">
                        <div class="like-count">
                          <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                          <span class="love-i"><i class="fas fa-heart"></i></span>
                          <span class="wow-i"><img src="{{asset('assets/img/services/wow.png')}}" alt="img"></span>
                          <span class="n-count">55</span>
                        </div>
                        <div class="l-c-s">
                          <span class="lcs-ul">
                            <span class="lcs-li">Like</span>
                            <span class="lcs-li">Comments</span>
                            <span class="lcs-li">Share</span>
                          </span>
                        </div>
                        <div class="agn-txt">prieš 2 d.</div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 col-md-5 col-lg-4 p-0">
                    <div class="post-d">
                      <div class="post-d-c-tlt post-d-tlt ">
                        <div class="com-logo"><img src="{{asset('assets/img/services/profile-img.png')}}" alt="img"></div>
                        <div class="post-d-txt-all">
                          <div class="post-d-txt">NAUJA PASLAUGA - PLAUKŲ LAKAVIMAS! Būtina išankstinė rezervacija.</div>
                          <div class="post-da-r">
                            <span class="post-da">prieš 2 d.</span>
                            <span class="post-r">...</span>
                          </div>
                        </div>
                      </div>

                      <div class="post-d-c-tlt post-d-comment ">
                        <div class="com-logo"><img src="{{asset('assets/img/services/demo.jpg')}}" alt="img"></div>
                        <div class="post-d-txt-all">
                          <div class="comment-txt">
                            <div class="usr-name">Angelina</div>
                            <div class="post-d-txt">Kokios kainos? Ar rytoj turite vietų?</div>
                            <div class="post-da-r">
                              <span class="post-da">prieš 2 d.</span>
                              <span class="post-da reply">Reply</span>
                            </div>
                          </div>

                          <div class="post-d-c-tlt post-d-reply ">
                            <div class="com-logo"><img src="{{asset('assets/img/services/demo.jpg')}}" alt="img"></div>
                            <div class="post-d-txt-all">
                              <div class="usr-name">Angelina</div>
                              <div class="post-d-txt">Kokios kainos? Ar rytoj turite vietų?</div>
                              <div class="post-da-r">
                                <span class="post-da">prieš 2 d.</span>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <div class="com-input">
                        <div class="input-group">
                          <input type="text" class="form-control" name="comment" placeholder="Add a comment...">
                          <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Pateikti</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> --}}

              {{-- <div class="live-zona-cnt mylz-content">
                <div class="row mar-0">
                  <div class="col-12 col-md-7 col-lg-8 p-0">
                    <div class="single-content img-v" id="del-live-zone-1">
                      <div class="content-img-p ">
                        <div class="content-img">
                          <img id="lz-img-1" src="{{asset('assets/img/services/demo.jpg')}}">
                        </div>
                      </div>
                      <div class="content-img-p video d-none ">
                        <div class="content-img">
                          <iframe id="lz-video-1" src="https://www.youtube.com/embed/AgX2II9si7w?list=RDAgX2II9si7w">
                          </iframe>
                        </div>
                      </div>

                      <div class="like-com-btn">
                        <div class="like-count">
                          <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                          <span class="love-i"><i class="fas fa-heart"></i></span>
                          <span class="wow-i"><img src="{{asset('assets/img/services/wow.png')}}" alt="img"></span>
                          <span class="n-count">55</span>
                        </div>
                        <div class="l-c-s">
                          <span class="lcs-ul">
                            <span class="lcs-li">Like</span>
                            <span class="lcs-li">Comments</span>
                            <span class="lcs-li">Share</span>
                          </span>
                        </div>
                        <div class="agn-txt">prieš 2 d.</div>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 col-md-5 col-lg-4 p-0">
                    <div class="post-d">
                      <div class="post-d-c-tlt post-d-tlt ">
                        <div class="com-logo"><img src="{{asset('assets/img/services/profile-img.png')}}" alt="img"></div>
                        <div class="post-d-txt-all">
                          <div class="post-d-txt">NAUJA PASLAUGA - PLAUKŲ LAKAVIMAS! Būtina išankstinė rezervacija.</div>
                          <div class="post-da-r">
                            <span class="post-da">prieš 2 d.</span>
                            <span class="post-r">...</span>
                          </div>
                        </div>
                      </div>

                      <div class="post-d-c-tlt post-d-comment ">
                        <div class="com-logo"><img src="{{asset('assets/img/services/demo.jpg')}}" alt="img"></div>
                        <div class="post-d-txt-all">
                          <div class="comment-txt">
                            <div class="usr-name">Angelina</div>
                            <div class="post-d-txt">Kokios kainos? Ar rytoj turite vietų?</div>
                            <div class="post-da-r">
                              <span class="post-da">prieš 2 d.</span>
                              <span class="post-da reply">Reply</span>
                            </div>
                          </div>

                          <div class="post-d-c-tlt post-d-reply ">
                            <div class="com-logo"><img src="{{asset('assets/img/services/demo.jpg')}}" alt="img"></div>
                            <div class="post-d-txt-all">
                              <div class="usr-name">Angelina</div>
                              <div class="post-d-txt">Kokios kainos? Ar rytoj turite vietų?</div>
                              <div class="post-da-r">
                                <span class="post-da">prieš 2 d.</span>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>

                      <div class="com-input">
                        <div class="input-group">
                          <input type="text" class="form-control" name="comment" placeholder="Add a comment...">
                          <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Pateikti</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              --}}

              {{-- live zone start --}}
              <input type="hidden" name="main_user_idd" id="main_user_idd" value="{{auth()->user()->id}}">
              <div class="live-zona live-zona-sec-cnt">
                {{-- <div class="live-zona-tlt mt-3"><img  src="{{asset('assets/img/services/triangle-r.png')}}" alt="img"> Live Zona</div> --}}
                <div class="live-zona-cnt">
                  <!-- class- add-new-live-zone -->
                  <div class="row mar-0">
                    <div class="col-12 p-0">
                      @if(Auth::check())
                      <form id="saveLiveZoneForm">
                        @csrf
                        <div class="write-post">
                          <div class="post-d-c-tlt">
                            <div class="post-d-txt">KURTI PRANEŠIMĄ
                              <span class="post-r">...</span></div>
                            </div>
                            <input type="hidden" name="show_edit" value="1">
                            <div class="comment-w-box">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">

                                    @if(Auth::check())
                                    @if(auth()->user()->profile_image)
                                    <img src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                                    @else
                                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                    @endif
                                    @else
                                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                    @endif
                                  </span>
                                </div>
                                <textarea class="form-control" placeholder="Rašykite naują pranešimą..." name="description" id="livezone-description"></textarea>
                                <pre class="default prettyprint prettyprinted"><code><span class="pln" id="output_div">fdfdf</span></code></pre>

                              </div>
                              <div class="input-group-append">
                                <span class="input-group-text">
                                  <span class="pv-i" data-toggle="modal" data-target="#liveZoneVideoModal">
                                    <span class="post-v-txt d-none d-md-inline-block">Įkelti <br> vIDEO</span>
                                    <span class="post-v-txt-i"> <img src="{{asset('assets/img/post-v.png')}}" alt="img"></span>
                                  </span>
                                  <span class="pv-i" data-toggle="modal" data-target="#liveZoneImgModal">
                                    <span class="post-v-txt d-none d-md-inline-block">ĮkeLTI <br> FOTO</span>
                                    <span class="post-v-txt-i"> <img src="{{asset('assets/img/post-c.png')}}" alt="img"></span>
                                  </span>
                                </span>
                              </div>
                            </div>
                            <div class="uploaded_image_file">
                              <div class="uploaded_image_file1">

                              </div>
                            </div>
                            <div class="submit-sec">
                              <span class="fel-tag-sec mb-3 d-none d-md-inline-block mb-md-0">
                                {{-- <span class="fel-i fel-m-s" data-toggle="modal" data-target="#liveZoneFellingModal"><img  src="{{asset('assets/img/felling-i.png')}}" alt="img"> Feeling/Activity</span>
                                <span class="tag-i" data-toggle="modal" data-target="#liveZoneTagModal"><img src="{{asset('assets/img/tag-i.png')}}" alt="img">Tag profiles</span> --}}
                              </span>
                              <input type="hidden" name="page_identy" id="page_identy" value="1">
                              <span class="post-sub-btn">
                                <span class="post-check">
                                  <span><label class="ctm-container">Profile <input type="checkbox" name="show_profile" value="1" checked="checked" class="show_profile"> <span class="checkmark"></span></label></span>
                                  <span><label class="ctm-container">Live zona <input type="checkbox" name="show_livezone" value="1" checked="checked" class="show_livezone"> <span class="checkmark"></span></label></span></span>
                                  <button class="float-right float-md-none" type="submit">submit</button>
                                </span>
                              </div>
                            </div>
                          </form>
                          @endif

                          {{-- start single livezone --}}
                          <div class="add-new-livezone">
                            @if(count($liveZones) > 0)
                            @foreach ($liveZones as $livezone)
                            <div class="single-content img-v" id="live-zone-number-{{$livezone->id}}">
                              <div class="post-d top-post-cnt">
                                <div class="post-d-c-tlt post-d-tlt ">
                                  <div class="com-logo">
                                    @if($livezone->user->profile_image)
                                    <img src="{{asset('userImage/fixPic/'.$livezone->user->profile_image)}}" alt="img">
                                    @else
                                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                    @endif
                                  </div>
                                  <div class="post-d-txt-all">
                                    <span class="post-r">
                                      <div class="btn-group">
                                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>...</span></button>
                                        <div class="dropdown-menu dropdown-menu-right">
                                          <button class="dropdown-item" type="button"> <span class="v-f-s" onclick="openLiveeZonePopUp({{$livezone->id}})">view full screen</span></button>
                                          <button class="dropdown-item" type="button"><span class="l-edit" onclick="openLiveZone({{$livezone->id}})">Edit</span></button>
                                          <button class="dropdown-item" type="button"><span class="l-delete" onclick="deleteLiveZone({{$livezone->id}})">Delete</span></button>
                                        </div>
                                      </div>
                                    </span>
                                    <div class="post-d-txt">{{ $livezone->user->name }}</div>
                                    <div class="post-da-r">
                                      <span class="post-da">{{ \Carbon\Carbon::parse($livezone->created_at)->diffForHumans() }}</span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="post-desc d-none d-md-block ev-livezone-des-{{$livezone->id}}">

                                @php
                                $str_len = strlen($livezone->description);
                                $main_str = substr($livezone->description,0,570);
                                $sub_str = substr($livezone->description,571,$str_len);
                                @endphp

                                {!! $main_str !!}

                                @if($str_len > 570)
                                <span class="sub_str_{{$livezone->id}}" id="s_see_more" onclick="showMoreString({{$livezone->id}})">See more..</span>
                                <span class="show_sub_str_{{$livezone->id}}" id="s_show">{!! $sub_str !!}</span>
                                @endif

                              </div>

                              <div class="img_or_video_{{$livezone->id}}">
                                @if($livezone->status == 1)
                                <div class="content-img-v-cnt ">
                                  <div class="content-img-p ">
                                    <div class="content-img">
                                      <img id="lz-img-1" src="{{asset('liveZone/'.$livezone->image)}}" onclick="openLiveeZonePopUp({{$livezone->id}})">
                                    </div>
                                  </div>
                                </div>
                                @endif
                                @if($livezone->status == 2)
                                <div class="content-img-v-cnt">
                                  <div class="content-img-p video ">
                                    <div class="content-img">
                                      <iframe id="lz-video-1" src="https://www.youtube.com/embed/{{$livezone->image}}">
                                      </iframe>
                                    </div>
                                  </div>
                                </div>
                                @endif
                              </div>

                              <div class="post-desc d-block d-md-none ev-livezone-des-{{$livezone->id}}">

                                @php
                                $str_len = strlen($livezone->description);
                                $main_str = substr($livezone->description,0,570);
                                $sub_str = substr($livezone->description,571,$str_len);
                                @endphp

                                {!! $main_str !!}

                                @if($str_len > 570)
                                <span class="sub_str_{{$livezone->id}}" id="s_see_more" onclick="showMoreString({{$livezone->id}})">See more..</span>
                                <span class="show_sub_str_{{$livezone->id}}" id="s_show">{!! $sub_str !!}</span>
                                @endif

                              </div>

                              <div class="like-com-lz-btn">
                                <span class="like-count mb-2 mb-md-0">
                                  <div class="like-reaction">
                                    @php
                                    $mainLike = App\Http\Controllers\LiveZoneController::firstLikeCount($livezone->id);
                                    echo $mainLike['reacts'];
                                    @endphp
                                    {{-- <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                                    <span class="love-i"><i class="fas fa-heart"></i></span> --}}
                                  </div>
                                  {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img">
                                  </span> --}}
                                  <span class="n-count total_post_like">{{$mainLike['total']}}</span>
                                </span>
                                <span class="l-c-s d-none d-md-inline-block">
                                  <span class="lcs-ul like-li">
                                    <span class="lcs-li lcs-like"><span><img src="{{asset('assets/img/like-i.png')}}" alt="img">Like</span>
                                    <div class="like-item">
                                      <span class="like-itm"><i class="fa fa-thumbs-up" onclick="giveFirstLike({{$livezone->id}},'like')"></i></span>
                                      <span class="love-itm"><i class="far fa-heart" onclick="giveFirstLike({{$livezone->id}},'love')"></i></span>
                                    </div>
                                  </span>
                                  <span class="lcs-li com-li" onclick="openComments({{$livezone->id}})"><img src="{{asset('assets/img/comment-i.png')}}" alt="img">Comments</span>
                                  <span class="lcs-li"><img src="{{asset('assets/img/share-i.png')}}" alt="img">Share</span>
                                </span>
                              </span>
                              <span class="agn-txt float-right"><span class="comment-c"><span class="put-comment-total-{{$livezone->id}}">
                                @php
                                $total_comment = App\Http\Controllers\LiveZoneController::totalComment($livezone->id);
                                echo $total_comment['total'];
                                @endphp
                              </span> comment</span><span class="share-c">0 Share</span>
                            </span>
                            </div>
                            
                            <div class="like-com-lz-btn d-block d-md-none">
                              <span class="l-c-s">
                                <span class="lcs-ul like-li d-flex border-none text-center">
                                  <span class="lcs-li pt-0 pb-0 lcs-like"><span><img src="{{asset('assets/img/like-i.png')}}" alt="img">Like</span>
                                  <div class="like-item">
                                    <span class="like-itm"><i class="fa fa-thumbs-up" onclick="giveFirstLike({{$livezone->id}},'like')"></i></span>
                                    <span class="love-itm"><i class="far fa-heart" onclick="giveFirstLike({{$livezone->id}},'love')"></i></span>
                                  </div>
                                </span>
                                <span class="lcs-li pt-0 pb-0 com-li" onclick="openComments({{$livezone->id}})"><img src="{{asset('assets/img/comment-i.png')}}" alt="img">Comments</span>
                                <span class="lcs-li pt-0 pb-0"><img src="{{asset('assets/img/share-i.png')}}" alt="img">Share</span>
                              </span>
                            </span>
                          </div>

                          <div class="comment-w-box">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  @if(Auth::check())
                                  @if(auth()->user()->profile_image)
                                  <img src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                                  @else
                                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                  @endif
                                  @else
                                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                  @endif
                                </span>
                              </div>
                              <textarea id="comment_box" name="comment" class="form-control first_comment" placeholder="Rašykite komentarą..." livezone-id="{{$livezone->id}}"></textarea>

                            </div>
                            {{-- <div class="input-group-append">
                              <span class="input-group-text">
                                <span class="pv-i"><i class="fas fa-smile"></i></span>
                                <span class="pv-i"><i class="fas fa-camera"></i></span>
                                <span class="pv-i"><i class="fas fa-video"></i></span>
                                <button class="sent-txt">Pateikti</button>
                              </span>
                            </div> --}}
                          </div>
                          <div class="load-main-comment-{{$livezone->id}}">
                            @if(count($livezone->comments) > 0)
                            @foreach ($livezone->comments as $comment)
                            <div class="post-d">
                              <div class="comment-all" id="comment_load">
                                <div class="post-d-c-tlt post-d-comment ">
                                  <div class="com-logo">
                                    @if($comment->commenter->profile_image)
                                    <img src="{{asset('userImage/fixPic/'.$comment->commenter->profile_image)}}" alt="img">
                                    @else
                                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                    @endif
                                  </div>
                                  <div class="post-d-txt-all">
                                    <div class="comment-txt">
                                      <div class="post-d-txt"><span class="usr-name">{{$comment->commenter->name}}</span>
                                        @php
                                        $str_len1 = strlen($comment->comments);
                                        $main_str1 = substr($comment->comments,0,200);
                                        $sub_str1 = substr($comment->comments,201,$str_len1);
                                        @endphp

                                        {!! $main_str1 !!}

                                        @if($str_len1 > 200)
                                        <span class="main_sub_str_{{$comment->id}}" id="s_see_more" onclick="showMoreString3({{$comment->id}})">See more..</span>
                                        <span class="main_show_sub_str_{{$comment->id}}" id="s_show">{!! $sub_str1 !!}</span>
                                        @endif
                                      </div>
                                      <div class="post-da-r">
                                        <span class="like-count" id="comment-like-react-{{$comment->id}}">
                                          @php
                                          $commentLike = App\Http\Controllers\LiveZoneController::commentLikeCount($comment->id);
                                          echo $commentLike['reacts'];
                                          @endphp
                                          {{-- <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                                          <span class="love-i"><i class="fas fa-heart"></i></span> --}}

                                          {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
                                          {{-- <span class="n-count">1</span> --}}
                                        </span>
                                        <span class="post-da reply reply-like">
                                          <span> Like</span>
                                          <div class="like-item">
                                            <span class="like-itm"><i class="fa fa-thumbs-up" onclick="commentLike({{$comment->id}},'like')"></i></span>
                                            <span class="love-itm"><i class="far fa-heart" onclick="commentLike({{$comment->id}},'love')"></i></span>
                                          </div>
                                        </span>
                                        <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                                        <span class="post-da">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</span>
                                      </div>

                                      <div class="comment-w-box reply-w-box hide-w-box">
                                        <div class="input-group">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text">
                                              @if(Auth::check())
                                              @if(auth()->user()->profile_image)
                                              <img src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                                              @else
                                              <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                              @endif
                                              @else
                                              <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                              @endif
                                            </span>
                                          </div>
                                          <textarea class="form-control reply-input main-comment-replay" comment-id="{{$comment->id}}" livezone-id="{{$livezone->id}}" name="reply" placeholder="Rašykite komentarą..."></textarea>

                                        </div>
                                        {{-- <div class="input-group-append">
                                          <span class="input-group-text">
                                            <span class="pv-i"><i class="fas fa-smile"></i></span>
                                            <span class="pv-i"><i class="fas fa-camera"></i></span>
                                            <span class="pv-i"><i class="fas fa-video"></i></span>
                                            <button class="sent-txt">Pateikti</button>
                                          </span>
                                        </div> --}}
                                      </div>

                                    </div>
                                    <div class="load-sub-comment-{{$comment->id}}">
                                      @php
                                      $total_sub_comment = App\Http\Controllers\LiveZoneController::countSubComment($comment->id);
                                      @endphp
                                      @if(count($comment->subcomments) > 0)
                                      @foreach($comment->subcomments as $sub_comment)
                                      <div class="post-d-c-tlt post-d-reply comment-txt">
                                        <div class="com-logo">
                                          @if($sub_comment->commenter->profile_image)
                                          <img src="{{asset('userImage/fixPic/'.$sub_comment->commenter->profile_image)}}" alt="img">
                                          @else
                                          <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                          @endif
                                        </div>
                                        <div class="post-d-txt-all">
                                          <div class="post-d-txt"><span class="usr-name">{{$sub_comment->commenter->name}}</span>
                                            @php
                                            $str_len2 = strlen($sub_comment->comments);
                                            $main_str2 = substr($sub_comment->comments,0,200);
                                            $sub_str2 = substr($sub_comment->comments,201,$str_len2);
                                            @endphp

                                            {!! $main_str2 !!}

                                            @if($str_len2 > 200)
                                            <span class="sub_sub_str_{{$sub_comment->id}}" id="s_see_more" onclick="showMoreString4({{$sub_comment->id}})">See more..</span>
                                            <span class="sub_show_sub_str_{{$sub_comment->id}}" id="s_show">{!! $sub_str2 !!}</span>
                                            @endif
                                          </div>
                                          <div class="post-da-r">
                                            <span class="like-count" id="subcomment-like-react-{{$sub_comment->id}}">
                                              @php
                                              $subCommentLike = App\Http\Controllers\LiveZoneController::subCommentLikeCount($sub_comment->id);
                                              echo $subCommentLike['reacts'];
                                              @endphp
                                              {{-- <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                                              <span class="love-i"><i class="fas fa-heart"></i></span> --}}

                                              {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
                                              {{-- <span class="n-count">1</span> --}}
                                            </span>

                                            <span class="post-da reply reply-like">
                                              <span> Like</span>
                                              <div class="like-item">
                                                <span class="like-itm"><i class="fa fa-thumbs-up" onclick="subCommentLike({{$sub_comment->id}},'like')"></i></span>
                                                <span class="love-itm"><i class="far fa-heart" onclick="subCommentLike({{$sub_comment->id}},'love')"></i></span>
                                              </div>
                                            </span>
                                            <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                                            <span class="post-da">{{ \Carbon\Carbon::parse($sub_comment->created_at)->diffForHumans() }}</span>
                                          </div>
                                          <div class="comment-w-box reply-w-box hide-w-box">
                                            <div class="input-group">
                                              <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                  @if(Auth::check())
                                                  @if(auth()->user()->profile_image)
                                                  <img src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                                                  @else
                                                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                                  @endif
                                                  @else
                                                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                                  @endif
                                                </span>
                                              </div>
                                              <textarea class="form-control reply-input reply-sub-comment" comment-id="{{$comment->id}}" livezone-id="{{$livezone->id}}" name="reply" placeholder="Rašykite komentarą..."></textarea>
                                            </div>
                                            {{-- <div class="input-group-append">
                                              <span class="input-group-text">
                                                <span class="pv-i"><i class="fas fa-smile"></i></span>
                                                <span class="pv-i"><i class="fas fa-camera"></i></span>
                                                <span class="pv-i"><i class="fas fa-video"></i></span>
                                                <button class="sent-txt">Pateikti</button>
                                              </span>
                                            </div> --}}
                                          </div>
                                        </div>
                                      </div>
                                      @endforeach
                                      @endif
                                      <div class="see-more-subcomment-div-{{$comment->id}}">
                                        @if($total_sub_comment > 1)
                                        <span onclick="seeMoreSubComment(event, {{$comment->id}}, 1, {{$total_sub_comment}})"> View {{$total_sub_comment - 1}} more comments</span>
                                        @endif
                                      </div>
                                    </div>



                                  </div>
                                </div>
                              </div>

                            </div>
                            @endforeach
                            @endif
                          </div>
                          <div class="see-m-c mb-3 mt-2 see-more-comment-div-{{$livezone->id}}">
                            @if($total_comment['main'] > 2)
                            <span class="see-more" onclick="seeMoreComments(event,{{$livezone->id}},2,{{$total_comment['main']}})">View {{$total_comment['main'] - 2}} more comments</span>
                            @endif
                          </div>
                        </div>
                        @endforeach
                        @endif
                      </div>
                      {{-- end single livezone --}}





                    </div>
                  </div>


                </div>
              </div>

              {{-- live zone end --}}






            </div>
          </div>
          <div class="tab-pane fade" id="nav-services" role="tabpanel" aria-labelledby="nav-services-tab">
            <div class="service-purchase-list-all">
              @foreach($services as $service)
              <div class="service-purchase-list" <?= ($service->types->count()) ? '' : 'onclick="goToService(\'' . route('user.myService', [$service->title, $service->id]) . '\')"' ?>>
                <!-- <div class="service-purchase-tlt show-pl">{{ $service->title }}: <span class="tlt-1">Make up</span><span class="tlt-2">({{ $service->types->count() }})</span> <span class="cv-d"><i class="fa fa-chevron-down"></i></span></div> -->

                <div class="service-purchase-add-all main-service show-pl">
                  <div class="service-purchase-add">

                    <div class="service-purchase-l">
                      <div class="service-m-img">
                        @if($service->image)
                        <img src="{{asset('assets/img/services/' . $service->image)}}" class="service-img" alt="img">
                        @else
                        <img src="{{asset('assets/img/services/cat-1.png')}}" alt="img">
                        @endif
                      </div>
                      <div class="service-m-info">
                        <div class="ser-n-s">
                          <div class="ser-n">
                            <span>{{ $service->title }}</span> <span class="tlt-2">({{ $service->types->count() }})</span>
                            <span class="pr-img"><img src="{{asset('assets/img/services/profile-img.png')}}" alt="img"></span>
                            <span class="star-txt"><img src="{{asset('assets/img/services/star.png')}}" alt="img"> 4.5/5</span>
                            <span class="per d-inline-block d-md-none float-right">20%</span>
                          </div>
                          <div class="ser-d d-none d-md-block">Kirpimas mašinėle | Barzdos skutimas | +4</div>
                        </div>
                      </div>
                    </div>

                     <!--  <div class="service-purchase-l d-block d-md-none">
                        <div class="service-m-info">
                          <div class="ser-n-s">
                            <div class="ser-n d-flex">
                              <span class="pr-img"><img src="{{asset('assets/img/services/profile-img.png')}}" alt="img"></span>
                              <span class="star-txt"><img src="{{asset('assets/img/services/star.png')}}" alt="img"> 4.5/5</span>
                            </div>
                            <div class="ser-d">Kirpimas mašinėle | Barzdos skutimas | +4</div>
                          </div>
                        </div>
                      </div> -->



                      <div class="service-purchase-r">
                        <div class="ser-prc-per">
                          <span class="dis-pr d-none d-md-inline-block m-0 active">
                            <span class="per">20%</span>
                            <span class="price">nuo 20 €</span>
                          </span>
                          <span class="ud-i d-none d-md-inline-block"><img src="{{asset('assets/img/services/up-d-i.png')}}" alt="img"></span>

                          <span class="time-t  d-none d-md-inline-block">
                            <div class="fa-ch"><i class="fas fa-check-circle"></i> Laisvo laiko</div>
                            <div class="time-m"><i class="far fa-clock"></i> 20 min -3 val</div>
                          </span>

                          <span class="time-t m-time-t d-flex d-md-none">
                            <div class="fa-ch"><i class="fas fa-check-circle"></i> Laisvo laiko</div>
                            <div class="time-m"><i class="far fa-clock"></i> 20 min -3 val</div>
                          </span>
                          <span class="m-price d-inline-block d-md-none float-right">10 - 150 €</span>
                        </div>
                      </div>
                      @if($service->types->count())
                      <span class="cv-d d-none d-md-inline-block mr-0"><img src="{{asset('assets/img/services/tra.png')}}" alt="img"></span>
                      <span class="cv-d d-inline-block d-md-none m-cv-d px-2 mr-0"><span>SUSKLEISTI</span ><span class="float-right"><img  src="{{asset('assets/img/services/tra.png')}}" alt="img"></span></span>
                      @endif
                    </div>
                  </div>


                  <div class="service-purchase-add-all show-purchase">
                    @foreach($service->types as $type)

                    <div class="service-purchase-add">
                      <div class="service-purchase-l">
                        <div class="service-m-img"><img src="{{asset('assets/img/services/demo.jpg')}}" alt="img"></div>
                        <div class="service-m-info">
                          <div class="ser-n-s">
                            <div class="ser-n">{{ $type->name }} 
                              <span class="cv-r"></span>
                              <span class="dis-pr m-perc float-right d-inline-block d-md-none">
                                <span class="">20%</span>
                              </span>
                            </div>
                            <div class="ser-d">
                              <span class=""><img src="{{asset('assets/img/services/star.png')}}" alt="img"> 4.5/5 (5)</span>
                              <span class="fa-ch"><i class="fas fa-check-circle"></i> Laisvo laiko</span>
                              <span class=""><i class="far fa-clock"></i>
                                @php
                                $hour = floor($type->time / 60);
                                $minute = $type->time % 60;
                                @endphp
                                @if($hour > 0)
                                {{ $hour . ' Hour'  }}
                                @endif
                                @if($minute > 0)
                                {{ $minute . ' Minute'  }}
                              @endif</span>
                              <span class="m-price float-sm-right float-none d-inline-block d-md-none">nuo {{ $type->price }} €</span>
                            </div>
                          </div>
                        </div>
                      </div>

                     <!--  <div class="service-purchase-l d-block d-md-none">
                        <div class="service-m-info wth-100-p">
                          <div class="ser-n-s">
                            <div class="ser-d">
                              <span class=""><img src="{{asset('assets/img/services/star.png')}}" alt="img"> 4.5/5 (5)</span>
                              <span class="fa-ch"><i class="fas fa-check-circle"></i> Laisvo laiko</span>
                              <span class=""><i class="far fa-clock"></i>
                                @php
                                $hour = floor($type->time / 60);
                                $minute = $type->time % 60;
                                @endphp
                                @if($hour > 0)
                                {{ $hour . ' Hour'  }}
                                @endif
                                @if($minute > 0)
                                {{ $minute . ' Minute'  }}
                              @endif</span>
                            </div>
                          </div>
                        </div>
                      </div> -->

                      <div class="service-purchase-r">
                        <div class="ser-prc-per">
                          <span class="dis-pr d-none d-md-inline-block active">
                            <span class="per m-0">20%</span>
                            <span class="price m-0">nuo {{ $type->price }} €</span>
                          </span>
                          <span class="ud-i d-none d-md-inline-block"><img src="{{asset('assets/img/services/up-d-i.png')}}" alt="img"></span>
                          <span class="n-p d-inline-block d-md-none"><button class="btn dr-btn1"><i class="fas fa-bookmark"></i> PRISIMINTI</button></span>
                          <span class="n-p">
                            <a href="{{route('user.myService', [$service->title, $service->id])}}">
                              <button class="btn dr-btn2"><i class="fas fa-plus"></i> Go to service</button>
                            </a>
                          </span>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>

                </div>
                @endforeach
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-lg-3 px-0 px-lg-3">

      <div class="pro-box">
        <div class="pro-cnt">
          <!-- <div class="pro-name">{{--$service->user->name--}} {{--$service->user->surname--}}</div> -->
          <div class="pro-m-info">
            <div class="pro-tlt">Susisiekite: <div class="edit-info-btn" onclick="proInfoC(event)">
              <i class="fa fa-edit" title="edit info"></i>
            </div>
          </div>
          <div class="pro-phn"><i class="fas fa-phone"></i>{{ $userInfo['phone'] }}</div>
          <div class="pro-email"><i class="fa fa-envelope"></i></i>{{ Auth::user()->email }}</div>
          <div class="pro-cn"><i class="fas fa-map-marker-alt"></i>{{ $userInfo['address'] }}</div>
        </div>
        <div class="pro-m-info">
          <div class="pro-tlt" onclick="businessHour()">Darbo valandos:
            <span class="p-inp"><i class="fa fa-plus"></i></span>
          </div>
          <div class="pro-m-time">
            @if(@Auth::user()->businessHours()->first())
            <div class="pro-m-time1 pro-t">
              @foreach(Auth::user()->businessHours()->first()->runningHours()->orderBy('day_no', 'asc')->get() as $hour)
              <div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> {{($hour->start) ? Carbon\Carbon::parse($hour->start)->format('H:i') : ''}} - {{($hour->end) ? Carbon\Carbon::parse($hour->end)->format('H:i') : ''}}</div>
              @endforeach
            </div>
            <div class="pro-m-time2 pro-t">
              @foreach(Auth::user()->businessHours()->first()->breakHours()->orderBy('day_no', 'asc')->get() as $hour)
              <div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> {{($hour->start) ? Carbon\Carbon::parse($hour->start)->format('H:i') : ''}} - {{($hour->end) ? Carbon\Carbon::parse($hour->end)->format('H:i') : ''}}</div>
              @endforeach
            </div>
            @else
            <div class="pro-m-time1 pro-t">
              <div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
            </div>
            <div class="pro-m-time2 pro-t">
              <div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
              <div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
            </div>
            @endif
          </div>
        </div>
        <div class="social-icon">
          <ul>
            <li><a class="fb" href="<?= @Auth::user()->facebook?: 'javascript:void(0)'?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
            <li><a class="lin" href="<?= @Auth::user()->linkedin?: 'javascript:void(0)'?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
            <li><a class="go" href="<?= @Auth::user()->gmail?: 'javascript:void(0)'?>" target="_blank"><i class="fab fa-google"></i></a></li>
            <li><a class="tw" href="<?= @Auth::user()->twitter?: 'javascript:void(0)'?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
            <li><a class="pin" href="<?= @Auth::user()->pinterest?: 'javascript:void(0)'?>" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
            <!-- <li><a class="pin" href="<?php //@Auth::user()->facebook?: 'javascript:void(0)'?>" target="_blank"><img src="{{asset('assets/img/services/g-map.png')}}" alt="img"></a></li> -->
          </ul>
        </div>
      </div>
    </div>
    <div class="add-pic">
      <div class="add-p-inp">
        <span class="g-txt"><img src="{{asset('assets/img/services/gallery-i.png')}}" alt="img">@lang('lang.gallery')</span>
        <div class="g-input">
          <!-- <span class="p-inp"><i class="fa fa-plus"></i></span> -->
          <label for="file-upload" class="custom-file-upload">
            <i class="fa fa-plus" title="edit image"></i>
          </label>
          <input id="file-upload" multiple onchange="uploadGallery()" type="file" />
        </div>
      </div>
      <div class="row mar-0" id="productGalleryV">
        @foreach($gallery as $gallery)
        <div class="col-6 pad-0">
          <span class="fa fa-times remveImg" onclick="removeImg(event, '<?= $gallery->id ?>')"></span>
          <a href="{{ asset('gallery/'.$gallery->picture) }}" data-fancybox="gallery">
            <div class="single-content">
              <div class="content-img-p">
                <div class="content-img">
                  <img src="{{ asset('gallery/re_'.$gallery->picture) }}" alt="img">
                </div>
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
      <div class="show-more {{($s_m) ? '' : 'd-none'}}" onclick="seeMoreGallery(<?= Auth::user()->id ?>)"><span>- RODYTI DAUGIAU -</span> </div>
    </div>
    <div class="add-service-side">
      <div class="add-p-inp">
        <span class="g-txt"><img src="{{asset('assets/img/services/gallery-i.png')}}" alt="img">Services</span>
        <div class="g-input">
          <span class="p-inp" onclick="Confirm('Create New Service', 'Are you sure you want to create new service', 'Yes', 'Cancel', '<?= route('user.addMyService') ?>');"><i class="fa fa-plus"></i></span>
        </div>
      </div>
      <div class="row mar-0">
        @foreach($services as $service)
        <div class="col-12 pad-0">
          <a href="{{ route('user.myService', [App\Services\MakeUrlService::url($service->title), $service->id]) }}" class="add-s">
            <div class="service-m-img">
              @if($service->image)
              <img src="{{asset('assets/img/services/' . $service->image)}}" class="service-img" alt="img">
              @else
              <img src="{{asset('assets/img/services/cat-1.png')}}" alt="img">
              @endif
            </div>
            <div class="service-title"><span>{{ $service->title }}</span></div>
          </a>
        </div>
        @endforeach
      </div>
    </div>

    @if(count($allVip)>0)
    <div class="side-blog">
      <section class="blog-section">
        <div class="blog-sidbar">
          <div class="side-tlt">
            <div class="side-txt">SKELBIMAI</div>
            <img src="{{asset('assets/img/services/vip-sm.png')}}" alt="img">
          </div>

          <div class="row m-0">
            @foreach ($allVip as $vip)
            @if(property_exists($vip,'title'))
            @php
            $page_title = $vip->title;
            $page_title = preg_replace('!\s+!', ' ', $page_title);
            $page_title = str_replace(' ', '-', $page_title);
            $see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($vip->title, $vip->service_main_id);
            @endphp
            <div class="co-12 col-sm-6 col-lg-12 p-0">
              <div class="single-content">
                <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
                <div class="content-img-p">
                  <div class="content-img">
                   @if($vip->image)
                   <img src="{{asset('assets/img/services/'.$vip->image)}}" alt="img">
                   @else 
                   <img src="{{asset('assets/img/services/no_service.png')}}" alt="img">
                   @endif
                 </div>
               </div>
               <div class="content-des">
                <div class="des-itm">
                  <div class="itm-name-all">
                    @php
                    $inners = App\Http\Controllers\SearchServiceController::serviceAndProfileCategory($vip->inner_category,$vip->sub_category_id);
                    @endphp
                    @if(count($inners['category']) > 0)
                    @foreach ($inners['category'] as $inner)
                    <div class="itm-name">{{$inner->name_lt}}</div>
                    @endforeach
                    @endif
                  </div>
                  <div class="itm-dis">
                    <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance, 2)}} km</div>
                    <div class="dis-txt"><span><a href="{{$see_more}}">Daugiau</a></span></div>
                  </div>
                </div>
              </div>
              <div class="content-tlt">
                <a href="{{$see_more}}" onclick="recentlyView('{{$vip->service_main_id}}')">{{ $vip->title }}</a>
              </div>
            </div>
          </div>
          @else
          @php
          $userCats = App\Http\Controllers\SearchServiceController::ProfileCategory($vip->user_main_id);
          @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                  @if($userCats['cover_img'])
                  <img src="{{asset('userImage/coverPic/'.$userCats['cover_img']->img)}}" alt="img">
                  @else 
                  <img src="{{asset('assets/img/default_cover.png')}}" alt="img">
                  @endif
                </div>
                <div class="s-pro-img">
                  @if($vip->profile_image)
                  <img src="{{asset('userImage/fixPic/'.$vip->profile_image)}}" alt="img">
                  @else 
                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif
                </div>
              </div>
              @php
              $full_name = '';
              if(@$vip->name){
              $full_name .= @$vip->name.' ';
            }
            if(@$vip->surname){
            $full_name .= @$vip->surname.' ';
          }
          if(@$vip->nickname){
          $full_name .= '('.@$vip->nickname.')';
        }
        $full_name = preg_replace('!\s+!', ' ', $full_name);
        $full_name = str_replace(' ', '-', $full_name);

        @endphp
        <div class="content-des">
          <div class="des-itm">
            <div class="itm-name-all">
              @if(count($userCats['cats']) > 0)
              @foreach($userCats['cats'] as $cat)
              <div class="itm-name">{{$cat->name_lt}}</div>
              @endforeach
              @endif

            </div>
            <div class="itm-dis">
              <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance,2)}} km</div>
              <div class="dis-txt"><span><a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">Daugiau</a></span></div>
            </div>
          </div>
        </div>
        <div class="content-tlt">
          <a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">{{ $vip->surname }}</a>
        </div>
      </div>
    </div>

    @endif
    @endforeach
  </div>
</div>
</section>
</div>
@endif


</div>
</div>
</div>
</section>


<!-- modal -->

<div class="modal fade cr-modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@lang('lang.crop_the_image')</h4>
      </div>
      <div class="modal-body" style="width: 100%; overflow: hidden;">
        <div class="img-container">
          <img id="image" src="https://avatars0.githubusercontent.com/u/3456749" style="width: 100%">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
        <button type="button" class="btn" id="crop">@lang('lang.crop')</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="proImgC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body edit-box-area">
        <div class="edit-box text-center">
          <span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
          <h3 class="">@lang('lang.update_your_picture')</h3>
          <div class="col-md-12" style="padding-bottom:13px">
            <div id="files">
              <?php
              if (Session::has('user_profile_img')) {
                $image = Session::get('user_profile_img');
                $html = '';
                $html .= '<span class="pip">';
                $html .= '<img class="imageThumb" src="' . asset('userImage/tmpPic/' . Auth()->user()->id . '/' . $image) . '">';
                $html .= '<br/>';
                $html .= '<span class="remove" id="' . $image . '">✖</span>';
                $html .= '</span>';
                echo $html;
              }
              ?>
            </div>
          </div>

          <label class="form-label text-dark"><b>@lang('lang.profile_picture')
            <div class="controls m-b-10">
              <label class="btn btn-primary btn-file">
                <i class="fa fa-upload"></i>@lang('lang.upload_profile_picture')
                <input type="file" style="display: none;" name="main_img" id="main_img">
              </label>
            </div>


            <form method="post" action="{{url('user/update-user-picture')}}" enctype="multipart/form-data">
              @csrf
              {{-- <input type="file" name="profile_image" class="form-control"> --}}
              <button class="btn btn-success">@lang('lang.update')</button>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal -->

  <div class="modal fade cr-modal" id="modal_cover" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">@lang('lang.crop_the_image')</h4>
        </div>
        <div class="modal-body" style="width: 100%; overflow: hidden;">
          <div class="img-container">
            <img id="c_image" src="https://avatars0.githubusercontent.com/u/3456749" style="width: 100%">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
          <button type="button" class="btn" id="c_crop">@lang('lang.crop')</button>
        </div>
      </div>
    </div>
  </div>

  {{-- live zone modal --}}
  <form action="{{url('user/add-live-zone')}}" id="live_zone_form" method="POST" enctype="multipart/form-data" onsubmit="return addUpdateLiveZone()">
    @csrf
    <div class="modal fade cr-modal" id="liveZoneModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">@lang('lang.add_live_zone')</h4>
          </div>
          <div class="modal-body" style="width: 100%; overflow: hidden;">
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="name">@lang('lang.title')</label>
                <input type="text" id="live_zone_title" class="form-control" placeholder="Title" aria-label="name" name="live_zone_title" required>
              </div>
            </div>
            <input type="hidden" name="live_zone_status" id="live_zone_status" value="">
            <input type="hidden" name="live_zone_id" id="live_zone_id" value="">
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="bio">@lang('lang.description')</label>
                <textarea id="live_zone_des" class="form-control" placeholder="Live zone description" name="live_zone_des"></textarea>
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="bio"><a href="javascript:;" class="add_a_img" onclick="imgEmbadeHideShow(1)">@lang('lang.update')</a></label>
                <label for="bio"><a href="javascript:;" class="embade_video" onclick="imgEmbadeHideShow(2)">@lang('lang.embade_a_video')</a></label>
              </div>
            </div>
            <div class="col-12 pad-0" id="link-area">
              <div class="form-group">
                <label for="name">@lang('lang.embade_link')</label>
                <input type="text" id="live_zone_vido_link" class="form-control" placeholder="Embade Video Link" aria-label="name" name="live_zone_vido_link">
              </div>
            </div>
            <div class="col-12 pad-0" id="img-area">
              <div class="form-group">
                <label for="name">@lang('lang.upload_image')</label>
                <input type="file" class="form-control-file" id="live_zone_img" name="live_zone_img">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
            <button type="submit" class="btn" id="c_crop">@lang('lang.save')</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  {{-- end live zone modal --}}


  <div class="modal fade pro-i-m" id="proInfoC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body edit-box-area">
          <div class="edit-box">
            <span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
            <h3 class="text-center">@lang('lang.update_your_profile_info')</h3>
            <form autocomplete="off" method="post" action="" id="profile_info_form" enctype="multipart/form-data">
              @csrf
              <div class="col-12 pad-0 acc-type">
                <div class="form-group">
                  <label for="RName">@lang('lang.account_type')<span class="c-red">*</span></label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-check">
                        <input class="form-check-input sls" type="radio" name="account_type" id="exampleRadios1" value="1">
                        <label class="form-check-label" for="exampleRadios1">
                          @lang('lang.salons')
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-check">
                        <input class="form-check-input cmp" type="radio" name="account_type" id="exampleRadios2" value="2">
                        <label class="form-check-label" for="exampleRadios2">
                          @lang('lang.personal')
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="name">@lang('lang.name')<span class="c-red">*</span></label>
                  <input type="text" id="name" class="form-control" placeholder="Name" aria-label="name" name="name">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="surname">@lang('lang.usurname')<span class="c-red">*</span></label>
                  <input type="text" id="surname" class="form-control" placeholder="Surame" aria-label="surname" name="surname">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="nickname">@lang('lang.nickname') @lang('lang.or') @lang('lang.company_name')<span class="c-red">*</span></label>
                  <input type="text" id="nickname" class="form-control" placeholder="@lang('lang.nickname') @lang('lang.or') @lang('lang.company_name')" aria-label="nickname" name="nickname">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="email">@lang('lang.email')</label>
                  <input type="email" id="email" class="form-control" readonly placeholder="Email" aria-label="Email" name="email">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="phone">@lang('lang.phone')</label>
                  <input type="text" id="phone" class="form-control" placeholder="Phone" aria-label="Phone" name="phone">
                </div>
              </div>
              <div class="col-12 pad-0">
                <?php
                $city_arr = ['Vilnius', 'Kaunas', 'Klaipėda', 'Panevėžys', 'Šiauliai'];
                ?>
                <label for="city">@lang('lang.city')</label>
                <div class="input-group">
                  <div class="ctm-select" ctm-slt-n="city_name">
                    <div class="ctm-select-txt pad-l-10">
                      <span class="select-txt noselect" id="city_name">@lang('lang.chose')...</span>
                      <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                    </div>
                    <div class="ctm-option-box noselect">
                      <div class="ctm-option noselect">@lang('lang.chose')...</div>
                      @foreach($city_arr as $city)
                      <div class="ctm-option noselect" ctm-otn-v="{{ $city }}">{{ $city }}</div>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="address">@lang('lang.address')</label>
                  <input type="text" id="autoAddress" class="form-control" placeholder="Address" aria-label="address" name="address">
                </div>
              </div>
              <input type="hidden" name="lat" id="lat" value="">
              <input type="hidden" name="long" id="long" value="">
              <input type="hidden" name="city" id="city" value="">
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="address">@lang('lang.show_on_profile')</label>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sh1" name="show_name[]" type="checkbox" id="inlineCheckbox1" value="1">
                        <label class="form-check-label" for="inlineCheckbox1">@lang('lang.name')</label>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sh2" name="show_name[]" type="checkbox" id="inlineCheckbox1" value="2">
                        <label class="form-check-label" for="inlineCheckbox1">@lang('lang.usurname')</label>
                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input sh3" name="show_name[]" type="checkbox" id="inlineCheckbox1" value="3">
                        <label class="form-check-label" for="inlineCheckbox1">@lang('lang.nickname') / @lang('lang.company_name')</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <h5>@lang('lang.social_info')</h5>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="facebook">@lang('lang.facebook_link')</label>
                  <input type="text" id="facebook" class="form-control" placeholder="Facebook Link" aria-label="facebook" name="facebook">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="twitter">@lang('lang.twitter_link')</label>
                  <input type="text" id="twitter" class="form-control" placeholder="Twitter Link" aria-label="twitter" name="twitter">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="gmail">@lang('lang.gmail_link')</label>
                  <input type="text" id="gmail" class="form-control" placeholder="Gmail Link" aria-label="gmail" name="gmail">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="linkedin">@lang('lang.linkedin_link')</label>
                  <input type="text" id="linkedin" class="form-control" placeholder="Linkedin Link" aria-label="linkedin" name="linkedin">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="pinterest">@lang('lang.pinterest_link')</label>
                  <input type="text" id="pinterest" class="form-control" placeholder="Pinterest Link" aria-label="pinterest" name="pinterest">
                </div>
              </div>
              <div class="col-12 pad-0">
                <div class="form-group">
                  <label for="bio">@lang('lang.short_bio')</label>
                  <textarea id="bio" class="form-control" placeholder="Short Bio" name="bio"></textarea>
                </div>
              </div>
              <button class="btn btn-success float-right">@lang('lang.update')</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade bus-h-m" id="businessHour" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body edit-box-area">
          <div class="edit-box">
            <span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
            <h3 class="text-center">Business Hours</h3>
            <form autocomplete="off" method="post" action="" id="business_hour_form" enctype="multipart/form-data">
              @csrf
              <h4>Monday</h4>
              <?php
              $hours = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00'];
              ?>
              <label>Running hour:</label>
              @if(@Auth::user()->businessHours()->first())
              @php
              $running = Auth::user()->businessHours()->first()->runningHours()->orderBy('day_no', 'asc')->get();
              $break = Auth::user()->businessHours()->first()->breakHours()->orderBy('day_no', 'asc')->get();
              @endphp
              @else
              @php
              $running = '';
              $break = '';
              @endphp
              @endif
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="mon_r_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[0]->start) ? Carbon\Carbon::parse($running[0]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($running[0]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[0]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="mon_r_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[0]->end) ? Carbon\Carbon::parse($running[0]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($running[0]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[0]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <label>Break hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="mon_b_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[0]->start) ? Carbon\Carbon::parse($break[0]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($break[0]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[0]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="mon_b_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[0]->end) ? Carbon\Carbon::parse($break[0]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$break[0]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[0]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h4>Tuesday</h4>
              <label>Running hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="tues_r_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[1]->start) ? Carbon\Carbon::parse($running[1]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$running[1]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[1]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="tues_r_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[1]->end) ? Carbon\Carbon::parse($running[1]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$running[1]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[1]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <label>Break hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="tues_b_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[1]->start) ? Carbon\Carbon::parse($break[1]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$break[1]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[1]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="tues_b_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[1]->end) ? Carbon\Carbon::parse($break[1]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$break[1]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[1]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h4>Wednesday</h4>
              <label>Running hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="wednes_r_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[2]->start) ? Carbon\Carbon::parse($running[2]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$running[2]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[2]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="wednes_r_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[2]->end) ? Carbon\Carbon::parse($running[2]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$running[2]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[2]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <label>Break hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="wednes_b_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[2]->start) ? Carbon\Carbon::parse($break[2]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$break[2]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[2]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="wednes_b_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[2]->end) ? Carbon\Carbon::parse($break[2]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$break[2]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[2]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h4>Thursday</h4>
              <label>Running hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="thurs_r_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[3]->start) ? Carbon\Carbon::parse($running[3]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$running[3]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[3]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="thurs_r_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[3]->end) ? Carbon\Carbon::parse($running[3]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$running[3]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[3]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <label>Break hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="thurs_b_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[3]->start) ? Carbon\Carbon::parse($break[3]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$break[3]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[3]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="thurs_b_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[3]->end) ? Carbon\Carbon::parse($break[3]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$break[3]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[3]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h4>Friday</h4>
              <label>Running hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="fri_r_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{(@$running[4]->start) ? Carbon\Carbon::parse($running[4]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if(@$running[4]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[4]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="fri_r_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[4]->end) ? Carbon\Carbon::parse($running[4]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($running[4]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[4]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <label>Break hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="fri_b_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[4]->start) ? Carbon\Carbon::parse($break[4]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($break[4]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[4]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="fri_b_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[4]->end) ? Carbon\Carbon::parse($break[4]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($break[4]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[4]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h4>Saturday</h4>
              <label>Running hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="satur_r_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[5]->start) ? Carbon\Carbon::parse($running[5]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($running[5]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[5]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="satur_r_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[5]->end) ? Carbon\Carbon::parse($running[5]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($running[5]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[5]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <label>Break hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="satur_b_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[5]->start) ? Carbon\Carbon::parse($break[5]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($break[5]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[5]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="satur_b_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[5]->end) ? Carbon\Carbon::parse($break[5]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($break[5]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[5]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <h4>Sunday</h4>
              <label>Running hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="sun_r_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[6]->start) ? Carbon\Carbon::parse($running[6]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($running[6]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[6]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="sun_r_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($running[6]->end) ? Carbon\Carbon::parse($running[6]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($running[6]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($running[6]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <label>Break hour:</label>
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="sun_b_start">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[6]->start) ? Carbon\Carbon::parse($break[6]->start)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($break[6]->start)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[6]->start)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <div class="ctm-select" ctm-slt-n="sun_b_end">
                      <div class="ctm-select-txt pad-l-10">
                        <span class="select-txt noselect">
                          @if(@Auth::user()->businessHours()->first())
                          {{($break[6]->end) ? Carbon\Carbon::parse($break[6]->end)->format('H:i') : 'choose'}}
                          @else
                          choose
                          @endif
                        </span>
                        <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                      </div>
                      <div class="ctm-option-box noselect">
                        @if(@Auth::user()->businessHours()->first())
                        @if($break[6]->end)
                        <div class="ctm-option noselect" ctm-otn-v="0">@lang('lang.choose')</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="0" ctm-select="true">@lang('lang.choose')</div>
                        @endif
                        @foreach($hours as $hour)
                        @if(@Auth::user()->businessHours()->first())
                        @if(Carbon\Carbon::parse($break[6]->end)->format('H:i') == $hour)
                        <div class="ctm-option noselect" ctm-select="true" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @else
                        <div class="ctm-option noselect" ctm-otn-v="{{$hour}}">{{$hour}}</div>
                        @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <button class="btn btn-success float-right">@lang('lang.update')</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="coverImgC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body edit-box-area">
          <div class="edit-box text-center">
            <form method="post" action="{{url('user/update-cover-img')}}" enctype="multipart/form-data">
              @csrf
              <span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
              <h3 class="">@lang('lang.update_your_cover_photos')</h3>
              <div class="col-md-12" style="padding-bottom:13px">
                <div class="row mar-0" id="cover_all_area">
                  @foreach ($UserCoverA as $uc)
                  <div class="col-6 col-sm-3 pad-l-0">
                    <span class="rmv-p" onclick="deleteCoverPic(event, {{ $uc->id }})">&#10006;</span>
                    <label class="image-checkbox {{ ($uc->status) ? 'image-checkbox-checked prv-has' : '' }}">
                      <img class="cover" src="{{ asset('userImage/coverPic/' . $uc->img) }}" />
                      <input type="checkbox" name="cover_img_d[]" value="{{ $uc->id }}" {{ ($uc->status) ? 'checked' : '' }} />
                      <i class="fa fa-check d-none"></i>
                    </label>
                  </div>
                  @endforeach
                </div>
              </div>

              <label class="form-label text-dark"><b>@lang('lang.cover_photos')</b></label>
              <div class="controls m-b-10">
                <label class="btn btn-primary btn-file">
                  <i class="fa fa-upload"></i>@lang('lang.upload_cover_photos')
                  <input type="file" style="display: none;" id="cover_img">
                </label>
              </div>


              {{-- <input type="file" name="profile_image" class="form-control"> --}}
              <button class="btn btn-success">@lang('lang.update')</button>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- progile payment --}}
  <div class="modal fade pro-i-m" id="profile-payment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body edit-box-area">
          <div class="edit-box">
            <span title="colse" class="removeBox float-right p-2">&#10006;</span>
            <h3 class="text-center">@lang('lang.make_your_profile_vip')</h3>
            <form autocomplete="off" method="post" action="" id="profile-payment-form">
              @csrf
              <input type="hidden" name="pkg_id" class="pkg_id">
              <div class="col-md-12 text-center" style="padding:20px; color:#45bd26">
                &#128;<span id="put-price">0.00</span>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <div class="ctm-select">
                    <div class="ctm-select-txt pad-l-10">
                      <span class="select-txt noselect" id="category">@lang('lang.choose_duration')</span>
                      <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                    </div>
                    <div class="ctm-option-box put-sub-category">
                      @if(count($profilePackages) >0)
                      @foreach ($profilePackages as $pkg)
                      <div onclick="getPrice( {{$pkg->id }} )" class="ctm-option noselect"> {{ $pkg->duration }}</div>
                      @endforeach
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for="RName">@lang('lang.payment_method')</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-check">
                        <input class="form-check-input paysera" type="radio" name="payment_type" id="exampleRadios1" value="1">
                        <label class="form-check-label" for="exampleRadios1">
                          @lang('lang.paysera')
                        </label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-check">
                        <input class="form-check-input stripe" type="radio" name="payment_type" id="exampleRadios2" value="2" checked>
                        <label class="form-check-label" for="exampleRadios2">
                          @lang('lang.card_payment')
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <button class="btn btn-success float-right">@lang('lang.pay_now')</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal p-0" id="ser-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
        <div class="modal-h">
          <h5 class="modal-title" id="exampleModalLongTitle">Evening makeup</h5>
          <div class="ser-s">
            <span class="r-star">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
            </span>
            <span class="p-txt">5.00</span>
            <span class="r-txt">(1)</span>
          </div>
          <div class="ser-d"><i class="fa fa-calendar-plus"></i>Laisvo laiko rezervavimas</div>
          <span class="close-m" data-dismiss="modal" aria-label="Close">&times;</span>
        </div>
        <div class="modal-body">
          <p class="ser-des">During this make-up, the skin is perfectly covered and the face is contoured, eyes, lips are highlighted, and artificial lashes are glued. The goal of makeup is to turn you into a real party star.</p>
          <div class="row">
            <div class="col-12 col-md-2 pr-0">
              <span class="s-t-img"><img src="{{asset('assets/img/duration.svg')}}"></span>
              <div class="ser-txt-a">
                <div class="ser-s-h">Duration</div>
                <div class="ser-s-txt">1 p.m. 20 min.</div>
              </div>
            </div>
            <div class="col-12 col-md-2 pr-0">
              <span class="s-t-img"><img src="{{asset('assets/img/valid.svg')}}"></span>
              <div class="ser-txt-a">
                <div class="ser-s-h">Validity</div>
                <div class="ser-s-txt">6 months</div>
              </div>
            </div>
            <div class="col-12 col-md-4 pr-0">
              <span class="s-t-img"><img src="{{asset('assets/img/registration.svg')}}"></span>
              <div class="ser-txt-a">
                <div class="ser-s-h">Pre-registration required</div>
                <div class="ser-s-txt">The details of the service provider will be visible when you purchase the e-mail. check</div>
              </div>
            </div>
            <div class="col-12 col-md-4 pr-0">
              <span class="s-t-img"><img src="{{asset('assets/img/multifunctional.svg')}}"></span>
              <div class="ser-txt-a">
                <div class="ser-s-h">This check is multi-type</div>
                <div class="ser-s-txt">It is possible to pay with multi-type gift vouchers and use the services of other partners. More</div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="s-per">-14%</div>
          <div class="">
            <div class="o-p"><del>45.00 €</del></div>
            <div class="n-p">45.00 €</div>
          </div>
          <button type="button" class="btn add-cart ">Add to cart</button>
        </div>
      </div>
    </div>
  </div>

  {{-- end prfile payment --}}

  {{-- upload live zone image --}}
  <form id="uploadLiveZoneImage">
    @csrf
    <div class="modal fade cr-modal" id="liveZoneImgModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Photos</h4>
          </div>
          <div class="modal-body" style="width: 100%;">
            <div class="col-12 pad-0">
              <div class="form-group">
                <div class="alert alert-danger image-error"></div>
                <label for="name">@lang('lang.upload_image')</label>
                <input type="file" class="form-control-file" id="live_zone_img" name="image" required>
              </div>
              <button type="submit" class="btn btn-info">Upload</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
  {{-- end upload live zone image --}}

  {{-- upload live zone vido --}}
  <form id="uploadLiveZoneVideo">
    @csrf
    <div class="modal fade cr-modal" id="liveZoneVideoModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Video</h4>
          </div>
          <div class="modal-body" style="width: 100%;">
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="name">Youtube video link:</label>
                <input type="text" id="live_zone_vido_link" class="form-control" aria-label="name" name="live_zone_vido_link" required>
              </div>
              <button type="submit" class="btn btn-info">Save</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
  {{-- end upload live zone video --}}
  {{-- picture pop up --}}
  <div class="modal  fade lz-post-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="picture-pop-up-modal" data-backdrop="static" data-keyboard="false">
    <span class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </span>
    <input type="hidden" name="p_live_zone_id" id="p_live_zone_id">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content put-live-zone-popup">

      </div>
    </div>
  </div>
  {{-- end picture pop up --}}

  <!-- edit live zone modal -->
  <form id="edit_live_zone" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="editModalLabel">Edit Livezone</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="exampleFormControlTextarea1">Description</label>
              <textarea class="form-control" id="u_des" name="u_des" rows="3"></textarea>
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="u_show_profile" name="u_show_profile">
              <label class="form-check-label" for="exampleCheck1">Show profile</label>
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="u_show_livezone" name="u_show_livezone">
              <label class="form-check-label" for="exampleCheck2">Show livezone</label>
            </div>
            <div class="form-group f-u-tab">
              <span class="u-i-tab">Upload Image</span>
              <span class="u-v-tab">Youtube video</span>
            </div>
            <div class="form-group upload-image">
              <label for="exampleFormControlFile1">Upload Image</label>
              <input type="file" class="form-control-file" name="u_image" id="u_image">
            </div>
            <div class="form-group video-link">
              <label for="exampleInputEmail1">Video Link</label>
              <input type="text" class="form-control" id="u_y_link" name="u_y_link" aria-describedby="emailHelp" placeholder="https://www.youtube.com/watch?v=F_0H2TVywRQ">
            </div>
            <input type="hidden" name="u_livezone_id" id="u_livezone_id" value="">
            <input type="hidden" name="u_livezone_status" id="u_livezone_status" value="">

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  </form>



  @endsection
  @section('script')
  <script src="{{ asset('assets/js/jquery.fancybox.js')}}"></script>
  <script src="{{ asset('assets/js/cropper.js')}}"></script>
  <script src="https://js.stripe.com/v3/"></script>
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>

  <script>
    window.gallery_up = '<?= route('user.galleryFileUp') ?>';
    window.gallery_remove = '<?= route('user.galleryRemover') ?>';
    window.cover_pic_remove = '<?= route('user.userCoverRemover') ?>';
    window.profile_single_tmp_img = '<?= url("user/process-single-tmp-image") ?>';
    window.profile_single_tmp_remove = '<?= url("user/single-tmp-img-remove") ?>';
    window.cover_img_upload = '<?= url("user/cover-image-upload") ?>';
    window.get_profile_info = '<?= url("user/get-profile-information") ?>';
    window.insert_profile_info = '<?= url("user/insert-profile-information") ?>';
    window.update_business_hour = '<?= url("user/update-business-hour") ?>';
    window.see_more_gallery = '<?= url("see-more-gallery") ?>';

  // autocomplete address
  $(function() {
    var autocomplete;
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('autoAddress')), {
      types: ['geocode'],
    });
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var near_place = autocomplete.getPlace();

      var address = $('#autoAddress').val();
      var lat = near_place.geometry.location.lat();
      var lng = near_place.geometry.location.lng();
      var city = '';
      for (var i = 0; i < near_place.address_components.length; i++) {
        for (var j = 0; j < near_place.address_components[i].types.length; j++) {
          if (near_place.address_components[i].types[j] == 'administrative_area_level_2') {
            city = near_place.address_components[i].long_name;
          }
        }
      }
      if (city.indexOf("District")) {
        city = city.replace("District", "");
      }
      $('#lat').val(lat);
      $('#long').val(lng);
      $('#city').val($.trim(city));
      console.log('city=' + city + ' address=' + address + ' lat=' + lat + ' lng=' + lng);
    });

  });





  $(function() {
    $('[data-fancybox="gallery"]').fancybox({
      thumbs: {
        autoStart: true
      }
    });
  });
</script>

@include('pages.comments.live-zone-script')
@include('pages.comments.live-zone-popup-script')

<script>
  function getPrice(id) {
    var data = new FormData();
    data.append('id', id);
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("vip-profile-amount")}}',
      success: function(response) {
        $('#put-price').text(response.price.toFixed(2));
        $('.pkg_id').val(id);
      }
    });
  }
</script>
<script>
  $('#profile-payment-form').on('submit', function() {
    var data = new FormData($('form#profile-payment-form')[0]);
    if ($('.pkg_id').val() < 1) {
      return false;
    }
    $('#loader').addClass('loading');
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("vip-profile-payment")}}',
      success: function(response) {
        if ($(".stripe").is(":checked")) {
          var stripe = Stripe('pk_test_SzoBR6NC8mp5FvJEwkOld0nF00UlcgpKHq');
          console.log(response.url);
          stripe.redirectToCheckout({
            sessionId: response.url.id
          }).then(function(result) {

          });
        } else {
          window.location.href = response.url;
        }

        console.log(response);
      },
      error: function (err) {
       $('#loader').removeClass('loading');
     },

   });
    return false;

  });
</script>
@if(Session::has('success'))
<script>
  var x = document.getElementById("snackbar");
  x.style.background = '#7d0000';
  x.innerHTML = '{{ Session::get("success")}}';
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 7000);
</script>
@endif
<script>
  function openLiveZone1() {
    $('#live_zone_status').val('');
    $('#live_zone_title').val('');
    $('#live_zone_des').val('');
    $('#live_zone_id').val('');
    $('#live_zone_vido_link').val('');
    $('#live_zone_img').val('');
    $('#live_zone_form').attr('action', "{{url('user/add-live-zone')}}");
    $('#link-area').hide();
    $('#img-area').hide();
    $('#live_zone_status').val('');
    $('#liveZoneModal').modal('show');

  }
</script>
<script>
  function imgEmbadeHideShow(number) {
    if (number == 1) {
      $('#link-area').hide();
      $('#img-area').show();
      $('#live_zone_status').val(1);
    } else if (number == 2) {
      $('#link-area').show();
      $('#img-area').hide();
      $('#live_zone_status').val(2);
    }
  }
</script>
<script>
  function addUpdateLiveZone() {
    var url = $('form#live_zone_form').attr('action');
    var pieces = url.split('/');
    var item = pieces[pieces.length - 1];
    if (item == 'add-live-zone') {
      AddLiveZone();
    } else if (item == 'update-live-zone') {
      updateLiveZone();
    }
    return false;
  }
</script>


<script>
  function openLiveZone(id) {
    var data = new FormData();
    $('#editModal').modal('show');
    data.append('id', id);
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("user/edit-live-zone")}}',
      success: function(response) {
        var des = response.description.replace(/<br\s*\/?>/g, '\n');
        $('#u_des').html(des);
        if (response.show_profile == 1) {
          $('#u_show_profile').prop('checked', true);
        }
        if (response.show_livezone == 1) {
          $('#u_show_livezone').prop('checked', true);
        }
        $('#u_livezone_id').val(response.id);

      }
    });
  }
</script>

<script>
  function AddLiveZone() {
    var data = new FormData($('form#live_zone_form')[0]);
    var url = $('form#live_zone_form').attr('action');
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: url,
      success: function(response) {
        console.log(response);
        $('.mylz-content').prepend(response);
        $('#liveZoneModal').modal('hide');
      }
    });

  }
</script>

<script>
  function updateLiveZone() {
    var data = new FormData($('form#live_zone_form')[0]);
    var url = $('form#live_zone_form').attr('action');
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: url,
      success: function(response) {
        $('#lz-title-' + response.id).text(response.title);
        $('#lz-des-' + response.id).text(response.description);
        if (response.status == 1) {
          $('.lz-img-' + response.id).removeClass('imgVideo');
          $('.lz-img-' + response.id).removeClass('imgVideoBlock');
          $('.lz-img-' + response.id).addClass('imgVideoBlock');

          $('.lz-video-' + response.id).addClass('imgVideo');
          $('.lz-video-' + response.id).removeClass('imgVideoBlock');
          var i_url = "{{asset('liveZone')}}";
          var img_url = i_url + '/' + response.image;
          $('#lz-img-' + response.id).attr('src', img_url);
        }
        if (response.status == 2) {
          $('.lz-img-' + response.id).removeClass('imgVideoBlock');
          $('.lz-img-' + response.id).addClass('imgVideo');
          $('.lz-video-' + response.id).removeClass('imgVideo');
          $('.lz-video-' + response.id).addClass('imgVideoBlock');
          var video_url = response.image;
          $('#lz-video-' + response.id).attr('src', video_url);
        }
        $('#live_zone_status').val('');
        $('#live_zone_title').val('');
        $('#live_zone_des').val('');
        $('#live_zone_id').val('');
        $('#live_zone_vido_link').val('');
        $('#live_zone_img').val('');
        $('#liveZoneModal').modal('hide');
      }
    });

  }
</script>
<script>
  $(document).ready(function() {

    $(document).on("click", ".reply-s", function() {
      var n = $(this).attr("ctm-val");
      console.log(n);
      $(".write-reply" + n).show();
      $(".write-reply" + n).find('input').focus();
    });
    $(document).on("click", ".add-c", function() {
      $(".write-comment-show").find('input').focus();
    });
    var visitor_lat = $('#visitor_lat').val();
    var visitor_lng = $('#visitor_lng').val();
    var visitor_address = $('#visitor_address').val();
    getServiceAndProfile(visitor_lat, visitor_lng, visitor_address);
  });

  function getServiceAndProfile(lat, lng, address) {
    var data = new FormData();
    data.append('lat', lat);
    data.append('lng', lng);
    data.append('address', address);
    data.append('_token', "{{csrf_token()}}");
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("get-profile-and-service")}}',
      success: function(response) {
        $('#vip-profile').html(response.profiles);
        $('#vip-services').html(response.services);
      }

    });
  }

  function deleteLiveZone(id) {
    var data = new FormData();
    data.append('id', id);
    data.append('_token', "{{csrf_token()}}");
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("user/delete-live-zone")}}',
      beforeSend: function() {
        return confirm('Are you sure to delete this item?');
      },
      success: function(response) {
        $('#live-zone-number-' + id).remove();
      }

    });
  }
</script>
<script>
  var page = 1;
  $(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() >= $(document).height() - 700) {
      page++;
      var user_id = $('#main_user_idd').val();
      var link = '{{url("user/live-zone/pagination")}}' + '?show_e_d=1&id=' + user_id + '&page=' + page;
      loadMoreData(link);
    }
  });

  function loadMoreData(link) {
    $.ajax({
      url: link,
      type: "get",
    })
    .done(function(data) {
      if (data.html == " ") {
        $('.ajax-load').html("No more records found");
        return;
      }
      console.log(data);
      $(".add-new-livezone").append(data.html);
    })
    .fail(function(jqXHR, ajaxOptions, thrownError) {

    });
  }
</script>
<script>
  $(document).on('click', '.u-i-tab', function() {
    $('.video-link').hide();
    $('.upload-image').show();
    $('#u_livezone_status').val(1);
  });
  $(document).on('click', '.u-v-tab', function() {

    $('.upload-image').hide();
    $('.video-link').show();
    $('#u_livezone_status').val(2);
  });

  $(document).on('submit', '#edit_live_zone', function() {
    var des = $('#u_des').val().replace(/\r\n|\r|\n/g, "<br/>");
    var data = new FormData($('form#edit_live_zone')[0]);
    data.append('des1', des);
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("user/update-live-zone")}}',
      success: function(response) {
        if (response.id) {
          $('.ev-livezone-des-' + response.id).empty();
          $('.ev-livezone-des-' + response.id).html(response.des);
          $('.img_or_video_' + response.id).empty();
          $('.img_or_video_' + response.id).html(response.img);
          $('#editModal').modal('hide');

        }


      }

    });
    return false;
  });
</script>

@endsection