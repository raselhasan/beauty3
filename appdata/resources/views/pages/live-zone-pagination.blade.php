@if(count($data['liveZones'])>0)
    @foreach ($data['liveZones'] as $liveZone)
    <div class="single-content">
      <div class="user-d">
        <div class="user-d-img">
          <img src="{{asset('assets/img/services/'.$liveZone->user->profile_image)}}" class="rounded-circle img-fluid">
        </div>
        <div class="user-d-d">
          <p>{{$liveZone->user->name}} @if($liveZone->user->surname) {{$liveZone->user->surname}} @endif @if($liveZone->user->nickname) ({{$liveZone->user->surname}}) @endif</p>
          <p><span class="sml-sp">{{date('M d H:i a',strtotime($liveZone->created_at))}} {{ $liveZone->created_at->diffForHumans()}}</span></p>
        </div>
      </div>
      <div class="live-tlt">
        <h1>{{$liveZone->title}}</h1>
      </div>
      <div class="live-des">
        <p>{{$liveZone->description}}</p>
      </div>
      @if($liveZone->status == 1)
      <div class="content-img-p">
        <div class="content-img">
          <img src="{{asset('liveZone/'.$liveZone->image)}}">
        </div>
      </div>
      @endif
      @if($liveZone->status == 2)
      <div class="content-img-p video">
        <div class="content-img">
          <iframe src="{{$liveZone->image}}">
          </iframe>
        </div>
      </div>
      @endif
    </div>
    @endforeach
@endif