 @extends('layout.app')
  @section('title')
Payment Histories | GrožioKalviai
@endsection
 @section('style')
 <style>
 	.all_service {
 		margin-left: 20px;
 	}
 </style>
 @endsection
 @section('content')
 @include('inc.header-filter-new')
 <section class="select-panel-section">
 	<div class="max-fix-width">
 		<div class="section-title">@lang('lang.service_payment')</div>
 		<div class="adition-service">
 			<div class="service-list">
 				{{-- start block --}}
 				@php
 				$is_item = 0;
 				@endphp
 				@if(count($payments) > 0)
 				@php
 				$is_item = 1;
 				@endphp
 				@foreach ($payments as $payment)
 				<div class="s-list">
 					<div class="row">
 						<div class="col-md-12">
 							<div class="row">
 								<div class="col-md-3">
 									<div class="all_service">{{ $payment['0']['service_name'] }}</div>
 								</div>
 								<div class="col-md-3">
 									@if(count($payment) > 0)
 									@foreach ($payment as $pay)
 									<div>{{ $pay['package_name'] }}</div>
 									@endforeach
 									@endif
 								</div>
 								<div class="col-md-2">
 									@php
 									$total = 0;
 									$ids = '';
 									@endphp
 									@if(count($payment) > 0)
 									@foreach ($payment as $pay)
 									<div class="adition-price">
 										&#128;<span>{{ number_format($pay['paid_amount'],2) }}</span>
 									</div>
 									@php
 									$total = $total + $pay['paid_amount'];
 									$ids .= $pay['id'].' ';
 									@endphp
 									@endforeach
 									@endif
 								</div>
 								<div class="col-md-2">
 									<div class="adition-price">
 										&#128;<span class="cr-price">{{ number_format($total,2) }}</span>
 									</div>
 								</div>
 								<div class="col-md-2">
 									<a href="{{url('download-service-payment?ids='.$ids)}}" target="_blank"><button class="btn ser-btn"><i class="fas fa-download"></i></button></a>

 								</div>
 							</div>
 						</div>

 					</div>
 				</div>
 				{{-- block end --}}
 				@endforeach
 				@endif
 				{{-- end service payment --}}

 				{{-- start profile payment --}}
 				@if(count($profile_payments)>0)
 				@php
 				$is_item = 1;
 				@endphp
 				<div class="section-title">@lang('lang.profile_payment')</div>
 				@foreach ($profile_payments as $p_payment)
 				<div class="s-list">
 					<div class="row">
 						<div class="col-md-12">
 							<div class="row">
 								<div class="col-md-3">
 									<div class="all_service">{{ $p_payment['user']['name']}}</div>
 								</div>
 								<div class="col-md-3">
 									{{$p_payment['vip_package']['pkg_title']}}
 								</div>
 								<div class="col-md-2">
 									{{$p_payment['vip_package']['duration']}}
 								</div>
 								<div class="col-md-2">
 									<div class="adition-price">
 										&#128;<span class="cr-price">{{$p_payment['vip_package']['price']}}</span>
 									</div>
 								</div>
 								<div class="col-md-2">
 									<a href="{{url('download-profile-payment/'.$p_payment['id'])}}" target="_blank"><button class="btn ser-btn"><i class="fas fa-download"></i></button></a>

 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 				@endforeach
 				@endif
 				{{-- end profile payment --}}

 				{{-- Add controll payment --}}
 				@if(count($AddShowingPayment)>0)
 				@php
 				$is_item = 1;
 				@endphp
 				<div class="section-title">Advertisement Controll payment</div>
 				@foreach ($AddShowingPayment as $add_payment)
 				<div class="s-list">
 					<div class="row">
 						<div class="col-md-12">
 							<div class="row">
 								<div class="col-md-3">
 									<div class="all_service">{{ $add_payment['user']['name']}}</div>
 								</div>
 								<div class="col-md-3">
 									{{$add_payment['add_showing_package']['name']}}
 								</div>
 								<div class="col-md-2">
 									{{$add_payment['add_showing_package']['duration']}}
 								</div>
 								<div class="col-md-2">
 									<div class="adition-price">
 										&#128;<span class="cr-price">{{$add_payment['add_showing_package']['price']}}</span>
 									</div>
 								</div>
 								<div class="col-md-2">
 									<a href="{{url('download-addcontroll-payment/'.$add_payment['id'])}}" target="_blank"><button class="btn ser-btn"><i class="fas fa-download"></i></button></a>

 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 				@endforeach
 				@endif
 				{{-- end add controll payment --}}
 				@if($is_item == 0)
 				<div class="alert alert-success" role="alert">
 					@lang('lang.you_have_no_payment_history')
 				</div>
 				@endif

 			</div>
 		</div>
 	</div>
 </section>
 @endsection
 @section('script')

 @endsection