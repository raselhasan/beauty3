@extends('layout.app')
@section('title')
Live Zone | GrožioKalviai
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fancybox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cropper.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}"> --}}
<style type="text/css">
  .date a {
    color: #E25B37;
  }

  .url-color {
    color: #E25B37;
  }

  #live-zone-textarea {
    border: none;
    background: black;
    margin-top: 9px;
  }

  .add-live-zone {
    margin-bottom: 10px;
    cursor: pointer;
  }

  .add_a_img {
    color: #2a9e63;
    text-decoration: underline;
    margin-right: 10px;
  }

  .embade_video {
    color: #E25B37;
    text-decoration: underline;
    margin-right: 10px;
  }

  #link-area,
  #img-area {
    display: none;
  }

  .imgVideo {
    display: none;
  }

  .imgVideoBlock {
    display: block;
  }

  .user-d img {
    height: 53px;
  }

  .user-d-img {
    float: left;
  }

  .user-d-d p {
    margin-left: 66px;
    margin-bottom: 0px;
    font-size: 18px;
  }

  .live-tlt h1 {
    font-size: 21px;
    margin-top: 33px;
  }

  .userPro-section .live-zona .live-zona-cnt .single-content {
    margin-bottom: 30px;
  }

  .userPro-section .live-zona .live-zona-cnt .single-content {
    background-color: none !important;
  }

  .sml-sp {
    font-size: 12px;
  }
</style>
@endsection
@section('content')
@include('inc.header-filter-new')
<section class="about-section userPro-section">
  <div class="max-fix-width">
    <div class="row mar-0">
      <div class="col-12 col-lg-7">
        @if(Auth::check())
        <div class="row">
          <div class="col-md-2">
            <img src="{{asset('assets/img/services/'.@$data['user']->profile_image)}}" class="rounded-circle img-fluid">
          </div>
          <div class="col-md-10">
            <div class="form-group">
              <textarea class="form-control" id="live-zone-textarea" rows="3" placeholder="Add live zone...." onclick="openLiveZone1()"></textarea>
            </div>
          </div>
        </div>
        @endif
        {{-- live zone add --}}



        <div class="live-zona">
          {{-- <div class="section-title">@lang('lang.live_zona')</div> --}}
          <div class="live-zona-cnt add-new-live-zone">
            @if(count($data['liveZones'])>0)
            @foreach ($data['liveZones'] as $liveZone)
            <div class="single-content">
              <div class="user-d">
                <div class="user-d-img">
                  @if($liveZone->user)
                  <img src="{{asset('userImage/fixPic/'.$liveZone->user->profile_image)}}" class="rounded-circle img-fluid">
                  @else
                  <img src="{{asset('userImage/fixPic/demo.jpg')}}" class="rounded-circle img-fluid">
                  @endif
                </div>
                <div class="user-d-d">
                  <p>
                    @if($liveZone->user)
                    {{$liveZone->user->name}} @if($liveZone->user->surname) {{$liveZone->user->surname}} @endif @if($liveZone->user->nickname) ({{$liveZone->user->surname}}) @endif
                    @endif
                  </p>
                  <p><span class="sml-sp">{{date('M d H:i a',strtotime($liveZone->created_at))}} {{ $liveZone->created_at->diffForHumans()}}</span></p>
                </div>
              </div>
              <div class="live-tlt">
                <h1>{{$liveZone->title}}</h1>
              </div>
              <div class="live-des">
                <p>{{$liveZone->description}}</p>
              </div>
              @if($liveZone->status == 1)
              <div class="content-img-p">
                <div class="content-img">
                  <img src="{{asset('liveZone/'.$liveZone->image)}}">
                </div>
              </div>
              @endif
              @if($liveZone->status == 2)
              <div class="content-img-p video">
                <div class="content-img">
                  <iframe src="{{$liveZone->image}}">
                  </iframe>
                </div>
              </div>
              @endif
            </div>
            @endforeach
            @endif

          </div>
        </div>
      </div>
      <div class="col-12 col-lg-5">
        <div class="side-blog">
          <section class="blog-section">
            <div id="vip-profile"></div>

          </section>
        </div>

        <div id="vip-services"></div>



      </div>
    </div>
  </div>
</section>
<section class="slider-section">
  <div class="max-fix-width">
    <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="{{asset('assets/img/slider1.jpg')}}">
          <div class="carousel-caption">
            <h1>@lang('lang.third_slider_lavel')</h1>
            <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="{{asset('assets/img/slider2.jpg')}}">
          <div class="carousel-caption">
            <h1>@lang('lang.third_slider_lavel')</h1>
            <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
          </div>
        </div>
        <div class="carousel-item">
          <img src="{{asset('assets/img/slider3.jpg')}}">
          <div class="carousel-caption">
            <h1>@lang('lang.third_slider_lavel')</h1>
            <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">@lang('lang.previous')</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">@lang('lang.next')</span>
      </a>
    </div>
  </div>
</section>


{{-- live zone modal --}}
<form action="{{url('user/add-live-zone')}}" id="live_zone_form" method="POST" enctype="multipart/form-data" onsubmit="return addUpdateLiveZone()">
  @csrf
  <div class="modal fade cr-modal" id="liveZoneModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">@lang('lang.add_live_zone')</h4>
        </div>
        <div class="modal-body" style="width: 100%; overflow: hidden;">
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="name">@lang('lang.title')</label>
              <input type="text" id="live_zone_title" class="form-control" placeholder="Title" aria-label="name" name="live_zone_title" required>
            </div>
          </div>
          <input type="hidden" name="live_zone_status" id="live_zone_status" value="">
          <input type="hidden" name="live_zone_id" id="live_zone_id" value="">
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="bio">@lang('lang.description')</label>
              <textarea id="live_zone_des" class="form-control" placeholder="Live zone description" name="live_zone_des"></textarea>
            </div>
          </div>
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="bio"><a href="javascript:;" class="add_a_img" onclick="imgEmbadeHideShow(1)">@lang('lang.update')</a></label>
              <label for="bio"><a href="javascript:;" class="embade_video" onclick="imgEmbadeHideShow(2)">@lang('lang.embade_a_video')</a></label>
            </div>
          </div>
          <div class="col-12 pad-0" id="link-area">
            <div class="form-group">
              <label for="name">@lang('lang.embade_link')</label>
              <input type="text" id="live_zone_vido_link" class="form-control" placeholder="Embade Video Link" aria-label="name" name="live_zone_vido_link">
            </div>
          </div>
          <div class="col-12 pad-0" id="img-area">
            <div class="form-group">
              <label for="name">@lang('lang.upload_image')</label>
              <input type="file" class="form-control-file" id="live_zone_img" name="live_zone_img">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
          <button type="submit" class="btn" id="c_crop">@lang('lang.save')</button>
        </div>
      </div>
    </div>
  </div>
</form>
{{-- end live zone modal --}}




@endsection
@section('script')
<script src="{{ asset('assets/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('assets/js/cropper.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>
<script>
  $(document).ready(function() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function(position) {
        lat = position.coords.latitude;
        lng = position.coords.longitude;
        getServiceAndProfile(lat, lng);
      }, function() {

      });
    }
  });

  function getServiceAndProfile(lat, lng) {
    var data = new FormData();
    data.append('lat', lat);
    data.append('lng', lng);
    data.append('_token', "{{csrf_token()}}");
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("get-profile-and-service")}}',
      success: function(response) {
        $('#vip-profile').html(response.profiles);
        $('#vip-services').html(response.services);
      }

    });
  }
</script>

<script>
  window.gallery_up = '<?= route('user.galleryFileUp') ?>';
  window.gallery_remove = '<?= route('user.galleryRemover') ?>';
  window.cover_pic_remove = '<?= route('user.userCoverRemover') ?>';
  window.profile_single_tmp_img = '<?= url("user/process-single-tmp-image") ?>';
  window.profile_single_tmp_remove = '<?= url("user/single-tmp-img-remove") ?>';
  window.cover_img_upload = '<?= url("user/cover-image-upload") ?>';
  window.get_profile_info = '<?= url("user/get-profile-information") ?>';
  window.insert_profile_info = '<?= url("user/insert-profile-information") ?>';


  $(function() {
    $('[data-fancybox="gallery"]').fancybox({
      thumbs: {
        autoStart: true
      }
    });
  });
</script>

<script>
  function openLiveZone1() {
    $('#live_zone_status').val('');
    $('#live_zone_title').val('');
    $('#live_zone_des').val('');
    $('#live_zone_id').val('');
    $('#live_zone_vido_link').val('');
    $('#live_zone_img').val('');
    $('#live_zone_form').attr('action', "{{url('user/add-live-zone')}}");
    $('#link-area').hide();
    $('#img-area').hide();
    $('#live_zone_status').val('');
    $('#liveZoneModal').modal('show');

  }
</script>
<script>
  function imgEmbadeHideShow(number) {
    if (number == 1) {
      $('#link-area').hide();
      $('#img-area').show();
      $('#live_zone_status').val(1);
    } else if (number == 2) {
      $('#link-area').show();
      $('#img-area').hide();
      $('#live_zone_status').val(2);
    }
  }
</script>


<script type="text/javascript">
  var page = 1;
  $(window).scroll(function() {
    // var scroll_height = $(window).scrollTop();
    // var window_height = $(window).height();
    // var document_height = $(document).height();
    // var scroll_height1 = scroll_height+window_height;
    // console.log('Scroll height='+scroll_height1);
    // console.log('Total Height='+document_height);
    // return false;
    if ($(window).scrollTop() + $(window).height() >= $(document).height() - 700) {
      page++;
      console.log('rasel');
      loadMoreData(page);
    }
  });

  function loadMoreData(page) {
    $.ajax({
        url: '?page=' + page,
        type: "get",
        beforeSend: function() {
          // $('.ajax-load').show();
        }
      })
      .done(function(data) {

        if (data.html == " ") {
          $('.ajax-load').html("No more records found");
          return;
        }
        // $('.ajax-load').hide();
        $(".add-new-live-zone").append(data.html);
      })
      .fail(function(jqXHR, ajaxOptions, thrownError) {

      });
  }
</script>


@endsection