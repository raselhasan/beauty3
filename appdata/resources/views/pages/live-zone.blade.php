@extends('layout.app')
@section('title')
Live Zone | GrožioKalviai
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fancybox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cropper.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}"> --}}
<style>
.date a {
  color: #E25B37;
}

.url-color {
  color: #E25B37;
}

#live-zone-textarea {
  border: none;
  background: black;
  margin-top: 9px;
}

.add-live-zone {
  margin-bottom: 10px;
  cursor: pointer;
}

.add_a_img {
  color: #2a9e63;
  text-decoration: underline;
  margin-right: 10px;
}

.embade_video {
  color: #E25B37;
  text-decoration: underline;
  margin-right: 10px;
}

#link-area,
#img-area {
  display: none;
}

.imgVideo {
  display: none;
}

.imgVideoBlock {
  display: block;
}

.user-d img {
  height: 53px;
}

.user-d-img {
  float: left;
}

.user-d-d p {
  margin-left: 66px;
  margin-bottom: 0px;
  font-size: 18px;
}

.live-tlt h1 {
  font-size: 21px;
  margin-top: 33px;
}

.userPro-section .live-zona .live-zona-cnt .single-content {
  margin-bottom: 30px;
}

.userPro-section .live-zona .live-zona-cnt .single-content {
  background-color: none !important;
}

.sml-sp {
  font-size: 12px;
}

.image-error {
  display: none;
}

.uploaded_image_file {
  border-bottom: 1px solid #704923;
}

.uploaded_image_file1 img {
  height: 315px;
  width: 840px;
  padding-left: 20px;
  margin-top: 5px;
  margin-bottom: 5px;
}

.f-avitiy {
  margin-top: 20px;
}

.see-more {
  margin-left: 19px;
}

.v-f-s {
  font-size: 12px !important;
  text-decoration: underline;
}

#s_show {
  display: none;
}

#s_see_more {
  color: orange;
  cursor: pointer;
}

.seeMore {
  color: #e25b37;
}
</style>
@endsection
@section('content')
@include('inc.header-filter-new')
<section class="about-section userPro-section live-zona-section">
  <div class="row mar-0">
    <div class="col-12 col-lg-9 p-0">
      {{-- live zone add --}}

      <div class="live-zona live-zona-sec-cnt">
        <div class="live-zona-tlt"><img src="{{asset('assets/img/services/triangle-r.png')}}" alt="img"> Live Zona</div>
        <div class="live-zona-cnt">
          <!-- class- add-new-live-zone -->
          <div class="row mar-0">
            <div class="col-12 p-0">
              @if(Auth::check())
              <form id="saveLiveZoneForm">
                @csrf
                <div class="write-post">
                  <div class="post-d-c-tlt">
                    <div class="post-d-txt">KURTI PRANEŠIMĄ
                      <span class="post-r">...</span>
                    </div>
                  </div>
                  <div class="comment-w-box">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          @if(Auth::check())
                          @if(auth()->user()->profile_image)
                          <img src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                          @else
                          <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                          @endif
                          @else
                          <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                          @endif

                        </span>
                      </div>
                      <textarea class="form-control" placeholder="Rašykite naują pranešimą..." name="description" id="livezone-description"></textarea>
                      <pre style="visibility: hidden;" class="default prettyprint prettyprinted"><code><span class="pln" id="output_div"></span></code></pre>

                    </div>
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <span class="pv-i" data-toggle="modal" data-target="#liveZoneVideoModal">
                          <span class="post-v-txt d-none d-md-inline-block">Įkelti <br> vIDEO</span>
                          <span class="post-v-txt-i"> <img src="{{asset('assets/img/post-v.png')}}" alt="img"></span>
                        </span>
                        <span class="pv-i" data-toggle="modal" data-target="#liveZoneImgModal">
                          <span class="post-v-txt d-none d-md-inline-block">ĮkeLTI <br> FOTO</span>
                          <span class="post-v-txt-i"> <img src="{{asset('assets/img/post-c.png')}}" alt="img"></span>
                        </span>
                      </span>
                    </div>
                  </div>
                  <div class="uploaded_image_file">
                    <div class="uploaded_image_file1">

                    </div>
                  </div>
                  <div class="submit-sec">
                    <span class="fel-tag-sec d-none d-md-inline-block mb-3 mb-md-0">
                    </span>
                    <input type="hidden" name="page_identy" id="page_identy" value="1">
                    <span class="post-sub-btn">
                      <span class="post-check">
                        <span><label class="ctm-container">Profile <input type="checkbox" name="show_profile" value="1" checked="checked" class="show_profile"> <span class="checkmark"></span></label></span>

                        <span><label class="ctm-container">Live zona <input type="checkbox" name="show_livezone" value="1" checked="checked" class="show_livezone"> <span class="checkmark"></span></label></span>
                      </span>
                      <button class="float-right float-md-none" type="submit">submit</button>
                    </span>
                  </div>
                </div>
              </form>
              @endif

              {{-- start single livezone --}}
              <div class="add-new-livezone">
                @if(count($data['liveZones']) > 0)
                @foreach ($data['liveZones'] as $livezone)
                <div class="single-content img-v" id="live-zone-number-{{$livezone->id}}">
                  <div class="post-d top-post-cnt">
                    <div class="post-d-c-tlt post-d-tlt ">
                      <div class="com-logo">
                        @if($livezone->user->profile_image)
                        <img src="{{asset('userImage/fixPic/'.$livezone->user->profile_image)}}" alt="img">
                        @else
                        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                        @endif
                      </div>
                      <div class="post-d-txt-all">
                        <span class="post-r">
                          <div class="btn-group">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>...</span></button>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button class="dropdown-item" type="button"><span class="v-f-s" onclick="openLiveeZonePopUp({{$livezone->id}})"> view full screen</span></button>
                            </div>
                          </div>
                        </span>
                        
                        <div class="post-d-txt">{{ $livezone->user->name }}</div>
                        <div class="post-da-r">
                          <span class="post-da">{{ \Carbon\Carbon::parse($livezone->created_at)->diffForHumans() }}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="post-desc d-none d-md-block ev-livezone-des-{{$livezone->id}}">

                    @php
                    $str_len = strlen($livezone->description);
                    $main_str = substr($livezone->description,0,570);
                    $sub_str = substr($livezone->description,571,$str_len);
                    @endphp

                    {!! $main_str !!}

                    @if($str_len > 570)
                    <span class="sub_str_{{$livezone->id}}" id="s_see_more" onclick="showMoreString({{$livezone->id}})">See more..</span>
                    <span class="show_sub_str_{{$livezone->id}}" id="s_show">{!! $sub_str !!}</span>
                    @endif

                  </div>

                  <div class="img_or_video_{{$livezone->id}}">
                    @if($livezone->status == 1)
                    <div class="content-img-v-cnt ">
                      <div class="content-img-p ">
                        <div class="content-img">
                          <img id="lz-img-1" src="{{asset('liveZone/'.$livezone->image)}}" onclick="openLiveeZonePopUp({{$livezone->id}})">
                        </div>
                      </div>
                    </div>
                    @endif
                    @if($livezone->status == 2)
                    <div class="content-img-v-cnt">
                      <div class="content-img-p video ">
                        <div class="content-img">
                          <iframe id="lz-video-1" src="https://www.youtube.com/embed/{{$livezone->image}}">
                          </iframe>
                        </div>
                      </div>
                    </div>
                    @endif
                  </div>

                  <div class="post-desc d-block d-md-none ev-livezone-des-{{$livezone->id}}">

                    @php
                    $str_len = strlen($livezone->description);
                    $main_str = substr($livezone->description,0,570);
                    $sub_str = substr($livezone->description,571,$str_len);
                    @endphp

                    {!! $main_str !!}

                    @if($str_len > 570)
                    <span class="sub_str_{{$livezone->id}}" id="s_see_more" onclick="showMoreString({{$livezone->id}})">See more..</span>
                    <span class="show_sub_str_{{$livezone->id}}" id="s_show">{!! $sub_str !!}</span>
                    @endif

                  </div>

                  <div class="like-com-lz-btn">
                    <span class="like-count mb-2 mb-md-0">
                      <div class="like-reaction">
                        @php
                        $mainLike = App\Http\Controllers\LiveZoneController::firstLikeCount($livezone->id);
                        echo $mainLike['reacts'];
                        @endphp
                        {{-- <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                        <span class="love-i"><i class="fas fa-heart"></i></span> --}}
                      </div>
                      {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img">
                      </span> --}}
                      <span class="n-count total_post_like">{{$mainLike['total']}}</span>
                    </span>
                    <span class="l-c-s d-none d-md-inline-block">
                      <span class="lcs-ul like-li">
                        <span class="lcs-li lcs-like"><span><img src="{{asset('assets/img/like-i.png')}}" alt="img">Like</span>
                        <div class="like-item">
                          <span class="like-itm"><i class="fa fa-thumbs-up" onclick="giveFirstLike({{$livezone->id}},'like')"></i></span>
                          <span class="love-itm"><i class="far fa-heart" onclick="giveFirstLike({{$livezone->id}},'love')"></i></span>
                        </div>
                      </span>
                      <span class="lcs-li com-li" onclick="openComments({{$livezone->id}})"><img src="{{asset('assets/img/comment-i.png')}}" alt="img">Comments</span>
                      <span class="lcs-li"><img src="{{asset('assets/img/share-i.png')}}" alt="img">Share</span>
                    </span>
                  </span>
                  <span class="agn-txt float-right"><span class="comment-c"><span class="put-comment-total-{{$livezone->id}}">
                    @php
                    $total_comment = App\Http\Controllers\LiveZoneController::totalComment($livezone->id);
                    echo $total_comment['total'];
                    @endphp
                  </span> comment</span><span class="share-c">0 Share</span></span>
                </div>
                <div class="like-com-lz-btn d-block d-md-none">
                  <span class="l-c-s">
                    <span class="lcs-ul like-li d-flex border-none text-center">
                      <span class="lcs-li pt-0 pb-0 lcs-like"><span><img src="{{asset('assets/img/like-i.png')}}" alt="img">Like</span>
                      <div class="like-item">
                        <span class="like-itm"><i class="fa fa-thumbs-up" onclick="giveFirstLike({{$livezone->id}},'like')"></i></span>
                        <span class="love-itm"><i class="far fa-heart" onclick="giveFirstLike({{$livezone->id}},'love')"></i></span>
                      </div>
                    </span>
                    <span class="lcs-li pt-0 pb-0 com-li" onclick="openComments({{$livezone->id}})"><img src="{{asset('assets/img/comment-i.png')}}" alt="img">Comments</span>
                    <span class="lcs-li pt-0 pb-0"><img src="{{asset('assets/img/share-i.png')}}" alt="img">Share</span>
                  </span>
                </span>
              </div>

              <div class="comment-w-box">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">

                      @if(Auth::check())
                      @if(auth()->user()->profile_image)
                      <img src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                      @else
                      <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                      @endif
                      @else
                      <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                      @endif


                    </span>
                  </div>
                  <textarea id="comment_box" name="comment" class="form-control first_comment" placeholder="Rašykite naują pranešimą..." livezone-id="{{$livezone->id}}"></textarea>

                </div>
              </div>
              <div class="load-main-comment-{{$livezone->id}}">
                @if(count($livezone->comments) > 0)
                @foreach ($livezone->comments as $comment)
                <div class="post-d">
                  <div class="comment-all" id="comment_load">
                    <div class="post-d-c-tlt post-d-comment ">
                      <div class="com-logo">
                        @if($comment->commenter->profile_image)
                        <img src="{{asset('userImage/fixPic/'.$comment->commenter->profile_image)}}" alt="img">
                        @else
                        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                        @endif
                      </div>
                      <div class="post-d-txt-all">
                        <div class="comment-txt">
                          <div class="post-d-txt"><span class="usr-name">{{$comment->commenter->name}}</span>
                            @php
                            $str_len1 = strlen($comment->comments);
                            $main_str1 = substr($comment->comments,0,200);
                            $sub_str1 = substr($comment->comments,201,$str_len1);
                            @endphp

                            {!! $main_str1 !!}

                            @if($str_len1 > 200)
                            <span class="main_sub_str_{{$comment->id}}" id="s_see_more" onclick="showMoreString3({{$comment->id}})">See more..</span>
                            <span class="main_show_sub_str_{{$comment->id}}" id="s_show">{!! $sub_str1 !!}</span>
                            @endif

                          </div>
                          <div class="post-da-r">
                            <span class="like-count" id="comment-like-react-{{$comment->id}}">
                              @php
                              $commentLike = App\Http\Controllers\LiveZoneController::commentLikeCount($comment->id);
                              echo $commentLike['reacts'];
                              @endphp
                            </span>
                            <span class="post-da reply reply-like">
                              <span> Like</span>
                              <div class="like-item">
                                <span class="like-itm"><i class="fa fa-thumbs-up" onclick="commentLike({{$comment->id}},'like','main')"></i></span>
                                <span class="love-itm"><i class="far fa-heart" onclick="commentLike({{$comment->id}},'love','main')"></i></span>
                              </div>
                            </span>
                            <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                            <span class="post-da">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</span>
                          </div>

                          <div class="comment-w-box reply-w-box hide-w-box">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  @if(Auth::check())
                                  @if(auth()->user()->profile_image)
                                  <img src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                                  @else
                                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                  @endif
                                  @else
                                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                  @endif
                                </span>
                              </div>
                              <textarea class="form-control reply-input main-comment-replay" comment-id="{{$comment->id}}" livezone-id="{{$livezone->id}}" name="reply" placeholder="RaÅ¡ykite komentarÄ…..."></textarea>

                            </div>
                          </div>

                        </div>
                        <div class="load-sub-comment-{{$comment->id}}">
                          @php
                          $total_sub_comment = App\Http\Controllers\LiveZoneController::countSubComment($comment->id);
                          @endphp
                          @if(count($comment->subcomments) > 0)
                          @foreach($comment->subcomments as $sub_comment)
                          <div class="post-d-c-tlt post-d-reply comment-txt">
                            <div class="com-logo">
                              @if($sub_comment->commenter->profile_image)
                              <img src="{{asset('userImage/fixPic/'.$sub_comment->commenter->profile_image)}}" alt="img">
                              @else
                              <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                              @endif
                            </div>
                            <div class="post-d-txt-all">
                              <div class="post-d-txt"><span class="usr-name">{{$sub_comment->commenter->name}}</span>
                                @php
                                $str_len2 = strlen($sub_comment->comments);
                                $main_str2 = substr($sub_comment->comments,0,200);
                                $sub_str2 = substr($sub_comment->comments,201,$str_len2);
                                @endphp

                                {!! $main_str2 !!}

                                @if($str_len2 > 200)
                                <span class="sub_sub_str_{{$sub_comment->id}}" id="s_see_more" onclick="showMoreString4({{$sub_comment->id}})">See more..</span>
                                <span class="sub_show_sub_str_{{$sub_comment->id}}" id="s_show">{!! $sub_str2 !!}</span>
                                @endif

                              </div>
                              <div class="post-da-r">
                                <span class="like-count" id="subcomment-like-react-{{$sub_comment->id}}">
                                  @php
                                  $subCommentLike = App\Http\Controllers\LiveZoneController::subCommentLikeCount($sub_comment->id);
                                  echo $subCommentLike['reacts'];
                                  @endphp
                                </span>

                                <span class="post-da reply reply-like">
                                  <span> Like</span>
                                  <div class="like-item">
                                    <span class="like-itm"><i class="fa fa-thumbs-up" onclick="subCommentLike({{$sub_comment->id}},'like','main')"></i></span>
                                    <span class="love-itm"><i class="far fa-heart" onclick="subCommentLike({{$sub_comment->id}},'love','main')"></i></span>
                                  </div>
                                </span>
                                <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                                <span class="post-da">{{ \Carbon\Carbon::parse($sub_comment->created_at)->diffForHumans() }}</span>
                              </div>
                              <div class="comment-w-box reply-w-box hide-w-box">
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      @if(Auth::check())
                                      @if(auth()->user()->profile_image)
                                      <img src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                                      @else
                                      <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                      @endif
                                      @else
                                      <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                      @endif
                                    </span>
                                  </div>
                                  <textarea class="form-control reply-input reply-sub-comment" comment-id="{{$comment->id}}" livezone-id="{{$livezone->id}}" name="reply" placeholder="RaÅ¡ykite komentarÄ…..."></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                          @endif
                        </div>
                        <div class="see-more-subcomment-div-{{$comment->id}}">
                          @if($total_sub_comment > 1)
                          <span onclick="seeMoreSubComment(event, {{$comment->id}}, 1, {{$total_sub_comment}})"> View {{$total_sub_comment - 1}} more comments</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                @endforeach
                @endif
              </div>
              <div class="see-m-c mt-2 mb-3 see-more-comment-div-{{$livezone->id}}">
                @if($total_comment['main'] > 2)
                <span class="see-more" onclick="seeMoreComments(event,{{$livezone->id}},2,{{$total_comment['main']}})">View {{$total_comment['main'] - 2}} more comments</span>
                @endif
              </div>
            </div>
            @endforeach
            @endif
          </div>
          {{-- end single livezone --}}
        </div>
      </div>
    </div>
  </div>
</div>

<div class="col-12 col-lg-3">
  <div class="live-zona-side-box">
    <div class="top-itm">
      <span class="com-img"><img src="{{asset('assets/img/services/profile-img.png')}}" alt="img"></span>
      <span class="com-txt">Mano profile</span>
      <span class="com-txt float-right"><i class="fa fa-bars"></i></span>
    </div>
    <div class="single-content">
      <div class="content-img-p">
        <div class="content-img">
          <img src="{{asset('assets/img/services/demo.jpg')}}" alt="img">
        </div>
      </div>
    </div>
    <div class="social-icon">
      <ul>
        <li><a class="fb" href="#"><i class="fab fa-facebook-f"></i></a></li>
        <li><a class="lin" href="#"><i class="fab fa-instagram"></i></a></li>
        <li><a class="go" href="#"><i class="fab fa-youtube"></i></a></li>
        <li><a class="tw" href="#"><i class="fab fa-twitter"></i></a></li>
      </ul>
    </div>
  </div>

  @if(count($allVip)>0)
  <div class="side-blog">
    <section class="blog-section">
      <div class="blog-sidbar">
        <div class="side-tlt">
          <div class="side-txt">SKELBIMAI</div>
          <img src="{{asset('assets/img/services/vip-sm.png')}}" alt="img">
        </div>

        <div class="row m-0">
          @foreach ($allVip as $vip)
          @if(property_exists($vip,'title'))
          @php
          $page_title = $vip->title;
          $page_title = preg_replace('!\s+!', ' ', $page_title);
          $page_title = str_replace(' ', '-', $page_title);
          $see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($vip->title, $vip->service_main_id);
          @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                  @if($vip->image)
                  <img src="{{asset('assets/img/services/'.$vip->image)}}" alt="img">
                  @else
                  <img src="{{asset('assets/img/services/no_service.png')}}" alt="img">
                  @endif
                </div>
              </div>
              <div class="content-des">
                <div class="des-itm">
                  <div class="itm-name-all">
                    @php
                    $inners = App\Http\Controllers\SearchServiceController::serviceAndProfileCategory($vip->inner_category,$vip->sub_category_id);
                    @endphp
                    @if(count($inners['category']) > 0)
                    @foreach ($inners['category'] as $inner)
                    <div class="itm-name">{{$inner->name_lt}}</div>
                    @endforeach
                    @endif
                  </div>
                  <div class="itm-dis">
                    <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance, 2)}} km</div>
                    <div class="dis-txt"><span><a href="{{$see_more}}">Daugiau</a></span></div>
                  </div>
                </div>
              </div>
              <div class="content-tlt">
                <a href="{{$see_more}}" onclick="recentlyView('{{$vip->service_main_id}}')">{{ $vip->title }}</a>
              </div>
            </div>
          </div>
          @else
          @php
          $userCats = App\Http\Controllers\SearchServiceController::ProfileCategory($vip->user_main_id);
          @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                  @if($userCats['cover_img'])
                  <img src="{{asset('userImage/coverPic/'.$userCats['cover_img']->img)}}" alt="img">
                  @else
                  <img src="{{asset('assets/img/default_cover.png')}}" alt="img">
                  @endif
                </div>
                <div class="s-pro-img">
                  @if($vip->profile_image)
                  <img src="{{asset('userImage/fixPic/'.$vip->profile_image)}}" alt="img">
                  @else
                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif
                </div>
              </div>
              @php
              $full_name = '';
              if(@$vip->name){
              $full_name .= @$vip->name.' ';
            }
            if(@$vip->surname){
            $full_name .= @$vip->surname.' ';
          }
          if(@$vip->nickname){
          $full_name .= '('.@$vip->nickname.')';
        }
        $full_name = preg_replace('!\s+!', ' ', $full_name);
        $full_name = str_replace(' ', '-', $full_name);

        @endphp
        <div class="content-des">
          <div class="des-itm">
            <div class="itm-name-all">
              @if(count($userCats['cats']) > 0)
              @foreach($userCats['cats'] as $cat)
              <div class="itm-name">{{$cat->name_lt}}</div>
              @endforeach
              @endif

            </div>
            <div class="itm-dis">
              <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance,2)}} km</div>
              <div class="dis-txt"><span><a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">Daugiau</a></span></div>
            </div>
          </div>
        </div>
        <div class="content-tlt">
          <a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">{{ $vip->surname }}</a>
        </div>
      </div>
    </div>

    @endif
    @endforeach
  </div>
</div>
</section>
</div>
@endif

{{-- live zone modal --}}
<form action="{{url('user/add-live-zone')}}" id="live_zone_form" method="POST" enctype="multipart/form-data" onsubmit="return addUpdateLiveZone()">
  @csrf
  <div class="modal fade cr-modal" id="liveZoneModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">@lang('lang.add_live_zone')</h4>
        </div>
        <div class="modal-body" style="width: 100%; overflow: hidden;">
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="name">@lang('lang.title')</label>
              <input type="text" id="live_zone_title" class="form-control" placeholder="Title" aria-label="name" name="live_zone_title" required>
            </div>
          </div>
          <input type="hidden" name="live_zone_status" id="live_zone_status" value="">
          <input type="hidden" name="live_zone_id" id="live_zone_id" value="">
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="bio">@lang('lang.description')</label>
              <textarea id="live_zone_des" class="form-control" placeholder="Live zone description" name="live_zone_des"></textarea>
            </div>
          </div>
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="bio"><a href="javascript:;" class="add_a_img" onclick="imgEmbadeHideShow(1)">@lang('lang.update')</a></label>
              <label for="bio"><a href="javascript:;" class="embade_video" onclick="imgEmbadeHideShow(2)">@lang('lang.embade_a_video')</a></label>
            </div>
          </div>
          <div class="col-12 pad-0" id="link-area">
            <div class="form-group">
              <label for="name">@lang('lang.embade_link')</label>
              <input type="text" id="live_zone_vido_link" class="form-control" placeholder="Embade Video Link" aria-label="name" name="live_zone_vido_link">
            </div>
          </div>
          <div class="col-12 pad-0" id="img-area">
            <div class="form-group">
              <label for="name">@lang('lang.upload_image')</label>
              <input type="file" class="form-control-file" id="live_zone_img" name="live_zone_img">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
          <button type="submit" class="btn" id="c_crop">@lang('lang.save')</button>
        </div>
      </div>
    </div>
  </div>
</form>

{{-- upload live zone image --}}
<form id="uploadLiveZoneImage">
  @csrf
  <div class="modal fade cr-modal" id="liveZoneImgModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Photos</h4>
        </div>
        <div class="modal-body" style="width: 100%;">
          <div class="col-12 pad-0">
            <div class="form-group">
              <div class="alert alert-danger image-error"></div>
              <label for="name">@lang('lang.upload_image')</label>
              <input type="file" class="form-control-file" id="live_zone_img" name="image" required>
            </div>
            <button type="submit" class="btn btn-info">Upload</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
{{-- end upload live zone image --}}

{{-- upload live zone vido --}}
<form id="uploadLiveZoneVideo">
  @csrf
  <div class="modal fade cr-modal" id="liveZoneVideoModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Video</h4>
        </div>
        <div class="modal-body" style="width: 100%;">
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="name">Youtube video link:</label>
              <input type="text" id="live_zone_vido_link" class="form-control" aria-label="name" name="live_zone_vido_link" required>
            </div>
            <button type="submit" class="btn btn-info">Save</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
{{-- end upload live zone video --}}

{{-- Add feling activity --}}
<form id="feelingActivity">
  <div class="modal fade cr-modal" id="liveZoneFellingModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Felling/Activity</h4>
        </div>
        <div class="modal-body" style="width: 100%;">
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="name">Felling/Activity:</label>
              <div class="show-search-input position-relative">
                <input type="text" id="felling_activity" class="form-control fel-input show-search-input" placeholder="Felling/Activity" aria-label="name" name="felling_activity" onkeyup="showTagF(event,1)">
                <div class="show-search-cnt show-fel">
                  <div class="show-search-ul">
                    <div class="show-search-li show-tag-li" onclick="setTagFelText(event,1,'Felling')">
                      <span class="fl-img"><img src="{{asset('assets/img/felling-i.png')}}" alt="img"></span>
                      <span class="li-txt">Felling</span>
                    </div>
                    <div class="show-search-li show-tag-li" onclick="setTagFelText(event,1,'Waching')">
                      <span class="fl-img"><img src="{{asset('assets/img/felling-i.png')}}" alt="img"></span>
                      <span class="li-txt">Waching</span>
                    </div>
                  </div>
                </div>
                <button class="btn btn-info f-avitiy" type="submit">Add</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
{{-- End feling activity --}}
<div class="modal fade cr-modal" id="liveZoneTagModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tag Post</h4>
      </div>
      <div class="modal-body" style="width: 100%;">
        <div class="col-12 pad-0">
          <div class="form-group">
            <label for="name">Freiens Name:</label>
            <div class="show-search-input position-relative">
              <input type="text" onkeyup="showTagF(event,2)" id="live_zone_tag_freind" class="form-control show-search-input" placeholder="Who are you with?" aria-label="name" name="live_zone_tag_freind">
              <div class="show-search-cnt show-tag">
                <div class="show-search-ul">
                  <div class="show-search-li show-tag-li" onclick="setTagFelText(event,2,'Danny')">
                    <span class="fr-img"><img src="{{asset('assets/img/services/demo.jpg')}}" alt="img"></span>
                    <span class="li-txt">Danny</span>
                  </div>
                  <div class="show-search-li show-tag-li" onclick="setTagFelText(event,2,'Manny')">
                    <span class="fr-img"><img src="{{asset('assets/img/services/demo.jpg')}}" alt="img"></span>
                    <span class="li-txt">Manny</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- end live zone modal --}}
{{-- picture pop up --}}
<div class="modal  fade lz-post-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="picture-pop-up-modal" data-backdrop="static" data-keyboard="false">
  <span class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" onclick="closePopUpUrl()">&times;</span>
  </span>
  <input type="hidden" name="p_live_zone_id" id="p_live_zone_id">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content put-live-zone-popup">

    </div>
  </div>
</div>
{{-- end picture pop up --}}
</div>
</div>
</section>



@endsection
@section('script')
<script src="{{ asset('assets/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('assets/js/cropper.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>
<script>
  $(function() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function(position) {
        lat = position.coords.latitude;
        lng = position.coords.longitude;
        getServiceAndProfile(lat, lng);
      }, function() {

      });
    }
  });

  function getServiceAndProfile(lat, lng) {
    var data = new FormData();
    data.append('lat', lat);
    data.append('lng', lng);
    data.append('_token', "{{csrf_token()}}");
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("get-profile-and-service")}}',
      success: function(response) {
        $('#vip-profile').html(response.profiles);
        $('#vip-services').html(response.services);
      }

    });
  }
  window.gallery_up = '<?= route('user.galleryFileUp') ?>';
  window.gallery_remove = '<?= route('user.galleryRemover') ?>';
  window.cover_pic_remove = '<?= route('user.userCoverRemover') ?>';
  window.profile_single_tmp_img = '<?= url("user/process-single-tmp-image") ?>';
  window.profile_single_tmp_remove = '<?= url("user/single-tmp-img-remove") ?>';
  window.cover_img_upload = '<?= url("user/cover-image-upload") ?>';
  window.get_profile_info = '<?= url("user/get-profile-information") ?>';
  window.insert_profile_info = '<?= url("user/insert-profile-information") ?>';


  $(function() {

    $('[data-fancybox="gallery"]').fancybox({
      thumbs: {
        autoStart: true
      }
    });

    $('.com-li').click(function() {
      $('#comment_box').focus();
      // console.log(555);
    });
    $('.fel-m-s').click(function() {
      $('.fel-input').focus();
      $('.show-fel').show();
      // console.log(555);
    });

  });

  function openLiveZone1() {
    $('#live_zone_status').val('');
    $('#live_zone_title').val('');
    $('#live_zone_des').val('');
    $('#live_zone_id').val('');
    $('#live_zone_vido_link').val('');
    $('#live_zone_img').val('');
    $('#live_zone_form').attr('action', "{{url('user/add-live-zone')}}");
    $('#link-area').hide();
    $('#img-area').hide();
    $('#live_zone_status').val('');
    $('#liveZoneModal').modal('show');

  }
</script>
<script>
  function imgEmbadeHideShow(number) {
    if (number == 1) {
      $('#link-area').hide();
      $('#img-area').show();
      $('#live_zone_status').val(1);
    } else if (number == 2) {
      $('#link-area').show();
      $('#img-area').hide();
      $('#live_zone_status').val(2);
    }
  }
</script>


{{-- get more live zone --}}
<script>
  var page = 1;
  $(window).scroll(function() {
    // var scroll_height = $(window).scrollTop();
    // var window_height = $(window).height();
    // var document_height = $(document).height();
    // var scroll_height1 = scroll_height+window_height;
    // console.log('Scroll height='+scroll_height1);
    // console.log('Total Height='+document_height);
    // return false;
    if ($(window).scrollTop() + $(window).height() >= $(document).height() - 700) {
      page++;
      console.log('rasel');
      loadMoreData(page);
    }
  });

  function loadMoreData(page) {
    $.ajax({
      url: '?page=' + page,
      type: "get",
      beforeSend: function() {
          // $('.ajax-load').show();
        }
      })
    .done(function(data) {

      if (data.html == " ") {
        $('.ajax-load').html("No more records found");
        return;
      }
        // $('.ajax-load').hide();
        $(".add-new-livezone").append(data.html);
      })
    .fail(function(jqXHR, ajaxOptions, thrownError) {

    });
  }
</script>

@include('pages.comments.live-zone-script')
@include('pages.comments.live-zone-popup-script')

@endsection