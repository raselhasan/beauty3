
                <div class="single-content img-v" id="live-zone-number-{{$livezone->id}}">
                  <div class="post-d top-post-cnt">
                   <div class="post-d-c-tlt post-d-tlt ">
                    <div class="com-logo">
                      @if(Auth::check())
                      @if(auth()->user()->profile_image)
                      <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                      @else 
                        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                      @endif
                      @else 
                        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                      @endif
                    </div>
                    <div class="post-d-txt-all">
                     {{-- <span class="post-r">
                       @if(@$edit == 1)
                      <span class="v-f-s" onclick="openLiveeZonePopUp({{$livezone->id}})">view full screen</span>
                      <span class="l-edit" onclick="openLiveZone({{$livezone->id}})">Edit</span>
                      <span class="l-delete" onclick="deleteLiveZone({{$livezone->id}})">Delete</span>
                      @else 
                      <span class="v-f-s" onclick="openLiveeZonePopUp({{$livezone->id}})">view full screen</span>
                      @endif

                     </span> --}}

                     <span class="post-r">
                          <div class="btn-group">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>...</span></button>
                            <div class="dropdown-menu dropdown-menu-right">
                              <button class="dropdown-item" type="button"><span class="v-f-s" onclick="openLiveeZonePopUp({{$livezone->id}})"> view full screen</span></button>
                              @if(@$edit == 1)
                                 <button class="dropdown-item" type="button"><span class="l-edit" onclick="openLiveZone({{$livezone->id}})"> Edit</span></button>
                                 <button class="dropdown-item" type="button"><span class="l-delete" onclick="deleteLiveZone({{$livezone->id}})"> Delete</span></button>

                              @endif
                            </div>
                          </div>
                        </span> 


                     <div class="post-d-txt">{{ auth()->user()->name}}</div>
                     <div class="post-da-r">
                       <span class="post-da">{{ \Carbon\Carbon::parse($livezone->created_at)->diffForHumans() }}</span>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="post-desc ev-livezone-des-{{$livezone->id}}">
                  @php
                      $str_len = strlen($livezone->description);
                      $main_str = substr($livezone->description,0,570);
                      $sub_str = substr($livezone->description,571,$str_len);
                    @endphp

                    {!! $main_str  !!}

                    @if($str_len > 570)
                      <span class="sub_str_{{$livezone->id}}" id="s_see_more" onclick="showMoreString({{$livezone->id}})">See more..</span>
                      <span class="show_sub_str_{{$livezone->id}}" id="s_show">{!! $sub_str !!}</span> 
                    @endif
               </div>
               <div class="img_or_video_{{$livezone->id}}"> 
               @if($livezone->status == 1) 
               <div class="content-img-v-cnt ">
                 <div class="content-img-p ">
                  <div class="content-img">
                    <img id="lz-img-1" src="{{asset('liveZone/'.$livezone->image)}}">
                  </div>
                </div>
              </div>
              @endif
              @if($livezone->status == 2)
              <div class="content-img-v-cnt">
                <div class="content-img-p video ">
                  <div class="content-img">
                    <iframe id="lz-video-1" src="https://www.youtube.com/embed/{{$livezone->image}}">
                    </iframe>
                  </div>
                </div>
              </div>
              @endif
              </div>

              <div class="like-com-lz-btn">
                <span class="like-count mb-2 mb-md-0">
                  <div class="like-reaction">
                   {{--  @php
                      $mainLike = App\Http\Controllers\LiveZoneController::firstLikeCount($livezone->id);
                      echo $mainLike['reacts'];
                    @endphp --}}
                    {{-- <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                    <span class="love-i"><i class="fas fa-heart"></i></span> --}}
                  </div>
                  {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
                  <span class="n-count total_post_like"></span>
                </span>
                <span class="l-c-s mb-2 mb-md-0">
                  <span class="lcs-ul like-li">
                    <span class="lcs-li lcs-like"><span><img  src="{{asset('assets/img/like-i.png')}}" alt="img">Like</span> 
                    <div class="like-item">
                      <span class="like-itm"><i class="fa fa-thumbs-up" onclick="giveFirstLike({{$livezone->id}},'like')"></i></span>
                      <span class="love-itm"><i class="far fa-heart" onclick="giveFirstLike({{$livezone->id}},'love')"></i></span>
                    </div>
                  </span>
                  <span class="lcs-li com-li" onclick="openComments({{$livezone->id}})"><img  src="{{asset('assets/img/comment-i.png')}}" alt="img">Comments</span>
                  <span class="lcs-li"><img  src="{{asset('assets/img/share-i.png')}}" alt="img">Share</span>
                </span>
              </span>
              <span class="agn-txt float-none float-md-right"><span class="comment-c"><span class="put-comment-total-{{$livezone->id}}">0</span> comment</span><span class="share-c">0 Share</span></span>
            </div>

            <div class="comment-w-box">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
      
                 @if(Auth::check())
                    @if(auth()->user()->profile_image)
                      <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                    @else 
                      <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                    @endif
                  @else 
                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif

                  </span>
                </div>
                <textarea id="comment_box" name="comment" class="form-control first_comment" placeholder="Rašykite komentarą..." livezone-id="{{$livezone->id}}"></textarea>

              </div>
              {{-- <div class="input-group-append">
                <span class="input-group-text">
                  <span class="pv-i"><i class="fas fa-smile"></i></span>
                  <span class="pv-i"><i class="fas fa-camera"></i></span>
                  <span class="pv-i"><i class="fas fa-video"></i></span>
                  <button class="sent-txt">Pateikti</button>
                </span>
              </div> --}}
            </div>
            <div class="load-main-comment-{{$livezone->id}}">
           
      </div>

    </div>
