 @if(count($subcomments) > 0)
   @foreach($subcomments as $sub_comment)
    <div class="post-d-c-tlt post-d-reply comment-txt">
      <div class="com-logo">
      @if($sub_comment->commenter->profile_image)
      <img  src="{{asset('userImage/fixPic/'.$sub_comment->commenter->profile_image)}}" alt="img">
      @else 
        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
      @endif

      </div>
      <div class="post-d-txt-all">
        <div class="post-d-txt"><span class="usr-name">{{$sub_comment->commenter->name}}</span> {{ $sub_comment->comments}}
          @php
            $str_len2 = strlen($sub_comment->comments);
            $main_str2 = substr($sub_comment->comments,0,200);
            $sub_str2 = substr($sub_comment->comments,201,$str_len2);
          @endphp

          {!! $main_str2  !!}

          @if($str_len2 > 200)
            <span class="sub_sub_str_{{$sub_comment->id}}" id="s_see_more" onclick="showMoreString4({{$sub_comment->id}})">See more..</span>
            <span class="sub_show_sub_str_{{$sub_comment->id}}" id="s_show">{!! $sub_str2 !!}</span> 
          @endif
        </div>
        <div class="post-da-r">
         <span class="like-count" id="subcomment-like-react-{{$sub_comment->id}}">
          @php
            $subCommentLike = App\Http\Controllers\LiveZoneController::subCommentLikeCount($sub_comment->id);
            echo $subCommentLike['reacts'];
          @endphp
           {{--  <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
            <span class="love-i"><i class="fas fa-heart"></i></span> --}}

            {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
            {{-- <span class="n-count">1</span> --}}
          </span>

          <span class="post-da reply reply-like">
           <span> Like</span>
           <div class="like-item">
            <span class="like-itm"><i class="fa fa-thumbs-up" onclick="subCommentLike({{$sub_comment->id}},'like')"></i></span>
            <span class="love-itm"><i class="far fa-heart" onclick="subCommentLike({{$sub_comment->id}},'love')"></i></span>
          </div>
        </span>
        <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
        <span class="post-da">{{ \Carbon\Carbon::parse($sub_comment->created_at)->diffForHumans() }}</span>
      </div>
      <div class="comment-w-box reply-w-box hide-w-box">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              @if(Auth::check())
                @if(auth()->user()->profile_image)
                    <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                @else 
                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                @endif
              @else 
                <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
              @endif
            </span>
          </div>
          <textarea class="form-control reply-input reply-sub-comment" comment-id="{{$sub_comment->comment_id}}" livezone-id="{{$sub_comment->livezone_id}}"  name="reply" placeholder="Rašykite komentarą..."></textarea>
        </div>
        {{-- <div class="input-group-append">
          <span class="input-group-text">
            <span class="pv-i"><i class="fas fa-smile"></i></span>
            <span class="pv-i"><i class="fas fa-camera"></i></span>
            <span class="pv-i"><i class="fas fa-video"></i></span>
            <button class="sent-txt">Pateikti</button>
          </span>
        </div> --}}
      </div>
    </div>
  </div>
    @endforeach
  @endif