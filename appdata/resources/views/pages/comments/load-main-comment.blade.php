 <div class="post-d">
          <div class="comment-all" id="comment_load">
           <div class="post-d-c-tlt post-d-comment ">
            <div class="com-logo">

            @if(Auth::check())
              @if(auth()->user()->profile_image)
                <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
              @else 
                <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
              @endif
            @else 
              <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
            @endif

            </div>
            <div class="post-d-txt-all">
              <div class="comment-txt">
                <div class="post-d-txt"><span class="usr-name">{{auth()->user()->name}}</span> 
                  @php
                    $str_len1 = strlen($new_comment->comments);
                    $main_str1 = substr($new_comment->comments,0,200);
                    $sub_str1 = substr($new_comment->comments,201,$str_len1);
                  @endphp

                  {!! $main_str1  !!}

                  @if($str_len1 > 200)
                    <span class="main_sub_str_{{$new_comment->id}}" id="s_see_more" onclick="showMoreString3({{$new_comment->id}})">See more..</span>
                    <span class="main_show_sub_str_{{$new_comment->id}}" id="s_show">{!! $sub_str1 !!}</span> 
                  @endif

                </div>
                <div class="post-da-r">
                  <span class="like-count" id="comment-like-react-{{$new_comment->id}}">
                   
                  </span>
                  <span class="post-da reply reply-like">
                   <span> Like</span>
                   <div class="like-item">
                    <span class="like-itm"><i class="fa fa-thumbs-up" onclick="commentLike({{$new_comment->id}},'like')"></i></span>
                    <span class="love-itm"><i class="far fa-heart" onclick="commentLike({{$new_comment->id}},'love')"></i></span>
                  </div>
                </span>
                <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                <span class="post-da">{{ \Carbon\Carbon::parse($new_comment->created_at)->diffForHumans() }}</span>
              </div>

              <div class="comment-w-box reply-w-box hide-w-box">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">

                   @if(Auth::check())
                      @if(auth()->user()->profile_image)
                          <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                      @else 
                          <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                      @endif
                    @else 
                      <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                    @endif

                    </span>
                  </div>
                   <textarea class="form-control reply-input  main-comment-replay" comment-id="{{$new_comment->id}}" livezone-id="{{$new_comment->livezone_id}}" name="reply" placeholder="Rašykite komentarą..."></textarea>
                  
                </div>
                {{-- <div class="input-group-append">
                  <span class="input-group-text">
                    <span class="pv-i"><i class="fas fa-smile"></i></span>
                    <span class="pv-i"><i class="fas fa-camera"></i></span>
                    <span class="pv-i"><i class="fas fa-video"></i></span>
                    <button class="sent-txt">Pateikti</button>
                  </span>
                </div> --}}
              </div>

            </div>
            <div class="load-sub-comment-{{$new_comment->id}}">
            
          </div>



        </div>
      </div>
    </div>
   {{--  <div class="see-m-c mt-2">
      <span class="see-more" onclick="seeMoreComments(event)">Rodyti daugiau komentarų</span>
    </div> --}}
  </div>