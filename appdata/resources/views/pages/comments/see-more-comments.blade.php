@if(count($comments) > 0)
  @foreach ($comments as $comment)
                <div class="post-d">
              <div class="comment-all" id="comment_load">
               <div class="post-d-c-tlt post-d-comment ">
                <div class="com-logo">
                @if($comment->commenter->profile_image)
                <img  src="{{asset('userImage/fixPic/'.$comment->commenter->profile_image)}}" alt="img">
                @else 
                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                @endif

                </div>
                <div class="post-d-txt-all">
                  <div class="comment-txt">
                    <div class="post-d-txt"><span class="usr-name">{{$comment->commenter->name}}</span> 
                      @php
                        $str_len1 = strlen($comment->comments);
                        $main_str1 = substr($comment->comments,0,200);
                        $sub_str1 = substr($comment->comments,201,$str_len1);
                      @endphp

                      {!! $main_str1  !!}

                      @if($str_len1 > 200)
                        <span class="main_sub_str_{{$comment->id}}" id="s_see_more" onclick="showMoreString3({{$comment->id}})">See more..</span>
                        <span class="main_show_sub_str_{{$comment->id}}" id="s_show">{!! $sub_str1 !!}</span> 
                      @endif

                    </div>
                    <div class="post-da-r">
                      <span class="like-count" id="comment-like-react-{{$comment->id}}">
                      @php
                        $commentLike = App\Http\Controllers\LiveZoneController::commentLikeCount($comment->id);
                        echo $commentLike['reacts'];
                      @endphp
                       {{--  <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                        <span class="love-i"><i class="fas fa-heart"></i></span> --}}

                        {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
                        {{-- <span class="n-count">1</span> --}}
                      </span>
                      <span class="post-da reply reply-like">
                       <span> Like</span>
                       <div class="like-item">
                        <span class="like-itm"><i class="fa fa-thumbs-up" onclick="commentLike({{$comment->id}},'like')"></i></span>
                        <span class="love-itm"><i class="far fa-heart" onclick="commentLike({{$comment->id}},'love')"></i></span>
                      </div>
                    </span>
                    <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                    <span class="post-da">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</span>
                  </div>

                  <div class="comment-w-box reply-w-box hide-w-box">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          @if(Auth::check())
                            @if(auth()->user()->profile_image)
                                <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                            @else 
                                <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                            @endif
                          @else 
                            <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                          @endif
                        </span>
                      </div>
                       <textarea class="form-control reply-input main-comment-replay" comment-id="{{$comment->id}}" livezone-id="{{$comment->livezone_id}}" name="reply" placeholder="Rašykite komentarą..."></textarea>
                      
                    </div>
                    {{-- <div class="input-group-append">
                      <span class="input-group-text">
                        <span class="pv-i"><i class="fas fa-smile"></i></span>
                        <span class="pv-i"><i class="fas fa-camera"></i></span>
                        <span class="pv-i"><i class="fas fa-video"></i></span>
                        <button class="sent-txt">Pateikti</button>
                      </span>
                    </div> --}}
                  </div>

                </div>
                @php
                  $total_sub_comment = App\Http\Controllers\LiveZoneController::countSubComment($comment->id);
                @endphp
                <div class="load-sub-comment-{{$comment->id}}">
                @if(count($comment->subcomments) > 0)
                @foreach($comment->subcomments as $sub_comment)
                <div class="post-d-c-tlt post-d-reply comment-txt">
                  <div class="com-logo">
                  @if($sub_comment->commenter->profile_image)
                  <img  src="{{asset('userImage/fixPic/'.$sub_comment->commenter->profile_image)}}" alt="img">
                  @else 
                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif

                  </div>
                  <div class="post-d-txt-all">
                    <div class="post-d-txt"><span class="usr-name">{{$sub_comment->commenter->name}}</span> 
                      @php
                        $str_len2 = strlen($sub_comment->comments);
                        $main_str2 = substr($sub_comment->comments,0,200);
                        $sub_str2 = substr($sub_comment->comments,201,$str_len2);
                      @endphp

                      {!! $main_str2  !!}

                      @if($str_len2 > 200)
                        <span class="sub_sub_str_{{$sub_comment->id}}" id="s_see_more" onclick="showMoreString4({{$sub_comment->id}})">See more..</span>
                        <span class="sub_show_sub_str_{{$sub_comment->id}}" id="s_show">{!! $sub_str2 !!}</span> 
                      @endif
                    </div>
                    <div class="post-da-r">
                     <span class="like-count" id="subcomment-like-react-{{$sub_comment->id}}">
                      @php
                        $subCommentLike = App\Http\Controllers\LiveZoneController::subCommentLikeCount($sub_comment->id);
                        echo $subCommentLike['reacts'];
                      @endphp
                       {{--  <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                        <span class="love-i"><i class="fas fa-heart"></i></span> --}}

                        {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
                        {{-- <span class="n-count">1</span> --}}
                      </span>

                      <span class="post-da reply reply-like">
                       <span> Like</span>
                       <div class="like-item">
                        <span class="like-itm"><i class="fa fa-thumbs-up" onclick="subCommentLike({{$sub_comment->id}},'like')"></i></span>
                        <span class="love-itm"><i class="far fa-heart" onclick="subCommentLike({{$sub_comment->id}},'love')"></i></span>
                      </div>
                    </span>
                    <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                    <span class="post-da">{{ \Carbon\Carbon::parse($sub_comment->created_at)->diffForHumans() }}</span>
                  </div>
                  <div class="comment-w-box reply-w-box hide-w-box">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                         @if(Auth::check())
                            @if(auth()->user()->profile_image)
                                <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                            @else 
                                <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                            @endif
                          @else 
                            <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                          @endif
                        </span>
                      </div>
                      <textarea class="form-control reply-input reply-sub-comment" comment-id="{{$comment->id}}" livezone-id="{{$comment->livezone_id}}"  name="reply" placeholder="Rašykite komentarą..."></textarea>
                    </div>
                    {{-- <div class="input-group-append">
                      <span class="input-group-text">
                        <span class="pv-i"><i class="fas fa-smile"></i></span>
                        <span class="pv-i"><i class="fas fa-camera"></i></span>
                        <span class="pv-i"><i class="fas fa-video"></i></span>
                        <button class="sent-txt">Pateikti</button>
                      </span>
                    </div> --}}
                  </div>
                </div>
              </div>
              @endforeach
              @endif
              
              </div>
              <div class="see-more-subcomment-div-{{$comment->id}}">
                @if($total_sub_comment > 1)
                <span onclick="seeMoreSubComment(event, {{$comment->id}}, 1, {{$total_sub_comment}})"> View {{$total_sub_comment - 1}} more comments</span>
                @endif
              </div>



            </div>
          </div>
        </div>
       
      </div>
  @endforeach
@endif