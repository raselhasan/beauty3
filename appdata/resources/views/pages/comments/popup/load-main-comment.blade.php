<div class="post-d-c-tlt post-d-comment ">
  <div class="com-logo">

  @if(Auth::check())
    @if(auth()->user()->profile_image)
        <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
    @else 
        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
    @endif
  @else 
    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
  @endif
  </div>
  <div class="post-d-txt-all">
    <div class="comment-txt">
      <div class="usr-name">{{auth()->user()->name}}</div>
      <div class="post-d-txt">{{ $new_comment->comments}}
        @php
          $str_len1 = strlen($new_comment->comments);
          $main_str1 = substr($new_comment->comments,0,200);
          $sub_str1 = substr($new_comment->comments,201,$str_len1);
        @endphp

        {!! $main_str1  !!}

        @if($str_len1 > 200)
          <span class="main_sub_str2_{{$new_comment->id}}" id="s_see_more" onclick="showMoreString5({{$new_comment->id}})">See more..</span>
          <span class="main_show_sub_str2_{{$new_comment->id}}" id="s_show">{!! $sub_str1 !!}</span> 
        @endif

      </div>
      <div class="post-da-r">
        <span id="comment-like-react1-{{$new_comment->id}}" class="like-count post-da">
          @php
            $commentLike = App\Http\Controllers\LiveZoneController::commentLikeCount($new_comment->id);
            echo $commentLike['reacts'];
          @endphp
        </span>
        <span class="post-da reply reply-like">
          <span>Like</span>
          <div class="like-item"><span class="like-itm"><i onclick="commentLike({{$new_comment->id}},'like','popup')" class="fa fa-thumbs-up"></i></span> <span class="love-itm"><i onclick="commentLike({{$new_comment->id}},'love','popup')" class="far fa-heart"></i></span></div>
        </span>
        <span onclick="replyBoxShow(event)" class="post-da reply-box-s reply">Reply</span>
        <span class="post-da">{{ \Carbon\Carbon::parse($new_comment->created_at)->diffForHumans() }}</span>
      </div>
      <div class="comment-w-box reply-w-box hide-w-box">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
            @if(Auth::check())
              @if(auth()->user()->profile_image)
                  <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
              @else 
                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
              @endif
            @else 
              <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
            @endif

            </span>
          </div>
          <textarea comment-id="{{$new_comment->id}}" livezone-id="{{$new_comment->livezone_id}}" name="reply" placeholder="Rašykite komentarą..." class="form-control reply-input main-comment-replay1"></textarea>
        </div>
      </div>
    </div>
    @php
      $total_sub_comment = App\Http\Controllers\LiveZoneController::countSubComment($new_comment->id);
    @endphp
    <div class="load-sub-commnet-div-put-{{$new_comment->id}}">

    </div>
  </div>
</div>