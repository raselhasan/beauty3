
<div class="post-d-c-tlt post-d-reply ">
  <div class="com-logo">

@if(Auth::check())
  @if(auth()->user()->profile_image)
      <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
  @else 
      <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
  @endif
@else 
  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
@endif

  </div>
  <div class="post-d-txt-all">
    <div class="usr-name">{{auth()->user()->name}}</div>
    <div class="post-d-txt">{{ $sub_comment->comments}}
       @php
          $str_len2 = strlen($sub_comment->comments);
          $main_str2 = substr($sub_comment->comments,0,200);
          $sub_str2 = substr($sub_comment->comments,201,$str_len2);
        @endphp

        {!! $main_str2  !!}

        @if($str_len2 > 200)
          <span class="sub_sub_str3_{{$sub_comment->id}}" id="s_see_more" onclick="showMoreString6({{$sub_comment->id}})">See more..</span>
          <span class="sub_show_sub_str3_{{$sub_comment->id}}" id="s_show">{!! $sub_str2 !!}</span> 
        @endif
    </div>
    <div class="post-da-r">
      <span id="subcomment-like-react1-{{$sub_comment->id}}" class="like-count post-da">
        @php
          $subCommentLike = App\Http\Controllers\LiveZoneController::subCommentLikeCount($sub_comment->id);
          echo $subCommentLike['reacts'];
        @endphp
      </span>
      <span class="post-da reply reply-like">
        <span>Like</span>
        <div class="like-item"><span class="like-itm"><i onclick="subCommentLike({{$sub_comment->id}},'like','popup')" class="fa fa-thumbs-up"></i></span> <span class="love-itm"><i onclick="subCommentLike({{$sub_comment->id}},'love','popup')" class="far fa-heart"></i></span></div>
      </span>
      <span onclick="replyBoxShow(event)" class="post-da reply-box-s reply">Reply</span>
      <span class="post-da">{{ \Carbon\Carbon::parse($sub_comment->created_at)->diffForHumans() }}</span>
    </div>
    <div class="comment-w-box reply-w-box hide-w-box">
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text">
            
            @if(Auth::check())
              @if(auth()->user()->profile_image)
                  <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
              @else 
                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
              @endif
            @else 
              <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
            @endif
          </span>
        </div>
        <textarea comment-id="{{$sub_comment->comment_id}}" livezone-id="{{$sub_comment->livezone_id}}" name="reply" placeholder="Rašykite komentarą..." class="form-control reply-input reply-sub-comment1"></textarea>
      </div>
    </div>
  </div>
</div>
