@foreach ($data['liveZones'] as $livezone)
  <div class="about-section userPro-section live-zona-section p-0">
    <div class="live-zona m-0">
      <div class="live-zona-cnt mylz-content m-0">
        <div class="row mar-0">
          <div class="col-12 col-md-7 col-lg-8 p-0">
            <div class="single-content img-v mb-0" id="del-live-zone-1">
              @if($livezone->status == 1) 
                <div class="content-img-p ">
                  <div class="content-img">
                    <img id="lz-img-1" src="{{asset('liveZone/'.$livezone->image)}}">
                  </div>
                </div>
              @endif
              @if($livezone->status == 2)
                <div class="content-img-v-cnt">
                  <div class="content-img-p video ">
                    <div class="content-img">
                      <iframe id="lz-video-1" src="https://www.youtube.com/embed/{{$livezone->image}}">
                      </iframe>
                    </div>
                  </div>
                </div>
              @endif
              <div class="like-com-btn">
                <div class="like-count like-reaction1">
                  @php
                      $mainLike = App\Http\Controllers\LiveZoneController::firstLikeCount($livezone->id);
                      echo $mainLike['reacts'];

                  @endphp
                  <span class="n-count total_post_like1">{{$mainLike['total']}}</span>
                </div>
                <div class="l-c-s">
                  <span class="lcs-ul like-li">
                    <span class="lcs-li lcs-like"> <span>Like</span>
                    <div class="like-item"><span class="like-itm"><i onclick="giveFirstLike({{$livezone->id}},'like','popup')" class="fa fa-thumbs-up"></i></span> <span class="love-itm"><i onclick="giveFirstLike({{$livezone->id}},'love','popup')" class="far fa-heart"></i></span></div>
                  </span>
                  @php
                  $total_comment = App\Http\Controllers\LiveZoneController::totalComment($livezone->id);
                  @endphp
                  <span class="lcs-li put-comment-total1-{{$livezone->id}}">{{$total_comment['total']}}</span>
                  <span class="lcs-li">Share</span>
                </span>
              </div>
              {{-- <div class="agn-txt">prieš 2 d.</div> --}}
            </div>
          </div>
        </div>
        <div class="col-12 col-md-5 col-lg-4 p-0">
          <div class="post-d ">
            <div class="post-d-comment-all">
              <div class="post-d-c-tlt post-d-tlt ">
                <div class="com-logo">
                @if($livezone->user->profile_image)
                <img  src="{{asset('userImage/fixPic/'.$livezone->user->profile_image)}}" alt="img">
                @else 
                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                @endif
                </div>
                <div class="post-d-txt-all">
                  <div class="post-d-txt">{{$livezone->user->name}}</div>
                  <div class="post-da-r w-100 d-block">
                    <span class="post-da">{{ \Carbon\Carbon::parse($livezone->created_at)->diffForHumans() }}</span>
                    <span class="post-r">...</span>
                  </div>
                  <div class="popup-des">
                    @php
                      $str_len = strlen($livezone->description);
                      $main_str = substr($livezone->description,0,200);
                      $sub_str = substr($livezone->description,201,$str_len);
                    @endphp

                    {!! $main_str  !!}

                    @if($str_len > 200)
                      <span class="sub_str1_{{$livezone->id}}" id="s_see_more" onclick="showMoreString1({{$livezone->id}})">See more..</span>
                      <span class="show_sub_str1_{{$livezone->id}}" id="s_show">{!! $sub_str !!}</span> 
                    @endif
                  </div>
                </div>
              </div>
              <div class="single-comment-modal">
                <div class="load-main-commnet-div-put">
                @if(count($livezone->comments)>0)
                  @foreach ($livezone->comments as $comment)
                    <div class="post-d-c-tlt post-d-comment ">
                      <div class="com-logo">
                      @if($comment->commenter->profile_image)
                      <img  src="{{asset('userImage/fixPic/'.$comment->commenter->profile_image)}}" alt="img">
                      @else 
                        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                      @endif

                      </div>
                      <div class="post-d-txt-all">
                        <div class="comment-txt">
                          <div class="usr-name">{{$comment->commenter->name}}</div>
                          <div class="post-d-txt">
                            @php
                              $str_len1 = strlen($comment->comments);
                              $main_str1 = substr($comment->comments,0,200);
                              $sub_str1 = substr($comment->comments,201,$str_len1);
                            @endphp

                            {!! $main_str1  !!}

                            @if($str_len1 > 200)
                              <span class="main_sub_str2_{{$comment->id}}" id="s_see_more" onclick="showMoreString5({{$comment->id}})">See more..</span>
                              <span class="main_show_sub_str2_{{$comment->id}}" id="s_show">{!! $sub_str1 !!}</span> 
                            @endif

                          </div>
                          <div class="post-da-r">
                            <span id="comment-like-react1-{{$comment->id}}" class="like-count post-da">
                              @php
                                $commentLike = App\Http\Controllers\LiveZoneController::commentLikeCount($comment->id);
                                echo $commentLike['reacts'];
                              @endphp
                            </span>
                            <span class="post-da reply reply-like">
                              <span>Like</span>
                              <div class="like-item"><span class="like-itm"><i onclick="commentLike({{$comment->id}},'like','popup')" class="fa fa-thumbs-up"></i></span> <span class="love-itm"><i onclick="commentLike({{$comment->id}},'love','popup')" class="far fa-heart"></i></span></div>
                            </span>
                            <span onclick="replyBoxShow(event)" class="post-da reply-box-s reply">Reply</span>
                            <span class="post-da">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</span>
                          </div>
                          <div class="comment-w-box reply-w-box hide-w-box">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">
                                  
                                  @if(Auth::check())
                                    @if(auth()->user()->profile_image)
                                        <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                                    @else 
                                        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                    @endif
                                  @else 
                                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                  @endif
                                </span>
                              </div>
                              <textarea comment-id="{{$comment->id}}" livezone-id="{{$comment->livezone_id}}" name="reply" placeholder="Rašykite komentarą..." class="form-control reply-input main-comment-replay1"></textarea>
                            </div>
                          </div>
                        </div>
                        @php
                          $total_sub_comment = App\Http\Controllers\LiveZoneController::countSubComment($comment->id);
                        @endphp
                        <div class="load-sub-commnet-div-put-{{$comment->id}}">
                        @if(count($comment->subcomments) > 0)
                          @foreach($comment->subcomments as $sub_comment)
                            <div class="post-d-c-tlt post-d-reply ">
                              <div class="com-logo">
                              @if($sub_comment->commenter->profile_image)
                              <img  src="{{asset('userImage/fixPic/'.$sub_comment->commenter->profile_image)}}" alt="img">
                              @else 
                                <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                              @endif
                              </div>
                              <div class="post-d-txt-all">
                                <div class="usr-name">{{$sub_comment->commenter->name}}</div>
                                <div class="post-d-txt">
                                  @php
                                    $str_len2 = strlen($sub_comment->comments);
                                    $main_str2 = substr($sub_comment->comments,0,200);
                                    $sub_str2 = substr($sub_comment->comments,201,$str_len2);
                                  @endphp

                                  {!! $main_str2  !!}

                                  @if($str_len2 > 200)
                                    <span class="sub_sub_str3_{{$sub_comment->id}}" id="s_see_more" onclick="showMoreString6({{$sub_comment->id}})">See more..</span>
                                    <span class="sub_show_sub_str3_{{$sub_comment->id}}" id="s_show">{!! $sub_str2 !!}</span> 
                                  @endif
                                </div>
                                <div class="post-da-r">
                                  <span id="subcomment-like-react1-{{$sub_comment->id}}" class="like-count post-da">
                                    @php
                                      $subCommentLike = App\Http\Controllers\LiveZoneController::subCommentLikeCount($sub_comment->id);
                                      echo $subCommentLike['reacts'];
                                    @endphp
                                  </span>
                                  <span class="post-da reply reply-like">
                                    <span>Like</span>
                                    <div class="like-item"><span class="like-itm"><i onclick="subCommentLike({{$sub_comment->id}},'like','popup')" class="fa fa-thumbs-up"></i></span> <span class="love-itm"><i onclick="subCommentLike({{$sub_comment->id}},'love','popup')" class="far fa-heart"></i></span></div>
                                  </span>
                                  <span onclick="replyBoxShow(event)" class="post-da reply-box-s reply">Reply</span>
                                  <span class="post-da">{{ \Carbon\Carbon::parse($sub_comment->created_at)->diffForHumans() }}</span>
                                </div>
                                <div class="comment-w-box reply-w-box hide-w-box">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        @if(Auth::check())
                                          @if(auth()->user()->profile_image)
                                              <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                                          @else 
                                              <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                          @endif
                                        @else 
                                          <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                                        @endif
                                      </span>
                                    </div>
                                    <textarea comment-id="{{$comment->id}}" livezone-id="{{$livezone->id}}" name="reply" placeholder="Rašykite komentarą..." class="form-control reply-input reply-sub-comment1"></textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                          @endforeach
                        @endif
                        </div>
                        <div class="see-m-c mt-2 mb-2 see-more-subcomment-div1-{{$comment->id}}">
                          @if($total_sub_comment > 1)
                            <span class="see-more" onclick="seeMoreSubComment1(event,{{$comment->id}},1,{{$total_sub_comment}})">View {{$total_sub_comment - 1}} more comments</span>
                          @endif
                        </div>
                      </div>
                    </div>
                  @endforeach
                @endif
                </div>
                <div class="see-m-c mt-2 mb-3 see-more-comment-div1-{{$livezone->id}}">
                  @if($total_comment['main'] > 2)
                  <span class="see-more" onclick="seeMoreComments1(event,{{$livezone->id}},2,{{$total_comment['main']}})">View {{$total_comment['main'] - 2}} more comments</span>
                  @endif
                </div>
              </div>
            </div>
            <div class="com-input">
              <div class="input-group">
                <textarea class="form-control first_comment1" livezone-id="{{$livezone->id}}" name="comment" placeholder="Add a comment..."></textarea>
                <div class="input-group-append">
                  {{-- <span class="input-group-text" id="basic-addon2">Pateikti</span> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
@endforeach