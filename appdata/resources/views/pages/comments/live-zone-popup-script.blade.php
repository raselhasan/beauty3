@if(@$view_live_zone)
  <script>
    var data = new FormData();
      data.append('livezone_id',{{$view_live_zone}});
      $.ajax( {
          processData: false,
        contentType: false,
          data: data,
          type: 'POST',
          url:'{{url("user/live-zone/open-popup")}}',
          success: function( response ){
            $('.put-live-zone-popup').html(response.data);
            $('#picture-pop-up-modal').modal('show');

          }

      });

  </script>
@endif

<script>
	function openLiveeZonePopUp(livezone_id)
	{
		  var data = new FormData();
	    data.append('livezone_id',livezone_id);
	    $.ajax( {
	      	processData: false,
	     	contentType: false,
	      	data: data,
	      	type: 'POST',
	      	url:'{{url("user/live-zone/open-popup")}}',
	      	success: function( response ){
	      		$('.put-live-zone-popup').html(response.data);
	        	$('#picture-pop-up-modal').modal('show');
            var c_url = window.location.href;
            var c_url = c_url+'?live_zone='+livezone_id;
            window.history.pushState("object or string", "Title", c_url);
	      	}

	    });
		// $('#p_live_zone_id').val(livezone_id);
		
	}

	function seeMoreComments1(e, livezone_id, offset, total_comment){
	    var data = new FormData();
	    data.append('livezone_id',livezone_id);
	    data.append('offset',offset);
	    data.append('total_comment',total_comment);
	    $.ajax( {
		    processData: false,
		    contentType: false,
		    data: data,
		    type: 'POST',
		    url:'{{url("user/live-zone/see-more-comment-popup")}}',
		    success: function( response ){
		    	$('.load-main-commnet-div-put').append(response)
		        var new_offset = parseInt(offset) + 10;
		        var new_total = parseInt(total_comment) - new_offset;
		        if(new_offset < parseInt(total_comment)){
		          var html = '<span class="see-more" onclick="seeMoreComments1(event,'+livezone_id+','+new_offset+','+total_comment+')">View '+new_total+' more comments</span>';
		          $('.see-more-comment-div1-'+livezone_id).html(html);
		        }else{
		          $('.see-more-comment-div1-'+livezone_id).empty();
		        }

		     }

	    }); 

  	}

  	function seeMoreSubComment1(e, comment_id, offset, total_sub_comment)
  	{
	    var data = new FormData();
	    data.append('comment_id',comment_id);
	    data.append('offset',offset);
	    data.append('total_sub_comment',total_sub_comment);
	    $.ajax( {
	      	processData: false,
	      	contentType: false,
	      	data: data,
	      	type: 'POST',
	      	url:'{{url("user/live-zone/see-more-sub-comment-popup")}}',
	      	success: function( response ){
	      		$('.load-sub-commnet-div-put-'+comment_id).append(response);
	        	var new_offset = parseInt(offset) + 10;
	        	var new_total = parseInt(total_sub_comment) - new_offset;
	        	if(new_offset < parseInt(total_sub_comment)){
	          		html = '<span onclick="seeMoreSubComment1(event, '+comment_id+', '+new_offset+','+total_sub_comment+')"> View '+new_total+' more comments</span>';
	          		$('.see-more-subcomment-div1-'+comment_id).html(html);
	        	}else{
	         		$('.see-more-subcomment-div1-'+comment_id).empty();
	       		}

	    	}

	   }); 
 	}


 	$(document).on('keyup','.main-comment-replay1',function(e){
     var code = e.keyCode ? e.keyCode : e.which;
     if (code == 13) {  // Enter keycode
       var text = $(this).val();
       $(this).val('');
       $(this).parents().find('.hide-w-box').hide();
       if($.trim(text) != ''){
        var livezone_id = $(this).attr('livezone-id');
        var comment_id = $(this).attr('comment-id');
        var data = new FormData();
        data.append('id',comment_id);
        data.append('livezone_id',livezone_id);
        data.append('text',text);
        $.ajax( {
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url:'{{url("user/live-zone/sub-comment-popup")}}',
          success: function( response ){
          	$('.load-sub-commnet-div-put-'+comment_id).prepend(response);
          	var get_total_comment = $('.put-comment-total1-'+livezone_id).text();
          	var total_comment = parseInt(get_total_comment) + 1;
          	$('.put-comment-total1-'+livezone_id).text(total_comment);
            // var get_total_comment = $('.put-comment-total-'+livezone_id).text();
            // var total_comment = parseInt(get_total_comment) + 1;
            // $('.put-comment-total-'+livezone_id).text(total_comment);
            // $('.load-sub-comment-'+comment_id).prepend(response);
            console.log(response);
          }

        }); 
      }

    }
  });

   $(document).on('keyup','.reply-sub-comment1',function(e){
   	 var code = e.keyCode ? e.keyCode : e.which;
     if (code == 13) {  // Enter keycode
       var text = $(this).val();
       $(this).val('');
       $(this).parents().find('.hide-w-box').hide();
       if($.trim(text) != ''){
        var livezone_id = $(this).attr('livezone-id');
        var comment_id = $(this).attr('comment-id');
        var data = new FormData();
        data.append('id',comment_id);
        data.append('livezone_id',livezone_id);
        data.append('text',text);
        $.ajax( {
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url:'{{url("user/live-zone/sub-comment-reply-popup")}}',
          success: function( response ){
          	$('.load-sub-commnet-div-put-'+comment_id).append(response);
          	var get_total_comment = $('.put-comment-total1-'+livezone_id).text();
          	var total_comment = parseInt(get_total_comment) + 1;
          	$('.put-comment-total1-'+livezone_id).text(total_comment);

            // var get_total_comment = $('.put-comment-total-'+livezone_id).text();
            // var total_comment = parseInt(get_total_comment) + 1;
            // $('.put-comment-total-'+livezone_id).text(total_comment);
            // $('.load-sub-comment-'+comment_id).append(response);
            console.log(response);
          }

        }); 
      }

    }
  });	


    $(document).on('keyup','.first_comment1',function(e){
   	  var code = e.keyCode ? e.keyCode : e.which;
     if (code == 13) {  // Enter keycode
       var text = $(this).val();
       $(this).val('');
       if($.trim(text) != ''){
        var livezone_id = $(this).attr('livezone-id');
        var data = new FormData();
        data.append('id',livezone_id);
        data.append('text',text);
        $.ajax( {
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url:'{{url("user/live-zone/first-comment-popup")}}',
          success: function( response ){
          	$('.load-main-commnet-div-put').prepend(response);
          	var get_total_comment = $('.put-comment-total1-'+livezone_id).text();
          	var total_comment = parseInt(get_total_comment) + 1;
          	$('.put-comment-total1-'+livezone_id).text(total_comment);

            // var get_total_comment = $('.put-comment-total-'+livezone_id).text();
            // var total_comment = parseInt(get_total_comment) + 1;
            // $('.put-comment-total-'+livezone_id).text(total_comment);
            // $('.load-main-comment-'+livezone_id).prepend(response);
            //console.log(response);
          }

        }); 
      }

    }
  });
  function showMoreString1(id)
  {
    $('.sub_str1_'+id).hide();
    $('.show_sub_str1_'+id).show();
  }
  function showMoreString5(id)
  {
    $('.main_sub_str2_'+id).hide();
    $('.main_show_sub_str2_'+id).show();
  }
  function showMoreString6(id)
  {
    $('.sub_sub_str3_'+id).hide();
    $('.sub_show_sub_str3_'+id).show();
  }
  function closePopUpUrl()
  {
    var newlink = "{{url('live-zone')}}";
    window.history.pushState("object or string", "Title", newlink);
  }


</script>