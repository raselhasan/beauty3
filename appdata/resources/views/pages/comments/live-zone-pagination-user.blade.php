                @if(count($data['liveZones']) > 0)
                @foreach ($data['liveZones'] as $livezone)
                <div class="single-content img-v" id="live-zone-number-{{$livezone->id}}">
                  <div class="post-d top-post-cnt">
                   <div class="post-d-c-tlt post-d-tlt ">
                    <div class="com-logo">
                    @if($livezone->user->profile_image)
                    <img  src="{{asset('userImage/fixPic/'.$livezone->user->profile_image)}}" alt="img">
                    @else 
                        <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                    @endif
                    </div>
                    <div class="post-d-txt-all">
                     <span class="post-r">
                        <div class="btn-group">
                          <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>...</span></button>
                          <div class="dropdown-menu dropdown-menu-right">
                            <button class="dropdown-item" type="button"> <span class="v-f-s" onclick="openLiveeZonePopUp({{$livezone->id}})">view full screen</span></button>
                            <button class="dropdown-item" type="button"><span class="l-edit" onclick="openLiveZone({{$livezone->id}})">Edit</span></button>
                            <button class="dropdown-item" type="button"><span class="l-delete" onclick="deleteLiveZone({{$livezone->id}})">Delete</span></button>
                          </div>
                        </div>
                      </span>   
                      
                     <div class="post-d-txt">{{ $livezone->user->name }}</div>
                     <div class="post-da-r">
                       <span class="post-da">{{ \Carbon\Carbon::parse($livezone->created_at)->diffForHumans() }}</span>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="post-desc ev-livezone-des-{{$livezone->id}}">
                  @php
                      $str_len = strlen($livezone->description);
                      $main_str = substr($livezone->description,0,570);
                      $sub_str = substr($livezone->description,571,$str_len);
                    @endphp

                    {!! $main_str  !!}

                    @if($str_len > 570)
                      <span class="sub_str_{{$livezone->id}}" id="s_see_more" onclick="showMoreString({{$livezone->id}})">See more..</span>
                      <span class="show_sub_str_{{$livezone->id}}" id="s_show">{!! $sub_str !!}</span> 
                    @endif
               </div>
               <div class="img_or_video_{{$livezone->id}}"> 
               @if($livezone->status == 1) 
               <div class="content-img-v-cnt ">
                 <div class="content-img-p ">
                  <div class="content-img">
                    @if($livezone->image)
                    <img id="lz-img-1" src="{{asset('liveZone/'.$livezone->image)}}" onclick="openLiveeZonePopUp({{$livezone->id}})">
                    @else 
                      <img id="lz-img-1" src="{{asset('assets/img/services/profile-img.png')}}" onclick="openLiveeZonePopUp({{$livezone->id}})">

                    @endif
                  </div>
                </div>
              </div>
              @endif
              @if($livezone->status == 2)
              <div class="content-img-v-cnt">
                <div class="content-img-p video ">
                  <div class="content-img">
                    <iframe id="lz-video-1" src="https://www.youtube.com/embed/{{$livezone->image}}">
                    </iframe>
                  </div>
                </div>
              </div>
              @endif
              </div>

              <div class="like-com-lz-btn">
                <span class="like-count mb-2 mb-md-0">
                  <div class="like-reaction">
                    @php
                      $mainLike = App\Http\Controllers\LiveZoneController::firstLikeCount($livezone->id);
                      echo $mainLike['reacts'];
                    @endphp
                    {{-- <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                    <span class="love-i"><i class="fas fa-heart"></i></span> --}}
                  </div>
                  {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
                  <span class="n-count total_post_like">{{$mainLike['total']}}</span>
                </span>
                <span class="l-c-s mb-2 mb-md-0">
                  <span class="lcs-ul like-li">
                    <span class="lcs-li lcs-like"><span><img  src="{{asset('assets/img/like-i.png')}}" alt="img">Like</span> 
                    <div class="like-item">
                      <span class="like-itm"><i class="fa fa-thumbs-up" onclick="giveFirstLike({{$livezone->id}},'like')"></i></span>
                      <span class="love-itm"><i class="far fa-heart" onclick="giveFirstLike({{$livezone->id}},'love')"></i></span>
                    </div>
                  </span>
                  <span class="lcs-li com-li" onclick="openComments({{$livezone->id}})"><img  src="{{asset('assets/img/comment-i.png')}}" alt="img">Comments</span>
                  <span class="lcs-li"><img  src="{{asset('assets/img/share-i.png')}}" alt="img">Share</span>
                </span>
              </span>
              <span class="agn-txt float-none float-md-right"><span class="comment-c"><span class="put-comment-total-{{$livezone->id}}">
                    @php
                      $total_comment = App\Http\Controllers\LiveZoneController::totalComment($livezone->id);
                      echo $total_comment['total'];
                    @endphp
              </span> comment</span><span class="share-c">0 Share</span></span>
            </div>

            <div class="comment-w-box">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                  @if(Auth::check())
                  @if(auth()->user()->profile_image)
                  <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                  @else 
                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif
                  @else
                   <img  src="{{asset('userImage/fixPic/Profile-icon.jpg')}}" alt="img">
                  @endif
                  </span>
                </div>
                <textarea id="comment_box" name="comment" class="form-control first_comment" placeholder="Rašykite komentarą..." livezone-id="{{$livezone->id}}" ></textarea>

              </div>
              {{-- <div class="input-group-append">
                <span class="input-group-text">
                  <span class="pv-i"><i class="fas fa-smile"></i></span>
                  <span class="pv-i"><i class="fas fa-camera"></i></span>
                  <span class="pv-i"><i class="fas fa-video"></i></span>
                  <button class="sent-txt">Pateikti</button>
                </span>
              </div> --}}
            </div>
            <div class="load-main-comment-{{$livezone->id}}">
            @if(count($livezone->comments) > 0)
            @foreach ($livezone->comments as $comment)
            <div class="post-d">
              <div class="comment-all" id="comment_load">
               <div class="post-d-c-tlt post-d-comment ">
                <div class="com-logo">
                @if($comment->commenter->profile_image)
                <img  src="{{asset('userImage/fixPic/'.$comment->commenter->profile_image)}}" alt="img">
                @else 
                  <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                @endif
                </div>
                <div class="post-d-txt-all">
                  <div class="comment-txt">
                    <div class="post-d-txt"><span class="usr-name">{{$comment->commenter->name}}</span> 
                      @php
                        $str_len1 = strlen($comment->comments);
                        $main_str1 = substr($comment->comments,0,200);
                        $sub_str1 = substr($comment->comments,201,$str_len1);
                      @endphp

                      {!! $main_str1  !!}

                      @if($str_len1 > 200)
                        <span class="main_sub_str_{{$comment->id}}" id="s_see_more" onclick="showMoreString3({{$comment->id}})">See more..</span>
                        <span class="main_show_sub_str_{{$comment->id}}" id="s_show">{!! $sub_str1 !!}</span> 
                      @endif

                    </div>
                    <div class="post-da-r">
                      <span class="like-count" id="comment-like-react-{{$comment->id}}">
                      @php
                        $commentLike = App\Http\Controllers\LiveZoneController::commentLikeCount($comment->id);
                        echo $commentLike['reacts'];
                      @endphp
                       {{--  <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                        <span class="love-i"><i class="fas fa-heart"></i></span> --}}

                        {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
                        {{-- <span class="n-count">1</span> --}}
                      </span>
                      <span class="post-da reply reply-like">
                       <span> Like</span>
                       <div class="like-item">
                        <span class="like-itm"><i class="fa fa-thumbs-up" onclick="commentLike({{$comment->id}},'like')"></i></span>
                        <span class="love-itm"><i class="far fa-heart" onclick="commentLike({{$comment->id}},'love')"></i></span>
                      </div>
                    </span>
                    <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                    <span class="post-da">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</span>
                  </div>

                  <div class="comment-w-box reply-w-box hide-w-box">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          @if(Auth::check())
                          @if(auth()->user()->profile_image)
                          <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                          @else 
                            <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                          @endif
                          @else
                           <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                          @endif
                        </span>
                      </div>
                       <textarea class="form-control reply-input main-comment-replay" comment-id="{{$comment->id}}" livezone-id="{{$livezone->id}}" name="reply" placeholder="Rašykite komentarą..."></textarea>
                      
                    </div>
                    {{-- <div class="input-group-append">
                      <span class="input-group-text">
                        <span class="pv-i"><i class="fas fa-smile"></i></span>
                        <span class="pv-i"><i class="fas fa-camera"></i></span>
                        <span class="pv-i"><i class="fas fa-video"></i></span>
                        <button class="sent-txt">Pateikti</button>
                      </span>
                    </div> --}}
                  </div>

                </div>
                <div class="load-sub-comment-{{$comment->id}}">
                @php
                  $total_sub_comment = App\Http\Controllers\LiveZoneController::countSubComment($comment->id);
                @endphp
                @if(count($comment->subcomments) > 0)
                @foreach($comment->subcomments as $sub_comment)
                <div class="post-d-c-tlt post-d-reply comment-txt">
                  <div class="com-logo">
                  @if($sub_comment->commenter->profile_image)
                  <img  src="{{asset('userImage/fixPic/'.$sub_comment->commenter->profile_image)}}" alt="img">
                  @else 
                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif
                  </div>
                  <div class="post-d-txt-all">
                    <div class="post-d-txt"><span class="usr-name">{{$sub_comment->commenter->name}}</span> 
                      @php
                        $str_len2 = strlen($sub_comment->comments);
                        $main_str2 = substr($sub_comment->comments,0,200);
                        $sub_str2 = substr($sub_comment->comments,201,$str_len2);
                      @endphp

                      {!! $main_str2  !!}

                      @if($str_len2 > 200)
                        <span class="sub_sub_str_{{$sub_comment->id}}" id="s_see_more" onclick="showMoreString4({{$sub_comment->id}})">See more..</span>
                        <span class="sub_show_sub_str_{{$sub_comment->id}}" id="s_show">{!! $sub_str2 !!}</span> 
                      @endif

                    </div>
                    <div class="post-da-r">
                     <span class="like-count" id="subcomment-like-react-{{$sub_comment->id}}">
                      @php
                        $subCommentLike = App\Http\Controllers\LiveZoneController::subCommentLikeCount($sub_comment->id);
                        echo $subCommentLike['reacts'];
                      @endphp
                       {{--  <span class="like-i"><i class="fa fa-thumbs-up"></i></span>
                        <span class="love-i"><i class="fas fa-heart"></i></span> --}}

                        {{-- <span class="wow-i"><img  src="{{asset('assets/img/services/wow.png')}}" alt="img"></span> --}}
                        {{-- <span class="n-count">1</span> --}}
                      </span>

                      <span class="post-da reply reply-like">
                       <span> Like</span>
                       <div class="like-item">
                        <span class="like-itm"><i class="fa fa-thumbs-up" onclick="subCommentLike({{$sub_comment->id}},'like')"></i></span>
                        <span class="love-itm"><i class="far fa-heart" onclick="subCommentLike({{$sub_comment->id}},'love')"></i></span>
                      </div>
                    </span>
                    <span class="post-da reply reply-box-s" onclick="replyBoxShow(event)">Reply</span>
                    <span class="post-da">{{ \Carbon\Carbon::parse($sub_comment->created_at)->diffForHumans() }}</span>
                  </div>
                  <div class="comment-w-box reply-w-box hide-w-box">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          @if(Auth::check())
                          @if(auth()->user()->profile_image)
                          <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
                          @else 
                            <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                          @endif
                          @else
                           <img  src="{{asset('userImage/fixPic/Profile-icon.jpg')}}" alt="img">
                          @endif
                        </span>
                      </div>
                      <textarea class="form-control reply-input reply-sub-comment" comment-id="{{$comment->id}}" livezone-id="{{$livezone->id}}"  name="reply" placeholder="Rašykite komentarą..."></textarea>
                    </div>
                    {{-- <div class="input-group-append">
                      <span class="input-group-text">
                        <span class="pv-i"><i class="fas fa-smile"></i></span>
                        <span class="pv-i"><i class="fas fa-camera"></i></span>
                        <span class="pv-i"><i class="fas fa-video"></i></span>
                        <button class="sent-txt">Pateikti</button>
                      </span>
                    </div> --}}
                  </div>
                </div>
              </div>
              @endforeach
              @endif
              </div>
              <div class="see-more-subcomment-div-{{$comment->id}}">
                @if($total_sub_comment > 1)
                <span onclick="seeMoreSubComment(event, {{$comment->id}}, 1, {{$total_sub_comment}})"> View {{$total_sub_comment - 1}} more comments</span>
                @endif
              </div>


            </div>
          </div>
        </div>
       
      </div>
      @endforeach
      @endif
      </div>
       <div class="see-m-c mt-2 see-more-comment-div-{{$livezone->id}}">
          @if($total_comment['main'] > 2)
          <span class="see-more" onclick="seeMoreComments(event,{{$livezone->id}},2,{{$total_comment['main']}})">View {{$total_comment['main'] - 2}} more comments</span>
          @endif
        </div>
    </div>
    @endforeach
    @endif
    </div>