
{{-- start live zone javascript --}}
<script type="text/javascript">
  $(document).on('submit','#uploadLiveZoneImage',function(){
    var data = new FormData( $( 'form#uploadLiveZoneImage' )[ 0 ] );
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("user/live-zone/upload-image")}}',
      success: function( response ){
        if(response.error){
          $('.image-error').html(response.error);
          $('.image-error').show();
        }
        if(response.success){
          $('.uploaded_image_file1').html(response.file);
          $('#liveZoneImgModal').modal('hide');
        }
          // 
        }

      }); 
    return false;
  });

  $(document).on('submit','#uploadLiveZoneVideo',function(){
    var data = new FormData( $( 'form#uploadLiveZoneVideo' )[ 0 ] );
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("user/live-zone/upload-youtube-video")}}',
      success: function( response ){
        if(response.success){
          $('.uploaded_image_file1').html(response.link);
          $('#liveZoneVideoModal').modal('hide');
        }

      }

    }); 
    return false;
  });

  $(document).on('submit','#saveLiveZoneForm',function(){
    var data = new FormData( $( 'form#saveLiveZoneForm' )[ 0 ] );
    var text = $('#output_div').html();
    data.append('text',$.trim(text));
    if($.trim(text) !=''){
      $.ajax( {
        processData: false,
        contentType: false,
        data: data,
        type: 'POST',
        url:'{{url("user/live-zone/save-livezone")}}',
        success: function( response ){
          var page = $('#page_identy').val();
          if(page == 1){
            if($('.show_livezone').is(':checked')){
              $('.add-new-livezone').prepend(response);
            }
          }else if(page == 2){
            $('.add-new-livezone').prepend(response);
          }else if(page == 3){
            if($('.show_profile').is(':checked')){
              $('.add-new-livezone').prepend(response);
            }
          }
          $('#livezone-description').val('');
          $('.uploaded_image_file1').empty();
          console.log(response);
        }

      }); 
    }
    return false;
  });

  function giveFirstLike(id, react,place)
  {
    var data = new FormData();
    data.append('id',id);
    data.append('react',react);
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("user/live-zone/firstlike")}}',
      success: function( response ){
        if(place == 'main'){
          $('#live-zone-number-'+id).find('.total_post_like').text(response.total_like);
          $('#live-zone-number-'+id).find('.like-reaction').html(response.reacts);
        }else{
          $('.total_post_like1').text(response.total_like);
          $('.like-reaction1').html(response.reacts);
          $('#live-zone-number-'+id).find('.total_post_like').text(response.total_like);
          $('#live-zone-number-'+id).find('.like-reaction').html(response.reacts);
        }
        
    
      }

    }); 
  }

  $(document).on('keyup','.first_comment',function(e){
   var code = e.keyCode ? e.keyCode : e.which;
     if (code == 13) {  // Enter keycode
       var text = $(this).val();
       $(this).val('');
       if($.trim(text) != ''){
        var livezone_id = $(this).attr('livezone-id');
        var data = new FormData();
        data.append('id',livezone_id);
        data.append('text',text);
        $.ajax( {
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url:'{{url("user/live-zone/first-comment")}}',
          success: function( response ){
            var get_total_comment = $('.put-comment-total-'+livezone_id).text();
            var total_comment = parseInt(get_total_comment) + 1;
            $('.put-comment-total-'+livezone_id).text(total_comment);
            $('.load-main-comment-'+livezone_id).prepend(response);
          }

        }); 
      }

    }
  });
  function commentLike(comment_id, react, place){
    var data = new FormData();
    data.append('comment_id',comment_id);
    data.append('react',react);
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("user/live-zone/comment-like")}}',
      success: function( response ){
        if(place == 'main'){
          $('#comment-like-react-'+comment_id).html(response.reacts);
        }else{
          $('#comment-like-react1-'+comment_id).html(response.reacts);
          $('#comment-like-react-'+comment_id).html(response.reacts);
        }
        
      }

    });
  }

  function subCommentLike(subcomment_id, react,place)
  {
    var data = new FormData();
    data.append('sub_comment_id',subcomment_id);
    data.append('react',react);
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("user/live-zone/sub-comment-like")}}',
      success: function( response ){
        if(place == 'main'){
          $('#subcomment-like-react-'+subcomment_id).html(response.reacts);
        }else{
           $('#subcomment-like-react1-'+subcomment_id).html(response.reacts);
           $('#subcomment-like-react-'+subcomment_id).html(response.reacts);
        }
        
      }

    });
  }

  $(document).on('keyup','.main-comment-replay',function(e){
   var code = e.keyCode ? e.keyCode : e.which;
     if (code == 13) {  // Enter keycode
       var text = $(this).val();
       $(this).val('');
       $(this).parents().find('.hide-w-box').hide();
       if($.trim(text) != ''){
        var livezone_id = $(this).attr('livezone-id');
        var comment_id = $(this).attr('comment-id');
        var data = new FormData();
        data.append('id',comment_id);
        data.append('livezone_id',livezone_id);
        data.append('text',text);
        $.ajax( {
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url:'{{url("user/live-zone/sub-comment")}}',
          success: function( response ){
            var get_total_comment = $('.put-comment-total-'+livezone_id).text();
            var total_comment = parseInt(get_total_comment) + 1;
            $('.put-comment-total-'+livezone_id).text(total_comment);
            $('.load-sub-comment-'+comment_id).prepend(response);
          }

        }); 
      }

    }
  });

  $(document).on('keyup','.reply-sub-comment',function(e){
   var code = e.keyCode ? e.keyCode : e.which;
     if (code == 13) {  // Enter keycode
       var text = $(this).val();
       $(this).val('');
       $(this).parents().find('.hide-w-box').hide();
       if($.trim(text) != ''){
        var livezone_id = $(this).attr('livezone-id');
        var comment_id = $(this).attr('comment-id');
        var data = new FormData();
        data.append('id',comment_id);
        data.append('livezone_id',livezone_id);
        data.append('text',text);
        $.ajax( {
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url:'{{url("user/live-zone/sub-comment-reply")}}',
          success: function( response ){
            var get_total_comment = $('.put-comment-total-'+livezone_id).text();
            var total_comment = parseInt(get_total_comment) + 1;
            $('.put-comment-total-'+livezone_id).text(total_comment);
            $('.load-sub-comment-'+comment_id).append(response);
          }

        }); 
      }

    }
  });

  $("#livezone-description").keyup(function() {
    $( "#output_div" ).html( $( this ).val().replace(/[\r\n]/g, "<br />") );
  });

  function seeMoreComments(e, livezone_id, offset, total_comment){
    var data = new FormData();
    data.append('livezone_id',livezone_id);
    data.append('offset',offset);
    data.append('total_comment',total_comment);
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("user/live-zone/see-more-comment")}}',
      success: function( response ){
        $('.load-main-comment-'+livezone_id).append(response);
        var new_offset = parseInt(offset) + 10;
        var new_total = parseInt(total_comment) - new_offset;
        if(new_offset < parseInt(total_comment)){
          var html = '<span class="see-more" onclick="seeMoreComments(event,'+livezone_id+','+new_offset+','+total_comment+')">View '+new_total+' more comments</span>';
          $('.see-more-comment-div-'+livezone_id).html(html);
        }else{
          $('.see-more-comment-div-'+livezone_id).empty();
        }

      }

    }); 

  }
  function seeMoreSubComment(e, comment_id, offset, total_sub_comment)
  {
    var data = new FormData();
    data.append('comment_id',comment_id);
    data.append('offset',offset);
    data.append('total_sub_comment',total_sub_comment);
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("user/live-zone/see-more-sub-comment")}}',
      success: function( response ){
        $('.load-sub-comment-'+comment_id).append(response);
        var new_offset = parseInt(offset) + 10;
        var new_total = parseInt(total_sub_comment) - new_offset;
        if(new_offset < parseInt(total_sub_comment)){
          html = '<span onclick="seeMoreSubComment(event, '+comment_id+', '+new_offset+','+total_sub_comment+')"> View '+new_total+' more comments</span>';
          $('.see-more-subcomment-div-'+comment_id).html(html);
        }else{
         $('.see-more-subcomment-div-'+comment_id).empty();
       }
     }

   }); 
  }


  function showMoreString(id)
  {
    $('.sub_str_'+id).hide();
    $('.show_sub_str_'+id).show();
  }
  function showMoreString3(id)
  {
    $('.main_sub_str_'+id).hide();
    $('.main_show_sub_str_'+id).show();
  }
  function showMoreString4(id)
  {
    $('.sub_sub_str_'+id).hide();
    $('.sub_show_sub_str_'+id).show();
  }
</script>