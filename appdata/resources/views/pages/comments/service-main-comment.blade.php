<div class="com-reply">
	<div class="comment" id="main-cmt">
		<div class="row">
			<div class="user-img-c">
				@if($comment['user']['profile_image'])
				<img src="{{asset('userImage/fixPic/'.$comment['user']['profile_image'])}}" alt="user-img">
				@else 
					<img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
				@endif
			</div>
			<div class="col-12 pad-0">
				<div class="comment-cnt">
					<div class="user-time">
						<span class="com-rew-s">
							@if($comment['star'] == 1)
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							@elseif($comment['star'] == 2)
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							@elseif($comment['star'] == 3)
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							@elseif($comment['star'] == 4)
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							@elseif($comment['star'] == 5)
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
							@endif
						</span>
						<span class="user-n">{{$comment['user']['name']}}</span>
						<span class="com-t">{{ \Carbon\Carbon::parse($comment['created_at'])->diffForHumans() }}</span>
					</div>
					<p>{!! $comment['comment'] !!}</p>
					<div class="main-comment-replay-box-{{$comment['id']}}">
					 @php
                      $isReplay = App\Http\Controllers\LiveZoneController::isServiceReplayAvaible($comment['id'], $comment['service_id'], $comment['commented_user']);
                    @endphp
                    @if($isReplay == 'yes')
					<div class="edit-com reply-box-s"><span>Reply</span></div>
					<form id="add-service-reply" class="reply-form-s">
						@csrf
						<input type="hidden" name="main_service_id" id="main_service_id" value="{{$comment['service_id']}}">
						<input type="hidden" name="main_comment_id" id="main_comment_id" value="{{$comment['id']}}">
						<div class="com-text-ar reply-text-ar">
							<div class="com-text-in">
								<div class="com-img">
								
								@if(Auth::check())
									@if(auth()->user()->profile_image)
									    <img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img">
									@else 
									    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
									@endif
								@else 
									<img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
								@endif

								</div>
								<div class="form-group">
									<label><span>{{auth()->user()->name}}</span></label>
									<textarea placeholder="Rašykite komentarą..." class="form-control" id="user_reply" name="main_service_replay"></textarea>
								</div>
							</div>
							<div class="form-group mb-0 com-btn">
								<button class="p-btn btn send" type="submit">Reply</button>
							</div>
						</div>
					</form>
					@endif
					</div>
				</div>
				<div class="main-comment-replay-{{$comment['id']}}">
					@if(count($comment['comment_replay']) > 0)
					@foreach($comment['comment_replay'] as $reply)
					<div class="cmt-replay">
						<div class="reply">
							<div class="row">
								<div class="user-img-c">
									@if($reply['user']['profile_image'])
									<img src="{{asset('userImage/fixPic/'.$reply['user']['profile_image'])}}" alt="user-img">
									@else 
										<img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
									@endif
								</div>
								<div class="col-12 pad-0">
									<div class="comment-cnt">
										<div class="user-time">
											<span class="user-n">{{$reply['user']['name']}}</span>
											<span class="com-t">{{ \Carbon\Carbon::parse($reply['created_at'])->diffForHumans() }}</span>
										</div>
										<p>{!! $reply['replay']!!}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endforeach
					@endif
				</div>



			</div>
		</div>
	</div>
</div>
