@if(count($comment_replay) > 0)
@foreach($comment_replay as $reply)
<div class="cmt-replay">
	<div class="reply">
		<div class="row">
			<div class="user-img-c">
				@if($reply['user']['profile_image'])
				<img src="{{asset('userImage/fixPic/'.$reply['user']['profile_image'])}}" alt="user-img">
				@else 
					<img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
				@endif
			</div>
			<div class="col-12 pad-0">
				<div class="comment-cnt">
					<div class="user-time">
						<span class="user-n">{{$reply['user']['name']}}</span>
						<span class="com-t">{{ \Carbon\Carbon::parse($reply['created_at'])->diffForHumans() }}</span>
					</div>
					<p>{!! $reply['replay']!!}</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endforeach
@endif