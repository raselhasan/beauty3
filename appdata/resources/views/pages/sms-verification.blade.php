 @extends('layout.app')
 @section('style')
 <style>
 	.all_service {
 		margin-left: 20px;
 	}
 	.card{
 		background: #252222;
 	}
 </style>
 @endsection
 @section('content')
 @include('inc.header-filter-new')
 <section class="select-panel-section">
 	<div class="max-fix-width">
 		<div class="row">
 			<div class="col-md-3"></div>
 			<div class="col-md-6">
 				<div class="card">
				  	<div class="card-header">
				    	Verify Phone
				 	</div>
				  	<div class="card-body">
				  		<form method="post" action="{{url('sms-verification')}}">
				  			@csrf
				  			<input type="hidden" name="name" value="{{$data['name']}}">
				  			<input type="hidden" name="email" value="{{$data['email']}}">
				  			<input type="hidden" name="password" value="{{$data['password']}}">
				  			@if(Session::has('error'))
								<p style="color:red">{{Session::get('error')}}</p>
				  			@endif
				  			@if(Session::has('success'))
								<p style="color:green">{{Session::get('success')}}</p>
				  			@endif
					    	<div class="form-group">
								<label for="REmail">Verification Code</label>
								<input type="text" id="v_code" class="form-control" aria-label="Email" name="v_code" autofocus value="{{old('v_code')}}">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success">Verify</button>
							</div>
						</form>
				  </div>
				</div>
 			</div>
 			<div class="col-md-3"></div>
 		</div>
 	</div>
 </section>
 @endsection
 @section('script')

 @endsection