 @extends('layout.app')
 @section('title')
Plans | GrožioKalviai
@endsection
 @section('style')
 <style>
 	.alert-primary {
 		color: #f5eaea;
 		background-color: #1a1a1a;
 		border-color: #1a1a1a;
 		font-size: 17px;
 	}
 </style>
 @endsection
 @section('content')
 @include('inc.header-filter-new')
 @if($package['service']->active == 0)
 <section class="select-panel-section" style="padding-bottom: 0px">
 	<div class="max-fix-width">
 		<div class="alert alert-primary" role="alert">
 			@lang('lang.hello') {{auth()->user()->name}} @lang('lang.we_are_giving')
 			<br />
 			<br />
 			<button class="btn btn-info">@lang('lang.more_info')</button>
 		</div>
 	</div>
 </section>
 @endif


 <section class="select-panel-section">
 	<div class="max-fix-width">
 		<div class="section-title">@lang('lang.additional_paymen')t</div>
 		<div class="adition-service">
 			<div class="service-list">
 				@if($package['package1'])
 				<div class="s-list">
 					<div class="row mar-0">
 						<div class="col-12 col-lg-3 pad-l-0 text-center">
 							<div class="add-ser-txt">{{ $package['package1']->title }}<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 22 22">
 									<g id="info_1_" data-name="info (1)" transform="translate(-1 -1)">
 										<circle id="Ellipse_31" data-name="Ellipse 31" cx="10" cy="10" r="10" transform="translate(2 2)" stroke-width="2" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" fill="none" />
 										<line id="Line_63" data-name="Line 63" y1="4" transform="translate(12 12)" fill="none" stroke="#757575" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
 										<line id="Line_64" data-name="Line 64" x2="0.01" transform="translate(12 8)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
 									</g>
 								</svg>
 							</div>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-l-0">
 							<div class="ctm-select">
 								<div class="ctm-select-txt pad-l-10">
 									<span class="select-txt duration1" duration1="0" id="category">@lang('lang.choose')</span>
 									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
 								</div>
 								<div class="ctm-option-box">
 									@if(count($package['package1']->duration) > 0)
 									@foreach ($package['package1']->duration as $duration)
 									<div class="ctm-option" package-id1="{{$package['package1']->id}}" id="{{ $duration->id }}" onclick="calculateAmount(this.id,{{ $package['package1']->id }},'price1','duration1','btn1')">{{ $duration->duration }}</div>
 									@endforeach
 									@endif
 								</div>
 							</div>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-l-0">

 						</div>
 						<div class="col-12 col-sm-6  col-lg-2 pad-l-0 ser text-center">
 							<button class="btn ser-btn" id="btn1" onclick="addToPayment('price1','btn1')">@lang('lang.add_to_cart')</button>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-0 text-center">
 							<div class="adition-price">
 								&#128;<span class="cr-price" id="price1">0.00</span>
 							</div>
 						</div>
 					</div>
 				</div>
 				@endif
 				@if($package['package2'])
 				<div class="s-list">
 					<div class="row mar-0">
 						<div class="col-12 col-lg-3 pad-l-0 text-center">
 							<div class="add-ser-txt">{{ $package['package2']->title }}<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 22 22">
 									<g id="info_1_" data-name="info (1)" transform="translate(-1 -1)">
 										<circle id="Ellipse_31" data-name="Ellipse 31" cx="10" cy="10" r="10" transform="translate(2 2)" stroke-width="2" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" fill="none" />
 										<line id="Line_63" data-name="Line 63" y1="4" transform="translate(12 12)" fill="none" stroke="#757575" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
 										<line id="Line_64" data-name="Line 64" x2="0.01" transform="translate(12 8)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
 									</g>
 								</svg>
 							</div>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-l-0">
 							<div class="ctm-select">
 								<div class="ctm-select-txt pad-l-10">
 									<span class="select-txt duration2" duration2="0" id="category">@lang('lang.choose')</span>
 									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
 								</div>
 								<div class="ctm-option-box">
 									@if(count($package['package2']->duration) > 0)
 									@foreach ($package['package2']->duration as $duration)
 									<div class="ctm-option" package-id2="{{$package['package2']->id}}" id="{{ $duration->id }}" onclick="calculateAmount(this.id,{{ $package['package2']->id }},'price2','duration2','btn2')">{{ $duration->duration }}</div>
 									@endforeach
 									@endif
 								</div>
 							</div>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-l-0">
 							<div class="ctm-select">
 								<div class="ctm-select-txt pad-l-10">
 									<span class="select-txt all-star" starid="{{$package['package2']->star->id}}" starnumber="0" id="category">@lang('lang.choose')</span>
 									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
 								</div>
 								<div class="ctm-option-box">
 									@if($package['package2']->star)
 									@for ($i = $package['package2']->star->min; $i <= $package['package2']->star->max; $i++)
 										<div class="ctm-option" id="{{ $i }}" onclick="amountWithStar({{ $package['package2']->star->id }},this.id,'price2')">{{ $i }}</div>
 										@endfor
 										@endif
 								</div>
 							</div>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2 pad-l-0 ser text-center">
 							<button class="btn ser-btn" id="btn2" onclick="addToPayment('price2','btn2')">@lang('lang.add_to_cart')</button>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-0 text-center">
 							<div class="adition-price">
 								&#128;<span class="cr-price" id="price2">0.00</span>
 							</div>
 						</div>
 					</div>
 				</div>
 				@endif
 				@if($package['package3'])
 				<div class="s-list">
 					<div class="row mar-0">
 						<div class="col-12 col-lg-3 pad-l-0 text-center">
 							<div class="add-ser-txt">{{ $package['package3']->title }}<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 22 22">
 									<g id="info_1_" data-name="info (1)" transform="translate(-1 -1)">
 										<circle id="Ellipse_31" data-name="Ellipse 31" cx="10" cy="10" r="10" transform="translate(2 2)" stroke-width="2" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" fill="none" />
 										<line id="Line_63" data-name="Line 63" y1="4" transform="translate(12 12)" fill="none" stroke="#757575" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
 										<line id="Line_64" data-name="Line 64" x2="0.01" transform="translate(12 8)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" />
 									</g>
 								</svg>
 							</div>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-l-0">
 							<div class="ctm-select">
 								<div class="ctm-select-txt pad-l-10">
 									<span class="select-txt duration3" duration3="0" id="category">@lang('lang.choose')</span>
 									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
 								</div>
 								<div class="ctm-option-box">
 									@if(count($package['package3']->duration) > 0)
 									@foreach ($package['package3']->duration as $duration)
 									<div class="ctm-option" package-id3="{{$package['package3']->id}}" id="{{ $duration->id }}" onclick="calculateAmount(this.id,{{ $package['package3']->id }},'price3','duration3','btn3')">{{ $duration->duration }}</div>
 									@endforeach
 									@endif
 								</div>
 							</div>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-l-0">

 						</div>
 						<div class="col-12 col-sm-6  col-lg-2 pad-l-0 ser text-center">
 							<button class="btn ser-btn" id="btn3" onclick="addToPayment('price3','btn3')">@lang('lang.add_to_cart')</button>
 						</div>
 						<div class="col-12 col-sm-6  col-lg-2  pad-0 text-center">
 							<div class="adition-price">
 								&#128;<span class="cr-price" id="price3">0.00</span>
 							</div>
 						</div>
 					</div>
 				</div>
 				@endif
 			</div>
 		</div>
 		<div class="checkout">
 			<div class="section-title">@lang('lang.checkout')</div>
 			<div class="row mar-0">
 				<div class="col-12 col-lg-6 pad-l-0">
 					<div class="order">
 						<div class="order-tlt">@lang('lang.your_order')</div>
 						<div class="order-list-all">
 							@if($package['service']->active == 0)
 							<div class="order-list list">
 								<span class="order-name" id="oreder-name">{{$package['service']->title}}</span>
 								<span class="order-price">€ <span class="cr-price" id="order-price">00.00</span></span>
 							</div>
 							@endif
 						</div>
 						<div class="total-price">
 							Total: &#128;<span class="cr-price" id="total-amount">0.00</span>
 						</div>
 					</div>
 				</div>
 				<div class="col-12 col-lg-6 pad-l-0">
 					<div class="payment">
 						<div class="payment-tlt text-center">@lang('lang.select_payment_method')</div>
 						<div class="payment-option">
 							<div class="row mar-0">
 								<div class="col-12 col-sm-6 pad-l-0">
 									<div class="pay-acc" onclick="setPaymentMethod(2)">
 										<span class="pay-slct">&#10004;</span>
 										<div class="pay-img"><img src="{{ asset('assets/img/paysera.png') }}" alt="lt flag"></div>
 									</div>
 								</div>
 								<div class="col-12 col-sm-6 pad-0">
 									<div class="pay-acc pay-selected" onclick="setPaymentMethod(1)">
 										<span class="pay-slct">&#10004;</span>
 										<div class="pay-img"><img src="{{ asset('assets/img/Stripe.png') }}" alt="lt flag"></div>
 									</div>
 								</div>
 							</div>
 						</div>
 						<div class="next-step">
 							<button class="btn more-btn next-btn" onclick="goToPayment()">
 								@if($package['service']->active == 0) Active Service @endif
 								@if($package['service']->active == 1) Upgrade Service @endif
 							</button>

 						</div>

 					</div>
 				</div>
 			</div>
 		</div>
 		<input type="hidden" name="pay1" id="pay1" value="0" pkg1="{{@$package['package1']->id}}">
 		<input type="hidden" name="pay2" id="pay2" value="0" pkg2="{{@$package['package2']->id}}">
 		<input type="hidden" name="pay3" id="pay3" value="0" pkg3="{{@$package['package3']->id}}">
 		<input type="hidden" name="payment_mathod" id="payment_mathod" value="1">
 		<input type="hidden" name="sevice_active" id="sevice_active" value="{{$package['service']->active}}">
 		<input type="hidden" name="package2_name" id="package2_name" value="{{@$package['package2']->title}}">
 	</div>
 </section>
 @endsection
 @section('script')
 <script src="https://js.stripe.com/v3/"></script>
 @include('payment_script.select_plan_script');
 @endsection