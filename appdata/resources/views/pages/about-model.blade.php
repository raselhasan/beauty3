@extends('layout.app')
@section('title')
{{@$page_title}} | GrožioKalviai
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fancybox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cropper.css') }}">
<style type="text/css">
.gm-style-iw {
	max-width: 300px !important;
	max-height: 300px !important;
}

.gm-style-iw-d {
	max-width: 300px !important;
	max-height: 300px !important;
}

.add-tab {
	display: none !important
}

.gallery .item-column:nth-child(1) .single-content {
	padding-bottom: 0 !important;
}
.star-checked {
	color: orange;
}
.load-more-service-comment span{
	cursor: pointer;
	text-decoration: underline;
}
.seeMore {
	color: #e25b37;
}
</style>
@endsection
@section('content')
@include('inc.header-filter-new')
<section class="about-section">
	<div class="max-fix-width">
		<div class="link-menu">
			<span class="arrow-l">
				<i class="fas fa-arrow-left"></i>
			</span>
			<span>
				@if($service->category)
				<i class="fas fa-caret-right"></i><a href="#">{{$service->category->name_lt}}</a>
				@endif
				<i class="fas fa-caret-right"></i>
			</span>
			<span class="l-sub-cat service-title">{{$service->title}}</span>
		</div>
		<div class="about-top-tlt ">
			<div class="service-logo"><img src="{{asset('assets/img/services/' . $service->image)}}" alt="img"></div>
			<h1 class="service-title">{{$service->title}}</h1>
			<div class="about-star">
				<div class="">
					<span>
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/star.png')}}" alt="img">
						<img src="{{asset('assets/img/services/half-star.png')}}" alt="img">
					</span>
					<span class="font-weight-bold">4.5/5</span>
				</div>
				<div class=""><span> 21 klientų įvertinimai </span>
				</div>
			</div>
			<button class="love-btn"><i class="fas fa-heart"></i> ĮSIMINTI</button>
		</div>
		<div class="row mar-0">
			<div class="col-12 col-lg-9">
				<div class="about-cnt mar-b-30">
					<div class="about-tlt">
						<input type="hidden" name="service_id_h" value="{{ $service->id }}">
					</div>
					<div class="about-txt">
						<p>
							<span><i class="fas fa-exclamation-circle"></i></span>
							<span class="service-description"> {!! $service->description !!}</span>
							<span class="read-more">read more</span>
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="cat-nav">
							@foreach($user_category as $val)
							<a href="{{$val['url']}}">
								<span class="cat-n-li {{ ($val['sid'] == $service->id) ? 'active' : '' }}"><img src="{{asset('assets/img/services/' . $val['img'])}}" alt="img"></span>
							</a>
							@endforeach
							<div class="short-select">
								<div class="btn-group d-inline-block d-md-none">
									<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<img src="{{asset('assets/img/sort-icon.png')}}" alt="img">
									</button>
									<div class="dropdown-menu dropdown-menu-right">
										<button class="dropdown-item" type="button">Naujausi viršuje</button>
										<button class="dropdown-item" type="button">Naujausi viršuje</button>
									</div>
								</div>
								<div class="ctm-select-srt d-none d-md-flex" style="align-items: center;">
									<span class="label-txt">Rikiavimas:</span>
									<div class="ctm-select" ctm-slt-n="cat_name">
										<div class="ctm-select-txt pad-l-10">
											<span class="select-txt" id="category">Naujausi viršuje</span>
											<span class="select-arr"><img src="{{asset('assets/img/services/triangle-d.png')}}" alt="img"></span>
										</div>
										<div class="ctm-option-box">
											<div class="ctm-option">Naujausi viršuje</div>
											<div class="ctm-option">Naujausi viršuje</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="apointment-area">
							<view-calendar></view-calendar>
						</div>
					</div>
				</div>
				<div class="about-nav">
					<div class="gallery" style="display:none;">
						<!-- <div class="single-content gallery-item wh-100">
							<div class="plus-c"><i class="fas fa-plus"></i></div>
						</div> -->
						@foreach($service->serviceGalleryTabs as $val)
						@foreach($val->serviceGalleries as $val1)
						<div class="single-content gallery-item" data-gallery-tag="{{ $val['tab'] }}">
							<a href="{{ asset('assets/img/services/galleries/' . $val1['img']) }}" data-fancybox="gallery">
								<div class="content-img-p">
									<div class="content-img">
										<img src="{{ asset('assets/img/services/galleries/' . $val1['img']) }}" alt="img" />
									</div>
								</div>
							</a>
						</div>
						@endforeach
						@endforeach
					</div>
					<div class="more"><button class="btn more-btn">@lang('lang.more_show')</button></div>
				</div>
				<div class="comment-s">
					<div class="com-tlt">
						<h1><img src="{{asset('assets/img/services/com-i.png')}}" alt="img"> Comments(<span id="total_comment">{{$total_comments}}</span>)</h1>
						<div class="short-select">
							<div class="btn-group d-inline-block d-md-none">
								<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img src="{{asset('assets/img/sort-icon.png')}}" alt="img">
								</button>
								<div class="dropdown-menu dropdown-menu-right">
									<button class="dropdown-item" type="button">Naujausi viršuje</button>
									<button class="dropdown-item" type="button">Naujausi viršuje</button>
								</div>
							</div>
							<div class="ctm-select-srt d-none d-md-flex" style="align-items: center;">
								<span class="label-txt">Rikiavimas:</span>
								<div class="ctm-select" ctm-slt-n="cat_name">
									<div class="ctm-select-txt pad-l-10">
										<span class="select-txt" id="category">Naujausi viršuje</span>
										<span class="select-arr"><img src="{{asset('assets/img/services/triangle-d.png')}}" alt="img"></span>
									</div>
									<div class="ctm-option-box">
										<div class="ctm-option">Naujausi viršuje</div>
										<div class="ctm-option">Naujausi viršuje</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@if(Auth::check())
					@if($total_booking > $total_main_comment)
					<form id="add-service-comment">
						@csrf
						<input type="hidden" name="main_service_id" id="main_service_id" value="{{$service->id}}">
						<input type="hidden" name="main_star_number" id="main_star_number" value="">
						<div class="com-text-ar">
							<div class="com-text-in">
								<div class="com-img"><img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img"></div>
								<div class="form-group">
									<label><span>Įvertinkite:</span>
										<span class="fa fa-star service-star" star-id="1" id="star-1"></span>
										<span class="fa fa-star service-star" star-id="2" id="star-2"></span>
										<span class="fa fa-star service-star" star-id="3" id="star-3"></span>
										<span class="fa fa-star service-star" star-id="4" id="star-4"></span>
										<span class="fa fa-star service-star" star-id="5" id="star-5"></span>
									</label>
									<div class="input-group m-input-c d-block d-md-none">
										<textarea placeholder="Rašykite komentarą..." class="form-control" id="user_comment"></textarea>
										<div class="input-group-append">
											<span class="input-group-text" id="basic-addon2"><img src="{{asset('assets/img/send-icon.png')}}" alt="img"></span>
										</div>
									</div>
									<textarea placeholder="Rašykite komentarą..." class="form-control d-none d-md-block" id="user_comment"></textarea>
								</div>
							</div>
							<div class="form-group com-btn d-none d-md-block">
								<button class="p-btn btn send" type="submit">@lang('lang.send')</button>
							</div>
						</div>
					</form>
					@endif
					@endif
					<div class="service-main-comment-div">
						@if(count($comments)>0)
						@foreach ($comments as $comment)
						<div class="com-reply">
							<div class="comment" id="main-cmt">
								<div class="row">
									<div class="user-img-c">
										<img src="{{asset('userImage/fixPic/'.$comment['user']['profile_image'])}}" alt="user-img">
									</div>
									<div class="col-12 pad-0">
										<div class="comment-cnt">
											<div class="user-time">
												<span class="com-rew-s">
													@if($comment['star'] == 1)
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													@elseif($comment['star'] == 2)
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													@elseif($comment['star'] == 3)
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													@elseif($comment['star'] == 4)
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													@elseif($comment['star'] == 5)
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													<img class="star-n" src="{{asset('assets/img/services/star.png')}}" alt="img">
													@endif
												</span>
												<span class="user-n">{{$comment['user']['name']}}</span>
												<span class="com-t">{{ \Carbon\Carbon::parse($comment['created_at'])->diffForHumans() }}</span>
											</div>
											<p>{!! $comment['comment'] !!}</p>
											<div class="main-comment-replay-box-{{$comment['id']}}">
												@php
												$isReplay = App\Http\Controllers\LiveZoneController::isServiceReplayAvaible($comment['id'], $service->id, $service->user->id);
												@endphp
												@if($isReplay == 'yes')
												<div class="edit-com reply-box-s"><span>Reply</span></div>
												<form id="add-service-reply" class="reply-form-s">
													@csrf
													<input type="hidden" name="main_service_id" id="main_service_id" value="{{$service->id}}">
													<input type="hidden" name="main_comment_id" id="main_comment_id" value="{{$comment['id']}}">
													<div class="com-text-ar reply-text-ar">
														<div class="com-text-in">
															<div class="com-img"><img  src="{{asset('userImage/fixPic/'.auth()->user()->profile_image)}}" alt="img"></div>
															<div class="form-group">
																<label><span>{{auth()->user()->name}}</span></label>
																<textarea placeholder="Rašykite komentarą..." class="form-control" id="user_reply" name="main_service_replay"></textarea>
															</div>
														</div>
														<div class="form-group mb-0 com-btn">
															<button class="p-btn btn send" type="submit">Reply</button>
														</div>
													</div>
												</form>
												@endif
											</div>
										</div>
										<div class="main-comment-replay-{{$comment['id']}}">
											@if(count($comment['comment_replay']) > 0)
											@foreach($comment['comment_replay'] as $reply)
											<div class="cmt-replay">
												<div class="reply">
													<div class="row">
														<div class="user-img-c">
															<img src="{{asset('userImage/fixPic/'.$reply['user']['profile_image'])}}" alt="user-img">
														</div>
														<div class="col-12 pad-0">
															<div class="comment-cnt">
																<div class="user-time">
																	<span class="user-n">{{$reply['user']['name']}}</span>
																	<span class="com-t">{{ \Carbon\Carbon::parse($reply['created_at'])->diffForHumans() }}</span>
																</div>
																<p>{!! $reply['replay']!!}</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											@endforeach
											@endif
										</div>



									</div>
								</div>
							</div>
						</div>
						@endforeach
						@endif
					</div>
					<div class="load-more-service-comment">
						@if($total_main_comment > 2)
						<span onclick="getServiceComment(event, {{$service->id}}, 2,{{$service->user->id}},{{$total_main_comment}})">View {{$total_main_comment - 2}} more comments</span>
						@endif
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-3">
				<div class="model-tlt">
					<img src="{{asset('userImage/fixPic/' . $service->user->profile_image)}}" alt="img">
					<div class="pro-btn">
						<a href="{{ route('publicProfile', [App\Services\MakeUrlService::url($service->user->name), $service->user->id]) }}">
							<button class="p-btn btn">{{$service->user->name}} {{$service->user->surname}} - Profle</button>
						</a>
					</div>
				</div>
				<div class="model-bg-img" onclick="changeServiceImage(event)">
					<div class="model-s-img">
						@if($service['image'])
						<img src="{{asset('assets/img/services/'. $service['image'])}}" class="wth-100-p" alt="service-img">
						@else
						<img src="{{asset('assets/img/services/no_service.png')}}" class="wth-100-p" alt="service-img">
						@endif
					</div>
				</div>
				<div class="pro-box">
					<div class="pro-cnt">
						<!-- <div class="pro-name">{{$service->user->name}} {{$service->user->surname}}</div> -->
						<div class="pro-m-info">
							<div class="pro-tlt">Susisiekite:</div>
							<div class="pro-email"><i class="fa fa-envelope"></i>{{$service->user->email}}</div>
							<div class="pro-phn"><i class="fas fa-mobile-alt"></i>{{$service->user->phone}}</div>
							<div class="pro-cn"><i class="fas fa-map-marker-alt"></i>{{$service->address}}</div>
						</div>
						<div class="pro-m-info">
							<div class="pro-tlt">Darbo valandos:</div>
							<div class="pro-m-time">
								@if($service->user->businessHours)
								<div class="pro-m-time1 pro-t">
									@if($service->user->businessHours()->first())
									@foreach($service->user->businessHours()->first()->runningHours()->orderBy('day_no', 'asc')->get() as $hour)
									<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> {{($hour->start) ? Carbon\Carbon::parse($hour->start)->format('H:i') : ''}} - {{($hour->end) ? Carbon\Carbon::parse($hour->end)->format('H:i') : ''}}</div>
									@endforeach
									@endif
								</div>
								<div class="pro-m-time2 pro-t">
									@if($service->user->businessHours()->first())
									@foreach($service->user->businessHours()->first()->breakHours()->orderBy('day_no', 'asc')->get() as $hour)
									<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> {{($hour->start) ? Carbon\Carbon::parse($hour->start)->format('H:i') : ''}} - {{($hour->end) ? Carbon\Carbon::parse($hour->end)->format('H:i') : ''}}</div>
									@endforeach
									@endif
								</div>
								@else
								<div class="pro-m-time1 pro-t">
									<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/cal.png')}}" alt="img"> - </div>
								</div>
								<div class="pro-m-time2 pro-t">
									<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
									<div><img src="{{asset('assets/img/services/break-i.png')}}" alt="img"> - </div>
								</div>
								@endif
							</div>
						</div>
						<div class="social-icon">
							<ul>
								<li class="{{ ($service->user->facebook) ? '' : 'd-none' }}"><a class="fb" href="{{ $service->user->facebook }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
								<li class="{{ ($service->user->twitter) ? '' : 'd-none' }}"><a class="tw" href="{{ $service->user->twitter }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
								<li class="{{ ($service->user->gmail) ? '' : 'd-none' }}"><a class="go" href="{{ $service->user->gmail }}" target="_blank"><i class="fab fa-goodreads-g"></i></a></li>
								<li class="{{ ($service->user->linkedin) ? '' : 'd-none' }}"><a class="lin" href="{{ $service->user->linkedin }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
								<li class="{{ ($service->user->pinterest) ? '' : 'd-none' }}"><a class="pin" href="{{ $service->user->pinterest }}" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
							</ul>
						</div>
						@php
						$full_name = '';
						if(@$service->user->name){
						$full_name .= @$service->user->name.' ';
					}
					if(@$service->user->surname){
					$full_name .= @$service->user->surname .' ';
				}
				if(@$service->user->nickname){
				$full_name .= '('.@$service->user->nickname.')';
			}
			$full_name = preg_replace('!\s+!', ' ', $full_name);
			$full_name = str_replace(' ', '-', $full_name);
			@endphp
		</div>
	</div>
	<div class="g-map">
		<div class="mapouter">
			<div class="gmap_canvas" id="map">

			</div>
		</div>
	</div>

	@if(count($allVip)>0)
	  <div class="side-blog">
    <section class="blog-section">
      <div class="blog-sidbar">
        <div class="side-tlt">
          <div class="side-txt">SKELBIMAI</div>
          <img src="{{asset('assets/img/services/vip-sm.png')}}" alt="img">
        </div>

        <div class="row m-0">
          @foreach ($allVip as $vip)
          @if(property_exists($vip,'title'))
          @php
          $page_title = $vip->title;
          $page_title = preg_replace('!\s+!', ' ', $page_title);
          $page_title = str_replace(' ', '-', $page_title);
          $see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($vip->title, $vip->service_main_id);
          @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                   @if($vip->image)
                    <img src="{{asset('assets/img/services/'.$vip->image)}}" alt="img">
                    @else 
                      <img src="{{asset('assets/img/services/no_service.png')}}" alt="img">
                    @endif
                </div>
              </div>
              <div class="content-des">
                <div class="des-itm">
                  <div class="itm-name-all">
                    @php
                    $inners = App\Http\Controllers\SearchServiceController::serviceAndProfileCategory($vip->inner_category,$vip->sub_category_id);
                    @endphp
                    @if(count($inners['category']) > 0)
                    @foreach ($inners['category'] as $inner)
                    <div class="itm-name">{{$inner->name_lt}}</div>
                    @endforeach
                    @endif
                  </div>
                  <div class="itm-dis">
                    <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance, 2)}} km</div>
                    <div class="dis-txt"><span><a href="{{$see_more}}">Daugiau</a></span></div>
                  </div>
                </div>
              </div>
              <div class="content-tlt">
                <a href="{{$see_more}}" onclick="recentlyView('{{$vip->service_main_id}}')">{{ $vip->title }}</a>
              </div>
            </div>
          </div>
          @else
           @php
            $userCats = App\Http\Controllers\SearchServiceController::ProfileCategory($vip->user_main_id);
            @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                  @if($userCats['cover_img'])
                  <img src="{{asset('userImage/coverPic/'.$userCats['cover_img']->img)}}" alt="img">
                  @else 
                   <img src="{{asset('assets/img/default_cover.png')}}" alt="img">
                  @endif
                </div>
                <div class="s-pro-img">
                  @if($vip->profile_image)
                  <img src="{{asset('userImage/fixPic/'.$vip->profile_image)}}" alt="img">
                  @else 
                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif
                </div>
              </div>
              @php
                  $full_name = '';
                  if(@$vip->name){
                    $full_name .= @$vip->name.' ';
                  }
                  if(@$vip->surname){
                    $full_name .= @$vip->surname.' ';
                  }
                  if(@$vip->nickname){
                    $full_name .= '('.@$vip->nickname.')';
                   }
                  $full_name = preg_replace('!\s+!', ' ', $full_name);
                  $full_name = str_replace(' ', '-', $full_name);

            @endphp
        <div class="content-des">
          <div class="des-itm">
            <div class="itm-name-all">
              @if(count($userCats['cats']) > 0)
              @foreach($userCats['cats'] as $cat)
              <div class="itm-name">{{$cat->name_lt}}</div>
              @endforeach
              @endif
              
            </div>
            <div class="itm-dis">
              <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance,2)}} km</div>
              <div class="dis-txt"><span><a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">Daugiau</a></span></div>
            </div>
          </div>
        </div>
        <div class="content-tlt">
          <a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">{{ $vip->surname }}</a>
        </div>
      </div>
    </div>

    @endif
    @endforeach
  </div>
</div>
</section>
</div>
@endif

</div>
</div>
</div>
</section>

@endsection
@section('script')
<script src="{{ asset('assets/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('assets/js/cropper.js')}}"></script>
<script src="{{ asset('assets/js/maugallery.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>

<script>
	$(document).on('click','.service-star',function(){
		var starId = $(this).attr('star-id');
		$('#main_star_number').val(starId);
		if(starId == 5){
			$('#star-1').addClass('star-checked');
			$('#star-2').addClass('star-checked');
			$('#star-3').addClass('star-checked');
			$('#star-4').addClass('star-checked');
			$('#star-5').addClass('star-checked');
		}else if(starId == 4){
			$('#star-1').addClass('star-checked');
			$('#star-2').addClass('star-checked');
			$('#star-3').addClass('star-checked');
			$('#star-4').addClass('star-checked');
			$('#star-5').removeClass('star-checked');
		}else if(starId == 3){
			$('#star-1').addClass('star-checked');
			$('#star-2').addClass('star-checked');
			$('#star-3').addClass('star-checked');
			$('#star-4').removeClass('star-checked');
			$('#star-5').removeClass('star-checked');
		}else if(starId == 2){
			$('#star-1').addClass('star-checked');
			$('#star-2').addClass('star-checked');
			$('#star-3').removeClass('star-checked');
			$('#star-4').removeClass('star-checked');
			$('#star-5').removeClass('star-checked');
		}else{
			$('#star-1').addClass('star-checked');
			$('#star-2').removeClass('star-checked');
			$('#star-3').removeClass('star-checked');
			$('#star-4').removeClass('star-checked');
			$('#star-5').removeClass('star-checked');
		}

	});

	$(document).on('click','.reply-box-s',function(e){
		$(this).siblings('.reply-form-s').slideToggle(400);
	});

	$(document).on('submit','#add-service-comment',function(e){
		var star = $('#main_star_number').val();
		var text = $.trim($('#user_comment').val());
		var service_id = $('#main_service_id').val();;
		if(star > 0 && text !=''){
			var data = new FormData();
			data.append('text',text);
			data.append('star',star);
			data.append('service_id',service_id);
			$.ajax( {
				processData: false,
				contentType: false,
				data: data,
				type: 'POST',
				url:'{{url("user/service/save-service-comment")}}',
				success: function( response ){
					$('.service-main-comment-div').prepend(response.html);
					$('#main_star_number').val('');
					$('#user_comment').val('');
					$('.service-star').removeClass('star-checked');
					if(response.hasComment == 0){
						$('#add-service-comment').hide();
					}
					var get_total = $.trim($('#total_comment').text());
					var new_total = parseInt(get_total) + 1;
					$('#total_comment').text(new_total);
				}

			}); 

		}else{
			alert('pleas leave a star and comment');
		}
		return false;
	});

	$(document).on('submit','#add-service-reply',function(){
		//var data = new FormData( $(this)[ 0 ] );
		var text = $.trim($(this).find('#user_reply').val());
		var comment_id = $(this).find('#main_comment_id').val();
		var service_id = $(this).find('#main_service_id').val();
		if(text !=''){
			var data = new FormData();
			data.append('text',text);
			data.append('comment_id',comment_id);
			data.append('service_id',service_id);
			var $t = $(this);
			$.ajax( {
				processData: false,
				contentType: false,
				data: data,
				type: 'POST',
				url:'{{url("user/service/save-service-comment-replay")}}',
				success: function( response ){
					$('.main-comment-replay-'+comment_id).append(response);
					$('.main-comment-replay-box-'+comment_id).hide();
					var get_total = $.trim($('#total_comment').text());
					var new_total = parseInt(get_total) + 1;
					$('#total_comment').text(new_total);
				}

			}); 

		}else{
			alert('pleas leave a comment');
		}
		return false;
	});

	function getServiceComment(e, service_id,offset,user_id,total_comment)
	{

		var data = new FormData();
		data.append('offset',offset);
		data.append('service_id',service_id);
		data.append('user_id',user_id);
		$.ajax( {
			processData: false,
			contentType: false,
			data: data,
			type: 'POST',
			url:'{{url("user/service/load-more-service-comment")}}',
			success: function( response ){
				$('.service-main-comment-div').append(response);
				var showed = parseInt(offset) + 10;
				var new_total = parseInt(total_comment) - showed;
				if(parseInt(total_comment) > showed){
					var html = '<span onclick="getServiceComment(event, '+service_id+', '+showed+','+user_id+','+total_comment+')">View '+new_total+' more comments</span>';
					$('.load-more-service-comment').html(html);
				}else{
					$('.load-more-service-comment').empty();
				}

			}

		}); 
	}
</script>


<script>
	$(function() {
		$('.gallery').mauGallery({
			columns: {
				xs: 1,
				sm: 2,
				md: 3,
				lg: 4,
				xl: 6
			},
			lightBox: true,
			lightboxId: 'myAwesomeLightbox',
			showTags: true,
			tagsPosition: 'top'
		});
	});
	window.change_service_details = "<?= route('user.changeServiceDetails')  ?>";
	window.service_tmp_img = "<?= route('user.serviceTmpImg')  ?>";
	window.service_single_tmp_remove = "<?= route('user.serviceTmpImgRemove')  ?>";
	window.change_service_sub_category = "<?= route('user.changeServiceSubCategory')  ?>";
	window.change_service_inner_category = "<?= route('user.changeServiceInnerCategory')  ?>";
	window.save_service_category = "<?= route('user.saveServiceCategory')  ?>";
	window.change_service_glry_tab = "<?= route('user.saveServiceGlryTab')  ?>";
	window.get_service_glry_tab = "<?= route('user.getServiceGlryTab')  ?>";
	window.service_gallery_img_upload = "<?= route('user.serviceGalleryImgUpload')  ?>";
	window.service_gallery_single_tmp_remove = "<?= route('user.serviceGalleryTmpImgRemove')  ?>";
	window.add_service_gallery_image = "<?= url('user/add-service-gallery-image') ?>";
	window.base_url = "<?= url('/') ?>";
</script>
<script>
	function goActive() {
		// add filtering code here
		var url = '<?= url("select-plan/" . $service["id"]) ?>';
		window.location.href = url;
	}
	$(function() {
		var autocomplete;
		autocomplete = new google.maps.places.Autocomplete((document.getElementById('autoAddress')), {
			types: ['geocode'],
		});

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var near_place = autocomplete.getPlace();
			var data = new FormData();
			data.append('address', $('#autoAddress').val());
			data.append('lat', near_place.geometry.location.lat());
			data.append('lng', near_place.geometry.location.lng());
			data.append('service_id', "{{$service['id']}}");
			var city = '';
			for (var i = 0; i < near_place.address_components.length; i++) {
				for (var j = 0; j < near_place.address_components[i].types.length; j++) {
					if (near_place.address_components[i].types[j] == 'administrative_area_level_2') {
						city = near_place.address_components[i].long_name;
					}
				}
			}
			data.append('city', city);
			$.ajax({
				processData: false,
				contentType: false,
				data: data,
				type: 'POST',
				url: '{{url("user/service-address")}}',
				success: function(response) {
					serviceMap(near_place.geometry.location.lat(), near_place.geometry.location.lng());
					$('.set-address').text($('#autoAddress').val());
					//console.log(response);
				}

			});
		});

	});
</script>
<script>

	$(function() {
		var lat = '{{$service["lat"]}}';
		var lng = '{{$service["lng"]}}';
		serviceMap(lat, lng);

	});

	function serviceMap(lat, lng) {
		lat = parseFloat(lat);
		lng = parseFloat(lng);
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {
				lat: lat,
				lng: lng
			},
			zoom: 8
		});
		var marker = new google.maps.Marker({
			position: {
				lat: lat,
				lng: lng
			},
			map: map,
			title: 'My Location'
		});

		var mainContent = '';
		mainContent += '<div style="color:#1014a0;font-size:17px; text-align:center;margin-bottom:5px"><?= $service['title'] ?></div>';
		mainContent += '<div style="color:#1014a0;font-size:17px; text-align:center;margin-bottom:5px"><?= $service->user->name ?>&nbsp;<?= $service->user->surname ?>&nbsp;<?= ($service->user->nickname) ? '(' . $service->user->nickname . ')' : '' ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fa fa-envelope" style="margin-right:6px;"></i> <?= $service->user->email ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fas fa-mobile-alt" style="margin-right:6px;"></i><?= $service->user->phone ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fas fa-map-marker-alt" style="margin-right:6px;"></i><?= $service->user->address ?></div>';

		var infowindow = new google.maps.InfoWindow();

		marker.addListener('click', function() {
			infowindow.setContent(mainContent);
			infowindow.open(map, marker);
		});
	}
</script>
@endsection
@section('wondowv')
<script>
	window.base_url = "<?= url('/') ?>";
	window.c_user_id = <?= ($c_user) ? @$c_user->id : 0 ?>;
	window.c_user = <?= $c_user ?>;
	window.service = <?= json_encode($service) ?>;
	window.window.s_id = <?= $s_id ?>;
</script>


@endsection