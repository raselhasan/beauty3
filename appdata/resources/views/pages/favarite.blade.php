 @extends('layout.app')
 @section('title')
 Favorites | GrožioKalviai
 @endsection
 @section('style')
 <style type="text/css">
 	.pagination {
 		justify-content: center;
 	}

 	.pagination>li>a {
 		color: #e2dede !important;
 	}

 	.page-link {
 		background-color: #1a1a1a !important;
 		border: 1px solid #333 !important;
 	}

 	.page-item.active .page-link {
 		z-index: 3;
 		color: #fff;
 		background-color: #ff8f4c !important;
 		border-color: #ff8f4c !important;
 	}

 	.seeMore {
 		color: #e25b37;
 	}

 	.single-content1 .content-img-p {
 		padding-bottom: 63% !important;
 	}

 	.single-content .content-img-p {
 		padding-bottom: 56%;
 	}

 	.single-content {
 		margin-bottom: 29px;
 	}

 	.vip {
 		float: right;
 		font-size: 13px;
 		color: #e24e2a;
 		border: 1px solid #e24e2a;
 		padding: 5px;
 	}

 	.star {
 		border: 1px solid #e24e2a;
 	}

 	.heart {
 		margin-left: 10px;
 		cursor: pointer;
 	}
 	.c-heart{
 		cursor: pointer;
 	}

 	.c-white {
 		color: #fff !important;
 	}

 	.c-red {
 		color: red !important;
 	}
 	.put-new-border{
 		border: 2px solid #ff8f4c !important;
 	}
 	.s-pro-img {
            position: absolute;
            height: 140px;
            width: 140px;
            border-radius: 50%;
            top: 50%;
            left: 50%;
            overflow: hidden;
            transform: translate(-50%, -50%);
            border: 2px solid #fff;
        }
        .s-pro-img img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
 </style>
 @endsection
 @section('content')
 @include('inc.header-filter-new')
 <section class="blog-section">
 	<div class="max-fix-width">
 		@if(count($services)>0)
 		<div class="row mar-0" id="services">
 			<div class="col-12 col-lg-9 pl-0">
 				<div class="short-s">
 					<div class="short-txt"><img src="{{asset('assets/img/services/triangle-r.png')}}" alt="img">@lang('lang.plaukai')</div>
 					<div class="short-select">
 						
 						
 					</div>
 				</div>
 				<div class="service-list-all">
 					@if(count($services)>0)
 						@foreach ($services as $service)
 							@php
								$see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($service->title, $service->id);
								$profile_link = App\Http\Controllers\SearchServiceController::profileLink($service->user->id, $service->user->name);
							@endphp

 							<div class="service-list @if($service->HighlightService) put-new-border @endif del-main-service-{{$service->id}}">
 								<div class="service-list-img">
 									<a href="{{ $profile_link }}">
 										<div class="service-logo">

 										@if($service->user->profile_image)
	 											<img src="{{asset('userImage/fixPic/' . $service->user->profile_image)}}" alt="img">
	 										@else 
	 										<img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">

	 										@endif

 										</div>
 									</a>
 									<div class="service-user-name">{{$service->user->name}} {{$service->user->surname}}</div>
 									@if($service->TakeVipService)
 									<div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
 									@endif
 									<div class="active-div">
 										<img src="{{asset('assets/img/services/active-add.png')}}">
 									</div>

 									<a href="{{$see_more}}" onclick="recentlyView('{{$service->id}}')">

 										@if($service->image)
	 									<img class="main-img" src="{{asset('assets/img/services/'.$service->image)}}">
	 									@else 
										<img class="main-img" src="{{asset('assets/img/services/no_service.png')}}">
	 									@endif
 									</a>

 									<div class="nav-img">
 										<div class="swiper-container blog-container">
 											<div class="swiper-wrapper">
 												@php
 													$stabs = App\ServiceGalleryTab::where('service_id', $service->id)->pluck('id');
 													$gimage = App\ServiceGallery::whereIn('service_gallery_tab_id', $stabs)->pluck('img')->random(4);
 												@endphp
		 										@foreach($gimage as $img)
		 											<div class="swiper-slide">
		 												<span class="li-img"><img src="{{asset('assets/img/services/galleries/' . $img)}}"></span>
		 											</div>
		 										@endforeach
 											</div>
 											<div class="swiper-button-next ctm-next-prev swiper-button-next1"><img src="{{ asset('assets/img/services/ar-r.png') }}" alt="Left Arrow Key"></div>
 											<div class="swiper-button-prev ctm-next-prev swiper-button-prev1"><img src="{{ asset('assets/img/services/ar-l.png') }}" alt="Right Arrow Key"></div>
 										</div>
 									</div>
 								</div>
 								<div class="service-list-cnt">
 									<a href="{{$see_more}}" onclick="recentlyView('{{$service->id}}')">
 										<div class="service-list-tlt">
 											{{$service->title}}
 										</div>
 									</a>
 									<div class="service-list-price">
 										<div class="service-list-price-tbl">
 											<table class="table">
 												<tbody>
 													@php
 														$tn = 0;
 													@endphp
 													@foreach($service->types as $type)
 														<tr>
 															<td class="service-name">{{ $type->name }}</td>
 															<td class="service-time"><i class="far fa-clock"></i>
	 															@php
	 																$hour = floor($type->time / 60);
	 																$minute = $type->time % 60;
	 															@endphp
	 															@if($hour > 0)
	 																{{ $hour . ' Hour'  }}
	 															@endif
	 															@if($minute > 0)
	 																{{ $minute . ' Minute'  }}
	 															@endif
 															</td>
			 												<td class="service-price text-right">
			 													<!-- <span class="o-price"><del>35.00 €</del></span> -->
			 													<span class="n-price">nuo {{ $type->price }} €</span>
			 												</td>
			 												<td class="text-right pr-0">
			 													<a href="{{route('user.myService', [$service->title, $service->id])}}">
			 														<span class="plus-i"><i class="fas fa-plus"></i></span>
			 													</a>
			 												</td>
 														</tr>
 														@php
 															$tn++;
 														@endphp
 														@if($tn > 4)
 															@break
 														@endif
 													@endforeach
 												</tbody>
 											</table>
 										</div>
 										<div class="more-txt text-right {{ ($service->types->count() < 5) ? 'd-none': '' }}"><span class="s-m"><a href="{{$see_more}}">@lang('lang.see_more')</a></span></div>

 										<div class="more-txt {{ ($service->types->count() < 4) ? '': 'd-none' }}">
 											<i class="fas fa-exclamation-circle"></i> {!! substr($service->description, 0, 400); !!}
 											<span class="s-m">
 												<a href="{{$see_more}}" class="seeMore" onclick="recentlyView('{{$service->id}}')">
 													@lang('lang.see_more')
 												</a>
 											</span>
 										</div>
 									</div>
 									<div class="service-list-footer">
 										<span class="auth">
 											<a href="{{ $profile_link }}">
 												<img src="{{asset('userImage/fixPic/' . $service->user->profile_image)}}" alt="img"> {{$service->user->name}} {{$service->user->surname}}
 											</a>
 										</span>
 										<span class="dis"><i class="fa fa-map-marker-alt"></i>{{number_format(@$service->distance, 2)}}</span>
 										<span class="s-point"><i class="fas fa-star"></i> 4/9 (5)</span>
 										<span class="tick"><i class="fas fa-check-circle"></i>YRA VIETŲ</span>
 										<i class="fa fa-heart c-heart c-red" action="remove" id="{{$service->id}}"></i>
 										<span class="service-list-point">
 											<span class="r-txt">@lang('lang.visos_paslaugos')</span>
 										</span>
 									</div>
 								</div>
 							</div>
 						@endforeach
 					@endif
 					{{-- <div>
 						{{$services->render()}}
 					</div> --}}
 				</div>
 			</div>
 			
 			<div class="col-12 mt-3 mt-sm-0 col-lg-3 pr-0">
 				  <div class="side-blog">
    <section class="blog-section">
      <div class="blog-sidbar">
        <div class="side-tlt">
          <div class="side-txt">SKELBIMAI</div>
          <img src="{{asset('assets/img/services/vip-sm.png')}}" alt="img">
        </div>

        <div class="row m-0">
          @foreach ($allVip as $vip)
          @if(property_exists($vip,'title'))
          @php
          $page_title = $vip->title;
          $page_title = preg_replace('!\s+!', ' ', $page_title);
          $page_title = str_replace(' ', '-', $page_title);
          $see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($vip->title, $vip->service_main_id);
          @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                   @if($vip->image)
                    <img src="{{asset('assets/img/services/'.$vip->image)}}" alt="img">
                    @else 
                      <img src="{{asset('assets/img/services/no_service.png')}}" alt="img">
                    @endif
                </div>
              </div>
              <div class="content-des">
                <div class="des-itm">
                  <div class="itm-name-all">
                    @php
                    $inners = App\Http\Controllers\SearchServiceController::serviceAndProfileCategory($vip->inner_category,$vip->sub_category_id);
                    @endphp
                    @if(count($inners['category']) > 0)
                    @foreach ($inners['category'] as $inner)
                    <div class="itm-name">{{$inner->name_lt}}</div>
                    @endforeach
                    @endif
                  </div>
                  <div class="itm-dis">
                    <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance, 2)}} km</div>
                    <div class="dis-txt"><span><a href="{{$see_more}}">Daugiau</a></span></div>
                  </div>
                </div>
              </div>
              <div class="content-tlt">
                <a href="{{$see_more}}" onclick="recentlyView('{{$vip->service_main_id}}')">{{ $vip->title }}</a>
              </div>
            </div>
          </div>
          @else
           @php
            $userCats = App\Http\Controllers\SearchServiceController::ProfileCategory($vip->user_main_id);
            @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                  @if($userCats['cover_img'])
                  <img src="{{asset('userImage/coverPic/'.$userCats['cover_img']->img)}}" alt="img">
                  @else 
                   <img src="{{asset('assets/img/default_cover.png')}}" alt="img">
                  @endif
                </div>
                <div class="s-pro-img">
                  @if($vip->profile_image)
                  <img src="{{asset('userImage/fixPic/'.$vip->profile_image)}}" alt="img">
                  @else 
                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif
                </div>
              </div>
              @php
                  $full_name = '';
                  if(@$vip->name){
                    $full_name .= @$vip->name.' ';
                  }
                  if(@$vip->surname){
                    $full_name .= @$vip->surname.' ';
                  }
                  if(@$vip->nickname){
                    $full_name .= '('.@$vip->nickname.')';
                   }
                  $full_name = preg_replace('!\s+!', ' ', $full_name);
                  $full_name = str_replace(' ', '-', $full_name);

            @endphp
        <div class="content-des">
          <div class="des-itm">
            <div class="itm-name-all">
              @if(count($userCats['cats']) > 0)
              @foreach($userCats['cats'] as $cat)
              <div class="itm-name">{{$cat->name_lt}}</div>
              @endforeach
              @endif
              
            </div>
            <div class="itm-dis">
              <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance,2)}} km</div>
              <div class="dis-txt"><span><a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">Daugiau</a></span></div>
            </div>
          </div>
        </div>
        <div class="content-tlt">
          <a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">{{ $vip->surname }}</a>
        </div>
      </div>
    </div>

    @endif
    @endforeach
  </div>
</div>
</section>
</div>
 			</div>
 		</div>
 	</div>
 	@else 
		<div class="col-12 col-md-12">
 				<div class="alert alert-primary" role="alert">
 					Sorry :) No data found.
 				</div>
 		</div>
 	@endif


 	</div>
</section>

 <section class="bg-dark-sec">
 	@if(count(@$item['recently_viewed'])>0)
 	<section class="section2">
 		<div class="section-title">@lang('lang.palaugos_salia_taves')</div>
 		<div class="row mar-0">
 			<div class="col-12">
 				<div class="swiper-container ctm-container3">
 					<div class="swiper-wrapper">

 						@foreach (@$item['recently_viewed'] as $viewed)
 						@php
				          $page_title = $viewed->title;
				          $page_title = preg_replace('!\s+!', ' ', $page_title);
				          $page_title = str_replace(' ', '-', $page_title);
				          $see_more1 = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($viewed->title, $viewed->id);
				          @endphp

 						<div class="swiper-slide">
 							<div class="side-blog" style="width: 100%">
							    <section class="blog-section p-0">
							      <div class="blog-sidbar">
							      <div class="single-content">
						              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
						              <div class="content-img-p">
						                <div class="content-img">
						                	@if($viewed->image)
						                    <img src="{{asset('assets/img/services/'.$viewed->image)}}" alt="img">
						                    @else 
						                      <img src="{{asset('assets/img/services/no_service.png')}}" alt="img">
						                    @endif
						                </div>
						              </div>
						              <div class="content-des">
						                <div class="des-itm">
						                  <div class="itm-name-all">
						                   @php
							                    $inners = App\Http\Controllers\SearchServiceController::serviceAndProfileCategory($viewed->inner_category,$viewed->sub_category_id);
							                    @endphp
							                    @if(count($inners['category']) > 0)
							                    @foreach ($inners['category'] as $inner)
							                    <div class="itm-name">{{$inner->name_lt}}</div>
							                    @endforeach
							                    @endif
						                  </div>
						                  <div class="itm-dis">
						                    <div class="dis-txt"><span><a href="{{$see_more1}}">Daugiau</a></span></div>
						                  </div>
						                </div>
						              </div>
						              <div class="content-tlt">
						                <a href="{{$see_more1}}" >{{ $viewed->title }}</a>
						              </div>
						            </div>
							      </div>
							     </section>

 						</div>
 						</div>
 						@endforeach
 					
 				</div>
 				
 					<div class="swiper-button-next swiper-button-next1" style="background:unset;"><img src="{{ asset('assets/img/right.png') }}" alt="Left Arrow Key"></div>
 					<div class="swiper-button-prev swiper-button-prev1" style="background:unset;"><img src="{{ asset('assets/img/left.png') }}" alt="Right Arrow Key"></div>
 				
 			</div>
 		</div>
 		{{-- <div class="more"><button class="btn more-btn">@lang('lang.visus_ancitos')</button></div> --}}
 	</section>
 	@endif

 	{{-- <section class="slider-section">
 		<div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
 			<div class="carousel-inner">
 				<div class="carousel-item active">
 					<img src="{{asset('assets/img/slider1.jpg')}}">
 					<div class="carousel-caption">
 						<h1>@lang('lang.third_slider_lavel')</h1>
 						<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 					</div>
 				</div>
 				<div class="carousel-item">
 					<img src="{{asset('assets/img/slider2.jpg')}}">
 					<div class="carousel-caption">
 						<h1>@lang('lang.third_slider_lavel')</h1>
 						<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 					</div>
 				</div>
 				<div class="carousel-item">
 					<img src="{{asset('assets/img/slider3.jpg')}}">
 					<div class="carousel-caption">
 						<h1>@lang('lang.third_slider_lavel')</h1>
 						<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 					</div>
 				</div>
 			</div>
 			<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
 				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
 				<span class="sr-only">@lang('lang.previous')</span>
 			</a>
 			<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
 				<span class="carousel-control-next-icon" aria-hidden="true"></span>
 				<span class="sr-only">@lang('lang.next')</span>
 			</a>
 		</div>
 	</section>
 </section> --}}
 @endsection
 @section('script')
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>
 <script>
 	var swiper = new Swiper('.swiper-container', {
 		slidesPerView: 4,
 		spaceBetween: 5,
 		loop: false,
 		allowTouchMove: false,
 		navigation: {
 			nextEl: '.swiper-button-next',
 			prevEl: '.swiper-button-prev',
 		},

 		breakpoints: {
 			450: {
 				slidesPerView: 4,
 			}
 		}
 	});

 	$('.blog-container .swiper-slide').on('click', function() {
 		var x = $(this).find(".li-img img").attr("src");
 		$(this).parents(".nav-img").siblings(".main-img").attr("src", x);
 	});
 	$('.service-logo').mouseover(function() {
 		$(this).parents('.service-list-img').find('.service-user-name').show();
 	})
 	$('.service-logo').mouseleave(function() {
 		$(this).parents('.service-list-img').find('.service-user-name').hide();
 	})
 </script>
  <script type="text/javascript">
 	$(document).ready(function() {
 		$(document).on('click', '.c-heart', function() {
 			var id = $.trim($(this).attr('id'));
 			var action = $.trim($(this).attr('action'));
 			$('#remove-' + id).remove();
 			if (action == 'add') {
 				$(this).removeClass('c-white');
 				$(this).addClass('c-red');
 				$(this).attr('action', 'remove');
 			} else {
 				$(this).removeClass('c-red');
 				$(this).addClass('c-white');
 				$(this).attr('action', 'add');
 			}
 			var data = new FormData();
 			data.append('id', id);
 			data.append('action', action);
 			$.ajax({
 				processData: false,
 				contentType: false,
 				data: data,
 				type: 'POST',
 				url: '{{url("favarite")}}',
 				success: function(response) {
 					$('.del-main-service-'+id).remove();
 				}

 			});
 		});
 	});
 </script>



 @endsection