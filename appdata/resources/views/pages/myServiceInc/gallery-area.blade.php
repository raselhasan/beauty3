<div class="gallery" style="display:none;">
    <div class="single-content gallery-item" onclick="changeServiceGalleryImage(event)">
        <div class="plus-c"><i class="fas fa-plus"></i></div>
    </div>
    @foreach($service->serviceGalleryTabs as $val)
    @foreach($val->serviceGalleries as $val1)
    <div class="single-content gallery-item" data-gallery-tag="{{ $val['tab'] }}">
        <span onclick="removeGalleryImg(event, <?= $val1->id ?>)" class="fa fa-times remveImg"></span>
        <a href="{{ asset('assets/img/services/galleries/' . $val1['img']) }}" data-fancybox="gallery">
            <div class="content-img-p">
                <div class="content-img">
                    <img src="{{ asset('assets/img/services/galleries/' . $val1['img']) }}" alt="img" />
                </div>
            </div>
        </a>
    </div>
    @endforeach
    @endforeach
</div>