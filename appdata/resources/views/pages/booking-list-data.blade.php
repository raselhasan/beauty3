<table id="bookingListTable" class="display" style="width:100%">
    <thead>
        <tr>
            <th class="d-none">si</th>
            <th>@lang('lang.service')</th>
            <th>@lang('lang.type')</th>
            <th>@lang('lang.worker')</th>
            <th>@lang('lang.date')</th>
            <th>@lang('lang.time')</th>
            <th>@lang('lang.action')</th>
        </tr>
    </thead>
    <tbody>
        @php $n = 1 @endphp
        @foreach($booking as $book)
        <tr>
            <td class="d-none"><?= $n++; ?></td>
            <td><?= $book->id; ?>{{ $book->service->title }}</td>
            <td>{{ $book->type->name }}</td>
            <td>{{ $book->worker->name }}</td>
            <td>{{ $book->date }}</td>
            <td>{{ $book->start . ' - ' . $book->end }}</td>
            <td>
                @if( $book->cancle == 1 )
                <button class="p-btn btn btn-danger" disabled>Cancled</button>
                @elseif( $book->complete == 1 )
                <button class="p-btn btn btn-success" disabled>@lang('lang.complete')</button>
                @elseif( $book->confirm == 1)
                <button class="p-btn btn btn-info" disabled>Confirmed</button>
                <button class="p-btn btn btn-danger" onclick="cancledBook(event, <?= $book->id; ?>, <?= $book->user_id; ?>)">Cancle</button>
                @else
                <button class="p-btn btn btn-danger" onclick="cancledBook(event, <?= $book->id; ?>, <?= $book->user_id; ?>)">Cancle</button>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>