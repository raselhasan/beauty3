 @extends('layout.app')
 @section('style')
 <style type="text/css">
 .pagination {
 	justify-content: center;
 	margin-bottom: 15px
 }

 .pagination>li>a {
 	color: #e2dede !important;
 }

 .page-link {
 	background-color: #1a1a1a !important;
 	border: 1px solid #1a1a1afc !important;
 }

 .page-item.active .page-link {
 	z-index: 3;
 	color: #fff;
 	background-color: #007bff !important;
 	border-color: #007bff !important;
 }

 .seeMore {
 	color: #e25b37;
 }

 .card {
 	background-color: #1a1a1a;
 	margin-bottom: 15px;
 }

 .dd-none {
 	display: none;
 }

 .sss-src-result {
 	background: #1a1a1a;
 	padding: 10px;
 	width: 100%;
 	max-height: 233px;
 	overflow: auto;
 }

 .src-rslt-con1 {
 	border-bottom: 1px solid #272424;
 	padding: 9px;
 	cursor: pointer;
 }
  	 .s-pro-img {
            position: absolute;
            height: 140px;
            width: 140px;
            border-radius: 50%;
            top: 50%;
            left: 50%;
            overflow: hidden;
            transform: translate(-50%, -50%);
            border: 2px solid #fff;
        }
        .s-pro-img img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
</style>
@endsection
@section('content')
@include('inc.header-filter-new')
<section class="blog-section">
	<div class="max-fix-width">
 		{{-- <div class="link-menu">
 			<span class="arrow-l"><i class="fas fa-arrow-left"></i></span> 
 		<span><a href="#">@lang('lang.cetagory')</a></span>
 		<span><i class="fas fa-chevron-right"></i></span>
 		<span>@lang('lang.sub_cetagory')</span>
 		</div> --}}
 		<form method="get" action="{{url('profile-list')}}">
 			<div class="row mar-0">
 				<div class="col-md-10">
 					<div class="form-group sss">
 						<input type="text" class="form-control profile_name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" name="profile_name" value="{{$data['profile_name']}}" onclick="searchProfileTitle(event, this.value)" onkeyup="searchProfileTitle(event, this.value)" autocomplete="off">
 						<div class="sss-src-result dd-none put-profile-result">

 						</div>
 					</div>
 				</div>
 				<div class="col-md-2">
 					<button type="submit" class="btn btn-primary">@lang('lang.search')</button>
 				</div>
 			</div>
 		</form>
 		<div class="row mar-0">
 			<div class="col-md-9">

 				{{-- <div class="short-s">
 					<div class="short-txt"><img src="{{asset('assets/img/services/triangle-r.png')}}" alt="img">@lang('lang.plaukai')</div>
 					<div class="short-select">
 						<span class="label-txt">@lang('lang.rikiavimas'):</span>
 						<div class="ctm-select" ctm-slt-n="cat_name">
 							<div class="ctm-select-txt pad-l-10">
 								<span class="select-txt" id="category">@lang('lang.naujausi_viršuje')</span>
 								<span class="select-arr"><img src="{{asset('assets/img/services/triangle-d.png')}}" alt="img"></span>
 							</div>
 							<div class="ctm-option-box">
 								<div class="ctm-option">@lang('lang.naujausi_viršuje')</div>
 								<div class="ctm-option">@lang('lang.naujausi_viršuje')</div>
 							</div>
 						</div>
 					</div>
 				</div> --}}
 				{{-- <div class="service-list-all">
 					@if(count($data['users']) >0)
 					@foreach($data['users'] as $user)
 					<div class="service-list">
 						<div class="service-list-img">
 							<a href="#">
 								<div class="service-logo">
 									<img src="{{asset('userImage/fixPic/'.$user->profile_image)}}" alt="img">
 								</div>
 							</a>
 							<div class="service-user-name">{{$user->name}}</div>
 							<div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
 							<div class="active-div">
 								<img src="{{asset('assets/img/services/active-add.png')}}">
 							</div>

 							<a href="#" onclick="recentlyView('{{$user->id}}')">
 								<img class="main-img" src="{{asset('userImage/fixPic/'.$user->profile_image)}}">
 							</a>

 							<div class="nav-img">
 								<div class="swiper-container blog-container">
 									<div class="swiper-wrapper">
 										<div class="swiper-slide">
 											<span class="li-img"><img src="{{asset('userImage/fixPic/'.$user->profile_image)}}"></span>
 										</div>
 									</div>
 									<div class="swiper-button-next ctm-next-prev swiper-button-next1"><img src="{{ asset('assets/img/services/ar-r.png') }}" alt="Left Arrow Key"></div>
 									<div class="swiper-button-prev ctm-next-prev swiper-button-prev1"><img src="{{ asset('assets/img/services/ar-l.png') }}" alt="Right Arrow Key"></div>
 								</div>
 							</div>
 						</div>
 						<div class="service-list-cnt">
 							<a href="#" onclick="recentlyView('{{$user->id}}')">
 								<div class="service-list-tlt">
 									Grožio salonas “STYLE4U”
 								</div>
 							</a>
 							<div class="service-list-price">
 								<div class="service-list-price-tbl">
 									<table class="table">
 										<tbody>
 											@for($i=1;$i < 4;$i++)
 											<tr>
 												<td class="service-name">Style4U</td>
 												<td class="service-time"><i class="far fa-clock"></i>
 													20
 												</td>
 												<td class="service-price text-right">
 													<!-- <span class="o-price"><del>35.00 €</del></span> -->
 													<span class="n-price">nuo 20 €</span>
 												</td>
 												<td class=" text-right pr-0">
 													<a href="#">
 														<span class="plus-i"><i class="fas fa-plus"></i></span>
 													</a>
 												</td>
 											</tr>
 											@endfor
 										</tbody>
 									</table>
 								</div>
 								<div class="more-txt text-right d-none"><span class="s-m"><a href="#">@lang('lang.see_more')</a></span></div>

 								<div class="more-txt">
 									<i class="fas fa-exclamation-circle"></i> Profesionalių stilistų komanda užtikrins aukščiausia paslaugų lygį bei padės ...
 									<span class="s-m">
 										<a href="#" class="seeMore" onclick="recentlyView('{{$user->id}}')">
 											@lang('lang.see_more')
 										</a>
 									</span>
 								</div>
 							</div>
 							<div class="service-list-footer">
 								<span class="auth">
 									<a href="#">
 										<img src="{{asset('userImage/fixPic/'.$user->profile_image)}}" alt="img"> {{$user->name}}
 									</a>
 								</span>
 								<span class="dis"><i class="fa fa-map-marker-alt"></i> 2 km</span>
 								<span class="s-point"><i class="fas fa-star"></i> 4/5 (5)</span>
 								<span class="tick"><i class="fas fa-check-circle"></i>@lang('lang.yra_vietu')</span>
 								<span class="service-list-point">
 									<span class="r-txt">Visos paslaugos</span>
 								</span>
 							</div>
 						</div>
 					</div>
 					@endforeach
 					@endif
 					<div class="see-more-u text-center"> <span>- RODYTI DAUGIAU REZULTATŲ -</span> </div>
 				</div> --}}
 				<section class="user-list-section">
 					<div class="row">
 						@if(count($data['users']) >0)
 						@foreach($data['users'] as $user)
 						<div class="col-12 col-md-6 col-lg-4">
 							<div class="single-content">
 							<div class="content-img-p">
 								<div class="content-img">
 									@if($user->profile_image)
 									<img class="card-img-top" src="{{asset('userImage/fixPic/'.$user->profile_image)}}" alt="Card image cap">
 									@else 
 									<img class="card-img-top" src="{{asset('assets/img/services/profile-img.png')}}" alt="Card image cap">
										
 									@endif
 								</div>
 							</div>
 							@php
				                  $full_name = '';
				                  if(@$user->name){
				                    $full_name .= @$user->name.' ';
				                  }
				                  if(@$user->surname){
				                    $full_name .= @$user->surname.' ';
				                  }
				                  if(@$user->nickname){
				                    $full_name .= '('.@$user->nickname.')';
				                   }
				                  $full_name = preg_replace('!\s+!', ' ', $full_name);
				                  $full_name = str_replace(' ', '-', $full_name);

				            @endphp

 							<div class="content-tlt">
 								<a href="{{url('public-profile/'.$full_name.'/'.$user->id)}}">{{$user->name}} {{$user->surname}} @if($user->nickname) ({{$user->nickname}}) @endif</a>
 							</div>
 							@if($user->services()->count())
 							<div class="usr-con-hover">
 								<div class="slider-info">
 									<div class="info-tlt"> - {{$user->name}} {{$user->surname}} -
 										<span class="top-b-icon">
 											<img src="{{ asset('assets/img/services/top-b-i.png') }}" alt="Left Arrow Key">
 										</span>
 									</div>
 									<div class="info-itm">
 										@php $n = 0 @endphp
 										@foreach($user->services as $service)
 										@if($n++ < 3) <div class="itm">
 											<span class="itm-name">{{$service->title}}</span>
 										</div>
 										@else
 										@break
 										@endif
 										@endforeach
 									</div>
 									<div class="info-btm">
 										<span class="dis"><i class="fa fa-map-marker-alt"></i> {{number_format(@$user->distance, 2)}} km</span>
 										<span class="star"><i class="fas fa-star"></i> 4/5</span>
 										<span class="btm-txt">LAISVAS</span>
 										<span class="btm-link">DAUGIAU</span>
 									</div>
 								</div>
 							</div>
 							@endif
 						</div>
 					</div>
 					@endforeach
 					@else
 					<div class="col-md-12">
 						<div class="alert alert-primary" role="alert">
 							@lang('lang.found'){{$data['profile_name']}}
 						</div>
 					</div>
 					@endif
 				</div>
 				<div>
 					{{$data['users']->render()}}
 				</div>
 			</section>
 		</div>
 		<div class="col-md-3 pr-0">
 			  @if(count($allVip)>0)
  <div class="side-blog">
    <section class="blog-section">
      <div class="blog-sidbar">
        <div class="side-tlt">
          <div class="side-txt">SKELBIMAI</div>
          <img src="{{asset('assets/img/services/vip-sm.png')}}" alt="img">
        </div>

        <div class="row m-0">
          @foreach ($allVip as $vip)
          @if(property_exists($vip,'title'))
          @php
          $page_title = $vip->title;
          $page_title = preg_replace('!\s+!', ' ', $page_title);
          $page_title = str_replace(' ', '-', $page_title);
          $see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($vip->title, $vip->service_main_id);
          @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                   @if($vip->image)
                    <img src="{{asset('assets/img/services/'.$vip->image)}}" alt="img">
                    @else 
                      <img src="{{asset('assets/img/services/no_service.png')}}" alt="img">
                    @endif
                </div>
              </div>
              <div class="content-des">
                <div class="des-itm">
                  <div class="itm-name-all">
                    @php
                    $inners = App\Http\Controllers\SearchServiceController::serviceAndProfileCategory($vip->inner_category,$vip->sub_category_id);
                    @endphp
                    @if(count($inners['category']) > 0)
                    @foreach ($inners['category'] as $inner)
                    <div class="itm-name">{{$inner->name_lt}}</div>
                    @endforeach
                    @endif
                  </div>
                  <div class="itm-dis">
                    <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance, 2)}} km</div>
                    <div class="dis-txt"><span><a href="{{$see_more}}">Daugiau</a></span></div>
                  </div>
                </div>
              </div>
              <div class="content-tlt">
                <a href="{{$see_more}}" onclick="recentlyView('{{$vip->service_main_id}}')">{{ $vip->title }}</a>
              </div>
            </div>
          </div>
          @else
           @php
            $userCats = App\Http\Controllers\SearchServiceController::ProfileCategory($vip->user_main_id);
            @endphp
          <div class="co-12 col-sm-6 col-lg-12 p-0">
            <div class="single-content">
              <div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
              <div class="content-img-p">
                <div class="content-img">
                  @if($userCats['cover_img'])
                  <img src="{{asset('userImage/coverPic/'.$userCats['cover_img']->img)}}" alt="img">
                  @else 
                   <img src="{{asset('assets/img/default_cover.png')}}" alt="img">
                  @endif
                </div>
                <div class="s-pro-img">
                  @if($vip->profile_image)
                  <img src="{{asset('userImage/fixPic/'.$vip->profile_image)}}" alt="img">
                  @else 
                    <img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
                  @endif
                </div>
              </div>
              @php
                  $full_name = '';
                  if(@$vip->name){
                    $full_name .= @$vip->name.' ';
                  }
                  if(@$vip->surname){
                    $full_name .= @$vip->surname.' ';
                  }
                  if(@$vip->nickname){
                    $full_name .= '('.@$vip->nickname.')';
                   }
                  $full_name = preg_replace('!\s+!', ' ', $full_name);
                  $full_name = str_replace(' ', '-', $full_name);

            @endphp
        <div class="content-des">
          <div class="des-itm">
            <div class="itm-name-all">
              @if(count($userCats['cats']) > 0)
              @foreach($userCats['cats'] as $cat)
              <div class="itm-name">{{$cat->name_lt}}</div>
              @endforeach
              @endif
              
            </div>
            <div class="itm-dis">
              <div class="dis-txt"><i class="fa fa-map-marker-alt"></i> {{number_format(@$vip->distance,2)}} km</div>
              <div class="dis-txt"><span><a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">Daugiau</a></span></div>
            </div>
          </div>
        </div>
        <div class="content-tlt">
          <a href="{{url('public-profile/'.$full_name.'/'.$vip->user_main_id)}}">{{ $vip->surname }}</a>
        </div>
      </div>
    </div>

    @endif
    @endforeach
  </div>
</div>
</section>
</div>
@endif
 		</div>

 	</div>
 </div>
</section>

<!-- <section class="bg-dark-sec">
	<div class="max-fix-width">
		<section class="slider-section">
			<div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img src="{{asset('assets/img/slider1.jpg')}}">
						<div class="carousel-caption">
							<h1>@lang('lang.third_slider_lavel')</h1>
							<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur').</p>
						</div>
					</div>
					<div class="carousel-item">
						<img src="{{asset('assets/img/slider2.jpg')}}">
						<div class="carousel-caption">
							<h1>@lang('lang.third_slider_lavel')</h1>
							<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur').</p>
						</div>
					</div>
					<div class="carousel-item">
						<img src="{{asset('assets/img/slider3.jpg')}}">
						<div class="carousel-caption">
							<h1>@lang('lang.third_slider_lavel')</h1>
							<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur').</p>
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">@lang('lang.previous')</span>
				</a>
				<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">@lang('lang.next')</span>
				</a>
			</div>
		</section>
	</div>
</section> -->
@endsection
@section('script')
<script type="text/javascript">

	var swiper = new Swiper('.blog-container', {
		slidesPerView: 4,
		spaceBetween: 5,
		loop: false,
		allowTouchMove: false,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},

		breakpoints: {
			450: {
				slidesPerView: 4,
			}
		}
	});
	$(document).on('click', '.pagination a', function(e) {
		e.preventDefault();

		var current_url = window.location.href.split('?')[0];
		var page_number = $(this).attr('href').split('page=')[1];
		var link = current_url + '?page=' + page_number;
		var profile_name = $('.profile_name').val();
		if ($.trim(profile_name) != '') {
			link = link + '&profile_name=' + profile_name;
		}
		window.location = link;


	});

	function searchProfileTitle(e, val) {
		if (val.length > 2) {
			var data = new FormData();
			data.append('keyword', val);
			$.ajax({
				processData: false,
				contentType: false,
				data: data,
				type: 'POST',
				url: '{{url("profile-search")}}',
				success: function(response) {
					console.log(response);
					if (response != '') {
						$('.put-profile-result').html(response);
						$(e.target).parents(".sss").find(".sss-src-result").removeClass("dd-none");
					} else {
						$(e.target).parents(".sss").find(".sss-src-result").addClass("dd-none");
					}

				}

			});


		} else {
			$(e.target).parents(".sss").find(".sss-src-result").addClass("dd-none");
		}

	}
	$(document).ready(function() {
		$(document).on('click', '.result-item1', function() {
			var id = $(this).attr('id');
			var name = $(this).text();
			var link = '{{url("profile-list")}}' + '?profile_name=' + name + '&profile_id=' + id;
			window.location = link;
		});
	});
</script>



@endsection