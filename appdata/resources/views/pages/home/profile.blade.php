@foreach($profiles as $profile)
@php
	$profile_link = App\Http\Controllers\SearchServiceController::profileLink($profile->id, $profile->name);
@endphp
<div class="swiper-slide">
	<div class="single-content">
		<div class="active-div">
			<a href="{{$profile_link}}"><img src="{{asset('assets/img/services/active-add.png')}}"></a>
		</div>
		<div class="content-img-p">
			<div class="content-img">
				@if($profile->profile_image)
				<img src="{{asset('userImage/fixPic/' . $profile->profile_image)}}">
				@else 
					<img src="{{asset('assets/img/services/profile-img.png')}}" alt="img">
				@endif
			</div>
			<div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
		</div>
		<div class="content-tlt">
			{{$profile->name}} {{$profile->surname}}
		</div>
	</div>
</div>
@endforeach