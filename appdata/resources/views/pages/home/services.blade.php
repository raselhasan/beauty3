@foreach($vip_services as $service)
<div class="swiper-slide" onclick="activeType(event,1)">
@php
	$see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($service->title, $service->id);
@endphp
	<div class="single-content ">
		<div class="active-div">
			<a href="{{$see_more}}"><img src="{{asset('assets/img/services/active-add.png')}}"></a>
		</div>
		<div class="content-img-p">
			<div class="content-img">
				@if($service->image)
				<img src="{{asset('assets/img/services/' . $service->image)}}">
				@else 
					<img src="{{asset('assets/img/services/no_service.png')}}" alt="img">
				@endif
			</div>
			<div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
		</div>
		<div class="content-tlt">
			{{$service->title}}
		</div>
	</div>
</div>
@endforeach