@foreach ($vip_services as $service)
	<div class="swiper-slide invisible">
			<div class="slider-info">
				<div class="info-tlt"> - {{$service->title}} - <span class="top-b-icon"><img src="{{ asset('assets/img/services/top-b-i.png') }}" alt="Left Arrow Key"></span></div>
				<div class="info-itm">
					@php
						$count = 0;
						$see_more = App\Http\Controllers\SearchServiceController::makeServiceSeeMore($service->title, $service->id);
					@endphp
					@if(count($service->types) >0)
						@foreach($service->types as $type)
							@if($count == 0 || $count == 1 || $count == 2)
								<div class="itm">
									<span class="itm-name">{{$type->name}}</span>
									<span class="float-right itm-price">nuo {{$type->price}} €</span>
								</div>
							@endif
							@php
								$count++;
							@endphp
						@endforeach
					@endif
			</div>
			<div class="info-btm">
				<span class="dis"><i class="fa fa-map-marker-alt"></i>{{number_format($service->distance,2)}} km</span>
				<span class="star"><i class="fas fa-star"></i> 4/5</span>
				<span class="btm-txt">LAISVAS</span>
				<a href="{{$see_more}}" class="btm-link">DAUGIAU</a>
			</div>
		</div>
</div>
@endforeach