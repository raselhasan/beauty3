@foreach($profiles as $profile) 
@php
	$profile_link = App\Http\Controllers\SearchServiceController::profileLink($profile->id, $profile->name);
@endphp
	<div class="swiper-slide invisible">
		<div class="slider-info">
			<div class="info-tlt"> - {{$profile->name}} {{$profile->surname}}  - <span class="top-b-icon"><img src="{{ asset('assets/img/services/top-b-i.png') }}" alt="Left Arrow Key"></span></div>
			<div class="info-itm">
				@php
					$count = 0;
				@endphp
				@if(count($profile->services) > 0)
					@foreach ($profile->services as $service)
						@if($count == 0 || $count == 1 || $count == 2)
							<div class="itm">
								<span class="itm-name">{{$service->title}}</span>
							</div>
						@endif	
						@php
							$count++;
						@endphp
					@endforeach
				@endif
		</div>
		<div class="info-btm">
			<span class="dis"><i class="fa fa-map-marker-alt"></i>{{number_format($profile->distance, 2)}} km</span>
			<span class="star"><i class="fas fa-star"></i> 4/5</span>
			<span class="btm-txt">LAISVAS</span>
			<a href="{{$profile_link}}" class="btm-link">DAUGIAU</a>
		</div>
	</div>
</div>
@endforeach