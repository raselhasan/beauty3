<!DOCTYPE html>
<html lang="en">

<head>
	<title>@yield('title')</title>
	@yield('meta_description')
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="verify-paysera" content="ff297396eb1864fcd1b755985cd44ac7">
	<link href="https://fonts.googleapis.com/css2?family=Work+Sans:ital@0;1&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('assets/fontawesome/css/fontawesome-all.min.css') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('assets/swiper/css/swiper.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/ionicons/css/ionicons.min.css') }}">
	{{-- <link rel="stylesheet" href="{{ asset('assets/range/html-input-range.css') }}"> --}}
	{{-- <link rel="stylesheet" href="{{ asset('assets/range/jquery.range.css') }}"> --}}
	<link rel="stylesheet" href="{{ asset('assets/range/wrunner-default-theme.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/summernote/summernote.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/mobile-menu/css/hs-menu.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/mobile-menu/fonts/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/calendar.css') }}">
	<link href="{{asset('assets/css/loader.css')}}" rel="stylesheet" />
	<style>
		.wrunner_theme_default.wrunner_direction_horizontal {
			position: relative;
		}

		.wrunner__pathPassed_theme_default.wrunner__pathPassed {
			border-radius: 0;
			background: #E25B37;
		}

		.wrunner__handle_theme_default.wrunner__handle {
			border-radius: 0;
			background-image: linear-gradient(to bottom, #eeeeee, #dddddd);
			background-repeat: repeat-x;
		}

		.wrunner__handle_theme_default.wrunner__handle:hover {
			border-radius: 0;
		}

		.wrunner__path_theme_default.wrunner__path {
			background-color: #fff;
			border-radius: 0;
		}

		.wrunner__valueNote_theme_default.wrunner__valueNote {
			background-color: #E25B37;
		}

		.acc-type {
			margin-top: 21px;
		}

		#language-box {
			margin-top: 5px;
			background: #0c0c0c;
			color: white;
		}

		.heart-menu {
			font-size: 17px !important;
			color: red !important;
		}

		.step-2 {
			display: none;
		}

		.step-3 {
			display: none;
		}

		.v_code_error {
			color: red;
		}

		.v_code_error2 {
			color: red;
		}

		#verifyEmail {
			color: black;
		}

		#verifyEmail .form-control {
			color: black;
		}

		#v-e-code {
			display: none;
		}
	</style>
	@yield('style')
</head>

<body>
	<div id="loader"></div>
	<div id="sckbr_p">
		<div id="snackbar">@lang('lang.some_text_some_message')..</div>
	</div>
	@include('inc.registration')
	@include('inc.login')
	@include('inc.header')

	<div id="calendar">
		@yield('content')
	</div>
	<div class="mobile-menu-bar d-block d-lg-none">
		<ul class="nav nav-pills" id="pills-tab" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-ho" role="tab" aria-controls="pills-home" aria-selected="true">PROFILIS<span class="active-icon"><img src="{{asset('assets/img/tri-icon.png')}}" alt="img"></span></a>

			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profi" role="tab" aria-controls="pills-profile" aria-selected="false">PASLAUGOS<span class="active-icon"><img src="{{asset('assets/img/tri-icon.png')}}" alt="img"></span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-conta" role="tab" aria-controls="pills-contact" aria-selected="false">KONTAKTAI<span class="active-icon"><img src="{{asset('assets/img/tri-icon.png')}}" alt="img"></span></a>
			</li>
		</ul>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-body p-0">
					<span class="close" data-dismiss="modal"> &times;</span>
					<div class="input-group">
						<input type="text" class="form-control" name="search_result">
						<div class="input-group-append p-0">
							<button class="input-group-text p-0" id="basic-addon2"><i class="fas fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('inc.verify-email')
	@include('inc.footer')
	@yield('wondowv')
	<script src="{{ asset('assets/js/calendar.js') }}"></script>
	<!-- <script src="{{ asset('assets/js/popper.min.js') }}"></script> -->
	<!-- <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script> -->
	<script src="{{ asset('assets/ionicons/js/ionicons.js') }}"></script>
	<script src="{{ asset('assets/swiper/js/swiper.js') }}"></script>
	{{-- <script src="{{ asset('assets/range/html-input-range.js') }}"></script> --}}
	{{-- <script src="{{ asset('assets/range/jquery.range.js') }}"></script> --}}
	<script src="{{ asset('assets/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('assets/range/wrunner-jquery.js') }}"></script>
	<script src="{{ asset('assets/mobile-menu/js/jquery.hsmenu.js') }}"></script>
	<script src="{{ asset('assets/js/custom.js') }}"></script>

	<!-- //get user location -->
	@include('service_script.app-script')
	@include('service_script.search-history-script')





	<script>
		$(function() {
			$("#hd1 .wrunner__valueNote_display_visible").eq(0).addClass('left-hand');
			$("#hd1 .wrunner__valueNote_display_visible").eq(1).addClass('right-hand');
			$("#hd2 .wrunner__valueNote_display_visible").eq(0).addClass('left-hand');
			$("#hd2 .wrunner__valueNote_display_visible").eq(1).addClass('right-hand');
		});
		$("#hd1").wRunner({
			step: 1,
			type: "single",

			limits: {

				minLimit: 0,

				maxLimit: 100

			},
			singleValue: $('#distance').val(),

			rangeValue: {
				minValue: 0,
				maxValue: 100
			},

			roots: document.body,

			divisionsCount: 0,

			valueNoteDisplay: true,

			theme: "default",

			direction: 'horizontal'

		});

		var price = $("#price").val();
		var low = price.split(",")[0];
		var high = price.split(",")[1];

		$("#hd2").wRunner({
			step: 1,
			type: "range",

			limits: {

				minLimit: 0,

				maxLimit: 100

			},
			singleValue: 40,

			rangeValue: {
				minValue: low,
				maxValue: high
			},

			roots: document.body,

			divisionsCount: 0,

			valueNoteDisplay: true,

			theme: "default",

			direction: 'horizontal'

		});

		function changeLanguage(lang) {
			var url = '{{url("localization")}}/' + lang;
			window.location = url;
		}


		// $(function(){
		// 	htmlInputRange.options({
		// 		tooltip: true,
		// 		max: 100,
		// 		labels: true,
		// 		labelsData: {
		// 			one: 'one',
		// 			two: 'two'
		// 		}
		// 	});
		// 	$('.single-slider').jRange({
		// 		from: 0,
		// 		to: 100,
		// 		step: 1,
		// 		format: '%s',
		// 		showLabels: false,
		// 		snap: true
		// 	});

		// 	$('.range-slider').jRange({
		// 		from: 0,
		// 		to: 100,
		// 		step: 1,
		// 		format: '%s',
		// 		showLabels: false,
		// 		isRange : true,
		// 		values: [ 10, 20 ]
		// 	});

		// });
		window.login = "{{ route('user.login.submit') }}";
		window.registration = "{{ route('registration') }}";
		window.profile = "{{ route('user.profile') }}";
		window.verifySms = "{{ url('verify-sms') }}";
		window.verifyEmail = "{{ url('verify-email') }}";

		window.sendEmailAfterLogin = "{{ url('user/send-email-after-login') }}";
		window.verifyEmailAfterLogin = "{{ url('user/verify-email-after-login') }}";
	</script>
	@if(session()->has('errorL'))
	<script>
		$(function() {
			$('[data-target="#loginPopUp"]').click();
		});
	</script>
	@endif
	@yield('script')


	@if(Auth::check())
	@if(auth()->user()->email_verified_at == '')
	<script>
		$(document).ready(function() {
			$('#verifyEmail').modal({
				backdrop: 'static',
				keyboard: false
			})
			$('#verifyEmail').modal('show');
		});
	</script>
	@endif
	@endif
	<script>
		$(document).ready(function() {
			$(document).on('submit', '#v-e-send', function() {
				var email = $('#v-e-email-address').val();
				$.ajax({
					url: window.sendEmailAfterLogin,
					method: "POST",
					data: {
						email: email
					},
					success: function(res) {
						$('#v-e-send').hide();
						$('#v-e-code').show();
						$('#emailHelp2').text("Verification code sent to you email please check and verify your account");
					}
				});
				return false;
			});
		});
		$(document).ready(function() {
			$(document).on('submit', '#v-e-code', function() {
				var code = $('#v-e-v-code').val();
				$.ajax({
					url: window.verifyEmailAfterLogin,
					method: "POST",
					data: {
						code: code
					},
					success: function(res) {
						window.location.href = res;
					}
				});
				return false;
			});
		});
	</script>
</body>

</html>