<script>
	$(function() {
		var visitor_lat = $('#visitor_lat').val();
		var visitor_lng = $('#visitor_lng').val();
		var visitor_address = $('#visitor_address').val();
		if (visitor_lat == '' && visitor_lng == '' && visitor_address == '') {
			if ("geolocation" in navigator) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var currentLatitude = position.coords.latitude;
					var currentLongitude = position.coords.longitude;
					var geocoder;
					geocoder = new google.maps.Geocoder();
					var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
					geocoder.geocode({
						'latLng': latlng
					}, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							if (results[1]) {
								var address = results[0].formatted_address;
								setSession(currentLatitude, currentLongitude, address);
								var dis = $('#distance').val();
								getIndexData(currentLatitude, currentLongitude, dis);
							}
						}
					});
				}, function() {
					var lat = '';
					var lng = '';
					var address = '';
					var dis = $('#distance').val();
					setSession(lat, lng, address);
					getIndexData(lat, lng, dist);
				});
			}
		} else {
			var dis = $.trim($('#distance').val());
			getIndexData(visitor_lat, visitor_lng, dis);
		}
	});

	function setSession(lat, lng, address) {
		var data = new FormData();
		data.append('lat', lat);
		data.append('lng', lng);
		data.append('address', address);
		$.ajax({
			processData: false,
			contentType: false,
			data: data,
			type: 'POST',
			url: '{{url("set-visitor-location")}}',
			success: function(response) {
				$('#visitor_lat').val(response.lat);
				$('#visitor_lng').val(response.lng);
				$('#visitor_address').val(response.address);
				$('#index-address').val(response.address);
				console.log(response);
			}

		});
	}

	function searchByTitle(e, val) {
		if (val.length > 2) {
			var lat = $('#visitor_lat').val();
			var lng = $('#visitor_lng').val();
			var address = $('#visitor_address').val();
			var distance = $('#distance').val();
			var data = new FormData();
			data.append('keyword', val);
			data.append('lat', lat);
			data.append('lng', lng);
			data.append('address', address);
			data.append('distance', distance);
			$.ajax({
				processData: false,
				contentType: false,
				data: data,
				type: 'POST',
				url: '{{url("search-service-title")}}',
				success: function(response) {
					console.log(response);
					$('.put-service-result').html(response.item);
					$(e.target).parents(".ss").find(".ss-src-result").removeClass("d-none");
				}

			});
		} else {
			$(e.target).parents(".ss").find(".ss-src-result").addClass("d-none");
		}

		// if (val.length) {
		//     $(e.target).parents(".ss").find(".ss-src-result").removeClass("d-none");
		// } else {
		//     $(e.target).parents(".ss").find(".ss-src-result").addClass("d-none");
		// }
	}

	$(document).on('click', '.result-item', function(e) {
		$('.search-keyword').val($(this).text());
		$(e.target).parents(".ss").find(".ss-src-result").addClass("d-none");
		var title = $.trim($(this).text());
		var main_link = '{{url("services")}}';
		var custom_link = 'category=';
		var title_status = 1;
		sendToService(custom_link, main_link, title_status);

	});
	$(document).on('click', '.comon-link', function(event) {
		event.preventDefault();
		var custom_link = $(this).attr('href').split('?')[1];
		var main_link = $(this).attr('href').split('?')[0];
		var title_status = 0;
		sendToService(custom_link, main_link, title_status);

	});

	function goService() {
		var title = $.trim($('.search-keyword').val());
		if (title != '') {
			var main_link = '{{url("services")}}';
			var custom_link = 'category=';
			var title_status = 1;
			sendToService(custom_link, main_link, title_status);
		}
	}

	//search-keyword

	$('.search-keyword').keyup(function(e) {
		if (e.keyCode == 13) {
			var keyword = $.trim($(this).val());
			if (keyword != '') {
				var main_link = '{{url("services")}}';
				var custom_link = 'category=';
				var title_status = 1;
				sendToService(custom_link, main_link, title_status);
			}
		}
	});

	function sendToService(custom_link, main_link, title_status) {

		var title = '';
		if (title_status == 1) {
			title = $('.search-keyword').val();
		}
		var lat = $('#visitor_lat').val();
		var lng = $('#visitor_lng').val();
		var address = $('#visitor_address').val();

		var price = $.trim($('#price').val());
		var distance = $.trim($('#distance').val());
		var salons = '';
		var personal = '';
		if ($('.salons').is(':checked')) {
			salons = 1;
		};
		if ($('.personal').is(':checked')) {
			personal = 1;
		};
		var extra = '?city=&lat=' + lat + '&lng=' + lng + '&address=' + address + '&title=' + title + '&salons=' + salons + '&personal=' + personal + '&distance=' + distance + '&price=' + price + '&' + custom_link + '&page=1';
		var final_link = main_link + extra;
		window.location = final_link;
	}

	function getIndexData(lat, lng, dis) {
		var link = 'lat=' + lat + '&lng=' + lng + '&distance=' + dis;
		$.ajax({
			url: 'index-vip-services?' + link
		}).done(function(response) {
			$('.put-vip-data').html(response.vip);
			$('.put-vip-type').html(response.vip_type);
			if(response.vip == '')
			{
				$('.vip-section1 .swiper-button-next').hide();
				$('.vip-section1 .swiper-button-prev').hide();
				$('.text-v-hide1').css('position','initial');
			}
			$('.put-profile').html(response.vip_profile);
			$('.put-profile-inner').html(response.vip_profile_inner);

			if(response.vip_profile == '')
			{
				$('.vip-section2 .swiper-button-next').hide();
				$('.vip-section2 .swiper-button-prev').hide();
				$('.text-v-hide2').css('position','initial');
			}

			var swiper = new Swiper('.vip-container1', {
				slidesPerView: 4,
				spaceBetween: 30,
				loop: false,
				allowTouchMove: false,
				navigation: {
					nextEl: '.vip-section1 .swiper-button-next',
					prevEl: '.vip-section1 .swiper-button-prev',
				},

				breakpoints: {
					1100: {
						slidesPerView: 3,
					},
					800: {
						slidesPerView: 2,
					},
					450: {
						slidesPerView: 1,
					}
				}
			});
			var swiper = new Swiper('.vip-info1', {
				slidesPerView: 4,
				spaceBetween: 30,
				loop: false,
				allowTouchMove: false,
				navigation: {
					nextEl: '.vip-sec-info1 .swiper-button-next',
					prevEl: '.vip-sec-info1 .swiper-button-prev',
				},

				breakpoints: {
					1100: {
						slidesPerView: 3,
					},
					800: {
						slidesPerView: 2,
					},
					450: {
						slidesPerView: 1,
					}
				}
			});
			var swiper = new Swiper('.vip-container2', {
				slidesPerView: 4,
				spaceBetween: 30,
				loop: false,
				allowTouchMove: false,
				navigation: {
					nextEl: '.vip-section2 .swiper-button-next',
					prevEl: '.vip-section2 .swiper-button-prev',
				},

				breakpoints: {
					1100: {
						slidesPerView: 3,
					},
					800: {
						slidesPerView: 2,
					},
					450: {
						slidesPerView: 1,
					}
				}
			});
			var swiper = new Swiper('.vip-info2', {
				slidesPerView: 4,
				spaceBetween: 30,
				loop: false,
				allowTouchMove: false,
				navigation: {
					nextEl: '.vip-sec-info1 .swiper-button-next',
					prevEl: '.vip-sec-info1 .swiper-button-prev',
				},

				breakpoints: {
					1100: {
						slidesPerView: 3,
					},
					800: {
						slidesPerView: 2,
					},
					450: {
						slidesPerView: 1,
					}
				}
			});



		});
	}

	function recentlyView(id) {
		// alert(id);
		var data = new FormData();
		data.append('id', id);
		$.ajax({
			processData: false,
			contentType: false,
			data: data,
			type: 'POST',
			url: '{{url("recently_view")}}',
			success: function(response) {
				// console.log(response);

			}

		});
	}

	$(document).on('click', '#index-address', function() {
		$(this).val('');
	});
	$(document).on('click', '#service-address', function() {
		$(this).val('');
	});
	$(function() {
		$(document).on('click', '.vip-section1 .swiper-slide', function() {
			alert(0);
		});
	});
</script>