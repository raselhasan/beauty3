<script>
    function getServices( number)
    {
        if(number == 1)
        {
            var get_distance = $("#hd1 .wrunner__valueNote_display_visible").eq(0).text();
            console.log(get_distance);
            $("input[name=distance]").val(get_distance);
            searchHistorySet('h_distance', get_distance);
        }
        if(number == 2){
          var min = $("#hd2 .wrunner__valueNote_display_visible").eq(0).text();
          var max = $("#hd2 .wrunner__valueNote_display_visible").eq(1).text();
          $("input[name=price]").val(min+','+max);
          var price = min+','+max;

          searchHistorySet('h_price', price);
        }

    } 

    $('.salons').click(function(){
        if($(this).prop("checked") == true){
            searchHistorySet('h_salons',1);
        }else{
            searchHistorySet('h_salons',0);
        }
    });
    $('.personal').click(function(){
        if($(this).prop("checked") == true){
            searchHistorySet('h_personal',1);
        }else{
            searchHistorySet('h_personal',0);
        }
    });
    function searchHistorySet(key, value)
    {
        var data = new FormData();
        data.append('key', key);
        data.append('value', value);
        $.ajax({
            processData: false,
            contentType: false,
            data: data,
            type: 'POST',
            url: '{{url("set-search-history")}}',
            success: function(response) {
                console.log(response);
                
            }

        });
    }
</script>
<script type="text/javascript">
    $(function() {
    var autocomplete;
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('index-address')), {
      types: ['geocode'],
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var near_place = autocomplete.getPlace();
      var address = $('#index-address').val();
      var lat = near_place.geometry.location.lat();
      var lng = near_place.geometry.location.lng();
      $('#visitor_lat').val(lat);
      $('#visitor_lng').val(lng);
      $('#visitor_address').val(address);
      setSession(lat,lng,address);
    });

  });
</script>