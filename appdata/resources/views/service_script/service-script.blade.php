@if(!$item['page'])
<script>
  $(document).ready(function(){
    var city = "{{$item['city']}}";
    var title = '';
    var lat = "{{$item['lat']}}";
    var lng = "{{$item['lng']}}";
    var address = "{{$item['address']}}";;

    var price = $.trim($('#price').val());
    var distance = $.trim($('#distance').val());
    var salons = '';
    var personal = '';
    if ($('.salons').is(':checked')) {
      salons = 1;
    };
    if ($('.personal').is(':checked')) {
      personal = 1;
    };

    var main_link = window.location.href.split('?')[0];
    var custom_link = '';

    var category = $('.s_category').val();
    var sub_category = $('.s_sub_category').val();
    var inner_category = $('.s_inner_category').val();
    if(category){
      custom_link = 'category='+category;
    }
    if(sub_category){
      custom_link = 'sub_category='+sub_category;
    }
    if(inner_category){
      custom_link = 'inner_category='+inner_category;
    }
    var extra = '?city='+city+'&lat=' + lat + '&lng=' + lng + '&address=' + address + '&title=' + title + '&salons=' + salons + '&personal=' + personal + '&distance=' + distance + '&price=' + price + '&' + custom_link + '&page=1';
    var final_link = main_link + extra;
    window.history.pushState("object or string", "Title", final_link);

  });
</script>

@endif
  



 <script>
    var swiper = new Swiper('.ctm-container3', {
        slidesPerView: 5,
        spaceBetween: 15,
        loop: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        breakpoints: {
            1400: {
                slidesPerView: 4,
            },
            1000: {
                slidesPerView: 3,
            },
            700: {
                slidesPerView: 2,
            },
            450: {
                slidesPerView: 1,
            }
        }
    });
 </script>

{{-- pagination data --}}
 <script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();

        var current_url = window.location.href.split('page=')[0];
        var page_number = $(this).attr('href').split('page=')[1];
        link = current_url+'page='+page_number;
        window.location = link;
    });
 </script>    
{{-- filter data --}}
<script type="text/javascript">
    function getServices( number)
    {

        if(number == 1)
        {
            var get_distance = $("#hd1 .wrunner__valueNote_display_visible").eq(0).text();
            $("input[name=distance]").val(get_distance);
            var current_url = window.location.href;
            var url = window.location.href.split('&');
            var new_url = url[0]+'&'+url[1]+'&'+url[2]+'&'+url[3]+'&'+url[4]+'&'+url[5]+'&'+url[6]+'&distance='+get_distance+'&'+url[8]+'&'+url[9]+'&page=1';
            
            window.history.pushState("object or string", "Title", new_url);
            ajaxData(new_url);

            
        }
        if(number == 2){
          var min = $("#hd2 .wrunner__valueNote_display_visible").eq(0).text();
          var max = $("#hd2 .wrunner__valueNote_display_visible").eq(1).text();
          $("input[name=price]").val(min+','+max);
          var new_price = min+','+max;
          var current_url = window.location.href;
          var url = window.location.href.split('&');
          var new_url = url[0]+'&'+url[1]+'&'+url[2]+'&'+url[3]+'&'+url[4]+'&'+url[5]+'&'+url[6]+'&'+url[7]+'&price='+new_price+'&'+url[9]+'&page=1';
         
          window.history.pushState("object or string", "Title", new_url);
          ajaxData(new_url);

        }
    }
</script>
<script type="text/javascript">
	function setCity(city)
	{
		var lat = '';
		var lng = '';
		var url = window.location.href.split('?')[0];
		var url1 = url+'?lat='+lat+'&lng='+lng;
		var url2 = window.location.href.split('&');
		var new_url = url1+'&city='+city+'&'+url2[3]+'&'+url2[4]+'&'+url2[5]+'&'+url2[6]+'&'+url2['7']+'&'+url2[8]+'&page=1';
		searchHistorySet('city', city);
		window.history.pushState("object or string", "Title", new_url);
		
    ajaxData(new_url);

	}
</script>
<script type="text/javascript">
	$('.salons').click(function(){
		var salons = '';
        if($(this).prop("checked") == true){
            salons = 1;
            
        }else{
        	
        }
        var current_url = window.location.href;
         var url = window.location.href.split('&');
         var new_url = url[0]+'&'+url[1]+'&'+url[2]+'&'+url[3]+'&'+url[4]+'&salons='+salons+'&'+url[6]+'&'+url[7]+'&'+url[8]+'&'+url[9]+'&page=1';
         window.history.pushState("object or string", "Title", new_url);
         ajaxData(new_url);

    });
</script>
<script type="text/javascript">
	$('.personal').click(function(){
		var personal = '';
        if($(this).prop("checked") == true){
            personal = 1;
        }else{
        }
        var current_url = window.location.href;
         var url = window.location.href.split('&');
         var new_url = url[0]+'&'+url[1]+'&'+url[2]+'&'+url[3]+'&'+url[4]+'&'+url[5]+'&personal='+personal+'&'+url[7]+'&'+url[8]+'&'+url[9]+'&page=1';
         window.history.pushState("object or string", "Title", new_url);
         ajaxData(new_url);

    });
</script>
{{-- end filter data --}}
<script type="text/javascript">
    function ajaxData(link)
    {
        $.ajax({
            url:link

        }).done(function(response){
            $('#services').html(response);
            var swiper = new Swiper('.blog-container', {
              slidesPerView: 4,
              spaceBetween: 5,
              loop: false,
              allowTouchMove: false,
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },

              breakpoints: {
                450: {
                  slidesPerView: 4,
                }
              }
            });
        });
    }
</script>
<script type="text/javascript">
	function searchHistorySet(key, value)
    {
        var data = new FormData();
        data.append('key', key);
        data.append('value', value);
        $.ajax({
            processData: false,
            contentType: false,
            data: data,
            type: 'POST',
            url: '{{url("set-search-history")}}',
            success: function(response) {
                console.log(response);
                
            }

        });
    }
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $(document).on('click','.c-heart',function(){
      var id = $.trim($(this).attr('id'));
      var action = $.trim($(this).attr('action'));
      if(action == 'add'){
        $(this).removeClass('c-white');
        $(this).addClass('c-red');
        $(this).attr('action','remove');
      }else{
        $(this).removeClass('c-red');
        $(this).addClass('c-white');
        $(this).attr('action','add');
      }
      var data = new FormData();
      data.append('id', id);
      data.append('action', action);
      $.ajax({
          processData: false,
          contentType: false,
          data: data,
          type: 'POST',
          url: '{{url("favarite")}}',
          success: function(response) {
              console.log(response);
              
          }

      });
    });
  });
</script>
<script type="text/javascript">
$(function() {
    var autocomplete;
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('service-address')), {
      types: ['geocode'],
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var near_place = autocomplete.getPlace();
      var address = $('#service-address').val();
      var lat = near_place.geometry.location.lat();
      var lng = near_place.geometry.location.lng();
      $('#visitor_lat').val(lat);
      $('#visitor_lng').val(lng);
      $('#visitor_address').val(address);
      var url = window.location.href.split('?')[0];
      var url1 = url+'?city=&lat='+lat+'&lng='+lng;
      var url2 = window.location.href.split('&');
      var new_url = url1+'&address='+address+'&'+url2[4]+'&'+url2[5]+'&'+url2[6]+'&'+url2[7]+'&'+url2[8]+'&'+url2[9]+'&page=1';
      
      window.history.pushState("object or string", "Title", new_url);
      ajaxData(new_url);
      
    });

  });
</script>
<script>
  function resetAll()
  {

         if ("geolocation" in navigator) {
             navigator.geolocation.getCurrentPosition(function(position) {
                 var currentLatitude = position.coords.latitude;
                 var currentLongitude = position.coords.longitude;
                 var geocoder;
                 geocoder = new google.maps.Geocoder();
                 var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                 geocoder.geocode({
                     'latLng': latlng
                 }, function(results, status) {
                  var address = results[0].formatted_address;
                     reset(currentLatitude, currentLongitude, address);
                 });
             }, function() {
                 var lat = '';
                 var lng = '';
                 var address = '';
                 reset(lat, lng, address);
             });
         }
     
  }

  function reset(lat, lng, address) {
    var data = new FormData();
    data.append('lat', lat);
    data.append('lng', lng);
    data.append('address', address);
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("reset-visitor-location")}}',
      success: function(response) {
          var url = window.location.href.split('?')[0];
          var url1 = url+'?city=&lat='+lat+'&lng='+lng;
          var url2 = window.location.href.split('&');
          var new_url = url1+'&address='+address+'&title=&salons=1&personal=1&distance=10&price=0,100&category=1&page=1';

          window.location = new_url;
      }

    });
  }


</script>
