<form action="" method="GET" id="searchForm">
<input type="hidden" name="price" id="price" value="@if(Session::has('h_price')) {{Session::get('h_price')}} @else 0,100 @endif">
<input type="hidden" name="distance" id="distance" value="@if(Session::has('h_distance')) {{Session::get('h_distance')}} @else 100 @endif">
<section class="header-filter">
	<div class="form-section form-sh">
		<div class="form-all">
			<div class="row mar-0"> 
				<div class="col-12 col-md-12 col-lg-9 pad-0">
					<div class="form-range">
						<div class="row mar-0">
							<div class="col-12 col-md-6 col-lg-6 r-d">
								<span class="range-tlt">@lang('lang.atstuumas')</span>
								{{-- <div class="range-s">
									<input type="range" id="html-input-range" onchange="getServices(1)">
								</div> --}}
								<div class="w-100" id="single_range" onclick="getServices(1)"></div>
								<!-- <div class="demo-output dm">
									<input class="range-slider r1"  type="hidden" value="0,0"/>
								</div> -->
							</div>
							<div class="col-12 col-md-6 col-lg-4 r-d">
								{{-- <div class="demo-output dm1">
									<input class="range-slider get-price r2" type="hidden" value="0,100" onchange="getServices(2)">
								</div> --}}
								<div class="w-100" id="multi_range" onclick="getServices(2)"></div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 pad-0">
					<div class="form-group">
						<input type="text" class="form-control" id="index-address" aria-describedby="emailHelp" placeholder="Type address" autocomplete="off" value="{{Session::get('address')}}">
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-1 pad-0">
					<div class="form-group mar-0">
						<div class="form-check">
							<input class="form-check-input salons" type="checkbox" id="gridCheck" name="salons" value="1" @if(Session::has('h_salons')) @if(Session::get('h_salons') == 1) checked @endif @else checked @endif>
							<label class="form-check-label" for="gridCheck">
								@lang('lang.salons')
							</label>
						</div><div class="form-check">
							<input class="form-check-input personal" type="checkbox" id="gridCheck" name="personal" value="1" @if(Session::has('h_personal')) @if(Session::get('h_personal') == 1) checked @endif @else checked @endif>
							<label class="form-check-label" for="gridCheck">
								@lang('lang.personal')
							</label>
						</div>
						<a href="{{url('reset-session-data')}}">@lang('lang.reset')</a>
					</div>

				</div>

			</div>
		</div>
	</div>
	<div class="line-h"><span><i class="fas fa-chevron-down"></i></span></div>
</section>
</form>