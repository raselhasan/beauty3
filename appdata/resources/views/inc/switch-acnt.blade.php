@if(Auth::guard('web')->check())
<div class="switch-acnt-box">
    <div class="acnt-list">
        <a href="{{url('/user/profile')}}">@lang('lang.profile')</a>
    </div>
    <div class="acnt-list">
        <a href="{{url('/user/calendar/create')}}">@lang('lang.create_calendar')</a>
    </div>
    <div class="acnt-list">
        <a href="{{url('payment-history')}}">@lang('lang.payment_history')</a>
    </div>
    <div class="acnt-list">
        <a href="{{url('/user/booking-list')}}">@lang('lang.booking_list')</a>
    </div>
    <div class="acnt-list">
        <a href="{{url('payment-list')}}">@lang('lang.service_payment')</a>
    </div>
    <div class="acnt-list">
        <a href="{{url('add-control-payment')}}">@lang('lang.remove_adds')</a>
    </div>
    <div class="acnt-list">
        <div class="lan-flag">
            <div class="en-flag {{ (config('app.locale')=='en' ) ? '' : 'd-none' }}">
                <img src="{{ asset('assets/lan/en.png') }}" alt="lan flag" />
            </div>
            <div class="lt-flag {{ (config('app.locale')=='lt' ) ? '' : 'd-none' }}">
                <img src="{{ asset('assets/lan/lt.png') }}" alt="lan flag" />
            </div>
            <div class="rs-flag {{ (config('app.locale')=='rs' ) ? '' : 'd-none' }}">
                <img src="{{ asset('assets/lan/rs.png') }}" alt="lan flag" />
            </div>
            <div class="lan-pop-up d-none">
                <div class="lan-name" onclick="changeLanguage('en')">@lang('lang.english')</div>
                <div class="lan-name" onclick="changeLanguage('lt')">@lang('lang.lithuania')</div>
                <div class="lan-name" onclick="changeLanguage('rs')">@lang('lang.rashiya')</div>
            </div>
        </div>
    </div>
    <div class="m-logout">
        <a href="{{ route('user.logout') }}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">
            <button class="btn p-btn l-btn">@lang('lang.log_out')</button>
            <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </a>
    </div>
</div>
@else
<div class="lg-btn">
    <!-- <span><i class="fas fa-chevron-right"></i> </span> -->
    <!-- <span><button class="btn p-btn r-btn" data-toggle="modal" data-target="#registrationPopUp">@lang('lang.register')</button> </span> -->
    <div class="lan-flag d-none d-lg-block">
        <div class="en-flag {{ (config('app.locale')=='en' ) ? '' : 'd-none' }}">
            <img src="{{ asset('assets/lan/en.png') }}" alt="lan flag" />
        </div>
        <div class="lt-flag {{ (config('app.locale')=='lt' ) ? '' : 'd-none' }}">
            <img src="{{ asset('assets/lan/lt.png') }}" alt="lan flag" />
        </div>
        <div class="rs-flag {{ (config('app.locale')=='rs' ) ? '' : 'd-none' }}">
            <img src="{{ asset('assets/lan/rs.png') }}" alt="lan flag" />
        </div>
        <div class="lan-pop-up d-none">
            <div class="lan-name" onclick="changeLanguage('en')">@lang('lang.english')</div>
            <div class="lan-name" onclick="changeLanguage('lt')">@lang('lang.lithuania')</div>
            <div class="lan-name" onclick="changeLanguage('rs')">@lang('lang.rashiya')</div>
        </div>
    </div>
    <span><button class="btn p-btn l-btn" data-toggle="modal" data-target="#loginPopUp">@lang('lang.log_in')</button> </span>
</div>
@endif