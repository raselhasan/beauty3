<section class="header-filter">
	<div class="form-section form-sh d-block">
		<div class="form-all">
			<div class="row mar-0">
				<div class="col-12 col-md-6 col-lg-5 pad-0">
					<div class="input-group search-in">
						<input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
						<div class="input-group-append">
							<span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 pad-0">
					<div class="input-group">
						<div class="ctm-select">
							<div class="ctm-select-txt pad-l-10">
								<span class="select-txt noselect" id="category">@lang('lang.choose')</span>
								<span class="select-arr"><i class="fas fa-caret-down"></i></span>
							</div>
							<div class="ctm-option-box">
								<div class="ctm-option noselect">@lang('lang.volksagen')</div>
								<div class="ctm-option noselect">@lang('lang.volksagen')</div>
								<div class="ctm-option noselect">@lang('lang.volksagen')</div>
								<div class="ctm-option noselect">@lang('lang.volksagen')</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 pad-0">
					<div class="form-group">
						<div class="ctm-select">
							<div class="ctm-select-txt pad-l-10">
								<span class="select-txt noselect" id="category">@lang('lang.choose')</span>
								<span class="select-arr"><i class="fas fa-caret-down"></i></span>
							</div>
							<div class="ctm-option-box">
								<div class="ctm-option-ch"><label class="ctm-con mar-0">@lang('lang.men')
										<input type="checkbox" name="category[]" value="men">
										<span class="checkmark"></span>
									</label></div>
								<div class="ctm-option-ch"><label class="ctm-con mar-0">@lang('lang.men')
										<input type="checkbox" name="category[]" value="men">
										<span class="checkmark"></span>
									</label></div>
								<div class="ctm-option-ch"><label class="ctm-con mar-0">@lang('lang.men')
										<input type="checkbox" name="category[]" value="men">
										<span class="checkmark"></span>
									</label></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 pad-0">
					<div class="input-group">
						<div class="ctm-select">
							<div class="ctm-select-txt pad-l-10">
								<span class="select-txt noselect" id="category">@lang('lang.choose')</span>
								<span class="select-arr"><i class="fas fa-caret-down"></i></span>
							</div>
							<div class="ctm-option-box noselect">
								<div class="ctm-option noselect">@lang('lang.volksagen')</div>
								<div class="ctm-option noselect">@lang('lang.volksagen')</div>
								<div class="ctm-option noselect">@lang('lang.volksagen')</div>
								<div class="ctm-option noselect">@lang('lang.volksagen')</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-1 pad-0">
					<div class="form-group mar-0">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" id="gridCheck">
							<label class="form-check-label" for="gridCheck">
								@lang('lang.salons')
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" id="gridCheck">
							<label class="form-check-label" for="gridCheck">
								@lang('lang.personal')
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-range">
			<div class="row mar-0">
				<div class="col-12 col-md-6 col-lg-6 r-d">
					<span class="range-tlt">@lang('lang.atstuumas')</span>
					<div class="range-s">
						<input type="range" name="distance" id="html-input-range">
					</div>
					<!-- <div class="demo-output dm">
						<input class="range-slider r1"  type="hidden" value="0,0"/>
					</div> -->
				</div>
				<div class="col-12 col-md-6 col-lg-4 r-d">
					<div class="demo-output dm1">
						<input class="range-slider r2" type="hidden" value="0,50" />
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 r-d">
					<div class="ran-bar">
						<span class="bar-txt">@lang('lang.personal')<i class="fas fa-bars"></i></span> <i class="fa fa-envelope"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="line-h"><span><i class="fas fa-chevron-down"></i></span></div>
</section>