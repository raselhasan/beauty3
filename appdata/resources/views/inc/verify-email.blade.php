<!-- Modal -->

<div class="modal fade" id="verifyEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Verify Email</h5>
      </div>
      <div class="modal-body">
        <form id="v-e-send" method="post" action="{{url('user/send-email-after-login')}}">
          @csrf
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control verifyEmail" id="v-e-email-address" aria-describedby="emailHelp" required>
            <small id="emailHelp1" class="form-text text-muted"></small>
          </div>
          <button type="submit" class="btn btn-primary">Verify</button>
        </form>
        <form id="v-e-code">
          <div class="form-group">
            <label for="exampleInputEmail1">Verification Code</label>
            <input type="text" class="form-control verifyEmail" id="v-e-v-code" aria-describedby="emailHelp" required>
            <small id="emailHelp2" class="form-text text-muted"></small>
          </div>
          <button type="submit" class="btn btn-primary">Verify</button>
        </form>
      </div>
    </div>
  </div>
</div>
