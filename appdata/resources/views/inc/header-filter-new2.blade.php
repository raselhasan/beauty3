<input type="hidden" name="category" id="category" value="{{$item['category']}}" class="s_category">
<input type="hidden" name="sub_category" id="sub_category" value="{{$item['sub_category']}}" class="s_sub_category">
<input type="hidden" name="inner_category" id="inner_category" value="{{@$item['inner_category']}}" class="s_inner_category">
<input type="hidden" name="price" id="price" value="{{$item['price']}}">
<input type="hidden" name="distance" id="distance" value="{{$item['distance']}}">
<section class="header-filter">
	<div class="form-section form-sh">
		<div class="form-all">
			<div class="row mar-0">
				<div class="col-12 col-md-6 col-lg-2 p-0">
					<div class="form-group p-0 m-0 slct-adr">
						<span class="adr-icon"><i class="fa fa-map-marker-alt"></i></span>
						<input type="text" class="form-control" id="service-address" aria-describedby="emailHelp" placeholder="Type address" autocomplete="off" value="@if(Session::has('address')){{Session::get('address')}}@else {{ 'Rasel' }} @endif">
						<span class="select-arr"><i class="fas fa-plus"></i></span>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2">
					<div class="form-group p-0 m-0 slct-adr">
						<span><label class="ctm-container">@lang('lang.paslaugos')
							<input type="checkbox" class="personal" name="personal" value="1" @if(@$item['personal'] ==1) checked @endif>
							<span class="checkmark"></span>
						</label></span>
						<span><label class="ctm-container">@lang('lang.meistrai')
							<input type="checkbox" class="salons" name="salons" value="1" @if(@$item['salons'] ==1) checked @endif>
							<span class="checkmark"></span>
						</label></span>
					</div>
				</div>
				<div class="col-12 col-md-4 col-lg-3 pr-0">
					<div class="form-group p-0 m-0 slct-adr">
						<span class="adr-icon">@lang('lang.km')</span>
						<!-- <span class="adr-min">@lang('lang.min')</span> -->
						<div class="range-mul range-mul1" id="hd1" onclick="getServices(1)"></div>
						<!-- <span class="adr-max">@lang('lang.max')</span> -->
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-4 pr-0">
					<div class="form-group p-0 m-0 slct-adr">
						<span class="adr-icon">@lang('lang.c')</span>
						<span class="adr-min">@lang('lang.min')</span>
						<div class="range-mul" id="hd2" onclick="getServices(2)"></div>
						<span class="adr-max">@lang('lang.max')</span>
					</div>
				</div>
				<div class="col-12 col-md-2 col-lg-1 pr-0">
					<div class="form-group p-0 m-0 slct-adr">
						<div class="ran-tlt" onclick="resetAll()">@lang('lang.anuliuoti')</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<div class="line-h">
		<!-- <span class="line-icon"><i class="fas fa-chevron-down"></i></span> -->
	</div>
</section>