<!-- Modal -->
<div class="modal fade" id="registrationPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<section class="header-filter mar-0">
					<div class="form-section">
						<div class="form-all">
							<div class="row mar-0">
								<div class="col-12 pad-0">
									<h1 class="title">@lang('lang.register')</h1>
								</div>
								<div class="col-12 pad-0 step-1">
									<div class="form-group">
										<a href="{{url('user/redirect',['facebook'])}}">
											<button class="loginBtn loginBtn--facebook" type="button">
												@lang('lang.login_with_facebook')
											</button>
										</a>
										<a href="{{url('user/redirect',['google'])}}">
											<button class="loginBtn loginBtn--google" type="button">
												@lang('lang.login_with_google')
											</button>
										</a>
									</div>
								</div>
								<form action="{{ route('registration') }}" method="post" class="wth-100-p step-1" onsubmit="return validateUserRes(event);">
									@csrf
									<div class="col-12 pad-0 acc-type">
										<div class="form-group">
											<label for="RName">@lang('lang.account_type')<span class="c-red">*</span></label>
											<div class="row">
												<div class="col-md-6">
													<div class="form-check">
														<input class="form-check-input account_type" type="radio" name="account_type" id="exampleRadios1" value="1">
														<label class="form-check-label" for="exampleRadios1">
															@lang('lang.salons')
														</label>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-check">
														<input class="form-check-input account_type" type="radio" name="account_type" id="exampleRadios2" value="2" checked>
														<label class="form-check-label" for="exampleRadios2">
															@lang('lang.personal')
														</label>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-12 pad-0">
										<div class="form-group">
											<label for="RName">@lang('lang.name')<span class="c-red">*</span></label>
											<input type="text" id="RName" class="form-control" placeholder="Name" aria-label="Name" name="name">
										</div>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<label for="RSurname">@lang('lang.usurname')</label>
											<input type="text" id="RSurname" class="form-control" placeholder="Surname" aria-label="Surname" name="surname">
										</div>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<label for="RNickname">@lang('lang.nickname') @lang('lang.or') @lang('lang.company_name')<span class="c-red">*</span></label>
											<input type="text" id="RNickname" class="form-control" placeholder="@lang('lang.nickname') @lang('lang.or') @lang('lang.company_name')" aria-label="Nickname" name="nickname">
										</div>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<label for="REmail">@lang('lang.email') <span class="c-red">*</span></label>
											<input type="email" id="REmail" class="form-control" placeholder="Email" aria-label="Email" name="email">
										</div>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<label for="REmail">@lang('lang.phone') <span class="c-red">*</span></label>
											<input type="number" id="RNPhone" class="form-control" placeholder="37064579739" aria-label="Email" name="phone">
										</div>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<label for="RPassword">@lang('lang.password')<span class="c-red">*</span></label>
											<input type="password" id="RPassword" class="form-control" placeholder="Password" aria-label="Password" name="password">
										</div>
									</div>
									<div class="col-12 pad-0">
										<h5 class="c-red p-l-10 reg-error d-none"></h5>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<button class="btn btn-ctm">@lang('lang.registration')</button>
										</div>
									</div>
								</form>
								<form class="wth-100-p step-2" onsubmit="return verifyPhone(event);">
									<div class="col-12 pad-0">
										<p>Verification Code has been sent to your phone please verify account</p>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<input type="text" id="RCode" class="form-control" placeholder="Enter Varification Code" aria-label="Password" name="v_code" required>
											<span class="v_code_error"></span>
										</div>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<button class="btn btn-ctm" type="submit">Verify Phone</button>
										</div>
									</div>
								</form>
								<form class="wth-100-p step-3" onsubmit="return verifyUserEmail(event);">
									<div class="col-12 pad-0">
										<p>Verification Code has been sent to your Email please verify account</p>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<input type="text" id="REmailV" class="form-control" placeholder="Enter Varification Code" aria-label="Password" name="v_code">
											<span class="v_code_error2"></span>
										</div>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<button class="btn btn-ctm" type="submit">Verify Email</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v5.0"></script>