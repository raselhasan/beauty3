<!-- Modal -->
<div class="modal fade" id="loginPopUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<section class="header-filter mar-0">
					<div class="form-section">
						<div class="form-all">
							<div class="row mar-0">
								<div class="col-12 pad-0">
									<h1 class="title">@lang('lang.login')</h1>
								</div>
								<div class="col-12 pad-0">
									<div class="form-group">
										<a href="{{url('user/redirect',['facebook'])}}">
											<button class="loginBtn loginBtn--facebook" type="button">
												@lang('lang.login_with_facebook')
											</button>
										</a>
										<a href="{{url('user/redirect',['google'])}}">
											<button class="loginBtn loginBtn--google" type="button">
												@lang('lang.login_with_google')
											</button>
										</a>
									</div>
								</div>
								<form action="{{ route('user.login.submit') }}" method="post" class="wth-100-p" onsubmit="validateUserLogin(event);">
									@csrf
									<div class="col-12 pad-0">
										<div class="form-group">
											<label for="LEmail">@lang('lang.email') <span class="c-red">*</span></label>
											<input type="text" id="LEmail" class="form-control" placeholder="Email or Phone" aria-label="Email" name="email">
										</div>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<label for="LPassword">@lang('lang.password')<span class="c-red">*</span></label>
											<input type="password" id="LPassword" class="form-control" placeholder="Password" aria-label="Password" name="password">
										</div>
									</div>
									<div class="col-12 pad-0">
										<h5 class="c-red p-l-10 login-error d-none"></h5>
									</div>
									<div class="col-12 pad-0">
										<div class="form-group">
											<button class="btn btn-ctm">@lang('lang.login')</button>
											<button class="btn r-btn" type="button" data-toggle="modal" data-target="#registrationPopUp" onclick="$('#loginPopUp').modal('hide')">
												@lang('lang.register')
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v5.0"></script>