<div class="header-section header1-section">
	<div class="main-menu">
		<nav class="navbar navbar-expand-lg navbar-light">
			<a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/img/logo1.jpg')}}"></a>
			<div class="ss" style="position: relative;">
				<div class="search-ic">
					<i class="fab fa-searchengin"></i>
				</div>
				<div class="search-show">
					<div class="input-group search-item">
						<input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
						<div class="input-group-append">
							<span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
						</div>
					</div>
				</div>
			</div>
			
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class=""><i class="fas fa-bars"></i></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<!-- <li class="nav-item search-li">
						<i class="fab fa-searchengin"></i>
					</li> -->
					<li class="nav-item">
						<a class="nav-link active" href="{{url('/')}}">@lang('lang.home')</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('/blog')}}">@lang('lang.pages')</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('/about-model')}}">@lang('lang.models')</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('/user-profile')}}">@lang('lang.appointment')</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="#">@lang('lang.faq')FAQ</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="#">@lang('lang.contact')Contact</a>
					</li>
				</ul>
			</div>
			<div class="lg-btn">
				<span><button class="btn p-btn l-btn">@lang('lang.log_in')</button> </span>
				<span><i class="fas fa-chevron-right"></i> </span>
				<span><button class="btn p-btn r-btn">@lang('lang.register')</button> </span>
			</div>
		</nav>
	</div>
	<div class="line-h"><span><i class="fas fa-chevron-down"></i></span></div>
</div>