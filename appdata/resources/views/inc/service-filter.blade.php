<form action="" method="GET" id="searchForm">
<input type="hidden" name="category" id="category" value="{{$item['category']}}">
<input type="hidden" name="sub_category" id="sub_category" value="{{$item['sub_category']}}">
<input type="hidden" name="inner_category" id="inner_category" value="{{@$item['inner_category']}}">
<input type="hidden" name="price" id="price" value="{{$item['price']}}">
<input type="hidden" name="distance" id="distance" value="{{$item['distance']}}">
<section class="header-filter">
	<div class="form-section form-sh">
		<div class="form-all">
			<div class="row mar-0"> 
				<div class="col-12 col-md-12 col-lg-9 pad-0">
					<div class="form-range">
						<div class="row mar-0">
							<div class="col-12 col-md-6 col-lg-6 r-d">
								<span class="range-tlt">@lang('lang.atstuumas')</span>
								{{-- <div class="range-s">
									<input type="range" id="html-input-range" onchange="getServices(1)">
								</div> --}}
								<div class="w-100" id="single_range" onclick="getServices(1)"></div>
								<!-- <div class="demo-output dm">
									<input class="range-slider r1"  type="hidden" value="0,0"/>
								</div> -->
							</div>
							<div class="col-12 col-md-6 col-lg-4 r-d">
								{{-- <div class="demo-output dm1">
									<input class="range-slider get-price r2" type="hidden" value="0,100" onchange="getServices(2)">
								</div> --}}
								<div class="w-100" id="multi_range" onclick="getServices(2)"></div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 pad-0">
					<div class="form-group">
						<input type="text" class="form-control" id="service-address" aria-describedby="emailHelp" placeholder="Type address" autocomplete="off" value="@if(Session::has('address')){{Session::get('address')}}@else {{$item['address']}} @endif">
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-1 pad-0">
					<div class="form-group mar-0">
						<div class="form-check">
							<input class="form-check-input salons" type="checkbox" id="gridCheck" name="salons" value="1" @if(@$item['salons'] ==1) checked @endif>
							<label class="form-check-label" for="gridCheck">
								@lang('lang.salons')
							</label>
						</div><div class="form-check">
							<input class="form-check-input personal" type="checkbox" id="gridCheck" name="personal" value="1"  @if(@$item['personal'] ==1) checked @endif>
							<label class="form-check-label" for="gridCheck">
								@lang('lang.personal')
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="line-h"><span><i class="fas fa-chevron-down"></i></span></div>
</section>
</form>