<section class="footer-section">
	<div class="max-fix-width">
		<div class="row mar-0">
			<div class="col-12 col-lg-4">
				<div class="d-con">
					<div class="f-logo"><img src="{{asset('assets/img/logo1.png')}}"></div>
					<div class="f-txt">@lang('lang.footer_text')</div>
					<div class="f-card">
						<img src="{{ asset('assets/img/pay-1.png') }}" alt="Left Arrow Key">
						<img src="{{ asset('assets/img/pay-2.png') }}" alt="Left Arrow Key">
						<img src="{{ asset('assets/img/pay-3.png') }}" alt="Left Arrow Key">
						<img src="{{ asset('assets/img/pay-4.png') }}" alt="Left Arrow Key">
						<img src="{{ asset('assets/img/pay-5.png') }}" alt="Left Arrow Key">
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-5">
				<div class="f-menu">
					<div class="f-menu1">
						<h1>@lang('lang.navigation')</h1>
						<ul>
							<li><a href="#">@lang('lang.home')</a></li>
							<li><a href="#">@lang('lang.pages')</a></li>
							<li><a href="#">@lang('lang.models')</a></li>
							<li><a href="#">@lang('lang.appointment')</a></li>
							<li><a href="#">@lang('lang.faq')</a></li>
							<li><a href="#">@lang('lang.contact')Cont</a></li>
						</ul>
					</div>
					<div class="f-menu1">
						<h1>@lang('lang.navigation')</h1>
						<ul>
							<li><a href="#">@lang('lang.home')</a></li>
							<li><a href="#">@lang('lang.pages')</a></li>
							<li><a href="#">@lang('lang.models')</a></li>
							<li><a href="#">@lang('lang.appointment')</a></li>
							<li><a href="#">@lang('lang.faq')</a></li>
							<li><a href="#">@lang('lang.contact')</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-3 p-0">
				<div id="carouselExampleControls1" class="carousel slide" data-interval="false" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="footer-adv">
								<h1>@lang('lang.ypatingi_pasiulymai')</h1>
								<div class="footer-adv-img">
									<img src="{{asset('assets/img/services/demo.jpg')}}">
								</div>
							</div>
						</div>
						<div class="carousel-item ">
							<div class="footer-adv">
								<h1>@lang('lang.ypatingi_pasiulymai')</h1>
								<div class="footer-adv-img">
									<img src="{{asset('assets/img/services/demo.jpg')}}">
								</div>
							</div>
						</div>
						<div class="carousel-item ">
							<div class="footer-adv">
								<h1>@lang('lang.ypatingi_pasiulymai')</h1>
								<div class="footer-adv-img">
									<img src="{{asset('assets/img/services/demo.jpg')}}">
								</div>
							</div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
						<span><i class="fas fa-chevron-left"></i></span>
						<span class="sr-only">@lang('lang.previous')</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
						<span><i class="fas fa-chevron-right"></i></span>
						<span class="sr-only">@lang('lang.next')</span>
					</a>
				</div>

				<div class="adv-btn">
					<button class="btn ad-btn1">@lang('lang.prenumeruoti_naujienas')</button>
					<button class="btn ad-btn2">@lang('lang.susisiekti')</button>
				</div>
			</div>
		</div>
		<div class="copy-txt">
			<a href="{{url('terms-and-conditions')}}">@lang('lang.terms_and_conditions')</a>
			|
			<a href="{{url('privacy-policy')}}">Privacy Policy</a>
			@lang('lang.reserved')
		</div>
	</div>
</section>
<div class="mbl-icon-menu d-flex d-lg-none">
	<a href="{{url('/')}}">
		<div class="menu-item"><img src="{{ asset('assets/img/home.png') }}" alt="menu-icon"></div>
	</a>
	<a href="#">
		<div class="menu-item"><img src="{{ asset('assets/img/offers.png') }}" alt="menu-icon"></div>
	</a>
	<a href="{{url('profile-list')}}">
		<div class="menu-item"><img src="{{ asset('assets/img/users.png') }}" alt="menu-icon"></div>
	</a>
	<a href="{{url('/user/calendar/create')}}" class="auth-check <?= (Auth::guard('web')->check()) ? '' : 'd-none'; ?>">
		<div class="menu-item">
			<img src="{{ asset('assets/img/sell_service.png') }}" alt="menu-icon">
			<span class="count-l">2</span>
		</div>
	</a>
	<a href="{{url('/user/booking-list')}}" class="auth-check <?= (Auth::guard('web')->check()) ? '' : 'd-none'; ?>">
		<div class="menu-item">
			<img src="{{ asset('assets/img/buy_service.png') }}" alt="menu-icon">
			<span class="count-l">2</span>
		</div>
	</a>
</div>