<div class="header-section header1-section">
	<div class="main-menu">
		<nav class="navbar navbar-expand-lg navbar-light">
			<a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/img/logo1.png')}}"></a>
			<div class="ss d-none d-lg-block">
				<div class="search-show">
					<input type="hidden" name="visitor_lat" id="visitor_lat" value="{{Session::get('lat')}}">
					<input type="hidden" name="visitor_lng" id="visitor_lng" value="{{Session::get('lng')}}">
					<input type="hidden" name="visitor_address" id="visitor_address" value="{{Session::get('address')}}">
					<div class="input-group search-item">
						<input type="text" onclick="searchByTitle(event, this.value)" onkeyup="searchByTitle(event, this.value)" class="form-control search-keyword" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2" value="{{@$item['title']}}">
						<div class="input-group-append">
							<span class="input-group-text" id="basic-addon2" onclick="goService()"><i class="fas fa-search"></i></span>
						</div>
					</div>
					<div class="ss-src-result d-none put-service-result">

					</div>
				</div>
			</div>
			<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class=""><i class="fas fa-bars"></i></span>
			</button> -->
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link active" href="{{url('/')}}">@lang('lang.home')</a>
					</li>
					<!-- <li class="nav-item">
						<a class="nav-link dot" href="#">@lang('lang.offers')</a>
					</li> -->
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('profile-list')}}">@lang('lang.users')</a>
					</li>
					<li class="nav-item auth-check <?= (Auth::guard('web')->check()) ? '' : 'd-none'; ?>">
						<a class="nav-link dot" href="{{url('/user/calendar/create')}}">@lang('lang.sell_services')</a>
						<span class="count-l">2</span>
					</li>
					<li class="nav-item auth-check <?= (Auth::guard('web')->check()) ? '' : 'd-none'; ?>">
						<a class="nav-link dot" href="{{url('/user/booking-list')}}">@lang('lang.buy_services')</a>
						<span class="count-l">2</span>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('live-zone')}}">@lang('lang.live_zone')</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('favarite')}}"><i class="fa fa-heart heart-menu"></i></a>

					</li>

				</ul>
			</div>

			<div class="mbl-lang d-block d-lg-none">
				<div class="short-select">
					<div class="ctm-select" ctm-slt-n="cat_name">
						<div class="ctm-select-txt pad-l-10">
							<span class="select-txt" id="category">EN</span>
						</div>
						<div class="ctm-option-box">
							<div class="ctm-option">EN</div>
							<div class="ctm-option">LI</div>
							<div class="ctm-option">RU</div>
						</div>
					</div>
					<span class="m-s-ico" data-toggle="modal" data-target="#searchModal"><i class="fas fa-search"></i></span>
				</div>
			</div>
			<div class="pro-cnf-area">
				<div class="usr-main-p {{ (Auth::guard('web')->check()) ? '' : 'd-none'}}">
					<a href="{{url('/user/profile')}}"><img class="cover" src="{{asset('assets/img/man.jpg')}}" alt="jglj"></a>
				</div>
				<div class="setting-btn {{ (Auth::guard('web')->check()) ? '' : 'd-none'}}">
					<i class="fas fa-cog"></i>
				</div>
			</div>
			<div class="switch-acnt-area">
				@include('inc.switch-acnt')
			</div>
			<button class="btn d-none d-lg-block filter-shw filter-m-btn">@lang('lang.filtruoti')</button>
		</nav>
		<div class="mobile-bottom-menu d-flex d-lg-none">
			<div class="m-b-li">
				<span class="hs-menubar">
					<div class="menu-trigger"> <img src="{{ asset('assets/img/category-icon.png') }}" alt="lan flag" /><span class="li-txt">KATEGORIJOS</span></div>
				</span>

			</div>
			<div class="m-b-li d-li">
				<span class="d-li filter-shw"><img src="{{ asset('assets/img/filter-icon.png') }}" alt="lan flag" /><span class="li-txt">FILTRAS</span></span>
			</div>
			<div class="m-b-li">
				<span class="d-li"><img src="{{ asset('assets/img/menu-icon.png') }}" alt="lan flag" /><span class="li-txt">menu</span></span>
			</div>
		</div>
	</div>
	<div class="mb-src-box d-none">
		<div class="ss d-block d-md-none">
			<div class="search-show">
				<div class="input-group search-item">
					<input type="text" onclick="searchByTitle(event, this.value)" onkeyup="searchByTitle(event, this.value)" class="form-control search-keyword" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
					<div class="input-group-append">
						<span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
					</div>
				</div>
				<div class="ss-src-result d-none put-service-result">

				</div>
			</div>
		</div>
	</div>

	<div class="multi-menu d-none d-lg-block">
		<div class="main-menu1">
			<nav class="navbar navbar-expand-lg navbar-light ">
				<button class="navbar-toggler" type="button">
					<span class=""><i class="fas fa-bars"></i></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					@php
					$menus = App\Category::get();
					@endphp
					@if($menus->count())
					<ul class="navbar-nav mr-auto">
						@foreach($menus as $menu)
						<li class="nav-item show-sub-m">
							<a class="nav-link comon-link" href="{{url('services/'.App\Services\MakeUrlService::url($menu->name_lt).'?category='.$menu->id)}}">{{ $menu->name_lt }}</a>
							<div class="sub-menu sub-menu1">
								@php
								$n = 0;
								$k = $menu->subCategories->count() > 4 ? 4 : $menu->subCategories->count();
								@endphp
								@for($i=0; $i < $k; $i++) <div class="inner-menu">
									@php
									$row = ceil($menu->subCategories->count() / 4)
									@endphp
									@if($i < $menu->subCategories->count() - (4 * ($row - 1)))
										@php $row = $row @endphp
										@else
										@php $row = $row - 1 @endphp
										@endif
										@for($j=0; $j < $row; $j++) @if($menu->subCategories->count() >= $n + 1)
											<div class="link-all">
												<div class="inner-tlt"> <a href="{{url('services/'.App\Services\MakeUrlService::url($menu->name_lt).'/'.App\Services\MakeUrlService::url($menu->subCategories[$n]->name_lt).'?sub_category='.$menu->subCategories[$n]->id)}}" class="comon-link">{{ $menu->subCategories[$n]->name_lt }}</a></div>
												<div class="inner-link">
													@php
													$inrCats = $menu->subCategories[$n]->innerCategories;
													@endphp
													@foreach($inrCats as $inrCat)
													<div><span>.</span><a href="{{url('services/'.App\Services\MakeUrlService::url($menu->name_lt).'/'.App\Services\MakeUrlService::url($menu->subCategories[$n]->name_lt).'/'.App\Services\MakeUrlService::url($inrCat->name_lt).'?inner_category='.$inrCat->id)}}" class="comon-link">{{ $inrCat->name_lt }}</a></div>
													@endforeach
													@php
													$n++
													@endphp
												</div>
											</div>
											@endif
											@endfor
							</div>
							@endfor
				</div>
				</li>
				@endforeach
				</ul>
				@endif
		</div>
		</nav>
	</div>
</div>

<nav class="hs-navigation">
	<ul class="nav-links">
		@php
		$menus = App\Category::get();
		@endphp
		@foreach($menus as $menu)
		<li class="{{ ($menu->subCategories->count()) ? 'has-child' : '' }}">
			<span class="{{ ($menu->subCategories->count()) ? 'its-parent' : '' }}">
				<a href="{{url('services/'.App\Services\MakeUrlService::url($menu->name_lt).'?category='.$menu->id)}}" class="comon-link">
					<span class="icon">
						<i class="zmdi zmdi-device-hub"></i>
					</span>
					{{ $menu->name_lt }}
				</a>
			</span>
			@if($menu->subCategories->count())
			<ul class="its-children">
				@foreach($menu->subCategories as $subCat)
				<li class="{{ ($subCat->innerCategories->count()) ? 'has-child' : '' }}">
					<span class="{{ ($subCat->innerCategories->count()) ? 'its-parent' : '' }}">
						<a href="{{url('services/'.App\Services\MakeUrlService::url($menu->name_lt).'/'.App\Services\MakeUrlService::url($subCat->name_lt).'?sub_category='.$subCat->id)}}" class="comon-link"> {{ $subCat->name_lt }}</a>
					</span>
					@if($subCat->innerCategories->count())
					<ul class="its-children">
						@foreach($subCat->innerCategories as $innerCat)
						<li> <a href="{{url('services/'.App\Services\MakeUrlService::url($menu->name_lt).'/'.App\Services\MakeUrlService::url($subCat->name_lt).'/'.App\Services\MakeUrlService::url($innerCat->name_lt).'?inner_category='.$innerCat->id)}}" class="comon-link">{{ $innerCat->name_lt }} </a> </li>
						@endforeach
					</ul>
					@endif
				</li>
				@endforeach
			</ul>
			@endif
		</li>
		@endforeach
	</ul>
</nav>
</div>

<div class="search-box header-filter">
	<div class="form-section">
		<div class="form-all">
			<div class="input-group search-in">
				<input type="text" class="form-control" placeholder="Search" aria-label="Recipient's username" aria-describedby="basic-addon2">
				<div class="input-group-append">
					<button class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</div>
	</div>
	<span class="s-close">&#10006;</span>
</div>