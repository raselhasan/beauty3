<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>@lang('lang.document')</title>

	<link rel="stylesheet" href="{{ asset('assets/fontawesome/css/fontawesome-all.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
</head>

<body>
	<div id="app">
		<create-calendar :api_token="'{{ $api_token }}'"></create-calendar>
	</div>
	<script>
		window.csrf_token = "<?= csrf_token() ?>";
		window.base_url = "<?= url('/') ?>";
		window.user_id = <?= Auth::user()->id ?>;
		window.user = <?= Auth::user() ?>;
	</script>
	<script src="{{ asset('assets/js/app.js') }}"></script>
	<script>
		$(function() {
			$(document).on("click", ".admin-p", function() {
				$(".admin-box").toggle();
			});
		});

		$(document).on("click", function(e) {
			if (!$(e.target).hasClass("admin-p") &&
				$(e.target).parents(".admin-p").length === 0 &&
				$(e.target).parents(".admin-box").length === 0
			) {
				$(".admin-box").hide();
			};
		});
	</script>
</body>

</html>