 @extends('layout.app')
 @section('title')
 Home - GrožioKalviai
 @endsection
 @section('meta_description')
 <meta name="description" content="Grižio meistrai ir grožio paslaugos vienoje vietoje. Plaukų kirpimas, masažai, veido proceduros, nagų tvarkymas, kuno prieziura, SPA procedūros, depiliacija ir kita.">
 @endsection

 @section('style')
 <style>
 	.slider-section .carousel .carousel-inner {
 		border-top: 1px solid #FF8F4C;
 	}

 	.seeMore {
 		color: #e25b37;
 	}
 </style>
 @endsection
 @section('content')
 @include('inc.header-filter-new')
 <section class="slider-section">
 	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
 		<ol class="carousel-indicators">
 			<li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
 			<li data-target="#carouselExampleControls" data-slide-to="1"></li>
 			<li data-target="#carouselExampleControls" data-slide-to="2"></li>
 		</ol>
 		<div class="carousel-inner">
 			<div class="carousel-item active">
 				<img src="{{asset('assets/img/services/slider-cover.png')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p><span> @lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur') </span></p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/services/slider-cover.png')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p><span> @lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur') </span></p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/services/slider-cover.png')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p><span> @lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur') </span></p>
 				</div>
 			</div>
 		</div>
 		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
 			<span><img src="{{ asset('assets/img/services/ar-l.png') }}" alt="Right Arrow Key"></span>
 			<span class="sr-only">@lang('lang.previous')</span>
 		</a>
 		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
 			<span><img src="{{ asset('assets/img/services/ar-r.png') }}" alt="Left Arrow Key"></span>
 			<span class="sr-only">@lang('lang.next')</span>
 		</a>
 	</div>
 </section>
 <!-- <div class="vip-profile-service">

</div> -->

 <section class="cat-vip-section">
 	<section class="vip-section vip-section1">
 		<div class="slider-all-cnt">
 			<div class="section-title">
 				<div><i class="fas fa-caret-right"></i> VIP PASLAUGOS <i class="fas fa-caret-left"></i></div>
 				<div class="star-tlt">
 					<span>
 						<img src="{{asset('assets/img/services/star.png')}}">
 						<img src="{{asset('assets/img/services/star.png')}}">
 						<img src="{{asset('assets/img/services/star.png')}}">
 						<img src="{{asset('assets/img/services/star.png')}}">
 						<img src="{{asset('assets/img/services/star.png')}}">
 					</span>
 				</div>
 				<button class="btn float-right vip-btn">VISOS PASLAUGOS</button>
 			</div>
 			<div class="swiper-container vip-container1">
 				<div class="swiper-wrapper put-vip-data">
 					{{-- @foreach($vipservices as $service)
 					<div class="swiper-slide">
 						<div class="single-content ">
 							<div class="active-div">
 								<img src="{{asset('assets/img/services/active-add.png')}}">
 							</div>
 							<div class="content-img-p">
 								<div class="content-img">
 									<img src="{{asset('assets/img/services/' . $service->service->image)}}">
 								</div>
 								<div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
 							</div>
 							<div class="content-tlt">
 								{{$service->service->title}}
 							</div>
 						</div>
 					</div>
 					@endforeach --}}
 				</div>
 			</div>
 			<div class="slider-btm-txt text-v-hide1">
 				<div class="btm-txt ">- Atraskite būtent tai ko jums reikia -</div>
 				<div class="btm-arr"><i class="fas fa-caret-down"></i></div>
 			</div>

 			<!-- Add Arrows -->
 			<div class="swiper-button-next  swiper-button-next1"><img src="{{ asset('assets/img/services/ar-r.png') }}" alt="Left Arrow Key"></div>
 			<div class="swiper-button-prev  swiper-button-prev1"><img src="{{ asset('assets/img/services/ar-l.png') }}" alt="Right Arrow Key"></div>
 		</div>
 	</section>
 	<section class="vip-section vip-sec-info1">
 		<div class="slider-all-cnt">
 			<div class="swiper-container vip-info1">
 				<div class="swiper-wrapper put-vip-type">
 					{{-- @for($i=0;$i< $vipservices->count(); $i++) 
 					<div class="swiper-slide invisible">
 							<div class="slider-info">
 								<div class="info-tlt"> - {{$vipservices[$i]->service->title}} - <span class="top-b-icon"><img src="{{ asset('assets/img/services/top-b-i.png') }}" alt="Left Arrow Key"></span></div>
 								<div class="info-itm">
 									@php $n = 0 @endphp
 									@foreach($vipservices[$i]->service->types as $type)
 									@if($n++ < 3) 
 									<div class="itm">
 										<span class="itm-name">{{$type->name}}</span>
 										<span class="float-right itm-price">nuo {{$type->price}} €</span>
 								</div>
 								@else
 								@break
 								@endif
 								@endforeach
 							</div>
 							<div class="info-btm">
 								<span class="dis"><i class="fa fa-map-marker-alt"></i> 1.2km</span>
 								<span class="star"><i class="fas fa-star"></i> 4/5</span>
 								<span class="btm-txt">LAISVAS</span>
 								<span class="btm-link">DAUGIAU</span>
 							</div>
 						</div>
 				</div>
 				@endfor --}}
 			</div>
 		</div>
 		<!-- Add Arrows -->
 		<div class="swiper-button-next  swiper-button-next1 invisible">
 			<img src="{{ asset('assets/img/services/ar-r.png') }}" alt="Left Arrow Key">
 		</div>
 		<div class="swiper-button-prev  swiper-button-prev1 invisible">
 			<img src="{{ asset('assets/img/services/ar-l.png') }}" alt="Right Arrow Key">
 		</div>
 	</section>
 	<section class="vip-section vip-section2">
 		<div class="slider-all-cnt">
 			<div class="section-title">
 				<div><i class="fas fa-caret-right"></i> VIP MEISTRAI <i class="fas fa-caret-left"></i></div>
 				<div class="star-tlt">
 					<span>
 						<img src="{{asset('assets/img/services/star.png')}}">
 						<img src="{{asset('assets/img/services/star.png')}}">
 						<img src="{{asset('assets/img/services/star.png')}}">
 						<img src="{{asset('assets/img/services/star.png')}}">
 						<img src="{{asset('assets/img/services/star.png')}}">
 					</span>
 				</div>
 				<button class="btn float-right vip-btn">VISOS PASLAUGOS</button>
 			</div>
 			<div class="swiper-container vip-container2">
 				<div class="swiper-wrapper put-profile">
 					@foreach($profiles as $profile)
 					{{-- <div class="swiper-slide">
 						<div class="single-content">
 							<div class="active-div">
 								<img src="{{asset('assets/img/services/active-add.png')}}">
 							</div>
 							<div class="content-img-p">
 								<div class="content-img">
 									<img src="{{asset('userImage/fixPic/' . $profile->profile_image)}}">
 								</div>
 								<div class="vip-logo"><img src="{{asset('assets/img/services/vip_logo.png')}}" alt="img"></div>
 							</div>
 							<div class="content-tlt">
 								{{$profile->name}} {{$profile->surname}}
 							</div>
 						</div>
 					</div> --}}
 					@endforeach
 				</div>
 			</div>
 			<div class="slider-btm-txt text-v-hide2">
 				<div class="btm-txt">- Atraskite būtent tai ko jums reikia -</div>
 				<div class="btm-arr"><i class="fas fa-caret-down"></i></div>
 			</div>

 			<!-- Add Arrows -->
 			<div class="swiper-button-next  swiper-button-next1"><img src="{{ asset('assets/img/services/ar-r.png') }}" alt="Left Arrow Key"></div>
 			<div class="swiper-button-prev  swiper-button-prev1"><img src="{{ asset('assets/img/services/ar-l.png') }}" alt="Right Arrow Key"></div>
 		</div>
 	</section>
 	<section class="vip-section vip-sec-info2">
 		<div class="slider-all-cnt">
 			<div class="swiper-container vip-info2">
 				<div class="swiper-wrapper put-profile-inner">
 					{{-- @for($i=0;$i< $profiles->count(); $i++) <div class="swiper-slide invisible">
 							<div class="slider-info">
 								<div class="info-tlt"> - {{$profiles[$i]->name}} {{$profiles[$i]->surname}} - <span class="top-b-icon"><img src="{{ asset('assets/img/services/top-b-i.png') }}" alt="Left Arrow Key"></span></div>
 								<div class="info-itm">
 									@php $n = 0 @endphp
 									@foreach($profiles[$i]->services as $service)
 									@if($n++ < 3) <div class="itm">
 										<span class="itm-name">{{$service->title}}</span>
 								</div>
 								@else
 								@break
 								@endif
 								@endforeach
 							</div>
 							<div class="info-btm">
 								<span class="dis"><i class="fa fa-map-marker-alt"></i> 1.2km</span>
 								<span class="star"><i class="fas fa-star"></i> 4/5</span>
 								<span class="btm-txt">LAISVAS</span>
 								<span class="btm-link">DAUGIAU</span>
 							</div>
 						</div>
 				</div>
 				@endfor --}}
 			</div>
 		</div>
 		<!-- Add Arrows -->
 		<div class="swiper-button-next  swiper-button-next1 invisible">
 			<img src="{{ asset('assets/img/services/ar-r.png') }}" alt="Left Arrow Key">
 		</div>
 		<div class="swiper-button-prev  swiper-button-prev1 invisible">
 			<img src="{{ asset('assets/img/services/ar-l.png') }}" alt="Right Arrow Key">
 		</div>
 	</section>
 	<section class="dr-section">
 		<div class="section-title pt-0"><span class="dr-af"><i class="fas fa-caret-right"></i> KATEGORIJOS <i class="fas fa-caret-left"></i></span></div>
 		<div class="dr-m">
 			<div class="row mar-0">
 				@if(count($categories) > 0)
 				@foreach ($categories as $category)
 				<div class="col-12 col-md-6 col-lg-4">
 					<div class="dr">
 						<div class="dr-l">
 							<span class="dr-ic1">
 								<img src="{{asset('assets/img/services/' . $category['img'])}}">
 							</span>
 							<span class="dr-txt"> {{ $category['name_lt'] }}</span>
 							<span class="dr-ic2">
 								<i class="fas fa-caret-down"></i>
 							</span>
 							<span class="dr-ic3">
 								<i class="fas fa-caret-up"></i>
 							</span>
 						</div>
 						<div class="dr-content">
 							@if(count($category['sub_categories']) > 0)
 							@foreach ($category['sub_categories'] as $subCat)
 							<p><a href="javascript:;" onclick="genereateLink('{{ $category["id"]}}','{{$subCat["id"]}}')">{{$subCat['name_lt']}}</a></p>
 							@endforeach
 							@endif
 						</div>
 					</div>
 				</div>
 				@endforeach
 				@endif
 			</div>
 		</div>
 	</section>
 </section>
 @endsection
 @section('script')
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>

 @include("service_script.index-script");


 <script>
 	var swiper = new Swiper('.ctm-container3', {
 		slidesPerView: 4,
 		spaceBetween: 30,
 		loop: false,
 		allowTouchMove: false,
 		navigation: {
 			nextEl: '.swiper-button-next',
 			prevEl: '.swiper-button-prev',
 		},

 		breakpoints: {
 			1400: {
 				slidesPerView: 4,
 			},
 			1000: {
 				slidesPerView: 3,
 			},
 			700: {
 				slidesPerView: 2,
 			},
 			450: {
 				slidesPerView: 1,
 			}
 		}
 	});
 	var swiper = new Swiper('.vip-container1', {
 		slidesPerView: 4,
 		spaceBetween: 30,
 		loop: false,
 		allowTouchMove: false,
 		navigation: {
 			nextEl: '.vip-section1 .swiper-button-next',
 			prevEl: '.vip-section1 .swiper-button-prev',
 		},

 		breakpoints: {
 			1100: {
 				slidesPerView: 3,
 			},
 			800: {
 				slidesPerView: 2,
 			},
 			450: {
 				slidesPerView: 1,
 			}
 		}
 	});
 	var swiper = new Swiper('.vip-info1', {
 		slidesPerView: 4,
 		spaceBetween: 30,
 		loop: false,
 		allowTouchMove: false,
 		navigation: {
 			nextEl: '.vip-sec-info1 .swiper-button-next',
 			prevEl: '.vip-sec-info1 .swiper-button-prev',
 		},

 		breakpoints: {
 			1100: {
 				slidesPerView: 3,
 			},
 			800: {
 				slidesPerView: 2,
 			},
 			450: {
 				slidesPerView: 1,
 			}
 		}
 	});
 	var swiper = new Swiper('.vip-info2', {
 		slidesPerView: 4,
 		spaceBetween: 30,
 		loop: false,
 		allowTouchMove: false,
 		navigation: {
 			nextEl: '.vip-sec-info2 .swiper-button-next',
 			prevEl: '.vip-sec-info2 .swiper-button-prev',
 		},

 		breakpoints: {
 			1100: {
 				slidesPerView: 3,
 			},
 			800: {
 				slidesPerView: 2,
 			},
 			450: {
 				slidesPerView: 1,
 			}
 		}
 	});
 	var swiper = new Swiper('.vip-container2', {
 		slidesPerView: 4,
 		spaceBetween: 30,
 		loop: false,
 		allowTouchMove: false,
 		navigation: {
 			nextEl: '.vip-section2 .swiper-button-next',
 			prevEl: '.vip-section2 .swiper-button-prev',
 		},

 		breakpoints: {
 			1100: {
 				slidesPerView: 3,
 			},
 			800: {
 				slidesPerView: 2,
 			},
 			450: {
 				slidesPerView: 1,
 			}
 		}
 	});
 	$(document).on('click', '.vip-section1 .swiper-button-next', function() {
 		$(this).parents('.vip-section1').next('.vip-sec-info1').find('.swiper-button-next').click();
 	});
 	$(document).on('click', '.vip-section1 .swiper-button-prev', function() {
 		$(this).parents('.vip-section1').next('.vip-sec-info1').find('.swiper-button-prev').click();
 	});
 	$(document).on('mouseover', '.vip-section1 .swiper-slide, .vip-info1 .swiper-slide', function() {
 		let i = $(this).index();
 		$(this).parents('.vip-section1').next('.vip-sec-info1').find('.swiper-slide').eq(i).removeClass('invisible');
 	}).on('mouseout', function(e) {
 		if ($(e.relatedTarget).hasClass("swiper-slide") ||
 			$(e.relatedTarget).parents(".vip-info1 .swiper-slide").length != 0
 		) {} else {
 			$('.vip-sec-info1').find('.swiper-slide').addClass('invisible');
 		}
 	});
 	$(document).on('click', '.vip-section2 .swiper-button-next', function() {
 		$(this).parents('.vip-section2').next('.vip-sec-info2').find('.swiper-button-next').click();
 	});
 	$(document).on('click', '.vip-section2 .swiper-button-prev', function() {
 		$(this).parents('.vip-section2').next('.vip-sec-info2').find('.swiper-button-prev').click();
 	});
 	$(document).on('mouseover', '.vip-section2 .swiper-slide, .vip-info2 .swiper-slide', function() {
 		let i = $(this).index();
 		$(this).parents('.vip-section2').next('.vip-sec-info2').find('.swiper-slide').eq(i).removeClass('invisible');
 	}).on('mouseout', function(e) {
 		if ($(e.relatedTarget).hasClass("swiper-slide") ||
 			$(e.relatedTarget).parents(".vip-info2 .swiper-slide").length != 0
 		) {} else {
 			$('.vip-sec-info2').find('.swiper-slide').addClass('invisible');
 		}
 	});
 </script>
 @endsection