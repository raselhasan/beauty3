	<script type="text/javascript">
		function calculateAmount(duration_id,package_id,price_id,set_current_duration,btn){
            var data = new FormData( $( 'form#mainForm' )[ 0 ] );
            data.append('package_id', package_id);
            data.append('duration_id', duration_id);
            if(price_id == 'price2'){
            	data.append('star',$('.all-star').attr('starid'));
            	data.append('number',$('.all-star').attr('starnumber'));
            }
            $.ajax( {
                processData: false,
                contentType: false,
                data: data,
                type: 'POST',
                url:'{{url("select-plan/amount-calculation")}}',
                success: function( response ){
                	$('#'+price_id).text(response.price.toFixed(2));
                	$('.'+set_current_duration).attr(set_current_duration,duration_id);
                	addUpdateCart(price_id,response.price.toFixed(2),btn);
                	makeTotal();
                }
                	
            }); 
		}
	</script>
	<script type="text/javascript">
		function addUpdateCart(price_id,total,btn)
		{
			if(price_id == 'price1'){
        		if(parseInt($('#pay1').val()) == 0){
        			$('#pay1').val(1);
					var item ='<div class="order-list list1"><span class="order-name" id="oreder-name1">{{@$package['package1']->title}}</span><span class="order-price">&#128; <span class="cr-price" id="order-price1">'+parseFloat($('#price1').text()).toFixed(2)+'</span></span></div>';
					$('.order-list-all').append(item);
					$('#'+btn).text('Remove from Cart');
        		}else{
        			$('#order-price1').text(total);
        		}
        	}else if(price_id == 'price2'){
        		if(parseInt($('#pay2').val()) == 0){
        			$('#pay2').val(1);
					var name2 = $('#package2_name').val();
					var item ='<div class="order-list list2"><span class="order-name" id="oreder-name2">'+name2+'</span><span class="order-price">&#128; <span class="cr-price" id="order-price2">'+parseFloat($('#price2').text()).toFixed(2)+'</span></span></div>';
					$('.order-list-all').append(item);
					$('#'+btn).text('Remove from Cart');
        		}else{
        			$('#order-price2').text(total);
        		}
        	}else if(price_id == 'price3'){
        		if(parseInt($('#pay3').val()) == 0){
        			$('#pay3').val(1);
					var name3 = '{{@$package['package3']->title}}';
					var item ='<div class="order-list list3"><span class="order-name" id="oreder-name3">'+name3+'</span><span class="order-price">&#128; <span class="cr-price" id="order-price3">'+parseFloat($('#price3').text()).toFixed(2)+'</span></span></div>';
					$('.order-list-all').append(item);
					$('#'+btn).text('Remove from Cart');
        		}else{
        			$('#order-price3').text(total);
        		}
        	}
		}
	</script>
	<script type="text/javascript">
		function amountWithStar(star_id, number,set_price){
			var duration_id = $('.duration2').attr('duration2');
			if(duration_id > 0){
				var data = new FormData( $( 'form#mainForm' )[ 0 ] );
            	data.append('star_id', star_id);
            	data.append('number', number);
            	data.append('duration_id', duration_id);
            	$.ajax( {
	                processData: false,
	                contentType: false,
	                data: data,
	                type: 'POST',
	                url:'{{url("select-plan/amount-with-star")}}',
	                success: function( response ){
	                	$('#'+set_price).text(response.amount.toFixed(2));
	                	$('.all-star').attr('starnumber',number);
	                	existAmount(response.amount.toFixed(2),'price2');
	                	makeTotal();

	                }
            	});
			}else{
				alert("please select duration");
				$('.all-star').attr('starnumber',number);
			}
		}
	</script>
	<script type="text/javascript">
		function addToPayment(id,btn){
			if($('#'+id).text()>0){
				if(id == 'price1'){
					if($('#pay1').val() == 0){
						$('#pay1').val(1);
						var item ='<div class="order-list list1"><span class="order-name" id="oreder-name1">{{@$package['package1']->title}}</span><span class="order-price">&#128; <span class="cr-price" id="order-price1">'+parseFloat($('#price1').text()).toFixed(2)+'</span></span></div>';
						$('.order-list-all').append(item);
						$('#'+btn).text('Remove from Cart');
					}else{
						$('.list1').remove();
						$('#pay1').val(0);
						$('#'+btn).text('Add to Cart');
					}
					
				}else if(id == 'price2'){
					if($('#pay2').val() == 0){
						$('#pay2').val(1);
						var name2 = $('#package2_name').val();
						var item ='<div class="order-list list2"><span class="order-name" id="oreder-name2">'+name2+'</span><span class="order-price">&#128; <span class="cr-price" id="order-price2">'+parseFloat($('#price2').text()).toFixed(2)+'</span></span></div>';
						$('.order-list-all').append(item);
						$('#'+btn).text('Remove from Cart');
					}else{
						$('.list2').remove();
						$('#pay2').val(0);
						$('#'+btn).text('Add to Cart');
					}
					
				}else if(id == 'price3'){
					if($('#pay3').val() == 0){
						$('#pay3').val(1);
						var name3 = '{{@$package['package3']->title}}';
						var item ='<div class="order-list list3"><span class="order-name" id="oreder-name3">'+name3+'</span><span class="order-price">&#128; <span class="cr-price" id="order-price3">'+parseFloat($('#price3').text()).toFixed(2)+'</span></span></div>';
						$('.order-list-all').append(item);
						$('#'+btn).text('Remove from Cart');
					}else{
						$('.list3').remove();
						$('#pay3').val(0);
						$('#'+btn).text('Add to Cart');
					}
					
				}
				makeTotal();
			}

		}
	</script>
	<script type="text/javascript">
		function existAmount(amount,identifire){
			if(identifire == 'price1'){
				$('#order-price1').text(amount);
			}else if(identifire == 'price2'){
				$('#order-price2').text(amount);
			}else if(identifire == 'price3'){
				$('#order-price3').text(amount);
			}
			
			
		}
	</script>
	<script type="text/javascript">
		function makeTotal(){
			var total = 0;
			if($('#pay1').val() == 1){
				total = total + parseFloat($('#price1').text());
			}
			if($('#pay2').val() == 1){
				total = total + parseFloat($('#price2').text());
			}
			if($('#pay3').val() == 1){
				total = total + parseFloat($('#price3').text());
			}
			$('#total-amount').text(total.toFixed(2));
		}
	</script>
	<script type="text/javascript">
		function setPaymentMethod(value){
			$('#payment_mathod').val(value);
		}
	</script>
	<script type="text/javascript">
		function goToPayment(){
			if(parseFloat($('#total-amount').text()) > 0){
				var data = new FormData();
            	data.append('pay1', $('#pay1').val());
            	data.append('pay2', $('#pay2').val());
            	data.append('pay3', $('#pay3').val());
            	data.append('sevice_active',$('#sevice_active').val());
            	data.append('payment_mathod', $('#payment_mathod').val());
            	data.append('service_id', "{{ $id }}");
            	if(parseInt($('#pay1').val()) > 0){
            		data.append('pkg1', $('#pay1').attr('pkg1'));
            		data.append('dur1', $('.duration1').attr('duration1'));
            	}
            	if(parseInt($('#pay2').val()) > 0){
            		data.append('pkg2', $('#pay2').attr('pkg2'));
            		data.append('dur2', $('.duration2').attr('duration2'));
            		data.append('number', $('.all-star').attr('starnumber'));
            		data.append('star_id', $('.all-star').attr('starid'));
            	}
            	if(parseInt($('#pay3').val()) > 0){
            		data.append('pkg3', $('#pay3').attr('pkg3'));
            		data.append('dur3', $('.duration3').attr('duration3'));
            	}
            	$('#loader').addClass('loading');
            	$.ajax( {
	                processData: false,
	                contentType: false,
	                data: data,
	                type: 'POST',
	                url:'{{url("select-plan/payment")}}',
	                success: function( response ){
	                	if($('#payment_mathod').val() == 1){
	                		var stripe = Stripe('pk_test_SzoBR6NC8mp5FvJEwkOld0nF00UlcgpKHq');
			                stripe.redirectToCheckout({
			                    // Make the id field from the Checkout Session creation API response
			                    // available to this file, so you can provide it as parameter here
			                    // instead of the  placeholder.
			                    sessionId: response.url.id
			                }).then(function (result) {
			                    // If `redirectToCheckout` fails due to a browser or network
			                    // error, display the localized error message to your customer
			                    // using `result.error.message`.
			                });
	                	}else{
	                		window.location.href = response.url;
	                	}
	                	//console.log(response);
	                },
	                error: function (err) {
			         $('#loader').removeClass('loading');
			      },
            	});

			}else{
				var active = $('#sevice_active').val();

				if(active == 0){
					var data = new FormData();
					data.append('service_id', "{{ $id }}");
					$.ajax( {
		                processData: false,
		                contentType: false,
		                data: data,
		                type: 'POST',
		                url:'{{url("active-service")}}',
		                success: function( response ){
		                	var url = "{{url('user/profile')}}";
		                	window.location.href = url;
		                }
		            });
				}else{
					alert('Please choose item');
				}
			}
		}
	</script>