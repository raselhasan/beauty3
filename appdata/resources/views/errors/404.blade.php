<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Page 404</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href='http://fonts.googleapis.com/css?family=Hind:400,300,500,600%7cMontserrat:400,700' rel='stylesheet' type='text/css'>
	<style>

	.section-404 {
		position: fixed;
		top: 0;
		left: 0;
		text-align: center;
		bottom: 0;
		right: 0;
		background-color: #0d0d0d;
		background-image: url("assets/img/services/category-bg.png");
		background-size: cover;
		background-repeat: no-repeat;
		font-family: "Montserrat";
	}
	.page-404 {
		position: absolute;
		max-width: 570px;
		width: 100%;
		text-align: center;
		padding: 15px;
		left: 50%;
		top: 50%;
		-webkit-transform: translate(-50%, -50%);
		-moz-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		-o-transform: translate(-50%, -50%);
	}
	.page-404 h1 {
		color: #fff;
		text-transform: uppercase;
		font-size: 80px;
		font-family: "Montserrat";
		line-height: 1;
		margin-bottom: 0;  
		margin-top: 15px;
		padding: 0 10px;
	}
	.page-404 h1 span {
		color: #fe8e4b;
	}
	.page-404 h6 {
		color: #fff;
		font-size: 27px;
		margin-top: 10px;
		margin-bottom: 0;
		padding: 0 10px;
	}
	.page-404 p {
		margin-bottom: 0;
		font-size: 14px;
		color: #fff;
		margin-top: 25px;
		padding: 0 10px;
	}
	.page-404 p:last-child {
		margin-top: 20px;
	}
	.page-404 p a {
		color: #fff;
		font-weight: 700;
		-webkit-transition: all 0.2s ease;
		-moz-transition: all 0.2s ease;
		-ms-transition: all 0.2s ease;
		-o-transition: all 0.2s ease;
	}
	.page-404 p a:hover {
		color: #fe8e4b;
	}
	.page-404 .search-404 {
		margin-top: 35px;  
		padding: 0 10px;
	}
	.page-404 .search-404 .filed-text {
		display: inline-block;
		vertical-align: middle;
		width: 370px;
		margin-bottom: 10px;
		border: 2px solid #fe8e4b;
		color: #fff;    
		height: 40px;
		font-size: 14px;
		padding: 0 12px;
		background-color: transparent;
		-webkit-transition: all 0.3s ease;
		-moz-transition: all 0.3s ease;
		-ms-transition: all 0.3s ease;
		-o-transition: all 0.3s ease;
	}
	.page-404 .search-404 .filed-text::-webkit-input-placeholder {
		border-color: #fe8e4b;
		box-shadow: none;
		outline: none;
		color: #fff;
	}
	.page-404 .search-404 .filed-text:-moz-placeholder {
		border-color: #fe8e4b;
		box-shadow: none;
		outline: none;
		color: #fff;
	}
	.page-404 .search-404 .filed-text::-moz-placeholder {
		border-color: #fe8e4b;
		box-shadow: none;
		outline: none;
		color: #fff;
	}
	.page-404 .search-404 .filed-text:-ms-input-placeholder {
		border-color: #fe8e4b;
		box-shadow: none;
		outline: none;
		color: #fff;
	}
	.page-404 .search-404 .filed-text:focus {
		border-color: #fe8e4b;
		box-shadow: none;
		outline: none;
		color: #fff;
	}
	.page-404 .search-404 .awe-btn {
		vertical-align: middle;
		font-size: 16px;
		color: #fff;
		margin-left: 10px;
		height: 46px;
		line-height: 46px;
		margin-bottom: 10px;
		padding-top: 0;
		padding-bottom: 0;
		min-width: 135px;
		text-align: center;
		color: #fff;
		background-color: #fe8e4b;
		border-color: #fe8e4b;
		text-transform: uppercase;
		cursor: pointer;
	}
	@media screen and (max-width: 500px) {
		.page-404 .search-404 .filed-text {
			width: 80%;
		}
		.page-404 h6 {
			font-size: 16px;
		}
		.page-404 p {
			font-size: 12px;
			margin-top: 15px;
		}
		.page-404 h1 { 
			font-size: 70px;
		}
	} 
</style>
</head>
<body> 
	<section class="section-404">
		<div class="page-404">
			<a href="{{url('/')}}"><img src="{{asset('assets/img/logo1.png')}}"></a>
			<h1><span>404</span></h1> 
			<h6>Oops! This Page Could Not Be Found</h6>
			<p>Sorry But The Page You Are Loking For Does Not Exist, Have Been Remove. Name Changed Or Temporariry Unavailable</p>

			<div class="search-404">
				<input type="text" class="filed-text" placeholder="Search page">  
				<button class="awe-btn awe-btn-12">SEARCH</button>  
			</div>

			<p> Go back to <a href="{{url('/')}}"> Home Page</a> </p>
		</div>

	</section>
</body>
</html>