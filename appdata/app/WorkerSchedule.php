<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkerSchedule extends Model
{
	public function workingDay()
	{
		return $this->belongsTo(WorkingDay::class);
	}
}
