<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RunningHour extends Model
{
    protected $table = 'running_hours';
    protected $fillable = [
        'business_hour_id', 'day_no', 'start', 'end'
    ];
    public function businessHour()
    {
        return $this->belongsTo(BusinessHour::class);
    }
}
