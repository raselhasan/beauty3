<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
	protected $guarded = ['image'];

	protected $hidden = ['pivot'];

	public function serviceSchedules()
	{
		return $this->hasMany(ServiceSchedule::class);
	}
	public function booking()
	{
		return $this->hasMany(Booking::class);
	}

	public function service()
	{
		return $this->belongsTo(Service::class);
	}

	public function workers()
	{
		return $this->belongsToMany(Worker::class);
	}
	public function types()
	{
		return $this->belongsToMany(CustomPackage::class);
	}
}
