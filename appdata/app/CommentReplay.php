<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentReplay extends Model
{
    public function user()
    {
    	return $this->belongsTo(User::class,'replay_user_id','id');
	}
}
