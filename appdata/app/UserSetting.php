<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    protected $fillable = ['user_id', 'is_calendar'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
