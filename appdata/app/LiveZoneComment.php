<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveZoneComment extends Model
{
    public function commenter(){

    	return $this->HasOne('App\User','id','commenter_id');
    }

    public function subcomments()
    {
    	return $this->HasMany('App\SubComment','comment_id','id')->orderBy('id');
    }
    
}
