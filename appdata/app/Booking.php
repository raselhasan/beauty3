<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = ['user_id', 'worker_id', 'type_id', 'service_id', 'date', 'start', 'end', 'confirm'];

    protected $appends = ['new_date', 'cancle_url'];

    public function type()
    {
        return $this->belongsTo(Type::class);
    }
    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function worker()
    {
        return $this->belongsTo(Worker::class);
    }
    public function getNewDateAttribute()
    {
        return Carbon::create($this->date . ' ' . $this->end);
    }
    public function getCancleUrlAttribute()
    {
        return url('user/services/' . $this->service_id . '/types/' . $this->type_id . '/bookings/' . $this->id . '/cancle');
    }
}
