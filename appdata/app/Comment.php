<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function CommentReplay()
    {
    	return $this->hasMany(CommentReplay::class)->orderBy('id','DESC');;
	}
	public function User()
	{
		return $this->belongsTo(User::class,'commented_user','id');
	}
	
}
