<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveZone extends Model
{
    public function user()
    {
    	return $this->HasOne('App\User','id','user_id');
    }
    public function comments()
    {
    	return $this->HasMany('App\LiveZoneComment','livezone_id','id')->orderBy('id','desc');
    }

}
