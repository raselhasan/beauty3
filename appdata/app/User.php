<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'surname', 'nickname'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }
    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
    public function userCovers()
    {
        return $this->hasMany(UserCover::class);
    }
    public function userSetting()
    {
        return $this->hasOne(UserSetting::class);
    }

    public function workers()
    {
        return $this->hasMany(Worker::class);
    }
    public function ProfilePayment()
    {
        return $this->hasOne(ProfilePayment::class);
    }
    public function businessHours()
    {
        return $this->hasMany(BusinessHour::class);
    }
}
