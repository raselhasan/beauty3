<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
   	public function package()
   	{
   		return $this->belongsTo(Package::class,"package_id","id");
   	}
   	public function duration()
   	{
   		return $this->belongsTo(Duration::class,"duration_id","id");
   	}
   	public function service()
   	{
   		return $this->belongsTo(Service::class,"service_id","id");
   	}
      public function payment()
      {
         return $this->belongsTo(Payment::class,"service_id","id");
      }
}
