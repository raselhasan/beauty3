<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BreakHour extends Model
{
    protected $fillable = [
        'business_hour_id', 'day_no', 'start', 'end'
    ];
    public function businessHour()
    {
        return $this->belongsTo(BusinessHour::class);
    }
}
