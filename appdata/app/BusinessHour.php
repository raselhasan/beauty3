<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessHour extends Model
{
    protected $fillable = [
        'user_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function runningHours()
    {
        return $this->hasMany(RunningHour::class);
    }
    public function breakHours()
    {
        return $this->hasMany(BreakHour::class);
    }
}
