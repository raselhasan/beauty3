<?php

namespace App\Http\Resources;

use App\Booking;
use App\Leave;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Resources\Json\JsonResource;

class TypeWorkerResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $from = Carbon::parse(request('start_date'));
        $to = Carbon::parse(request('end_date'));
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'time' => $this->time,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            // 'event' => $this->getEvent($from, $to, $this->time, $this->workers, $this->service->id)
            'event' => $this->margeEvents(array_collapse($this->array_remove_null($this->getEvent($from, $to, $this->time, $this->workers, $this->service->id))))
        ];
    }
    private function margeEvents($array)
    {
        $new = [];
        foreach ($array as $key => $value) {
            $key = array_search($value['start'], array_column($new, 'start'));
            if ($key !== false) {
                if (in_array($value['name'][0], $new[$key]["name"])) {
                    continue;
                }
                $new[$key]["name"][] = $value['name'][0];
                $new[$key]["title"] .= $value['title'];
                $value['workers'][0]['class'] = $value['class'];
                $new[$key]["workers"][] = $value['workers'][0];
                $key = false;
            } else {
                $value['workers'][0]['class'] = $value['class'];
                array_push($new, $value);
            }
        }
        return $new;
    }
    private function array_remove_null($item)
    {
        if (!is_array($item)) {
            return $item;
        }

        return collect($item)
            ->reject(function ($item) {
                return is_null($item);
            })
            ->flatMap(function ($item, $key) {

                return is_numeric($key)
                    ? [$this->array_remove_null($item)]
                    : [$key => $this->array_remove_null($item)];
            })
            ->toArray();
    }

    private function getEvent($from, $to, $time, $workers, $service)
    {
        $events = [];
        foreach ($workers as $worker) {
            $day = $from->copy();
            while ($day->lte($to)) {
                $event = $this->getEventByWorker($day, $time, $worker, $service);

                array_push($events, $event);

                $day->addDay();
            }
        }
        return $events;
    }

    private function getEventByWorker($day, $time, $worker, $service)
    {
        $events = [];
        $modifiedWorkingDays = [];
        $today = $day->shortEnglishDayOfWeek;
        $workingDays = $worker->workingDays()->with('workerSchedules')->orderBy('repeat', 'desc')->get();
        // return $workingDays;
        foreach ($workingDays as $wd) {
            $key = array_search($wd->day, array_column($modifiedWorkingDays, 'day'));
            if ($key) {
                if ($wd->repeat == 0 && $wd->end_repeat == $day->copy()->format('Y-m-d')) {
                    $modifiedWorkingDays[$key] = $wd;
                }
            } else {
                if (
                    $wd->repeat == 1
                    ||
                    ($wd->repeat == 0 && $wd->end_repeat == $day->copy()->format('Y-m-d'))
                ) {
                    array_push($modifiedWorkingDays, $wd);
                }
            }
        }

        foreach (collect($modifiedWorkingDays) as $days) {
            if ($days->day == $today) {
                // foreach ($days->workerSchedules as $schedule) {
                $schedule = [];
                $schedule["start"] = $days->start;
                $schedule["end"] = $days->end;

                $schedule_time = $this->getScheduleTime($day, $time, $days->start, $days->end);

                $leaves = Leave::where('worker_id', $worker->id)->where('date', $day->copy()->format('Y-m-d'))->get()->first();
                $period = CarbonPeriod::create($day->copy()->format('Y-m-d') . ' ' . $schedule_time['start'], $time . ' minutes', $day->copy()->format('Y-m-d') . ' '  . $schedule_time['end']);
                foreach ($period as $date) {
                    if ($this->isWorkerBusy($service, $worker->id, $date->copy()->format('Y-m-d'), $date->copy()->format('H:i') . ":00")) {
                        if ($this->isWorkerComplete($service, $worker->id, $date->copy()->format('Y-m-d'), $date->copy()->format('H:i') . ":00")) {
                            $class = "wkr-status-complete";
                        } elseif ($this->isWorkerConfirm($service, $worker->id, $date->copy()->format('Y-m-d'), $date->copy()->format('H:i') . ":00")) {
                            $class = "wkr-status-confirm";
                        } else {
                            $class = "wkr-status-busy";
                        }
                    } else {
                        if ($leaves && $this->isWorkerInLeave($leaves, $date->copy()->format('H:i') . ":00")) {
                            $class = "wkr-status-leave";
                        } else {
                            $class = "wkr-status-free";
                        }
                    }
                    $data = [
                        "start" => $date->copy()->format('Y-m-d H:i'),
                        "end" => $date->copy()->addMinutes($time)->format('Y-m-d H:i'),
                        "title" => "<span class='" . $class . "'>" . strtoupper(substr($worker->name, 0, 2)) . ". </span>",
                        "name" => [$worker->name],
                        "content" => '',
                        "contentFull" => '',
                        "class" => $class,
                        "workers" => [$worker]
                    ];
                    array_push($events, $data);
                }
                // }
            }
        }
        return $events;
    }
    private function getScheduleTime($day, $time, $start, $end)
    {
        $schedule_time = [];
        $new = [];

        $periods = CarbonPeriod::create($day->copy()->format('Y-m-d') . ' 00:00:00', $time . ' minutes', $day->copy()->format('Y-m-d') . ' 23:00:00');
        foreach ($periods as $date) {
            $schedule_time[] = $date->format('H:i:s');
        }
        for ($i = 0; $i < count($schedule_time) - 1; $i++) {
            $date1 = Carbon::parse($start)->timestamp;
            $date2 = Carbon::parse($schedule_time[$i])->timestamp;
            $date3 = Carbon::parse($schedule_time[$i + 1])->timestamp;
            if ($date1 >= $date2 && $date1 <= $date3) {
                // return [$date3, $date2, $date1];
                $new['start'] = $schedule_time[$i + 1];
            }
            $date1 = Carbon::parse($end)->timestamp;
            if ($date1 >= $date2 && $date1 <= $date3) {
                $new['end'] = $schedule_time[$i];
            }
        }
        return $new;
    }
    private function isWorkerBusy($service, $worker, $date, $time)
    {
        return Booking::where('worker_id', $worker)
            ->where('service_id', $service)
            ->where('date', $date)
            ->where('start', $time)
            ->where('cancle', 0)
            ->exists();
    }
    private function isWorkerConfirm($service, $worker, $date, $time)
    {
        return Booking::where('worker_id', $worker)
            ->where('service_id', $service)
            ->where('date', $date)
            ->where('start', $time)
            ->where('confirm', 1)
            ->exists();
    }
    private function isWorkerComplete($service, $worker, $date, $time)
    {
        return Booking::where('worker_id', $worker)
            ->where('service_id', $service)
            ->where('date', $date)
            ->where('start', $time)
            ->where('complete', 1)
            ->exists();
    }

    private function isWorkerInLeave($leaves, $time)
    {
        if (strtotime($time) >= strtotime($leaves->start) && strtotime($time) <= strtotime($leaves->end)) {
            return true;
        }
        return false;
    }
}
