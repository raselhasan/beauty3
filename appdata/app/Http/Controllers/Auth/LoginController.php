<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }
    public function login(Request $request)
    {
        $isValid = $this->emailValidOrNot($request->email);
        if ($isValid['iplug']) {
            $isValid = $this->phoneValidOrNot($request->email, $isValid);
        }
        if ($isValid['iplug']) {
            return response()->json(['error' => $isValid['msg']], 200);
        }
        $checkUser = User::where($isValid['type'], $request->email)->first();
        if ($checkUser) {
            $checkPassword = Hash::check($request->password, $checkUser->password);
            if (!$checkPassword) {
                return response()->json(['error' => 'Password not match.'], 200);
            } else {
                if (!$checkUser->sms_verified_at) {
                    return response()->json(['error' => 'You must need to validate your phone number.'], 200);
                }
            }
        } else {
            return response()->json(['error' => 'User credential not match.'], 200);
        }
        $headers = [
            'Accept' => 'application/json'
        ];
        $client = new \GuzzleHttp\Client([
            'headers' => $headers
        ]);
        $url = url('/') . '/api/login';
        $myBody = [];
        $myBody['email'] = $request->email;
        $myBody['password'] = $request->password;


        try {
            $response = $client->post($url,  ['form_params' => $myBody]);
        } catch (Exception $e) {
            //throw $th;
            // return response()->json(['error' => 'sg.'], 200);
            return response()->json(['error' => 'Username or Password not match.'], 200);
        }
        $data = json_decode($response->getBody()->getContents());


        if ($data->success) {
            // Attempt to log the admin in
            if (
                Auth::guard('web')->attempt([$isValid['type'] => $request->email, 'password' => $request->password], $request->remember)
            ) {
                // if successful, then redirect to their intend locati

                $request->session()->put('api_token', $data->data->token);
                // return redirect()->route('user.profile');
                $view = view('inc.switch-acnt')->render();
                return response()->json(['user' => Auth::user(), 'view' => $view], 200);
            }
        } else {
            return response()->json(['error' => $data->message], 200);
        }

        // if unsuccessful, then redirect back to login page with form data
        return response()->json(['error' => 'Username or Password not match.'], 200);
    }
    public function emailValidOrNot($email)
    {
        $data['iplug'] = false;
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $data['type'] = 'email';
        } else {
            $data['msg'] = "Invalid email address.";
            $data['iplug'] = true;
        }
        return $data;
    }
    public function phoneValidOrNot($phone, $data)
    {
        if (preg_match('/^\+?[0-9]{10,14}+$/', $phone)) {
            $data['iplug'] = false;
            $data['type'] = 'phone';
        } else {
            if (preg_match('/^[0-9]+$/', $phone)) {
                $data['msg'] = "Invalid phone number.";
                $data['iplug'] = true;
            }
        }
        return $data;
    }
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        return $this->loggedOut($request) ?: redirect()->route('index');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web');
    }
}
