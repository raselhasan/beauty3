<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Mail\EmailVerification;
use Mail;
use Session;
use Illuminate\Support\Facades\Input;

use Twilio\Rest\Client;


class RegisterController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:admin');
    }
    public function registration(Request $request)
    {
        // dd($req->all());
        $checkUser = User::where('email', $request->email)->exists();
        if ($checkUser) {
            return response()->json(['error' => 'This email already have one account.'], 200);
        }
        $digits = 5;
        $verification_code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $email_code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $verification_code = '123';
        $email_code = '123';
        $reg_info['name'] = $request->name;
        $reg_info['surname'] = $request->surname;
        $reg_info['nickname'] = $request->nickname;
        $reg_info['email'] = $request->email;
        $reg_info['password'] = $request->password;
        $reg_info['account_type'] = $request->account_type;
        $reg_info['phone'] = $request->phone;
        $reg_info['sms_code'] = $verification_code;
        $reg_info['email_code'] = $email_code;

        //$send_sms = $this->sendSms($request->phone,$verification_code);
        Session::put('reg_info',$reg_info);

        return 'success';

    }

    public function sendSms($number, $code)
    {

        $text = 'Your Grožiokalviai verification code is: '.$code;
        $recipients = [8801909888251];
        $url = "https://gatewayapi.com/rest/mtsms";
        $api_token = "ipFQocVySkGQoe3F3xxvBpFuyVKLAKEoJp_vYW9SR7gdX7GqMzcTtJVwLAqD2hcQ";
        $json = [
            'sender' => 'ExampleSMS',
            'message' => $text,
            'recipients' => [],
        ];
        foreach ($recipients as $msisdn) {
            $json['recipients'][] = ['msisdn' => $msisdn];
        }

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch,CURLOPT_USERPWD, $api_token.":");
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($json));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        // print($result);
        // $json = json_decode($result);
        // print_r($json->ids);

    }

    public function verifyUserSms(Request $request)
    {
        $reg_info = Session::get('reg_info');
        if ($request->v_code != $reg_info['sms_code']) {
            return response()->json(['error' => 'Verification code not match']);
        }
        $user = new User;
        $user->name = $reg_info['name'];
        $user->surname = $reg_info['surname'];
        $user->nickname = $reg_info['nickname'];
        $user->email = $reg_info['email'];
        $user->password = Hash::make($reg_info['password']);
        $user->account_type = $reg_info['account_type'];
        $user->phone = $reg_info['phone'];
        $user->sms_verified_at = date('Y-m-d H:i:s');
        if ($request->account_type == 1) {
            $user->show_name = '["3"]';
        }
        $user->save();
        $data['name'] = $reg_info['nickname'];
        $data['email'] = $reg_info['email'];
        $data['code'] = $reg_info['email_code'];
        Mail::send(new EmailVerification($data));
        return 'success';
    }
    public function verifyUserEmail(Request $request)
    {
        $reg_info = Session::get('reg_info');
        if ($request->v_code != $reg_info['email_code']) {
            return response()->json(['error' => 'Verification code not match']);
        }
        $getUser = User::where('email', $reg_info['email'])->first();
        $updateUser = User::find($getUser->id);
        $updateUser->email_verified_at = date('Y-m-d H:i:s');
        $updateUser->save();
        $headers = [
            'Accept' => 'application/json'
        ];
        $client = new \GuzzleHttp\Client([
            'headers' => $headers
        ]);
        $url = url('/') . '/api/login';
        $myBody = [];
        $myBody['email'] = $reg_info['email'];
        $myBody['password'] = $reg_info['password'];

        try {
            $response = $client->post($url,  ['form_params' => $myBody]);
        } catch (Exception $e) {
            //throw $th;
            return response()->json(['error' => 'Username or Password not match.'], 200);
        }
        $data = json_decode($response->getBody()->getContents());

        if ($data->success) {
            // Attempt to log the admin in
            if (Auth::guard('web')->attempt(['email' => $reg_info['email'], 'password' => $reg_info['password']])) {
                // if successful, then redirect to their intend locati

                Session::put('api_token', $data->data->token);
                Session::forget('reg_info');
                return redirect()->route('user.profile');
                //return response()->json(['user' => Auth::user()], 200);
            }
        }
    }

    public function testSms()
    {
        $recipients = [8801909888251];
        $url = "https://gatewayapi.com/rest/mtsms";
        $api_token = "ipFQocVySkGQoe3F3xxvBpFuyVKLAKEoJp_vYW9SR7gdX7GqMzcTtJVwLAqD2hcQ";
        $json = [
            'sender' => 'ExampleSMS',
            'message' => 'Hello world',
            'recipients' => [],
        ];
        foreach ($recipients as $msisdn) {
            $json['recipients'][] = ['msisdn' => $msisdn];
        }

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch,CURLOPT_USERPWD, $api_token.":");
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($json));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        print($result);
        $json = json_decode($result);
        print_r($json->ids);
    }
}
