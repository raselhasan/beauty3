<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Duration;
use App\District;
use App\Star;
use App\Paysera\WebToPay;
use Session;
use App\Payment;
use App\PaymentService;
use App\Service;
use App\VipPackage;
use App\ProfilePayment;
use App\TopService;
use App\VipService;
use App\AddShowingPackage;
use App\AddShowingPayment;
use App\ProfilePaymentHistory;
use App\AddShowingHistory;
use App\User;
use App\HighlightService;
class PaymentController extends Controller
{
    public function amountCalculation(Request $request)
    {
    	$duration = Duration::find($request->duration_id);
        $price = $duration->price;
        
        if($request->number > 0)
        {
            $star = Star::find($request->star);
            $amount = $star->price * $request->number;
            $price = $price + $amount;
        }
        

    	return response()->json(['price'=>$price]);
    }
    public function starAmount(Request $request)
    {
    	$duration = Duration::find($request->duration_id);
    	$star = Star::find($request->star_id);
    	$price = $duration->price + $request->number * $star->price;
    	return response()->json(['amount'=>$price]);
    }
    public function payment(Request $request)
    {
        $total = 0;
        $data = [];
        if($request->pay1 > 0){
            $dur = Duration::find($request->dur1);
            $pak = Package::find($request->pkg1);
            $total = $total + $dur->price;
            $pkg1 = array(
                'package_id' => $request->pkg1,
                'duration_id' => $request->dur1,
                'duration_name' => $dur->duration,
                'package_name' => $pak->title,
                'type' => $pak->type,
                'price' => $dur->price
            );
            $data [] = $pkg1;
        }
        if($request->pay2 > 0){
            $dur2 = Duration::find($request->dur2);
            $pak2 = Package::find($request->pkg2);
            $total = $total + $dur2->price;
            $star_id = '';
            $star = '';
            $makeStarPrice = 0;
            if($request->star_id > 0){
                $star_id = $request->star_id;
                if($request->number > 0){
                    $find_star = Star::find($request->star_id);
                    $makeStarPrice = $find_star->price * $request->number;
                    $total = $total + $makeStarPrice;
                    $star = $request->number;
                }
            }
            $pkg2 = array(
                'package_id' => $request->pkg2,
                'duration_id' => $request->dur2,
                'duration_name' => $dur2->duration,
                'package_name' => $pak2->title,
                'price' => $dur2->price+$makeStarPrice,
                'star_id' => $star_id,
                'star_price' => $makeStarPrice,
                'star' => $star,
                'type' => $pak2->type
            );
            $data [] = $pkg2;
        }
        if($request->pay3 > 0){
            $dur3 = Duration::find($request->dur3);
            $pak3 = Package::find($request->pkg3);
            $total = $total + $dur3->price;
            $pkg3 = array(
                'package_id' => $request->pkg3,
                'duration_id' => $request->dur3,
                'duration_name' => $dur3->duration,
                'package_name' => $pak3->title,
                'price' => $dur3->price,
                'type' => $pak3->type
            );
            $data [] = $pkg3;
        }
        // print_r($data);
        // return 'rasel';
        $extra['order_id'] = time() . mt_rand() . Auth()->user()->id;
        $extra['service_id'] = $request->service_id;
        $find_service = Service::find($request->service_id);
        $extra['service_name'] = $find_service->title;
        $extra['payment_method'] = $request->payment_mathod;
        $extra['sevice_active'] = $request->sevice_active;
        Session::put('payment_data',$data);
        Session::put('extra_payment',$extra);
        if($request->payment_mathod == 1){
            $url = $this->stripePayment($data,$total);
            return response()->json(['url'=>$url]);
        }else if($request->payment_mathod == 2){
            return $url = $this->payseraPayment($data,$total);
        }


    }

    public function stripePayment($data,$total)
    {
        \Stripe\Stripe::setApiKey('sk_test_QYIZ5JBIdfGglZi84PA46PEN00WSoaoxnH');
        $session = \Stripe\Checkout\Session::create([
            'customer_email' => Auth()->user()->email,
            'payment_method_types' => ['card'],
            'line_items' => [
                [
                    'name' => 'Service Payment',
                    'amount' => $total*100,
                    'currency' => 'eur',
                    'quantity' => 1,
                ]
            ],
            'success_url' => url('stripe/success'),
            'cancel_url' => url('stripe/cancel'),
        ]);
        return $session;
    }
    public function paymentSuccess()
    {
        if(Session::has('payment_data')){
            $data = Session::get('payment_data');
            $extra = Session::get('extra_payment');
            $topServiceId = '';
            foreach ($data as $p_data) {
                if($p_data['package_id'] == 1){
                    $this->saveToVipService($p_data,$extra);
                }
                if($p_data['package_id'] == 2){
                    $topServiceId = $this->saveToTopService($p_data, $extra);
                }
                if($p_data['package_id'] == 3){
                    $this->saveToHighlight($p_data,$extra);
                }

                //$this->saveToPaymentService($p_data,$extra);
                $this->saveToPayment($p_data,$extra);
            }
            if($extra['sevice_active'] == 0){
                $service = Service::find($extra['service_id']);
                $service->active = 1;
                $service->s_start_date = date('Y-m-d H:i:s');
                $service->s_end_date = date("Y-m-d H:i:s", strtotime("+3 months", strtotime(date('Y-m-d H:i:s'))));
                $service->payment_type = 'Free';
                $service->save();
            }
            Session::forget('payment_data');
            Session::forget('extra_payment');
            return redirect('user/profile')->with('msg','Payment has been successfully completed');
        }else{
            return redirect('/');
        }

    }

    public function saveToVipService($data, $extra)
    {
        $vip_service = new VipService();
        $vip_service->service_id = $extra['service_id'];
        $start_date = date('Y-m-d H:i:s');
        $end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration_name'], strtotime($start_date)));

        $check_vip_service = VipService::where('service_id',$extra['service_id'])->where('package_id',$data['package_id'])->get()->toArray();
        if(count($check_vip_service)>0){
            $item_number2 = count($check_vip_service) -1;
            $now_date2 = strtotime(date('Y-m-d H:i:s'));
            $exist_date2 = strtotime($check_vip_service[$item_number2]['end_date']);
            if($exist_date2 > $now_date2){
                $start_date = $check_vip_service[$item_number2]['end_date'];
                $end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration_name'], strtotime($start_date)));
            }
        }
        $vip_service->start_date = $start_date;
        $vip_service->end_date = $end_date;
        $vip_service->package_id = $data['package_id'];
        $vip_service->save();
        $service = Service::find($extra['service_id']);
        $service->vip_service = 1;
        $service->save();
    }

    public function saveToHighlight($data, $extra)
    {
        $vip_service = new HighlightService();
        $vip_service->service_id = $extra['service_id'];
        $start_date = date('Y-m-d H:i:s');
        $end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration_name'], strtotime($start_date)));

        $check_vip_service = HighlightService::where('service_id',$extra['service_id'])->where('package_id',$data['package_id'])->get()->toArray();
        if(count($check_vip_service)>0){
            $item_number2 = count($check_vip_service) -1;
            $now_date2 = strtotime(date('Y-m-d H:i:s'));
            $exist_date2 = strtotime($check_vip_service[$item_number2]['end_date']);
            if($exist_date2 > $now_date2){
                $start_date = $check_vip_service[$item_number2]['end_date'];
                $end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration_name'], strtotime($start_date)));
            }
        }
        $vip_service->start_date = $start_date;
        $vip_service->end_date = $end_date;
        $vip_service->package_id = $data['package_id'];
        $vip_service->save();
        
    }

    public function saveToTopService($data, $extra)
    {
        $TopService = new TopService();
        $TopService->service_id = $extra['service_id'];
        if(array_key_exists('star',$data)){
            $TopService->star = $data['star'];
        }
        $today = date('Y-m-d H:i:s');
        $new_end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration_name'], strtotime($today)));

        $check_top_service = TopService::where('service_id',$extra['service_id'])->where('package_id',$data['package_id'])->get()->toArray();
        if(count($check_top_service)){
            $item_number1 = count($check_top_service) -1;
            $now_date1 = strtotime(date('Y-m-d H:i:s'));
            $exist_date1 = strtotime($check_top_service[$item_number1]['end_date']);
            if($exist_date1 > $now_date1){
                
                $today = $check_top_service[$item_number1]['end_date'];
                $new_end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration_name'], strtotime($today)));
            }
        }
        $TopService->start_date = $today;
        $TopService->end_date = $new_end_date;


        $TopService->package_id = $data['package_id'];
        $TopService->save();

        $service = Service::find($extra['service_id']);
        $service->top_service = 1;
        $service->save();
        return $TopService->id;
    }

    public function setColor($topServiceId){
        if($topServiceId){
            $TopService = TopService::find($topServiceId);
            $TopService->color = '#ffffff';
            $TopService->save();
        }
    }
    // public function saveToPaymentService($p_data,$extra)
    // {
    //     $find = PaymentService::where('user_id',Auth()->user()->id)->where('package_id',$p_data['package_id'])->where('service_id',$extra['service_id'])->first();
    //     if(!$find){
    //         $payment_service = new PaymentService();
    //         $payment_service->user_id = Auth()->user()->id;
    //         $payment_service->service_id = $extra['service_id'];
    //         $payment_service->package_id = $p_data['package_id'];
    //         $payment_service->package_type = $p_data['type'];
    //         $start_date = date('Y-m-d H:i:s');
    //         $end_date = date("Y-m-d H:i:s", strtotime("+ ".$p_data['duration_name'], strtotime($start_date)));
    //         $payment_service->start_date = $start_date;
    //         $payment_service->end_date = $end_date;
    //         $payment_service->save();
    //     }else{
    //         $payment_service = PaymentService::find($find->id);
    //         $end_date = $find->end_date;
    //         $today = date('Y-m-d H:i:s');
    //         if(strtotime($end_date) < strtotime($today)){
    //             $new_end_date = date("Y-m-d H:i:s", strtotime("+ ".$p_data['duration_name'], strtotime($today)));
    //             $payment_service->start_date = $today;


    //         }else{
    //             $new_end_date = date("Y-m-d H:i:s", strtotime("+ ".$p_data['duration_name'], strtotime($end_date)));
    //             // echo 'update end date';
    //         }
    //         $payment_service->end_date = $new_end_date;
    //         $payment_service->save();

    //     }
    // }

    public function saveToPayment($p_data,$extra)
    {
        $payment = new Payment();
        $payment->user_id = Auth()->user()->id;
        $payment->service_id = $extra['service_id'];
        $payment->service_name = $extra['service_name'];
        $payment->package_id = $p_data['package_id'];
        $payment->package_name = $p_data['package_name'];
        $payment->duration_name = $p_data['duration_name'];
        if(array_key_exists('star_price',$p_data)){
            $payment->star_price = $p_data['star_price'];
            
        }
        if(array_key_exists('star',$p_data)){
            $payment->star_number = $p_data['star'];
        }
        $payment->order_id = $extra['order_id'];
        $payment->paid_amount = $p_data['price'];
        $payment->payment_method = $extra['payment_method'];


        $start_date = date('Y-m-d H:i:s');
        $end_date = date("Y-m-d H:i:s", strtotime("+ ".$p_data['duration_name'], strtotime($start_date)));

        $check_payment = Payment::where('service_id',$extra['service_id'])->where('package_id',$p_data['package_id'])->get()->toArray();
        if(count($check_payment)>0){
            $item_number = count($check_payment) -1;
            $now_date = strtotime(date('Y-m-d H:i:s'));
            $exist_date = strtotime($check_payment[$item_number]['end_date']);
            if($exist_date > $now_date){
                $start_date = $check_payment[$item_number]['end_date'];
                $end_date = date("Y-m-d H:i:s", strtotime("+ ".$p_data['duration_name'], strtotime($start_date)));
            }
        }
        $payment->start_date = $start_date;
        $payment->end_date = $end_date;
        $payment->save();
    }
    public function stripeCancel()
    {
        Session::forget('payment_data');
        Session::forget('extra_payment'); 
        return redirect('user/profile')->with('msg','Payment has been canceled');  
    }
    public function get_self_url()
    {
        $s = substr(strtolower($_SERVER['SERVER_PROTOCOL']), 0,
            strpos($_SERVER['SERVER_PROTOCOL'], '/'));

        if (!empty($_SERVER["HTTPS"])) {
            $s .= ($_SERVER["HTTPS"] == "on") ? "s" : "";
        }

        $s .= '://'.$_SERVER['HTTP_HOST'];

        if (!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '80') {
            $s .= ':'.$_SERVER['SERVER_PORT'];
        }

        $s .= dirname($_SERVER['SCRIPT_NAME']);

        return $s;
    }

    public function payseraPayment($data,$total)
    {
        $extra = Session::get('extra_payment');
        $order_id = $extra['order_id'];
        try {
            $self_url = $this->get_self_url();
            $paysera_ammount = $total * 100;
            $requestToPaysera = WebToPay::redirectToPayment(array(
                'projectid'     =>158086,
                'sign_password' => 'd93591bdf7860e1e4ee2fca799911215',
                'orderid'       => $order_id,
                'amount'        => $paysera_ammount,
                'currency'      => 'EUR',
                'country'       => 'LT',
                'accepturl'     => $self_url.'/paysera/success',
                'cancelurl'     => $self_url.'/paysera/cancel',
                'callbackurl'   => $self_url.'/paysera/callback',
                'test'          => 1,
            ));
            return response()->json(['url'=>$requestToPaysera]);
        } catch (WebToPayException $e) {
            // handle exception
        }
    }
    
    public function payseraCancel(Request $request)
    {
        Session::forget('payment_data');
        Session::forget('extra_payment');
        return redirect('user/profile')->with('msg','Payment has been canceled');
    }
    public function payseraCallback(Request $request)
    {
        try {
            $response = WebToPay::checkResponse($_GET, array(
                'projectid'     =>158086,
                'sign_password' => 'd93591bdf7860e1e4ee2fca799911215',
            ));
            if ($response['test'] !== '0') {
                throw new Exception('Testing, real payment was not made');
            }
            if ($response['type'] !== 'macro') {
                throw new Exception('Only macro payment callbacks are accepted');
            }

            $orderId = $response['orderid'];
            $amount = $response['amount'];
            $currency = $response['currency'];
            echo 'OK';
        } catch (Exception $e) {
            echo get_class($e) . ': ' . $e->getMessage();
        }
    }

    public function getProfileAmount(Request $request)
    {
        $pkg = VipPackage::find($request->id);
        return $pkg;
    }
    public function profilePayment(Request $request)
    {
        $pkg = VipPackage::find($request->pkg_id)->toArray();
        $pkg['order_id'] = time() . mt_rand() . Auth()->user()->id;
        Session::put('profile_payment',$pkg);
        if($request->payment_type == 2){
            $url = $this->profileStripPayment($pkg);
            return response()->json(['url'=>$url]);
        }else{
            return $url = $this->profilePaymentPaysera($pkg);
        }
        

    }
    public function profileStripPayment($pkg)
    {
        \Stripe\Stripe::setApiKey('sk_test_QYIZ5JBIdfGglZi84PA46PEN00WSoaoxnH');
        $session = \Stripe\Checkout\Session::create([
            'customer_email' => Auth()->user()->email,
            'payment_method_types' => ['card'],
            'line_items' => [
                [
                    'name' => 'Vip profile payment',
                    'amount' => $pkg['price']*100,
                    'currency' => 'eur',
                    'quantity' => 1,
                ]
            ],
            'success_url' => url('profile-payment/success'),
            'cancel_url' => url('profile-payment/cancel'),
        ]);
        return $session;
    }

    public function profilePaymentPaysera($pkg)
    {
        try {
            $self_url = $this->get_self_url();
            $paysera_ammount = $pkg['price'] * 100;
            $requestToPaysera = WebToPay::redirectToPayment(array(
                'projectid'     =>158086,
                'sign_password' => 'd93591bdf7860e1e4ee2fca799911215',
                'orderid'       => $pkg['order_id'],
                'amount'        => $paysera_ammount,
                'currency'      => 'EUR',
                'country'       => 'LT',
                'accepturl'     => $self_url.'/profile-payment/success',
                'cancelurl'     => $self_url.'/profile-payment/cancel',
                'callbackurl'   => $self_url.'/paysera/callback',
                'test'          => 1,
            ));
            return response()->json(['url'=>$requestToPaysera]);
        } catch (WebToPayException $e) {
            // handle exception
        }
    }


    public function profilePaymentSuccess()
    {
        if(Session::has('profile_payment')){
            $data = Session::get('profile_payment');
            $check_history = ProfilePayment::where('user_id',auth()->user()->id)->first();
            $start_date = date('Y-m-d H:i:s');
            $end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration'], strtotime(date('Y-m-d H:i:s'))));
            $status = 0;

            if($check_history){
                if(strtotime($check_history->end_date) > strtotime($start_date)){
                    $start_date = $check_history->start_date;
                    $end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration'], strtotime($check_history->end_date)));
                }
                $status = 1;
                $ProfilePayment = ProfilePayment::find($check_history->id);
            }else{
                $ProfilePayment = new ProfilePayment();
            }
            
            $ProfilePayment->user_id = Auth()->user()->id;
            $ProfilePayment->package_id = $data['id'];
            $ProfilePayment->order_id = $data['order_id'];
            $ProfilePayment->price = $data['price'];
            $ProfilePayment->start_date = $start_date;
            $ProfilePayment->end_date = $end_date;
            $ProfilePayment->save();
            $user = User::find(auth()->user()->id);
            $user->vip_profile = 1;
            $user->save();
            $this->profilePaymentHistory($data,$start_date,$end_date,$status);
            Session::forget('profile_payment');
            return redirect('user/profile')->with('success','Payment has been success! Your profile now VIP');
        }
    }
    public function profilePaymentHistory($data,$start_date,$end_date,$status){
        $ProfilePaymentHistory = new ProfilePaymentHistory();
        $ProfilePaymentHistory->user_id = Auth()->user()->id;
        $ProfilePaymentHistory->package_id = $data['id'];
        $ProfilePaymentHistory->order_id = $data['order_id'];
        $ProfilePaymentHistory->price = $data['price'];
        $ProfilePaymentHistory->start_date = $start_date;
        $ProfilePaymentHistory->end_date = $end_date;
        $ProfilePaymentHistory->status = $status;
        $ProfilePaymentHistory->save();
    }
    public function profilePaymentCancel()
    {
        Session::forget('profile_payment');
    }

    public function addControlPayment()
    {
        $AddShowingPackage = AddShowingPackage::all();
        $service['cities'] = District::all();
        return view('pages.add-control-payment',compact('AddShowingPackage','service'));
    }

    public function addControlPaymentPrice(Request $request)
    {
        return $AddShowingPackage = AddShowingPackage::find($request->id);
    }

    public function addControlMakePayment(Request $request)
    {
        $pkg = AddShowingPackage::find($request->pkg_id)->toArray();
        $pkg['order_id'] = time() . mt_rand() . Auth()->user()->id;
        Session::put('add_controll',$pkg);
        if($request->payment_type == 2){
            $url = $this->addControllStripPayment($pkg);
            return response()->json(['url'=>$url]);
        }else{
            return $url = $this->addControllPaymentPaysera($pkg);
        }
    }
    public function addControllStripPayment($pkg)
    {
        \Stripe\Stripe::setApiKey('sk_test_QYIZ5JBIdfGglZi84PA46PEN00WSoaoxnH');
        $session = \Stripe\Checkout\Session::create([
            'customer_email' => Auth()->user()->email,
            'payment_method_types' => ['card'],
            'line_items' => [
                [
                    'name' => 'Profile add controll payment',
                    'amount' => $pkg['price']*100,
                    'currency' => 'eur',
                    'quantity' => 1,
                ]
            ],
            'success_url' => url('add-control-payment/success'),
            'cancel_url' => url('add-control-payment/cancel'),
        ]);
        return $session;
    }
    public function addControllPaymentPaysera($pkg)
    {
        try {
            $self_url = $this->get_self_url();
            $paysera_ammount = $pkg['price'] * 100;
            $requestToPaysera = WebToPay::redirectToPayment(array(
                'projectid'     =>158086,
                'sign_password' => 'd93591bdf7860e1e4ee2fca799911215',
                'orderid'       => $pkg['order_id'],
                'amount'        => $paysera_ammount,
                'currency'      => 'EUR',
                'country'       => 'LT',
                'accepturl'     => $self_url.'/add-control-payment/success',
                'cancelurl'     => $self_url.'/add-control-payment/cancel',
                'callbackurl'   => $self_url.'/paysera/callback',
                'test'          => 1,
            ));
            return response()->json(['url'=>$requestToPaysera]);
        } catch (WebToPayException $e) {
            // handle exception
        }
    }
    public function addControllPaymentSuccess()
    {
        if(Session::has('add_controll')){
            $data = Session::get('add_controll');
            $check_history = AddShowingPayment::where('user_id',auth()->user()->id)->first();
            $start_date = date('Y-m-d H:i:s');
            $end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration'], strtotime(date('Y-m-d H:i:s'))));
            $status = 0;

            if($check_history){
                if(strtotime($check_history->end_date) > strtotime($start_date)){
                    $start_date = $check_history->start_date;
                    $end_date = date("Y-m-d H:i:s", strtotime("+ ".$data['duration'], strtotime($check_history->end_date)));
                }
                $status = 1;
                $AddShowingPayment = AddShowingPayment::find($check_history->id);
            }else{
                $AddShowingPayment = new AddShowingPayment();
            }
            $AddShowingPayment->user_id = Auth()->user()->id;
            $AddShowingPayment->package_id = $data['id'];
            $AddShowingPayment->order_id = $data['order_id'];
            $AddShowingPayment->price = $data['price'];
            $AddShowingPayment->start_date = $start_date;
            $AddShowingPayment->end_date = $end_date;
            $AddShowingPayment->save();
            $this->AddShowingHistory($data,$start_date,$end_date,$status);
            Session::forget('add_controll');
            return redirect('user/profile')->with('success','Payment has been success! Your profile add not will show');
        }
    }
    public function AddShowingHistory($data,$start_date,$end_date,$status){
        $AddShowingHistory = new AddShowingHistory();
        $AddShowingHistory->user_id = Auth()->user()->id;
        $AddShowingHistory->package_id = $data['id'];
        $AddShowingHistory->order_id = $data['order_id'];
        $AddShowingHistory->price = $data['price'];
        $AddShowingHistory->start_date = $start_date;
        $AddShowingHistory->end_date = $end_date;
        $AddShowingHistory->status = $status;
        $AddShowingHistory->save();
    }

    public function addControllPaymentCancel()
    {
        Session::forget('add_controll');
    }
    public function activeService(Request $request)
    {
        $service = Service::find($request->service_id);
        $service->active = 1;
        $service->s_start_date = date('Y-m-d H:i:s');
        $service->s_end_date = date("Y-m-d H:i:s", strtotime("+3 months", strtotime(date('Y-m-d H:i:s'))));
        $service->payment_type = 'Free';
        $service->save();
        return 'success';
    }


}
