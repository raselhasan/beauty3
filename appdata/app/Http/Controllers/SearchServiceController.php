<?php

namespace App\Http\Controllers;

use DB;
use Cookie;
use Session;
use App\User;
use App\Service;
use App\Category;
use App\District;
use App\UserCover;
use Carbon\Carbon;
use App\SubCategory;
use App\InnerCategory;
use App\AddShowingPayment;
use Illuminate\Http\Request;
use App\Services\MakeUrlService;

class SearchServiceController extends Controller
{
    public function index($slug1 = null, $slug2 = null, $slug3 = null, Request $request)
    {


        $ct_id = '';
        if (!$request->page) {
            if ($slug1) {
                $get_category = Category::where('name_lt', $slug1)->first();
                $ct_id = $get_category->id;
            }
        }

        $s_ct_id = '';
        if (!$request->page) {
            if ($slug2) {
                $get_sub_category = SubCategory::where('name_lt', $slug2)->first();
                $s_ct_id = $get_sub_category->id;
            }
        }
        $i_ct_id = '';
        if (!$request->page) {
            if ($slug3) {
                $get_inner_category = InnerCategory::where('name_lt', $slug3)->first();
                $i_ct_id = $get_inner_category->id;
            }
        }
        $city = $request->city;
        $u_lat = '';
        $u_lng = '';
        $sln = '';
        $psnl = '';
        if (!$request->page) {
            if (!$city) {
                $ip = request()->ip();
                if ($ip == '::1') {
                    $ip = '203.78.146.6';
                }

                $getCity = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
                $u_lat = $getCity['lat'];
                $u_lng = $getCity['lon'];
            }
            $sln = 1;
            $psnl = 1;
        }
        $item['page'] = $request->page;
        $item['city'] = $request->city;
        $item['lat'] = $request->lat ?: $u_lat;
        $item['lng'] = $request->lng ?: $u_lng;
        $item['salons'] = $request->salons ?: $sln;
        $item['title'] = $request->title;
        $item['address'] = $request->address ?: $city;
        $item['personal'] = $request->personal ?: $psnl;
        $item['distance'] = $request->distance ?: 10;
        $item['price'] = $request->price ?: '0,100';
        $item['category'] = $request->category ?: $ct_id;
        $item['sub_category'] = $request->sub_category ?: $s_ct_id;
        $item['inner_category'] = $request->inner_category ?: $i_ct_id;
        $item['page_title'] = '';
        if ($slug1) {
            $item['page_title'] = $slug1;
        }
        if ($slug2) {
            $item['page_title'] = $slug2;
        }
        if ($slug3) {
            $item['page_title'] = $slug3;
        }
        if ($request->title) {
            $item['page_title'] = $request->title;
        }
        $item['city'] = $request->city;
        if ($city) {
            $item['page_title'] .= ' | ' . $city;
        }
        $item['page'] = $request->page;
        $service['cities'] = District::all();

        $get_recently = Cookie::get('recently');
        $recently = array();
        if ($get_recently) {
            $recently = explode(',', $get_recently);
            $recently = array_filter($recently);
            $recently = array_reverse($recently);
        }
        $item['recently_viewed'] = Service::whereIn('id', $recently)->get();

        $earthRadiusMiles = 3959;
        $title = $request->title;
        $lat = $request->lat ?: $u_lat;
        $lng = $request->lng ?: $u_lng;
        $category = $request->category ?: $ct_id;
        $sub_category = $request->sub_category ?: $s_ct_id;
        $inner_category = [];
        if ($request->inner_category) {
            $inner_category[] = $request->inner_category;
        } else {
            $inner_category[] =  $i_ct_id;
        }
        $inner_category = array_filter($inner_category);
        $personal = $request->personal ?: $psnl;
        $salons = $request->salons ?: $sln;
        $distance = $request->distance ?: 10;
        $page = $request->page;
        $today = date('Y-m-d H:i:s');
        $min = 0;
        $max = 0;
        if ($request->price) {
            $exp = explode(",", $request->price);
            $min = $exp[0];
            $max = $exp[1];
        } else {
            $min = 0;
            $max = 100;
        }

        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $top_services = Service::with([
            'types', 'user', 'topService' => function ($q) use ($today) {
                $q->whereDate('end_date', '>=', $today)
                    ->whereDate('start_date', '<=', $today);
            },

            'TakeVipService' => function ($r) use ($today) {
                $r->whereDate('end_date', '>=', $today);
            },
            "HighlightService" => function ($t) use ($today) {
                $t->whereDate('end_date', '>=', $today);
            }


        ])
            ->selectRaw("services.*")
            ->when($title != null, function ($q) use ($title) {
                return $q->where('title', 'LIKE', "%{$title}%");
            })
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->when($category != null, function ($q) use ($category) {
                return $q->where('category_id', $category);
            })
            ->when($sub_category != null, function ($q) use ($sub_category) {
                return $q->where('sub_category_id', $sub_category);
            })
            ->when(count($inner_category) > 0, function ($q) use ($inner_category) {
                return $q->whereJsonContains('inner_category', $inner_category);
            })
            ->where('active', 1)
            ->where('s_end_date', '>=', new Carbon(date('Y-m-d H:i:s')))
            ->when($lat == null && $city != null, function ($q) use ($city) {
                return $q->where('city', $city);
            })
            ->where('top_service', 1)
            // ->whereDate('top_services.end_date', '>=', $today)
            // ->orderBy('topServices.star', 'desc')
            ->inRandomOrder()
            ->limit(10)
            ->get();

        $totalTopService = count($top_services);
        if ($totalTopService == 0) {
            if ($page) {
                $top_services = $this->ifNotFoundTopService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category);
            }
        }
        $totalTopService = count($top_services);
        $top_services = $top_services->sortByDesc("topService.star");
        $count_top_services = count($top_services);


        $services = Service::when($title != null, function ($q) use ($title) {
            return $q->where('title', 'LIKE', "%{$title}%");
        })
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("*, {$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance)
                    ->orderBy('distance', 'ASC');
            })
            ->where('services.top_service', 0)
            ->where('services.active', 1)
            ->where('services.s_end_date', '>=', new Carbon(date('Y-m-d H:i:s')))
            ->when($category != null, function ($q) use ($category) {
                return $q->where('category_id', $category);
            })
            ->when($sub_category != null, function ($q) use ($sub_category) {
                return $q->where('sub_category_id', $sub_category);
            })
            ->when(count($inner_category) > 0, function ($q) use ($inner_category) {
                return $q->whereJsonContains('inner_category', $inner_category);
            })
            ->when($lat == null && $city != null, function ($q) use ($city) {
                return $q->where('services.city', $city);
            })

            ->with([
                "types" => function ($q) use ($min, $max) {
                    $q->when($max != null, function ($iq) use ($min, $max) {
                        return $iq->whereBetween('types.price', [$min, $max]);
                    });
                },
                "user" => function ($q) use ($personal, $salons) {
                    $q->when($personal != null && $salons > 1, function ($iq) use ($personal) {
                        return $iq->where('users.account_type', 2);
                    })
                        ->when($salons != null && $personal > 1, function ($iq) use ($salons) {
                            return $iq->where('users.account_type', 1);
                        });
                },
                "TakeVipService" => function ($r) use ($today) {
                    $r->whereDate('end_date', '>=', $today);
                },
                "HighlightService" => function ($t) use ($today) {
                    $t->whereDate('end_date', '>=', $today);
                }
            ])

            ->paginate(15);
        //dd($services->toArray());
        $total_services = count($services);

        if ($total_services == 0) {
            if ($page) {
                $services = $this->ifNotFoundService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category, $min, $max, $personal, $salons);
            }
        }
        $total_services = count($services);

        //dd($services->toArray());
        if ($page == 1) {
            $total_services = $count_top_services + $total_services;
        } else if ($page > 1) {
            $top_services = [];
        } else {
            $total_services = $count_top_services + $total_services;
        }
        $divider = $total_services / 2;
        $take_service = $divider;
        $take_profile = $divider;

        if (strpos($divider, '.') !== false) {
            $take_service = $take_service + 0.5;
            $take_profile = $take_profile - 0.5;
        }
        $serviceData = $this->takeVipServices($take_service, $lat, $lng, $distance, $city);
        $profileData = $this->takeVipProfile($take_profile, $lat, $lng, $distance, $city);
        //dd($profileData->toArray());
        $allVip = $serviceData->merge($profileData);
        $allVip = $allVip->sortBy('distance');
        //cookie
        $get_favarite = Cookie::get('favarite');
        $favarite = array();
        if ($get_favarite) {
            $favarite = explode(',', $get_favarite);
            $favarite = array_filter($favarite);
        }
        Session::put('h_price', $item['price']);
        Session::put('h_distance', $item['distance']);
        Session::put('h_salons', $salons);
        Session::put('h_personal', $personal);
        Session::put('lat', $item['lat']);
        Session::put('lng', $item['lng']);
        Session::put('address', $item['address']);


        //dd($services->toArray());
        if ($request->ajax()) {
            return view('pages.services-data', compact('top_services', 'services', 'allVip', 'favarite'));
        } else {
            return view('pages.blog', compact('item', 'service', 'services', 'top_services', 'allVip', 'favarite'));
        }
    }
    public function ifNotFoundTopService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category)
    {
        $today = date('Y-m-d H:i:s');
        $distance = $distance + 10;
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $top_services = Service::with([
            'types', 'user', 'topService' => function ($q) use ($today) {
                $q->whereDate('end_date', '>=', $today)
                    ->whereDate('start_date', '<=', $today);
            },
            'TakeVipService' => function ($r) use ($today) {
                $r->whereDate('end_date', '>=', $today);
            },
            "HighlightService" => function ($t) use ($today) {
                $t->whereDate('end_date', '>=', $today);
            }

        ])
            ->selectRaw("services.*")
            ->when($title != null, function ($q) use ($title) {
                return $q->where('title', 'LIKE', "%{$title}%");
            })
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->when($category != null, function ($q) use ($category) {
                return $q->where('category_id', $category);
            })
            ->when($sub_category != null, function ($q) use ($sub_category) {
                return $q->where('sub_category_id', $sub_category);
            })
            ->when(count($inner_category) > 0, function ($q) use ($inner_category) {
                return $q->whereJsonContains('inner_category', $inner_category);
            })
            // ->join('top_services', 'services.id', '=', 'top_services.service_id')
            // ->join('users', 'services.user_id', '=', 'users.id')
            ->where('active', 1)
            ->where('s_end_date', '>=', new Carbon(date('Y-m-d H:i:s')))
            ->where('top_service', 1)
            // ->whereDate('top_services.end_date', '>=', $today)
            // ->orderBy('topServices.star', 'desc')
            ->inRandomOrder()
            ->limit(10)
            ->get();
        $totalTopServices  = count($top_services);
        if ($totalTopServices < 1) {
            if ($distance < 101) {
                $distance = $distance + 10;
                $top_services = $this->ifNotFoundTopService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category);
            } elseif ($distance > 100 && $distance < 500) {
                $distance = 500;
                $top_services = $this->ifNotFoundTopService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category);
            } elseif ($distance > 499 && $distance < 9999) {
                $distance = 10000;
                $top_services = $this->ifNotFoundTopService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category);
            } else {
                return $top_services;
            }
        }
        return $top_services;
    }
    public function ifNotFoundService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category, $min, $max, $personal, $salons)
    {
        $today = date('Y-m-d H:i:s');
        $distance = $distance + 10;
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $services = Service::when($title != null, function ($q) use ($title) {
            return $q->where('title', 'LIKE', "%{$title}%");
        })
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("*, {$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance)
                    ->orderBy('distance', 'ASC');
            })
            ->where('services.top_service', 0)
            ->where('services.active', 1)
            ->where('services.s_end_date', '>=', new Carbon(date('Y-m-d H:i:s')))
            ->when($category != null, function ($q) use ($category) {
                return $q->where('category_id', $category);
            })
            ->when($sub_category != null, function ($q) use ($sub_category) {
                return $q->where('sub_category_id', $sub_category);
            })
            ->when(count($inner_category) > 0, function ($q) use ($inner_category) {
                return $q->whereJsonContains('inner_category', $inner_category);
            })
            ->with([
                "types" => function ($q) use ($min, $max) {
                    $q->when($max != null, function ($iq) use ($min, $max) {
                        return $iq->whereBetween('types.price', [$min, $max]);
                    });
                },
                "user" => function ($q) use ($personal, $salons) {
                    $q->when($personal != null && $salons != 1, function ($iq) use ($personal) {
                        return $iq->where('users.account_type', 2);
                    })
                        ->when($salons != null && $personal != 1, function ($iq) use ($salons) {
                            return $iq->where('users.account_type', 1);
                        });
                },
                "TakeVipService" => function ($r) use ($today) {
                    $r->whereDate('end_date', '>=', $today);
                },
                "HighlightService" => function ($t) use ($today) {
                    $t->whereDate('end_date', '>=', $today);
                }
            ])
            ->paginate(15);
        $totalServices  = count($services);
        if ($totalServices == 0) {
            if ($distance < 101) {
                $distance = $distance + 10;
                $services = $this->ifNotFoundService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category, $min, $max, $personal, $salons);
            } elseif ($distance > 100 && $distance < 500) {
                $distance = 500;
                $services = $this->ifNotFoundService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category, $min, $max, $personal, $salons);
            } elseif ($distance > 499 && $distance < 9999) {
                $distance = 10000;
                $services = $this->ifNotFoundService($lat, $lng, $title, $distance, $category, $sub_category, $inner_category, $min, $max, $personal, $salons);
            } else {
                return $services;
            }
        }
        return $services;
    }

    public function takeVipServices($take_service, $lat, $lng, $distance, $city)
    {
        $today = date('Y-m-d H:i:s');
        $distance = $distance + 10;
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $vip_services = \DB::table('services')->selectRaw("
            services.*,services.id as service_main_id,services.s_end_date,services.city,vip_services.*,users.id as user_main_id,users.name,users.surname,users.nickname
        ")
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->when($lat == null && $city != null, function ($q) use ($city) {
                return $q->where('services.city', $city);
            })
            ->join('vip_services', 'services.id', '=', 'vip_services.service_id')
            ->join('users', 'services.user_id', '=', 'users.id')
            ->where('services.vip_service', 1)
            ->whereDate('vip_services.end_date', '>=', $today)
            ->where('services.s_end_date', '>=', new Carbon(date('Y-m-d H:i:s')))
            ->inRandomOrder()
            ->limit($take_service)
            ->get();
        if (count($vip_services) == $take_service) {
            $vip_services = $vip_services;
        } else if ($distance < 100) {
            $distance = $distance + 10;
            $vip_services = $this->takeVipServices($take_service, $lat, $lng, $distance, $city);
        } else if ($distance > 99 && $distance < 500) {
            $distance = 500;
            $vip_services = $this->takeVipServices($take_service, $lat, $lng, $distance, $city);
        } else if ($distance > 499 && $distance < 10000) {
            $distance = 10000;
            $vip_services = $this->takeVipServices($take_service, $lat, $lng, $distance, $city);
        } else {
            $vip_services = $vip_services;
        }
        return $vip_services;
    }

    public function takeVipProfile($take_profile, $lat, $lng, $distance, $city)
    {
        $today = date('Y-m-d H:i:s');
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( user_lat ) ) ' .
            '* cos( radians( user_lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( user_lat ) ) ) )';

        $vip_profile = \DB::table('users')->selectRaw("
            users.*, users.id as user_main_id,users.user_city,profile_payments.id as payment_id,profile_payments.start_date,profile_payments.end_date
        ")
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->when($lat == null && $city != null, function ($q) use ($city) {
                return $q->where('users.user_city', $city);
            })

            ->join('profile_payments', 'users.id', '=', 'profile_payments.user_id')
            ->where('users.vip_profile', 1)
            ->whereDate('profile_payments.end_date', '>=', $today)
            ->inRandomOrder()
            ->limit($take_profile)
            ->get();

        if (count($vip_profile) == $take_profile) {
            $vip_profile = $vip_profile;
        } else if ($distance < 100) {
            $distance = $distance + 10;
            $vip_profile = $this->takeVipProfile($take_profile, $lat, $lng, $distance, $city);
        } else if ($distance > 99 && $distance < 500) {
            $distance = 500;
            $vip_profile = $this->takeVipProfile($take_profile, $lat, $lng, $distance, $city);
        } else if ($distance > 499 && $distance < 10000) {
            $distance = 10000;
            $vip_profile = $this->takeVipProfile($take_profile, $lat, $lng, $distance, $city);
        } else {
            $vip_profile = $vip_profile;
        }
        return $vip_profile;
    }

    public static function makeServiceSeeMore($service_name, $id)
    {
        $name = MakeUrlService::url($service_name);
        return $url = url('/') . '/' . 'service/' . $name . '/' . $id;
    }
    public static function profileLink($id, $name)
    {
        $name = MakeUrlService::url($name);
        return $url = url('/') . '/' . 'public-profile/' . $name . '/' . $id;
    }
    public static function innerCategory($inner)
    {
        //$inner = ["1","2"];
        if ($inner) {
            $inner = json_decode($inner);
        }
        return $category = InnerCategory::whereIn('id', $inner)->take(2)->get();
    }

    public static function serviceAndProfileCategory($inner, $sub_category)
    {
        $inner_cat = [];
        $category = [];
        if ($inner) {
            $inner_cat = json_decode($inner);
            $category = InnerCategory::whereIn('id', $inner_cat)->take(2)->get();
        }
        if (count($category) < 1) {
            $category = SubCategory::where('id', $sub_category)->get();
        }
        $cat['category'] = $category;
        return $cat;
    }

    public static function ProfileCategory($user_id)
    {
        $cover_img = UserCover::where('user_id', $user_id)->first();

        $services = Service::where('user_id', $user_id)->select('category_id')->take(2)->distinct('category_id')->get();
        $ids = [];
        foreach ($services as $key => $ser) {
            $ids[] = $ser->category_id;
        }
        $category = Category::whereIn('id', $ids)->get();

        $data['cover_img'] = $cover_img;
        $data['cats'] = $category;
        return $data;
    }

    public static function hasProfilePayment($id)
    {
        $havePayment = AddShowingPayment::where('user_id', $id)->where('end_date', '>=', new Carbon(date('Y-m-d H:i:s')))->first();
        if ($havePayment) {
            return 'yes';
        }
    }

    public function indexVipService($lat, $lng, $distance)
    {
        $today = date('Y-m-d H:i:s');
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $vip_services = Service::with(['types', 'user', 'VipService' => function ($q) use ($today) {
            $q->whereDate('end_date', '>=', $today)
                ->whereDate('start_date', '<=', $today);
        }])
            ->selectRaw("services.*")
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->where('active', 1)
            ->where('s_end_date', '>=', new Carbon(date('Y-m-d H:i:s')))
            ->where('vip_service', 1)
            ->inRandomOrder()
            ->limit(15)
            ->get();

        if (count($vip_services) == 15) {
            $vip_services = $vip_services;
        } else if ($distance < 100) {
            $distance = $distance + 10;
            $vip_services = $this->indexVipService($lat, $lng, $distance);
        } else if ($distance > 99 && $distance < 500) {
            $distance = 500;
            $vip_services = $this->indexVipService($lat, $lng, $distance);
        } else if ($distance > 499 && $distance < 10000) {
            $distance = 10000;
            $vip_services = $this->indexVipService($lat, $lng, $distance);
        } else {
            $vip_services = $vip_services;
        }
        return $vip_services;
    }
    public function indexVipProfile($lat, $lng, $distance)
    {
        $today = date('Y-m-d H:i:s');
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( user_lat ) ) ' .
            '* cos( radians( user_lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( user_lat ) ) ) )';

        $vip_profile = User::with(['services' => function ($q) use ($today) {
            $q->whereDate('s_end_date', '>=', $today);
        }])
            ->selectRaw("users.*")

            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })

            ->inRandomOrder()
            ->limit(15)
            ->get();
        if (count($vip_profile) == 15) {
            $vip_profile = $vip_profile;
        } else if ($distance < 100) {
            $distance = $distance + 10;
            $vip_profile = $this->indexVipProfile($lat, $lng, $distance);
        } else if ($distance > 99 && $distance < 500) {
            $distance = 500;
            $vip_profile = $this->indexVipProfile($lat, $lng, $distance);
        } else if ($distance > 499 && $distance < 10000) {
            $distance = 10000;
            $vip_profile = $this->indexVipProfile($lat, $lng, $distance);
        } else {
            $vip_profile = $vip_profile;
        }
        return $vip_profile;
    }








    public function getUserCity()
    {
        $ip = request()->ip();
        if ($ip == '::1') {
            $ip = '203.78.146.6';
        }
        $getCity = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        return $getCity['city'];
    }

    public function indexData(Request $request)
    {
        $earthRadiusMiles = 3959;
        $lat = $request->lat;
        $lng = $request->lng;
        $distance = $request->distance;
        $city = $request->city;
        $title = $request->title;
        $category = $request->category;
        $sub_category = $request->sub_category;
        $personal = $request->personal;
        $salons = $request->salons;
        if ($sub_category[0] == '') {
            $sub_category = [];
        }
        $today = date('Y-m-d H:i:s');
        $price = $request->price;
        $min = 0;
        $max = 0;
        if ($price) {
            $exp = explode(",", $price);
            $min = $exp[0];
            $max = $exp[1];
        }
        if ($min == 'NaN') {
            $min = 0;
        }

        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $top_services = \DB::table('services')->selectRaw("
            services.*,services.id as main_id,top_services.*,users.name,users.surname
        ")
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->join('top_services', 'services.id', '=', 'top_services.service_id')
            ->join('users', 'services.user_id', '=', 'users.id')
            ->when($city != null, function ($q) use ($city) {
                return $q->where('services.city', $city);
            })
            ->where('services.top_service', 1)
            ->whereDate('top_services.end_date', '>=', $today)
            ->inRandomOrder()
            ->limit(10)
            ->get();
        $top_services = $top_services->sortByDesc("star");

        $count_top_services = count($top_services);


        $services = Service::when($title != null, function ($q) use ($title) {
            return $q->where('title', 'LIKE', "%{$title}%");
        })
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("*, {$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->where('services.top_service', 0)
            ->when($category != null, function ($q) use ($category) {
                return $q->where('category_id', $category);
            })
            ->when(count($sub_category) > 0, function ($q) use ($sub_category) {
                return $q->whereJsonContains('sub_category', $sub_category);
            })
            ->when($city != null, function ($q) use ($city) {
                return $q->where('city', $city);
            })

            ->with([
                "types" => function ($q) use ($min, $max) {
                    $q->when($max != null, function ($iq) use ($min, $max) {
                        return $iq->whereBetween('types.price', [$min, $max]);
                    });
                },
                "user" => function ($q) use ($personal, $salons) {
                    $q->when($personal != null, function ($iq) use ($personal) {
                        return $iq->where('users.account_type', 1);
                    })
                        ->when($salons != null, function ($iq) use ($salons) {
                            return $iq->where('users.account_type', 2);
                        });
                }
            ])
            ->paginate(25);

        $count_services = count($services);
        $total_services = $count_top_services + $count_services;
        $divider = $total_services / 2;
        $take_service = $divider;
        $take_profile = $divider;
        if (strpos($divider, '.') !== false) {
            $take_service = $take_service + 0.5;
            $take_profile = $take_profile - 0.5;
        }
        $serviceData = $this->takeVipServices($take_service, $lat, $lng, $distance);
        $profileData = $this->takeVipProfile($take_profile, $lat, $lng, $distance);
        $allVip = $serviceData->merge($profileData);
        $allVip = $allVip->sortBy('distance');
        return view('pages.services-data', compact('services', 'top_services', 'allVip'));
    }

    public function paginationData(Request $request)
    {
        $earthRadiusMiles = 3959;
        $lat = $request->lat;
        $lng = $request->lng;
        $distance = $request->distance;
        $city = $request->city;
        $title = $request->title;
        $category = $request->category;
        $sub_category = $request->sub_category;
        if ($sub_category[0] == '') {
            $sub_category = [];
        }
        $today = date('Y-m-d H:i:s');
        $price = $request->price;
        $min = 0;
        $max = 0;
        if ($price) {
            $exp = explode(",", $price);
            $min = $exp[0];
            $max = $exp[1];
        }
        if ($min == 'NaN') {
            $min = 0;
        }
        $personal = $request->personal;
        $salons = $request->salons;

        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $services = Service::when($title != null, function ($q) use ($title) {
            return $q->where('title', 'LIKE', "%{$title}%");
        })
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("*, {$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->where('services.top_service', 0)
            ->when($category != null, function ($q) use ($category) {
                return $q->where('category_id', $category);
            })
            ->when(count($sub_category) > 0, function ($q) use ($sub_category) {
                return $q->whereJsonContains('sub_category', $sub_category);
            })
            ->when($city != null, function ($q) use ($city) {
                return $q->where('city', $city);
            })
            ->with([
                "types" => function ($q) use ($min, $max) {
                    $q->when($max != null, function ($iq) use ($min, $max) {
                        return $iq->whereBetween('types.price', [$min, $max]);
                    });
                },
                "user" => function ($q) use ($personal, $salons) {
                    $q->when($personal != null, function ($iq) use ($personal) {
                        return $iq->where('users.account_type', 1);
                    })
                        ->when($salons != null, function ($iq) use ($salons) {
                            return $iq->where('users.account_type', 2);
                        });
                }
            ])
            ->paginate(25);

        $total_services = count($services);
        $divider = $total_services / 2;
        $take_service = $divider;
        $take_profile = $divider;
        if (strpos($divider, '.') !== false) {
            $take_service = $take_service + 0.5;
            $take_profile = $take_profile - 0.5;
        }
        $serviceData = $this->takeVipServices($take_service, $lat, $lng, $distance);
        $profileData = $this->takeVipProfile($take_profile, $lat, $lng, $distance);
        $allVip = $serviceData->merge($profileData);
        $allVip = $allVip->sortBy('distance');

        return view('pages.top-service', compact('services', 'allVip'));
    }





    public function indexVipData(Request $request)
    {
        $lat = $request->lat;
        $lng = $request->lng;
        $distance = $request->distance;
        $take = 8;

        $services = $this->takeVipServices($take, $lat, $lng, $distance);
        $profiles = $this->takeVipProfile($take, $lat, $lng, $distance);
        $services = $services->sortBy('distance');
        $profiles = $profiles->sortBy('distance');
        return view('pages.vip-profile-service', compact('services', 'profiles'));
    }











    public function getSubCategory(Request $request)
    {
        $sub_categories = SubCategory::where('category_id', $request->category_id)->get();
        $html = '';
        if (count($sub_categories) > 0) {
            foreach ($sub_categories as $key => $sub_cat) {
                $html .= '<div class="ctm-option-ch"><label class="ctm-con mar-0">' . $sub_cat->name . '
					<input type="checkbox" name="sub_category[]" value="' . $sub_cat->id . '" onclick="getServices(0)">
					<span class="checkmark"></span>
				</label></div>';
            }
        }
        return $html;
    }

    public function test()
    {
        $title = "maiore";
        $category = 5;
        $sub_category = [20];
        $city = "Dhaka";
        $min = 10;
        $max = 35;
        $lat = '23.810332';
        $lng = '90.4125181';
        $distance = '10';
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $data = Service::when($title != null, function ($q) use ($title) {
            return $q->where('title', 'LIKE', "%{$title}%");
        })
            ->selectRaw("*, {$selectDistance} AS distance")
            ->whereRaw("{$selectDistance} < ?", $distance)
            ->when($category != null, function ($q) use ($category) {
                return $q->where('category_id', $category);
            })
            ->when(count($sub_category) > 0, function ($q) use ($sub_category) {
                return $q->whereJsonContains('sub_category', $sub_category);
            })
            ->when($city != null, function ($q) use ($city) {
                return $q->where('city', $city);
            })
            ->with(["types" => function ($q) use ($min, $max) {
                $q->when($max != null, function ($iq) use ($min, $max) {
                    return $iq->whereBetween('types.price', [$min, $max]);
                });
            }])
            ->paginate(15);
        dd($data->toArray());
    }

    public function profileList(Request $request)
    {
        $name = $request->profile_name;
        $profile_id = $request->profile_id;




        $lat = Session::get('lat');
        $lng = Session::get('lng');
        $distance = 50000;

        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( user_lat ) ) ' .
            '* cos( radians( user_lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( user_lat ) ) ) )';

        $data['users'] = User::with(['services'])
        ->selectRaw("users.*")
        ->when($name != null && $profile_id == null, function ($q) use ($name) {
            return $q->where('name', 'LIKE', "%{$name}%")
                   ->orWhere('surname', 'LIKE', "%{$name}%")
                   ->orWhere('nickname', 'LIKE', "%{$name}%");
        })
        ->when($profile_id != null, function ($r) use ($profile_id) {
            return $r->where('id', $profile_id);
                   
        })
        ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
            return $q->selectRaw("{$selectDistance} AS distance")
                ->whereRaw("{$selectDistance} < ?", $distance)
                ->orderBy('distance');
        })
        ->inRandomOrder()
        ->paginate(15); 





        // if ($name == '') {
        //     $data['users'] = User::orderBy(DB::raw('RAND()'))->paginate(12);
        // } else {
        //     $data['users'] = User::where('name', 'LIKE', "%{$name}%")
        //         ->orWhere('surname', 'LIKE', "%{$name}%")
        //         ->orWhere('nickname', 'LIKE', "%{$name}%")
        //         ->paginate(12);
        // }
        // dd($data['users']->toArray());
         $data['profile_name'] = $name;
        // if ($profile_id != '' && $name != '') {
        //     $data['users'] = User::where('id', $profile_id)->paginate(12);
        // }
        $service = Service::where('vip_service', 1)->orderBy(DB::raw('RAND()'))->limit(3)->get();
        $profile = User::where('vip_profile', 1)->orderBy(DB::raw('RAND()'))->limit(2)->get();
        $data['vip_item'] = $service->merge($profile);
        $service['cities'] = District::all();

        $city = '';
        $serviceData = app('App\Http\Controllers\SearchServiceController')->takeVipServices(2, $lat, $lng, $distance, $city);
        $profileData = app('App\Http\Controllers\SearchServiceController')->takeVipProfile(2, $lat, $lng, $distance, $city);
        $allVip = $serviceData->merge($profileData);
        $allVip = $allVip->sortBy('distance');

        return view('pages.profile-list', compact('data', 'service','allVip'));
    }

    public function profileAndService(Request $request)
    {

        $distance = Session::get('h_distance');
        if ($distance == '') {
            $distance = 100;
        }
        $serviceData = $this->takeVipServices(4, $request->lat, $request->lng, $distance);
        $profileData = $this->takeVipProfile(4, $request->lat, $request->lng, $distance);
        $profiles = '';
        $services = '';
        if (count($profileData) > 0) {
            foreach ($profileData as $key => $profile) {
                $img = asset('assets/img/services/' . $profile->profile_image);
                $url = url("public-profile") . '/' . $profile->id;
                $profiles .= '<div class="blog">
                <div class="blog-img">
                  <img src="' . $img . '">
                </div>
                <div class="blog-des">
                  <div class="blog-txt">
                   ' . substr($profile->bio, 0, 85) . '...
                  </div>
                  <div class="blog-dta star">
                    <span class="date"><a href="' . $url . '">View profile</a></span>
                  </div>
                </div>
              </div>';
            }
        }
        if (count($serviceData) > 0) {
            foreach ($serviceData as $key => $service) {
                $img = asset('assets/img/services/' . $service->image);
                $url = url("about-model") . '/' . $service->main_id;
                $services .= '<div class="one-content">
                  <div class="single-content">
                    <div class="content-img-p">
                      <div class="content-img">
                        <img src="' . $img . '">
                      </div>
                    </div>
                  </div>
                  <div class="content-des">
                    <h5><a href="' . $url . '" class="url-color">' . $service->title . '</a></h5>
                  </div>
                  <div class="content-tlt">
                    ' . substr($service->description, 0, 120) . '...
                  </div>

                </div>';
            }
        }
        return response()->json(['profiles' => $profiles, 'services' => $services]);
    }
    public function searchProfile(Request $request)
    {
        $name = $request->keyword;
        $datas = User::where('name', 'LIKE', "%{$name}%")
            ->orWhere('surname', 'LIKE', "%{$name}%")
            ->orWhere('nickname', 'LIKE', "%{$name}%")
            ->get();

        $items = '';
        if (count($datas) > 0) {
            foreach ($datas as $key => $data) {
                $name = '';
                if ($data->name) {
                    $name .= $data->name . ' ';
                }
                if ($data->surname) {
                    $name .= $data->surname . ' ';
                }
                if ($data->nickname) {
                    $name .= '(' . $data->nickname . ')';
                }

                $items .= '<div class="src-rslt-con1 result-item1" id="' . $data->id . '">' . $name . '</div>';
            }
        }
        return $items;
    }

    public function indexVipServices(Request $request)
    {
        $lat = $request->lat ?: Session::get('lat');
        $lng = $request->lng ?: Session::get('lng');
        $distance = $request->distance ?: Session::get('h_distance');
        $vip_services = $this->indexVipService($lat, $lng, $distance);
        $vip_services = $vip_services->sortBy("distance");
        $vip = view('pages.home.services', compact('vip_services'))->render();
        $vip_type = view('pages.home.service_type', compact('vip_services'))->render();
        $profiles = $this->indexVipProfile($lat, $lng, $distance);

        $vip_profile = view('pages.home.profile', compact('profiles'))->render();
        $vip_profile_inner = view('pages.home.profile_inner_cat', compact('profiles'))->render();

        return response()->json(['vip' => $vip, 'vip_type' => $vip_type, 'vip_profile' => $vip_profile, 'vip_profile_inner' => $vip_profile_inner]);

        // echo '<pre>';
        // print_r($vip_profile->toArray());
        // echo '</pre>';
        //return $vip;
    }

    public function indexVipServicesRecuresive($lat, $lng, $distance, $call)
    {
        $today = date('Y-m-d H:i:s');
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';
        $vip_services = Service::with(['types', 'user', 'VipService' => function ($q) use ($today) {
            $q->whereDate('end_date', '>=', $today)
                ->whereDate('start_date', '<=', $today);
        }])
            ->selectRaw("services.*")
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->where('active', 1)
            ->where('s_end_date', '>=', new Carbon(date('Y-m-d H:i:s')))
            ->where('vip_service', 1)
            ->inRandomOrder()
            ->limit(100)
            ->get();

        if (count($vip_services) < 1) {
            if ($call == 1) {
                $distance = 100;
                $call++;
                return $this->indexVipServicesRecuresive($lat, $lng, $distance, $call);
            }
            if ($call == 2) {
                $call++;
                $lat = '';
                $lng = '';
                return $this->searchServiceTitle2($lat, $lng, $distance, $call);
            } else {
                return $vip_services;
            }
        } else {
            return $vip_services;
        }
    }
    public function indexVipProfileRecuresive($lat, $lng, $distance, $call)
    {
        $today = date('Y-m-d H:i:s');
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( user_lat ) ) ' .
            '* cos( radians( user_lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( user_lat ) ) ) )';
        $vip_profile = User::with(['ProfilePayment' => function ($q) use ($today) {
            $q->whereDate('end_date', '>=', $today)
                ->whereDate('start_date', '<=', $today);
        }])
            ->selectRaw("users.*")
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->where('vip_profile', 1)
            ->inRandomOrder()
            ->limit(100)
            ->get();

        if (count($vip_profile) < 1) {
            if ($call == 1) {
                $distance = 100;
                $call++;
                return $this->indexVipProfileRecuresive($lat, $lng, $distance, $call);
            }
            if ($call == 2) {
                $call++;
                $lat = '';
                $lng = '';
                return $this->indexVipProfileRecuresive($lat, $lng, $distance, $call);
            } else {
                return $vip_profile;
            }
        } else {
            return $vip_profile;
        }
    }
}
