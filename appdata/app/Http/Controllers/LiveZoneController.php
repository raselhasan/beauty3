<?php

namespace App\Http\Controllers;

use Session;
use App\User;
use App\Comment;
use App\Gallery;
use App\Package;
use App\Service;
use App\Category;
use App\District;
use App\LiveZone;
use App\UserCover;
use App\VipPackage;
use App\SubCategory;
use App\CommentReplay;
use Carbon\Carbon;
use App\InnerCategory;
use App\ServiceGalleryTab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Cookie;
use Illuminate\Support\Facades\Validator;
use App\Like;
use App\LiveZoneComment;
use App\CommentLike;
use App\SubCommentLike;
use App\SubComment;
use App\Booking;

class LiveZoneController extends Controller
{
    public function index(Request $request)
    {
        
        if (auth()->user()) {
            $data['user'] = User::find(auth()->user()->id);
        }

        $data['liveZones'] = LiveZone::with(['comments.commenter', 'comments.subcomments.commenter'])->orderBy('id', 'DESC')->where('show_livezone',1)->paginate(5)->map(function ($query) {
            $query->setRelation('comments', $query->comments->take(2));
            $query->comments->map(function ($q) {
                $q->setRelation('subcomments', $q->subcomments->take(1));
                return $q;
            });
            return $query;
        });
        $view_live_zone = $request->live_zone;


        // $livezone = LiveZone::with('comments.commenter','comments.subcomments.commenter')->where('id',60)->first();

        // dd($livezone->toArray());
        // $comments = LiveZoneComment::with(['commenter', 'subcomments.commenter'])->where('livezone_id',$livezone_id)->orderBy('id', 'DESC')->offset(2)->take(10)->get()->map(function ($query) {
        //     $query->setRelation('subcomments', $query->subcomments->take(1));
        //     return $query;
        // });


        //dd($comments->toArray());


        $lat = Session::get('lat');
        $lng = Session::get('lng');
        $distance = Session::get('h_distance') ? : 10;
        $city = '';
        $serviceData = app('App\Http\Controllers\SearchServiceController')->takeVipServices(4, $lat, $lng, $distance, $city);

        $profileData = app('App\Http\Controllers\SearchServiceController')->takeVipProfile(4, $lat, $lng, $distance, $city);
        $allVip = $serviceData->merge($profileData);
        $allVip = $allVip->sortBy('distance');
        //dd($allVip->toArray());

        if ($request->ajax()) {
            $view = view('pages.comments.live-zone-pagination', compact('data'))->render();
            return response()->json(['html' => $view]);
        } else {
            return view('pages.live-zone', compact('data','view_live_zone','allVip'));
        }
    }

    public function getPagiantionData(Request $request)
    {
        $data['liveZones'] = LiveZone::with(['comments.commenter', 'comments.subcomments.commenter'])->orderBy('id', 'DESC')->where('show_profile',1)->where('user_id',$request->id)->paginate(5)->map(function ($query) {
            $query->setRelation('comments', $query->comments->take(2));
            $query->comments->map(function ($q) {
                $q->setRelation('subcomments', $q->subcomments->take(1));
                return $q;
            });
            return $query;
        });
        $show_e_d = $request->show_e_d;
        if($show_e_d == 1){
            $view = view('pages.comments.live-zone-pagination-user', compact('data'))->render();
        }else{
            $view = view('pages.comments.live-zone-pagination', compact('data'))->render();
        }
        
        return response()->json(['html' => $view]);
    }

    public function UploadImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        if ($validator->passes()){
            $data = $request->all();
            $data['image'] = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move('liveZone', $data['image']);
            $img['image'] = $data['image'];
            $img['status'] = 1;
            Session::put('live_zone_image',$img);
            $image_path = url('LiveZone/'.$data['image']);
            $file = '<img src="'.$image_path.'">';
            return response()->json(['success'=>'Data is successfully added','file'=>$file]);
        }else{
            $all_error = '';
            foreach ($validator->errors()->all() as $key => $error) {
               $all_error .= $error.'</br>'; 
            }
            return response()->json(['error'=>$all_error]);
        }
        
    }

    public function UploadYourtubeVideo(Request $request)
    {
        $video_link = $request->live_zone_vido_link;
        $exp = explode('=', $video_link);
        $video_link = $exp[1];

        $img['image'] = $video_link;
        $img['status'] = 2;
        Session::put('live_zone_image',$img);
        $link = '<iframe width="855" height="315"
                    src="https://www.youtube.com/embed/'.$video_link.'">
                </iframe>';
        return response()->json(['success'=>'Data is successfully added','link'=>$link]);

    }
    public function saveLiveZone(Request $request)
    {
        $user_id = auth()->user()->id;
        $liveZone = new LiveZone;
        $liveZone->user_id = $user_id;
        $liveZone->description = $request->text;
        if(Session::has('live_zone_image')){
            $img = Session::get('live_zone_image');
            $liveZone->image = $img['image'];
            $liveZone->status = $img['status'];
            Session::forget('live_zone_image');
        }
        $liveZone->show_profile = $request->show_profile;
        $liveZone->show_livezone = $request->show_livezone;
        $liveZone->save();
        $livezone = LiveZone::find($liveZone->id);
        $edit = $request->show_edit;
        return view('pages.comments.load-live-zone',compact('livezone','edit'));
        
    }
    public function saveFirstLike(Request $request)
    {
        if(Auth::check()){
            $check = Like::where('liker_id',auth()->user()->id)->where('livezone_id',$request->id)->first();
            if($check){
                $like = Like::find($check->id);
            }else{
               $like = new Like; 
            }
            $like->livezone_id = $request->id;
            $like->react = $request->react;
            $like->liker_id = Auth::user()->id;
            $like->save();
            $total_like = Like::where('livezone_id',$request->id)->count();
            $reacts = '';
            $get_like = Like::where('react','like')->where('livezone_id',$request->id)->first();
            if($get_like){
                $reacts .= '<span class="like-i"><i class="fa fa-thumbs-up"></i></span>';
            }
            $get_love = Like::where('react','love')->where('livezone_id',$request->id)->first();
            if($get_love){
                $reacts .= '<span class="love-i"><i class="fas fa-heart"></i></span>';
            }
            
            return response()->json(['total_like'=>$total_like,'reacts'=>$reacts]);
        }
        
    }

    public static function firstLikeCount($livezone_id)
    {
        $item['total'] = Like::where('livezone_id',$livezone_id)->count();
        if($item['total']<1){
           $item['total'] = ''; 
        }
        $reacts = '';
        $get_like = Like::where('react','like')->where('livezone_id',$livezone_id)->first();
        if($get_like){
            $reacts .= '<span class="like-i"><i class="fa fa-thumbs-up"></i></span>';
        }
        $get_love = Like::where('react','love')->where('livezone_id',$livezone_id)->first();
        if($get_love){
            $reacts .= '<span class="love-i"><i class="fas fa-heart"></i></span>';
        }
        $item['reacts'] = $reacts;
        return $item;

    }

    public function saveFirstComment(Request $request)
    {
        $comment = new LiveZoneComment;
        $comment->livezone_id = $request->id;
        $comment->commenter_id = Auth::user()->id;
        $comment->comments = $request->text;
        $comment->save();
        $new_comment = LiveZoneComment::find($comment->id);
        return view('pages.comments.load-main-comment',compact('new_comment'));
        //return response()->json(['new_comment'=>$data]);
    }
    public function saveFirstCommentPopup(Request $request)
    {
        $comment = new LiveZoneComment;
        $comment->livezone_id = $request->id;
        $comment->commenter_id = Auth::user()->id;
        $comment->comments = $request->text;
        $comment->save();
        $new_comment = LiveZoneComment::find($comment->id);
        return view('pages.comments.popup.load-main-comment',compact('new_comment'));
        //return response()->json(['new_comment'=>$data]);
    }

    public function saveSubComment(Request $request){
        $comment = new SubComment;
        $comment->comment_id = $request->id;
        $comment->commenter_id = Auth::user()->id;
        $comment->comments = $request->text;
        $comment->livezone_id = $request->livezone_id;
        $comment->save();
        $new_comment = SubComment::find($comment->id);
        return view('pages.comments.load-sub-comment',compact('new_comment'));
    }
    public function saveSubCommentPopup(Request $request){
        $comment = new SubComment;
        $comment->comment_id = $request->id;
        $comment->commenter_id = Auth::user()->id;
        $comment->comments = $request->text;
        $comment->livezone_id = $request->livezone_id;
        $comment->save();
        $sub_comment = SubComment::find($comment->id);
        return view('pages.comments.popup.add-sub-comment',compact('sub_comment'));
    }

    public function saveSubCommentReply(Request $request){
        $comment = new SubComment;
        $comment->comment_id = $request->id;
        $comment->commenter_id = Auth::user()->id;
        $comment->comments = $request->text;
        $comment->livezone_id = $request->livezone_id;
        $comment->save();
        $new_comment = SubComment::find($comment->id);
        return view('pages.comments.load-sub-comment',compact('new_comment'));
    }
    public function saveSubCommentReplyPopup(Request $request){
        $comment = new SubComment;
        $comment->comment_id = $request->id;
        $comment->commenter_id = Auth::user()->id;
        $comment->comments = $request->text;
        $comment->livezone_id = $request->livezone_id;
        $comment->save();
        $sub_comment = SubComment::find($comment->id);
        return view('pages.comments.popup.add-sub-comment',compact('sub_comment'));
    }

    public function commentLike(Request $request)
    {
        if(Auth::check()){
            $check = CommentLike::where('liker_id',auth()->user()->id)->where('comment_id',$request->comment_id)->first();
            if($check){
                $like = CommentLike::find($check->id);
            }else{
               $like = new CommentLike; 
            }
            $like->comment_id = $request->comment_id;
            $like->react = $request->react;
            $like->liker_id = Auth::user()->id;
            $like->save();
            $total_like = CommentLike::where('comment_id',$request->comment_id)->count();
            if($total_like < 1){
                $total_like = '';
            }
            $reacts = '';
            $get_like = CommentLike::where('react','like')->where('comment_id',$request->comment_id)->first();
            if($get_like){
                $reacts .= '<span class="like-i"><i class="fa fa-thumbs-up"></i></span>';
            }
            $get_love = CommentLike::where('react','love')->where('comment_id',$request->comment_id)->first();
            if($get_love){
                $reacts .= '<span class="love-i"><i class="fas fa-heart"></i></span>';
            }
            $reacts .= '<span class="n-count">'.$total_like.'</span>';
            
            return response()->json(['total_like'=>$total_like,'reacts'=>$reacts]);
        }
    }
    public static function commentLikeCount($comment_id)
    {
        $item['total'] = CommentLike::where('comment_id',$comment_id)->count();
        if($item['total']<1){
           $item['total'] = ''; 
        }
        $reacts = '';
        $get_like = CommentLike::where('react','like')->where('comment_id',$comment_id)->first();
        if($get_like){
            $reacts .= '<span class="like-i"><i class="fa fa-thumbs-up"></i></span>';
        }
        $get_love = CommentLike::where('react','love')->where('comment_id',$comment_id)->first();
        if($get_love){
            $reacts .= '<span class="love-i"><i class="fas fa-heart"></i></span>';
        }
        $reacts .= '<span class="n-count">'.$item['total'].'</span>';
        $item['reacts'] = $reacts;
        return $item;

    }

    public function subCommentLike(Request $request)
    {
        if(Auth::check()){
            $check = SubCommentLike::where('liker_id',auth()->user()->id)->where('sub_comment_id',$request->sub_comment_id)->first();
            if($check){
                $like = SubCommentLike::find($check->id);
            }else{
               $like = new SubCommentLike; 
            }
            $like->sub_comment_id = $request->sub_comment_id;
            $like->react = $request->react;
            $like->liker_id = Auth::user()->id;
            $like->save();
            $total_like = SubCommentLike::where('sub_comment_id',$request->sub_comment_id)->count();
            if($total_like < 1){
                $total_like = '';
            }
            $reacts = '';
            $get_like = SubCommentLike::where('react','like')->where('sub_comment_id',$request->sub_comment_id)->first();
            if($get_like){
                $reacts .= '<span class="like-i"><i class="fa fa-thumbs-up"></i></span>';
            }
            $get_love = SubCommentLike::where('react','love')->where('sub_comment_id',$request->sub_comment_id)->first();
            if($get_love){
                $reacts .= '<span class="love-i"><i class="fas fa-heart"></i></span>';
            }
            $reacts .= '<span class="n-count">'.$total_like.'</span>';
            
            return response()->json(['total_like'=>$total_like,'reacts'=>$reacts]);
        }
    }
    public static function subCommentLikeCount($sub_comment_id)
    {
        $item['total'] = SubCommentLike::where('sub_comment_id',$sub_comment_id)->count();
        if($item['total']<1){
           $item['total'] = ''; 
        }
        $reacts = '';
        $get_like = SubCommentLike::where('react','like')->where('sub_comment_id',$sub_comment_id)->first();
        if($get_like){
            $reacts .= '<span class="like-i"><i class="fa fa-thumbs-up"></i></span>';
        }
        $get_love = SubCommentLike::where('react','love')->where('sub_comment_id',$sub_comment_id)->first();
        if($get_love){
            $reacts .= '<span class="love-i"><i class="fas fa-heart"></i></span>';
        }
        $reacts .= '<span class="n-count">'.$item['total'].'</span>';
        $item['reacts'] = $reacts;
        return $item;
    }

    public static function totalComment($livezone_id)
    {
        $comment1 = LiveZoneComment::where('livezone_id',$livezone_id)->count();
        $comment2 = SubComment::where('livezone_id',$livezone_id)->count();
        $comment['main'] = $comment1;
        $comment['total'] = $comment1 + $comment2;
        return $comment;

    }

    public function seeMoreComment(Request $request)
    {
        $livezone_id = $request->livezone_id;
        $offset = $request->offset;
        $total_comment = $request->total_comment;
        $comments = LiveZoneComment::with(['commenter', 'subcomments.commenter'])->where('livezone_id',$livezone_id)->orderBy('id', 'DESC')->offset($offset)->take(10)->get()->map(function ($query) {
            $query->setRelation('subcomments', $query->subcomments->take(1));
            return $query;
        });
        //dd($live_zones->toArray());
        return view('pages.comments.see-more-comments',compact('comments'));
    }

    public function seeMoreCommentPopup(Request $request)
    {
        $livezone_id = $request->livezone_id;
        $offset = $request->offset;
        $total_comment = $request->total_comment;
        $comments = LiveZoneComment::with(['commenter', 'subcomments.commenter'])->where('livezone_id',$livezone_id)->orderBy('id', 'DESC')->offset($offset)->take(10)->get()->map(function ($query) {
            $query->setRelation('subcomments', $query->subcomments->take(1));
            return $query;
        });
        //dd($live_zones->toArray());
        return view('pages.comments.popup.load-more-comment',compact('comments'));
    }
    public static function countSubComment($comment_id)
    {
        return SubComment::where('comment_id',$comment_id)->count();
    }
    public function seeMoreSubComment(Request $request)
    {
        $comment_id = $request->comment_id;
        $offset = $request->offset;
        $total_sub_comment = $request->total_sub_comment;

        // $comment_id = 40;
        // $offset = 1;
        // $total_sub_comment = $request->total_sub_comment;

        $subcomments = SubComment::where('comment_id',$comment_id)->with('commenter')->take(10)->skip($offset)->orderBy('id')->get();
        //dd($comments->toArray());
        return view('pages.comments.see-more-sub-comments',compact('subcomments'));
    }

    public function seeMoreSubCommentPopup(Request $request)
    {
        $comment_id = $request->comment_id;
        $offset = $request->offset;
        $total_sub_comment = $request->total_sub_comment;


        $subcomments = SubComment::where('comment_id',$comment_id)->with('commenter')->take(10)->skip($offset)->orderBy('id')->get();

        return view('pages.comments.popup.load-more-sub-comment',compact('subcomments'));

    }


    public function openPopUp(Request $request)
    {
        $data['liveZones'] = LiveZone::with(['comments.commenter', 'comments.subcomments.commenter'])->where('id',$request->livezone_id)->get()->map(function ($query) {
            $query->setRelation('comments', $query->comments->take(2));
            $query->comments->map(function ($q) {
                $q->setRelation('subcomments', $q->subcomments->take(1));
                return $q;
            });
            return $query;
        });
        $data = view('pages.comments.popup.load-popup',compact('data'))->render();
        return response()->json(['data'=>$data]);
    }





    public function saveServiceComment(Request $request)
    {
        $saveComment = new Comment;
        $saveComment->service_id = $request->service_id;
        $saveComment->comment = $request->text;
        $saveComment->commented_user = auth()->user()->id;
        $saveComment->star = $request->star;
        $saveComment->save();
        $comment = Comment::where('id', $saveComment->id)->with('user','CommentReplay.user')->first()->toArray();
        $html = view('pages.comments.service-main-comment',compact('comment'))->render();

        $total_booking = Booking::where('service_id',$request->service_id)->where('confirm',1)->where('user_id',auth()->user()->id)->count();
        $total_main_comment = Comment::where('service_id',$request->service_id)->where('commented_user',auth()->user()->id)->count();
        $hasComment = 0;
        if($total_booking > $total_main_comment){
            $hasComment = 1;
        }

        return response()->json(['html'=>$html,'hasComment'=>$hasComment]);
    }

    public function saveServiceCommentReplay(Request $request)
    {
        $replay = new CommentReplay;
        $replay->replay = $request->text;
        $replay->comment_id = $request->comment_id;
        $replay->replay_user_id = auth()->user()->id;
        $replay->service_id = $request->service_id;
        $replay->save();
        $comment_replay = CommentReplay::where('id', $replay->id)->with('user')->get()->toArray();
        return view('pages.comments.service-comment-replay',compact('comment_replay'));
    }
    public static function isServiceReplayAvaible($comment_id,$service_id, $user_id)
    {
        if(Auth::check()){

            if($user_id == auth()->user()->id){
                $check_replay = CommentReplay::where('comment_id',$comment_id)->where('replay_user_id',auth()->user()->id)->first();
                if(!$check_replay){
                    return 'yes';
                } 
            }
        }
    }

    public function loadMoreServiceComment(Request $request)
    {
        $comments = Comment::where('service_id', $request->service_id)->with('CommentReplay.user', 'user')->orderBy('id', 'DESC')->limit(10)->offset($request->offset)->get()->toArray();
        $user_id = $request->user_id;
        return view('pages.comments.load-more-service-comment',compact('comments','user_id'));
    }


}
