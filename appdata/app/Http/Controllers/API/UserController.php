<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\User;
use App\UserSetting;
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Carbon\Carbon;

class UserController extends ApiController
{

	public $successStatus = 200;

	public function login()
	{
		// $user_check = User::where('email',request('email'))->where('email_verified_at','<>','')->where('sms_verified_at','<>','')->first();
		// if(!$user_check){
		// 	return $this->sendError('Not possible to login', ['error' => 'Unauthorised'], 401);
		// }
		$email = request('email');
		$userCheck = User::where(function ($q) use ($email) {
			$q->where('email', $email);
			$q->orWhere('phone', $email);
		})
			->whereNotNull('sms_verified_at')
			->first();
		if ($userCheck) {
			if (Auth::attempt(['email' => request('email'), 'password' => request('password')]) || Auth::attempt(['phone' => request('email'), 'password' => request('password')])) {
				$user = Auth::user();
				$success['token'] =  $user->createToken('MyApp')->accessToken;
				$success['user'] =  $user;
				return $this->sendResponse($success, 'Logged in');
			} else {
				return $this->sendError('Username or Password not match', ['error' => 'Unauthorised'], 401);
			}
		} else {
			return $this->sendError('Phone is not verified', ['error' => 'Unauthorised'], 401);
		}
	}

	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'email' => 'required|email',
			'password' => 'required',
			'c_password' => 'required|same:password',
		]);
		if ($validator->fails()) {
			return response()->json(['error' => $validator->errors()], 401);
		}
		$input = $request->all();
		$input['password'] = bcrypt($input['password']);

		if (User::where('email', $input['email'])->exists())
			return $this->sendError('User already exists', ['error' => ''], 404);

		$user = User::create($input);
		$success['token'] =  $user->createToken('MyApp')->accessToken;
		$success['name'] =  $user->name;
		return $this->sendResponse($success, 'User Created');
	}

	public function addUser(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required'
		]);
		if ($validator->fails()) {
			return response()->json(['error' => $validator->errors()], 401);
		}
		$input = $request->all();
		$input['password'] = bcrypt('password');
		$input['sms_verified_at'] = Carbon::now()->format("Y-m-d H:i:s");
		if (User::where('phone', $input['phone'])->exists())
			return $this->sendError('User already exists', ['error' => ''], 404);

		$user = User::first();
		$msgBody = "Your username: " . $request->phone . " password: password" . " and website url: " . url('/');
		$send_sms = $this->sendSms($request->phone, $msgBody);
		$user = User::create($input);
		$success['token'] =  $user->createToken('MyApp')->accessToken;
		$success['user'] =  $user;
		return $this->sendResponse($success, 'User Created');
	}
	public function sendSms($number, $code)
	{
		$text = 'Your Grožiokalviai verification code is: ' . $code;
		$recipients = [8801909888251];
		$url = "https://gatewayapi.com/rest/mtsms";
		$api_token = "ipFQocVySkGQoe3F3xxvBpFuyVKLAKEoJp_vYW9SR7gdX7GqMzcTtJVwLAqD2hcQ";
		$json = [
			'sender' => 'ExampleSMS',
			'message' => $text,
			'recipients' => [],
		];
		foreach ($recipients as $msisdn) {
			$json['recipients'][] = ['msisdn' => $msisdn];
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_USERPWD, $api_token . ":");
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		// print($result);
		// $json = json_decode($result);
		// print_r($json->ids);
	}

	public function details($user)
	{
		$user = User::find($user);
		return $this->sendResponse($user, 'User Data');
	}
	public function getUsers(Request $request)
	{
		$users = User::where('name', 'like', '%' . $request->src . '%')
			->orWhere('surname', 'like', '%' . $request->src . '%')
			->orWhere('nickname', 'like', '%' . $request->src . '%')
			->orWhere('email', 'like', '%' . $request->src . '%')
			->orWhere('phone', 'like', '%' . $request->src . '%')
			->get();
		return $this->sendResponse($users, 'User search result');
	}
	public function settings($user, Request $request)
	{
		$check = UserSetting::where('user_id', $user)->first();
		if ($check) {
			$userSetting = UserSetting::find($check->id);
			$userSetting->is_calendar = $request->is_calendar;
			$userSetting->save();
		} else {
			$userSetting = new UserSetting;
			$userSetting->user_id = $user;
			$userSetting->is_calendar = $request->is_calendar;
			$userSetting->save();
		}
		return $this->sendResponse($userSetting, 'User settings successfully saved');
	}
	public function getSettings($user)
	{
		$userSetting = UserSetting::where('user_id', $user)->first();
		return $this->sendResponse($userSetting, 'User settings result');
	}
}
