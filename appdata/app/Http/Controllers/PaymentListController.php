<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Carbon\Carbon;
use App\Payment;
use PDF;
use App\ProfilePayment;
use App\VipPackage;
use App\AddShowingPackage;
use App\AddShowingPayment;
use App\District;
use App\User;
use App\ProfilePaymentHistory;
use App\AddShowingHistory;
class PaymentListController extends Controller
{
    public function index()
    {
    	$services = Service::where('user_id',auth()->user()->id)->where('active',1)->with('payment')->get();
        if(count($services)>0){
            $services = $services->toArray();
        }
        $profile_payments = ProfilePayment::where('user_id',auth()->user()->id)->with('vipPackage')->first();
        if($profile_payments){
            $profile_payments = $profile_payments->toArray();
        }
        $add_controll = AddShowingPayment::where('user_id',auth()->user()->id)->with('AddShowingPackage')->first();
        if($add_controll){
            $add_controll = $add_controll->toArray();
        }
        $service['cities'] = District::all();
    	return view('pages.payment-list',compact('services','profile_payments','add_controll','service'));
    }

    public function paymentHistory()
    {
    	$payments = Payment::where('user_id',Auth()->user()->id)->get()->groupBy('order_id')->toArray();
        $profile_payments = ProfilePaymentHistory::where('user_id',Auth()->user()->id)->with('user','vipPackage')->orderBy('id','DESC')->get()->toArray();

        $AddShowingPayment = AddShowingHistory::where('user_id',Auth()->user()->id)->with('user','AddShowingPackage')->orderBy('id','DESC')->get()->toArray();

    	//dd($AddShowingPayment);
        $service['cities'] = District::all();
    	return view('pages.payment-history',compact('payments','profile_payments','AddShowingPayment','service'));
    }

    public function paymentDownload(Request $request)
    {
    	$ids = explode(' ',trim($request->ids));
    	$payments = Payment::whereIn('id',$ids)->get()->toArray();
    	$pdf = PDF::loadView('pdf/payment-pdf',['payments'=>$payments]);
        return $pdf->download('invoice.pdf');
    }

    public function downloadServicePayment(Request $request)
    {
        $ids = explode(' ',trim($request->ids));
        $payments = Payment::whereIn('id',$ids)->get()->toArray();
        $user = User::where('id',auth()->user()->id)->first();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(\View::make('pdf.service-payment')->with(['payments'=>$payments,'user'=>$user])->render());
        $mpdf->Output('invoice.pdf','d');
        //$mpdf->Output();
    }
    public function downloadProfilePayment($id)
    {
        $payments = ProfilePaymentHistory::where('id',$id)->with('user','vipPackage')->orderBy('id','DESC')->first()->toArray();
        $user = User::where('id',auth()->user()->id)->first();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(\View::make('pdf.profile-payment')->with(['payments'=>$payments,'user'=>$user])->render());
        $mpdf->Output('invoice.pdf','d');

    }
    public function downloadAddControllPayment($id)
    {
        $payments = AddShowingHistory::where('id',$id)->with('user','AddShowingPackage')->first()->toArray();
        $user = User::where('id',auth()->user()->id)->first();
        //dd($payments);
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML(\View::make('pdf.add-controll-payment')->with(['payments'=>$payments,'user'=>$user])->render());
        $mpdf->Output('invoice.pdf','d');
        //$mpdf->Output();

    }
}
