<?php

namespace App\Http\Controllers;

use Hash;
use Mpdf;
use Cookie;
use Session;
use App\User;
use App\Booking;
use App\Comment;
use App\Gallery;
use App\Package;
use App\Service;
use App\Category;
use App\District;
use App\LiveZone;
use App\UserCover;
use Carbon\Carbon;
use App\VipPackage;
use App\VipService;
use App\SubCategory;
use App\CommentReplay;
use App\InnerCategory;
use App\ServiceGalleryTab;
use Illuminate\Http\Request;
use App\Services\MakeUrlService;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function setVisitorLatLng($lat, $lng, $address)
    {
        Session::put('lat', $lat);
        Session::put('lng', $lng);
        Session::put('address', $address);
    }
    public function getVisitorLatLng()
    {
        $latlng['lat'] = Session::get('lat');
        $latlng['lng'] = Session::get('lng');
        $latlng['city'] = Session::get('city');
        return $latlng;
    }

    public function setVisitorLocation(Request $request)
    {
        $visitor_lat = $request->lat;
        $visitor_lng = $request->lng;
        $visitor_address = trim($request->address);
        if ($visitor_lat == '' && $visitor_lng == '' && $visitor_address == '') {
            $ip = request()->ip();
            if ($ip == '::1') {
                $ip = '203.78.146.6';
            }
            $getCity = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
            $visitor_lat = $getCity['lat'];
            $visitor_lng = $getCity['lon'];
            $visitor_address = $getCity['city'];
        }
        $this->setVisitorLatLng($visitor_lat, $visitor_lng, $visitor_address);
        return response()->json(['lat' => $visitor_lat, 'lng' => $visitor_lng, 'address' => $visitor_address]);
    }
    public function searchServiceTitle(Request $request)
    {
        $title = $request->keyword;
        $distance = $request->distance;;
        $lat = $request->lat;
        $lng = $request->lng;
        $address = $request->address;
        $call = 1;
        $services = $this->searchServiceTitle2($title, $distance, $lat, $lng, $call);

        $item = '';
        if (count($services) > 0) {
            foreach ($services as $key => $service) {
                $item .= '<div class="src-rslt-con result-item">' . $service->title . '</div>';
            }
        }
        return response()->json(['item' => $item]);
    }

    public function searchServiceTitle2($title, $distance, $lat, $lng, $call)
    {
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $services = Service::when($title != null, function ($q) use ($title) {
            return $q->where('title', 'LIKE', "%{$title}%");
        })
            ->where('services.active', 1)
            ->where('services.s_end_date', '>=', new Carbon(date('Y-m-d H:i:s')))
            ->when($lat != null && $lng != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("*, {$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->get();
        if (count($services) < 1) {
            if ($call == 1) {
                $distance = 100;
                $call++;
                return $this->searchServiceTitle2($title, $distance, $lat, $lng, $call);
            }
            if ($call == 2) {
                $call++;
                $lat = '';
                $lng = '';
                return $this->searchServiceTitle2($title, $distance, $lat, $lng, $call);
            } else {
                return $services;
            }
        } else {
            return $services;
        }
    }
    public function setSearchHistory(Request $request)
    {

        if ($request->key == 'city') {
            Session::put('lat', '');
            Session::put('lng', '');
        }
        Session::put($request->key, $request->value);
        return 'success';
    }
    public function resetSession()
    {
        Session::forget('lat');
        Session::forget('lng');
        Session::forget('address');
        Session::forget('h_distance');
        Session::forget('h_price');
        Session::forget('h_salons');
        Session::forget('h_personal');
        return back();
    }
    public function resetVisitorLocation(Request $request)
    {
        $lat = $request->lat;
        $lng = $request->lng;
        $address = $request->address;
        if ($lat == '' && $lng == '' && $address == '') {
            $ip = request()->ip();
            if ($ip == '::1') {
                $ip = '203.78.146.6';
            }
            $getCity = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
            $lat = $getCity['lat'];
            $lng = $getCity['lon'];
            $address = $getCity['city'];
        }
        Session::put('lat', $lat);
        Session::put('lng', $lng);
        Session::put('address', $address);
        Session::put('h_salons', 1);
        Session::put('h_personal', 1);
        Session::put('h_distance', 100);
        Session::put('h_price', '0,100');
    }
    public function index()
    {
        // $email = 'benny.kuhlman@example.com';
        // $userCheck = User::where(function($q) use($email) {
        //     $q->where('email', $email);
        //     $q->orWhere('phone', $email);
        // })
        // ->whereNotNull('sms_verified_at')
        // ->first();
        // dd($userCheck);

        $data['categories'] = Category::with('subCategories')->get()->toArray();
        $data['vipservices'] = VipService::limit(10)->get();
        $data['profiles'] = User::limit(10)->get();
        return view('index', $data);
    }
    public function blog(Request $request)
    {
        $item['salons'] = $request->salons;
        $item['personal'] = $request->personal;
        $item['distance'] = $request->distance;
        $item['price'] = $request->price;
        return $item['category'] = $request->category;
        $item['sub_category'] = $request->sub_category;
        $item['inner_category'] = $request->inner_category;
        $item['city'] = $request->city;
        $item['page'] = $request->page;
        //return view('pages.blog',compact('item'));
    }
    public function getInnerCategory($id)
    {
        return InnerCategory::find($id)->toArray();
    }
    public function searchForId($id, $array)
    {
        foreach ($array as $key => $val) {
            if ($val['id'] == $id) {
                return $val['id'];
            }
        }
        return 0;
    }
    public function aboutM($page_title = null, $id)
    {
        $data['page_title'] = $page_title;
        $data['comments'] = Comment::where('service_id', $id)->with('CommentReplay.user', 'user')->orderBy('id', 'DESC')->limit(2)->get()->toArray();
        if (Auth::check()) {
            $data['total_booking'] = Booking::where('service_id', $id)->where('confirm', 1)->where('user_id', auth()->user()->id)->count();
            $data['total_main_comment'] = Comment::where('service_id', $id)->where('commented_user', auth()->user()->id)->count();
        }
        $total_comment = Comment::where('service_id', $id)->count();
        $total_replay = CommentReplay::where('service_id', $id)->count();
        $data['total_comments'] = $total_comment + $total_replay;
        $data['total_main_comment'] = $total_comment;
        //dd($data['comments']);
        $data['service'] = Service::where('id', $id)
            ->with('user', 'user.userSetting')
            ->get();
        $main_user_id = $data['service'][0]->user->id;
        $services = Service::where('user_id', $data['service'][0]->user->id)
            ->with('category')
            ->get();
        $data['user_category'] = [];
        $n = 0;
        foreach ($services as $key => $val) {
            if ($val->category) {
                $ck = $this->searchForId($val->category->id, $data['user_category']);
                if (!$ck) {
                    $data['user_category'][$n]['id'] = $val->category->id;
                    $data['user_category'][$n]['sid'] = $val->id;
                    $data['user_category'][$n]['img'] = $val->category->img;
                    $data['user_category'][$n++]['url'] = route('aboutM', [MakeUrlService::url($val->title), $val->id]);
                }
            }
        }
        // dd($data['user_category']);
        $data['s_id'] = $id;
        $data['service'] = $data['service']->map(function ($service) {
            $arr = [];
            $inner_cat_id = json_decode($service->inner_category);
            if ($inner_cat_id) {
                foreach ($inner_cat_id as $key => $id) {
                    $arr[$key] = $this->getInnerCategory($id);
                }
            }
            $service['takeInnerCategory'] = $arr;
            return $service;
        })->first();

        $data['mainComment'] = Comment::where('service_id', $id)->withCount('CommentReplay')->get()->toArray();
        $data['replayComment'] = array_sum(array_column($data['mainComment'], 'comment_replay_count'));
        $data['total_comment'] = count($data['mainComment']) + $data['replayComment'];
        // $data['page_title'] = str_replace('-', ' ', $page_title);

        $this->saveRecentlyViewed($id);
        $data['c_user'] = (Auth::check()) ? User::find(Auth::user()->id) : 0;


        $allVip = [];
        $hasProfilePayment = app('App\Http\Controllers\SearchServiceController')->hasProfilePayment($main_user_id);
        if ($hasProfilePayment != 'yes') {
            $lat = Session::get('lat');
            $lng = Session::get('lng');
            $distance = Session::get('h_distance') ?: 10;
            $city = '';
            $serviceData = app('App\Http\Controllers\SearchServiceController')->takeVipServices(2, $lat, $lng, $distance, $city);

            $profileData = app('App\Http\Controllers\SearchServiceController')->takeVipProfile(2, $lat, $lng, $distance, $city);
            $allVip = $serviceData->merge($profileData);
            $allVip = $allVip->sortBy('distance');
        }


        return view('pages.about-model', $data, compact('allVip'));
    }

    public function ServiceComments($id)
    {
    }

    public function saveRecentlyViewed($id)
    {
        if (Session::has('recently_viewed')) {
            $viewed = Session::get('recently_viewed');
            if (!in_array($id, $viewed)) {
                array_push($viewed, $id);
                session()->put('recently_viewed', $viewed);
                session()->save();
            }
        } else {
            $viewed = array($id);
            session()->put('recently_viewed', $viewed);
            session()->save();
        }
    }
    // public function userProfile()
    // {
    //     $userInfo = User::find(Auth::user()->id);
    //     dd(userInfo);
    //     return view('pages.user-profile');
    // }
    public function selectPlan($id)
    {
        $package['package1'] = Package::where('type', 1)->with('duration')->first();
        $package['package2'] = Package::where('type', 2)->with('duration', 'star')->first();
        $package['package3'] = Package::where('type', 3)->with('duration')->first();
        //dd($package['package2']->toArray());
        $package['service'] = Service::find($id);
        return view('pages.select-plan', compact('package', 'id'));
    }

    public function publicProfile($page_title = null, $id, Request $request)
    {
        $data['id'] = $id;
        $data['gallery'] = Gallery::orderBy('id', 'DESC')
            ->where('user_id', $id)
            ->get();
        Session::put('pgallery_total', $data['gallery']->count());
        if (6 < $data['gallery']->count()) {
            $limit = 6;
            $data['s_m'] = 1;
        } else {
            $limit = $data['gallery']->count();
            $data['s_m'] = 0;
        }
        Session::put('pgallery_limit', $limit);
        $data['gallery'] = $data['gallery']->take($limit);
        $data['UserCoverA'] = UserCover::orderBy('id', 'DESC')
            ->where('user_id', $id)
            ->get();
        $data['UserCover'] = UserCover::orderBy('id', 'DESC')
            ->where('status', 1)
            ->where('user_id', $id)
            ->get();
        $data['services'] = Service::orderBy('id', 'DESC')
            ->where('user_id', $id)
            ->get();
        // dd($data['UserCoverA']);
        $data['userInfo'] = User::find($id);
        //dd($data['userInfo']->toArray());
        $show_name = json_decode($data['userInfo']->show_name);
        $makeName = '';
        foreach ($show_name as $name) {
            if ($name == 1) {
                $makeName .= $data['userInfo']->name . ' ';
            }
            if ($name == 2) {
                $makeName .= $data['userInfo']->surname . ' ';
            }
            if ($name == 3) {
                if ($makeName == '') {
                    $makeName = $data['userInfo']->nickname;
                } else {
                    $makeName .= '(' . $data['userInfo']->nickname . ')';
                }
            }
        }
        $data['makeName'] = trim($makeName);

        $data['title_name'] = str_replace('-', ' ', $page_title);

        $data['userInfo'] = User::where('id', $id)->with('ProfilePayment')->first();
        $data['profilePackages'] = VipPackage::all();

        $data['liveZones'] = LiveZone::where('user_id', $id)->orderBy('id', 'DESC')->get();
        $service['cities'] = District::all();

        $data['liveZones'] = LiveZone::with(['comments.commenter', 'comments.subcomments.commenter'])->orderBy('id', 'DESC')->where('user_id', $id)->paginate(5)->map(function ($query) {
            $query->setRelation('comments', $query->comments->take(2));
            $query->comments->map(function ($q) {
                $q->setRelation('subcomments', $q->subcomments->take(1));
                return $q;
            });
            return $query;
        });
        $view_live_zone = $request->live_zone;


        $allVip = [];
        $hasProfilePayment = app('App\Http\Controllers\SearchServiceController')->hasProfilePayment($id);
        if ($hasProfilePayment != 'yes') {
            $lat = Session::get('lat');
            $lng = Session::get('lng');
            $city = '';
            $distance = Session::get('h_distance') ?: 10;
            $serviceData = app('App\Http\Controllers\SearchServiceController')->takeVipServices(4, $lat, $lng, $distance, $city);

            $profileData = app('App\Http\Controllers\SearchServiceController')->takeVipProfile(4, $lat, $lng, $distance, $city);
            $allVip = $serviceData->merge($profileData);
            $allVip = $allVip->sortBy('distance');
        }

        return view('pages.public-profile', $data, compact('service', 'view_live_zone', 'allVip'));
    }
    public function publicService($id, $title, $s_id)
    {
        $data['id'] = $id;
        $data['s_id'] = $s_id;
        $data['service'] = Service::where('id', $s_id)
            ->get();
        $data['service'] = $data['service']->map(function ($service) {
            $arr = [];
            $inner_cat_id = json_decode($service->inner_category);
            if ($inner_cat_id) {
                foreach ($inner_cat_id as $key => $id) {
                    $arr[$key] = $this->getInnerCategory($id);
                }
            }
            $service['takeSubCategory'] = $arr;
            return $service;
        })->first();
        $data['service_glry_tab'] = ServiceGalleryTab::where('service_id', $s_id)->get();
        return view('pages.service', $data);
    }
    public function getServiceGlryTab(Request $req)
    {
        $srv_glry_tab = ServiceGalleryTab::where('service_id', $req->service_id)->get();
        return $srv_glry_tab;
    }
    public function test()
    {
        $earthRadiusMiles = 3959;
        $lat = '23.810332';
        $lng = '90.4125181';
        $distance = 5; //max distance in km


        $today = date('Y-m-d H:i:s');
        $top_services = \DB::table('services')->selectRaw("
            services.*,top_services.*, ( $earthRadiusMiles * acos( cos( radians($lat) ) * cos( radians( services.lat ) ) 
            * cos( radians( services.lng ) - radians($lng) ) + sin( radians($lat) ) *
            sin(radians(services.lat)) ) ) AS distance, lat, lng
        ")
            ->join('top_services', 'services.id', '=', 'top_services.service_id')
            ->where('services.city', 'dhaka')
            ->whereDate('top_services.end_date', '>=', $today)
            ->havingRaw("distance < $distance")
            ->inRandomOrder()
            ->limit(2)
            ->get();

        $top_services = $top_services->sortByDesc("star");
        //$services = $a->merge($b);

        //$services = $services->paginate(5);

        dd($top_services->toArray());
    }

    public function favarite(Request $request)
    {
        $item = '';
        if ($request->action == 'add') {
            if (Cookie::has('favarite')) {
                $item = Cookie::get('favarite');
                $item .= $request->id . ',';
                Cookie::queue('favarite', $item, 2147483647);
            } else {
                $item = $request->id . ',';
                Cookie::queue('favarite', $item, 2147483647);
            }
        } else {
            $item = Cookie::get('favarite');
            $remove_item = $request->id . ',';
            $item = str_replace($remove_item, '', $item);
            Cookie::queue('favarite', $item, 2147483647);
        }

        return Cookie::get('favarite');
    }

    public function getFavarite()
    {

        $get_favarite = Cookie::get('favarite');
        $favarite = array();
        if ($get_favarite) {
            $favarite = explode(',', $get_favarite);
            $favarite = array_filter($favarite);
            $favarite = array_reverse($favarite);
        }
        //dd($favarite);
        $ids_ordered = implode(',', $favarite);
        $lat = Session::get('lat');
        $lng = Session::get('lng');
        $distance = 10000;
        $today = date('Y-m-d H:i:s');
        $selectDistance =
            '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
            '* cos( radians( lat ) ) ' .
            '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
            '+ sin( radians(' . $lat . ') ) ' .
            '* sin( radians( lat ) ) ) )';

        $services = Service::with([
            'types', 'user', 'topService', 'TakeVipService' => function ($r) use ($today) {
                $r->whereDate('end_date', '>=', $today);
            },
            "HighlightService" => function ($t) use ($today) {
                $t->whereDate('end_date', '>=', $today);
            }
        ])
            ->selectRaw("services.*")
            ->whereIn('id', $favarite)
            ->when($lat != null, function ($q) use ($lat, $lng, $selectDistance, $distance) {
                return $q->selectRaw("{$selectDistance} AS distance")
                    ->whereRaw("{$selectDistance} < ?", $distance);
            })
            ->when($ids_ordered, function ($q) use ($ids_ordered) {
                return $q->orderByRaw("FIELD(id, $ids_ordered)");
            })

            ->get();
        //dd($services);
        $take_service = 0;
        $take_profile = 0;
        if (count($services) > 0) {
            $divider = count($services) / 2;
            $take_service = ceil($divider);
            $take_profile = ceil($divider);
        }
        $lat = Session::get('lat');
        $lng = Session::get('lng');
        $distance = Session::get('h_distance');
        if ($distance == '') {
            $distance = 100;
        }

        $city = '';
        $serviceData = app('App\Http\Controllers\SearchServiceController')->takeVipServices($take_service, $lat, $lng, $distance, $city);

        $profileData = app('App\Http\Controllers\SearchServiceController')->takeVipProfile($take_profile, $lat, $lng, $distance, $city);

        $allVip = $serviceData->merge($profileData);
        $allVip = $allVip->sortBy('distance');
        $item['recently_viewed'] = [];
        if (Session::has('recently_viewed')) {
            $item['recently_viewed'] = Service::whereIn('id', Session::get('recently_viewed'))->get();
        }



        // $service['cities'] = District::all();
        // $item['recently_viewed'] = [];
        // if (Session::has('recently_viewed')) {
        //     $item['recently_viewed'] = Service::whereIn('id', Session::get('recently_viewed'))->get();
        // }
        // $get_favarite = Cookie::get('favarite');
        // $favarite = array();
        // if ($get_favarite) {
        //     $favarite = explode(',', $get_favarite);
        //     $favarite = array_filter($favarite);
        //     $favarite = array_reverse($favarite);
        // }

        // $services = Service::whereIn('id', $favarite)->with('top_services', 'user')->paginate(15);
        // // dd($services->toArray());
        // $total_services = count($services);
        // $take_service = 0;
        // $take_profile = 0;
        // if ($total_services > 0) {
        //     $divider = $total_services / 2;
        //     $take_service = $divider;
        //     $take_profile = $divider;

        //     if (strpos($divider, '.') !== false) {
        //         $take_service = $take_service + 0.5;
        //         $take_profile = $take_profile - 0.5;
        //     }
        // }
        // $lat = Session::get('lat');
        // $lng = Session::get('lng');
        // $distance = Session::get('h_distance');
        // $city = Session::get('city');
        // if ($distance == '') {
        //     $distance = 100;
        // }
        // $serviceData = app('App\Http\Controllers\SearchServiceController')->takeVipServices($take_service, $lat, $lng, $distance, $city);

        // $profileData = app('App\Http\Controllers\SearchServiceController')->takeVipProfile($take_profile, $lat, $lng, $distance, $city);

        // $allVip = $serviceData->merge($profileData);
        // $allVip = $allVip->sortBy('distance');



        return view('pages.favarite', compact('item', 'services', 'allVip'));
    }

    public function recentlyView(Request $request)
    {
        $item = '';

        if (Cookie::has('recently')) {
            $item = Cookie::get('recently');
            $number = $request->id . ',';
            if (strpos($item, $number) === false) {
                $item .= $request->id . ',';
            }
            Cookie::queue('recently', $item, 2147483647);
        } else {
            $item = $request->id . ',';
            Cookie::queue('recently', $item, 2147483647);
        }
    }
    public function seeMoreGallery(Request $req)
    {
        $gallery = Gallery::orderBy('id', 'DESC')
            ->where('user_id', $req->id)
            ->get();
        Session::put('pgallery_total', $gallery->count());
        $limit_c = Session::get('pgallery_limit');
        if ($limit_c == 0) {
            $limit_c = 6;
        }
        if ($limit_c + 6 < $gallery->count()) {
            $limit = $limit_c + 6;
            $data['s_m'] = 1;
        } else {
            $limit = $gallery->count();
            $data['s_m'] = 0;
        }
        Session::put('pgallery_limit', $limit);
        $gallery = $gallery->take($limit);
        $data['html'] = '';
        foreach ($gallery as $gallery) {
            $data['html'] .= '<div class="col-6 pad-0">
            <span class="fa fa-times remveImg" onclick="removeImg(event, \'' . $gallery->id . '\')"></span>
            <a href="' . asset('gallery/' . $gallery->picture) . '" data-fancybox="gallery">
              <div class="single-content">
                <div class="content-img-p">
                  <div class="content-img">
                    <img src="' . asset('gallery/re_' . $gallery->picture) . '" alt="img">
                  </div>
                </div>
              </div>
            </a>
          </div>';
        }
        return $data;
    }
    public function termsConditions()
    {
        return view('pages.terms-and-conditions');
    }
    public function privacyPolicy()
    {
        return view('pages.privacy-policy');
    }
    public function errors()
    {
        return view('errors.404');
    }
}
