<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\CommentReplay;
class CommentController extends Controller
{
    public function addComment(Request $request)
    {
    	$Comment = new Comment();
    	$Comment->service_id = $request->service_id;
    	$Comment->comment = $request->comment;
    	$Comment->commented_user = Auth()->user()->id;
    	$Comment->save();
    	return $this->getInitialComment($Comment->id);
	}

	public function getInitialComment($id)
	{
		$comment = Comment::where('id',$id)->with('user')->first()->toArray();
		return view('pages.comment',compact('comment'));

	}
	public function replayMainComment(Request $request)
	{
		$comment_id = $request->comment_id;
		$exp = explode('-',$comment_id);
		$id = end($exp);
		$CommentReplay = new CommentReplay();
		$CommentReplay->replay = $request->comment;
		$CommentReplay->comment_id = $id;
		$CommentReplay->replay_user_id = Auth()->user()->id;
		$CommentReplay->save();

		$replay = CommentReplay::where('id',$CommentReplay->id)->with('user')->first()->toArray();
		return view('pages.replay-comment',compact('replay'));

	}
}
