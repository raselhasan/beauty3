<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\Category;
use App\InnerCategory;
use App\SubCategory;
use App\ServiceGallery;
use App\ServiceGalleryTab;
use App\Services\MakeUrlService;
use Auth;
use Session;

class ServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function getInnerCategory($id)
    {
        return InnerCategory::find($id)->toArray();
    }
    public function index(Request $req, $title, $id)
    {
        $folder_path = 'assets/img/services/tmpImg/' . Auth::user()->id;
        $files = glob($folder_path . '/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
        Session::forget('service_img');
        $folder_path = 'assets/img/services/tmpSrvcGlryImg/' . Auth::user()->id;
        $files = glob($folder_path . '/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
        Session::forget('service_gallery_img');
        $data['service'] = Service::where('id', $id)
            ->with('user', 'user.userSetting')
            ->get();
        $data['s_id'] = $id;
        $data['service'] = $data['service']->map(function ($service) {
            $arr = [];
            $inner_cat_id = json_decode($service->inner_category);
            if ($inner_cat_id) {
                foreach ($inner_cat_id as $key => $id) {
                    $arr[$key] = $this->getInnerCategory($id);
                }
            }
            $service['takeInnerCategory'] = $arr;
            return $service;
        })->first();
        $data['categories'] = Category::all();
        $data['sub_categories'] = SubCategory::where('category_id', $data['service']->category_id)->get();
        $data['inner_categories'] = InnerCategory::where('category_id', $data['service']->category_id)
            ->where('sub_category_id', $data['service']->sub_category_id)
            ->get();
        $data['service_glry_tab'] = ServiceGalleryTab::where('service_id', $id)->get();
        // dd($data['service']->user);
        $data['title'] = $title;

        $allVip = [];
        $hasProfilePayment = app('App\Http\Controllers\SearchServiceController')->hasProfilePayment(Auth::user()->id);
        if ($hasProfilePayment != 'yes') {
            $lat = Session::get('lat');
            $lng = Session::get('lng');
            $distance = Session::get('h_distance') ?: 10;
            $city = '';
            $serviceData = app('App\Http\Controllers\SearchServiceController')->takeVipServices(2, $lat, $lng, $distance, $city);

            $profileData = app('App\Http\Controllers\SearchServiceController')->takeVipProfile(2, $lat, $lng, $distance, $city);
            $allVip = $serviceData->merge($profileData);
            $allVip = $allVip->sortBy('distance');
        }
        return view('pages.my-service', $data, compact('allVip'));
    }
    public function add()
    {
        $service = new Service;
        $service->user_id = Auth::user()->id;
        $service->title = 'new-service';
        $service->save();
        return redirect()->route('user.myService', [MakeUrlService::url($service->title), $service->id]);
    }
    public function changeServiceDetails(Request $req)
    {
        $service = Service::find($req->id);
        $service->title = $req->title;
        $service->description = $req->description;
        $service->save();

        return $service;
    }
    public function serviceTmpImg(Request $request)
    {
        $file = $request->file('avatar');
        if (Session::has('service_img')) {
            $name = Session::get('service_img');
        } else {
            $name = 'profile_pic' . time() . '.jpg';
        }
        Session::put('service_img', $name);
        $file->move('assets/img/services/tmpImg/' . Auth::user()->id, $name);
        $image = Session::get('service_img');
        $html = '';
        $html .= '<span class="pip">';
        $html .= '<img class="imageThumb" src="' . asset('assets/img/services/tmpImg/' . Auth::user()->id . '/' . $image) . '">';
        $html .= '<br/>';
        $html .= '<span class="service-remove" id="' . $image . '">✖</span>';
        return $html;
    }
    public function updateServiceImg(Request $req)
    {
        $service_pic = Service::find($req->id)->image;
        if ($service_pic) {
            $path =  'assets/img/services/' . $service_pic;
            if (file_exists($path)) {
                unlink($path);
            }
        }
        if (Session::has('service_img')) {
            $img = Session::get('service_img');
        } else {
            $img = '';
        }
        Session::forget('service_img');
        if ($img) {
            $path =  'assets/img/services/tmpImg/' . Auth::user()->id . '/' . $img;
            $npath = 'assets/img/services/' . $img;
            copy($path, $npath);
            unlink($path);
        }

        Service::where('id', $req->id)->update([
            'image' => $img
        ]);
        return back();
    }
    public function serviceTmpImgRemove(Request $req)
    {
        $image_name = $req->img_name;
        Session::forget('service_img');
        $path = $path = 'assets/img/services/tmpImg/' . Auth::user()->id . '/' . $image_name;
        unlink($path);
    }
    public function changeServiceSubCategory(Request $req)
    {
        $sub_categories = SubCategory::where('category_id', $req->cat_id)->get();
        $html = '<div class="ctm-option noselect">' . trans('lang.choose') . '</div>';
        foreach ($sub_categories as $sub_cat) {
            $html .= '<div class="ctm-option noselect" ctm-otn-v="' . $sub_cat->id . '">' . $sub_cat->name_lt . '</div>';
        }
        return $html;
    }
    public function changeServiceInnerCategory(Request $req)
    {
        $inner_categories = InnerCategory::where('sub_category_id', $req->sub_cat_id)->get();
        $html = '';
        foreach ($inner_categories as $inner_cat) {
            $html .= '<div class="ctm-option-ch">
            <label class="ctm-con mar-0 noselect">' . $inner_cat->name_lt . '
            <input type="checkbox" class="srvc-inner-cat" name="srvc_inner_cat[]" value="' . $inner_cat->id . '">
            <span class="checkmark"></span>
            </label>
            </div>';
        }
        return $html;
    }
    public function saveServiceCategory(Request $req)
    {
        // return $req->all();
        $service = Service::find($req->id);
        $service->category_id = $req->cat_id;
        $service->city = $req->city;
        $service->sub_category_id = $req->sub_cat_id;
        $service->inner_category = json_encode($req->inner_cat_id);
        $service->save();
    }
    public function saveServiceGlryTab(Request $req)
    {
        $srv_glry_tab = new ServiceGalleryTab;
        $srv_glry_tab->service_id = $req->service_id;
        $srv_glry_tab->tab = $req->tab;
        $srv_glry_tab->save();
        $srv_glry_tabs = ServiceGalleryTab::where('service_id', $req->service_id)->get();

        $html = '<div class="ctm-option noselect">choose...</div>';
        foreach ($srv_glry_tabs as $tab) {
            $html .= '<div ctm-otn-v="' . $tab->id . '" class="ctm-option noselect">' . $tab->tab . '</div>';
        }
        $data['html'] = $html;
        $data['tab'] = $req->tab;
        return $data;
    }
    public function delServiceGlryTab(Request $request)
    {
        $tab_del = ServiceGalleryTab::find($request->id);
        $tab_name = $tab_del->tab;
        foreach ($tab_del->serviceGalleries as $gm) {
            $path =  'assets/img/services/galleries/' . $gm->img;
            if (file_exists($path)) {
                unlink($path);
            }
        }
        $tab_del->serviceGalleries()->delete();
        $tab_del->delete();
        $srv_glry_tabs = ServiceGalleryTab::where('service_id', $request->service_id)->get();

        $html = '<div class="ctm-option noselect">choose...</div>';
        foreach ($srv_glry_tabs as $tab) {
            $html .= '<div ctm-otn-v="' . $tab->id . '" class="ctm-option noselect">' . $tab->tab . '</div>';
        }
        $data['html'] = $html;
        $data['tab'] = $tab_name;
        $service = Service::where('id', $request->service_id)
            ->with('user', 'user.userSetting')
            ->get();
        $service = $service->map(function ($service) {
            $arr = [];
            $inner_cat_id = json_decode($service->inner_category);
            if ($inner_cat_id) {
                foreach ($inner_cat_id as $key => $id) {
                    $arr[$key] = $this->getInnerCategory($id);
                }
            }
            $service['takeInnerCategory'] = $arr;
            return $service;
        })->first();
        $data['view'] = view('pages.myServiceInc.gallery-area', compact('service'))->render();
        return $data;
    }
    public function serviceGalleryImgDelete(Request $request)
    {
        $gi = ServiceGallery::find($request->id);
        if ($gi->img) {
            $path =  'assets/img/services/galleries/' . $gi->img;
            if (file_exists($path)) {
                unlink($path);
            }
        }
        $gi->delete();
        return 'true';
    }
    public function updateServiceGlryTab(Request $request)
    {
        $tab_del = ServiceGalleryTab::find($request->id);
        $tab_name = $tab_del->tab;
        $tab_del->tab = $request->txt;
        $tab_del->save();
        $srv_glry_tabs = ServiceGalleryTab::where('service_id', $request->service_id)->get();

        $html = '<div class="ctm-option noselect">choose...</div>';
        foreach ($srv_glry_tabs as $tab) {
            $html .= '<div ctm-otn-v="' . $tab->id . '" class="ctm-option noselect">' . $tab->tab . '</div>';
        }
        $data['html'] = $html;
        $data['tab'] = $tab_name;
        $data['ntab'] = $request->txt;
        return $data;
    }
    public function serviceGalleryImgUpload(Request $request)
    {
        $file = $request->file('avatar');
        if (Session::has('service_gallery_img')) {
            $name = Session::get('service_gallery_img');
        } else {
            $name = 'srvc_glry' . time() . '.jpg';
        }
        Session::put('service_gallery_img', $name);
        $file->move('assets/img/services/tmpSrvcGlryImg/' . Auth::user()->id, $name);
        $image = Session::get('service_gallery_img');
        $html = '';
        $html .= '<span class="pip">';
        $html .= '<img class="imageThumb" src="' . asset('assets/img/services/tmpSrvcGlryImg/' . Auth::user()->id . '/' . $image) . '">';
        $html .= '<br/>';
        $html .= '<span class="service-gallery-remove" id="' . $image . '">✖</span>';
        return $html;
    }
    public function serviceGalleryTmpImgRemove(Request $req)
    {
        $image_name = $req->img_name;
        Session::forget('service_gallery_img');
        $path = $path = 'assets/img/services/tmpSrvcGlryImg/' . Auth::user()->id . '/' . $image_name;
        unlink($path);
    }
    public function addServiceGalleryImg(Request $req)
    {
        if (Session::has('service_gallery_img')) {
            $img = Session::get('service_gallery_img');
        } else {
            $img = '';
        }
        Session::forget('service_gallery_img');
        if ($img) {
            $path =  'assets/img/services/tmpSrvcGlryImg/' . Auth::user()->id . '/' . $img;
            $npath = 'assets/img/services/galleries/' . $img;
            copy($path, $npath);
            unlink($path);
        }

        $service_gallery = new ServiceGallery;
        $service_gallery->service_gallery_tab_id = $req->id;
        $service_gallery->img = $img;
        $service_gallery->save();

        $service_gallery_tab = ServiceGalleryTab::find($req->id)->tab;
        $data = [];
        $data['path'] = $npath;
        $data['tab'] = $service_gallery_tab;
        $data['gid'] = $service_gallery->id;
        return $data;
    }

    public function saveServiceAddress(Request $request)
    {
        $service = Service::find($request->service_id);
        $service->address = $request->address;
        $service->lat = $request->lat;
        $service->lng = $request->lng;
        $city = '';
        if ($request->city) {
            $city = str_replace("District", "", $request->city);
        }
        $service->city = $city;
        $service->save();
    }
}
