<?php

namespace App\Http\Controllers\User;

use Auth;
use Session;
use App\Type;
use App\District;
use App\User;
use App\Booking;
use App\BreakHour;
use App\BusinessHour;
use App\Gallery;
use App\Service;
use App\LiveZone;
use App\UserCover;
use Carbon\Carbon;
use App\VipPackage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RunningHour;
use Mail;
use App\Mail\EmailVerification;
use App\AddShowingPayment;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index()
    {
        $folder_path = 'userImage/tmpPic/' . Auth::user()->id;
        $files = glob($folder_path . '/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
        Session::forget('user_profile_img');

        $data['gallery'] = Gallery::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        Session::put('pgallery_total', $data['gallery']->count());
        if (6 < $data['gallery']->count()) {
            $limit = 6;
            $data['s_m'] = 1;
        } else {
            $limit = $data['gallery']->count();
            $data['s_m'] = 0;
        }
        Session::put('pgallery_limit', $limit);
        $data['gallery'] = $data['gallery']->take($limit);
        $data['UserCoverA'] = UserCover::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        $data['UserCover'] = UserCover::orderBy('id', 'DESC')
            ->where('status', 1)
            ->where('user_id', Auth::user()->id)
            ->get();
        $data['services'] = Service::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        // dd($data['UserCoverA']);
        $data['userInfo'] = User::find(Auth()->user()->id);
        //dd($data['userInfo']->toArray());
        $show_name = json_decode($data['userInfo']->show_name);

        $makeName = '';
        if ($show_name) {
            foreach ($show_name as $name) {
                if ($name == 1) {
                    $makeName .= $data['userInfo']->name . ' ';
                }
                if ($name == 2) {
                    $makeName .= $data['userInfo']->surname . ' ';
                }
                if ($name == 3) {
                    if ($makeName == '') {
                        $makeName = $data['userInfo']->nickname;
                    } else {
                        $makeName .= '(' . $data['userInfo']->nickname . ')';
                    }
                }
            }
        } else {
            $makeName .= $data['userInfo']->name . ' ';
        }
        $data['makeName'] = trim($makeName);
        //$data['userInfo'] = User::where('id', Auth()->user()->id)->with('ProfilePayment')->first()->toArray();
        $today = date('Y-m-d H:i:s');
        $data['userInfo'] = User::with(['ProfilePayment' => function ($q) use ($today) {
            $q->whereDate('end_date', '>=', $today);
        }])
            ->selectRaw("users.*")
            ->where('id', Auth()->user()->id)
            ->first()->toArray();
        //dd($data['userInfo']);

        $data['profilePackages'] = VipPackage::all();
        $data['liveZones'] = LiveZone::where('user_id', Auth()->user()->id)->orderBy('id', 'DESC')->get();
        // $data['liveZones'] = [];
        $service['cities'] = District::all();


        $data['liveZones'] = LiveZone::with(['comments.commenter', 'comments.subcomments.commenter'])->orderBy('id', 'DESC')->where('user_id', auth()->user()->id)->paginate(5)->map(function ($query) {
            $query->setRelation('comments', $query->comments->take(2));
            $query->comments->map(function ($q) {
                $q->setRelation('subcomments', $q->subcomments->take(1));
                return $q;
            });
            return $query;
        });

        $allVip = [];
        $hasProfilePayment = app('App\Http\Controllers\SearchServiceController')->hasProfilePayment(Auth::user()->id);
        if ($hasProfilePayment != 'yes') {
            $lat = Session::get('lat');
            $lng = Session::get('lng');
            $distance = Session::get('h_distance') ?: 10;
            $city = '';
            $serviceData = app('App\Http\Controllers\SearchServiceController')->takeVipServices(4, $lat, $lng, $distance, $city);

            $profileData = app('App\Http\Controllers\SearchServiceController')->takeVipProfile(4, $lat, $lng, $distance, $city);
            $allVip = $serviceData->merge($profileData);
            $allVip = $allVip->sortBy('distance');
        }

        return view('pages.user-profile', $data, compact('service', 'allVip'));
    }

    public function galleryFileUp(Request $request)
    {
        $html = '';
        for ($x = 0; $x < $request->total; $x++) {

            if ($request->hasFile('pic' . $x)) {
                $file = $request->file('pic' . $x);
                $img_i = getimagesize($file);
                if ($img_i[0] > 400) {
                    $width = 400;
                    $height = ($img_i[1] * 400) / $img_i[0];
                } else {
                    $width = $img_i[0];
                    $height = $img_i[1];
                }
                $file_name = time() . '_' . $x . '.' . $file->getClientOriginalExtension();
                $file->move('gallery', $file_name);
                $imager = new \Imager('gallery/' . $file_name);
                $imager->resize($width, $height)->write('gallery/re_' . $file_name);
                $dataG  = new Gallery;
                $dataG->picture = $file_name;
                $dataG->user_id = Auth::user()->id;
                $dataG->save();
            }
        }
        $gallery = Gallery::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        Session::put('pgallery_total', $gallery->count());
        $limit_c = Session::get('pgallery_limit');
        if ($limit_c < $gallery->count()) {
            $limit = $limit_c;
            $data['s_m'] = 1;
        } else {
            $limit = $gallery->count();
            $data['s_m'] = 0;
        }
        Session::put('pgallery_limit', $limit);
        $gallery = $gallery->take($limit);
        $data['html'] = '';
        foreach ($gallery as $gallery) {
            $data['html'] .= '<div class="col-6 pad-0">
            <span class="fa fa-times remveImg" onclick="removeImg(event, \'' . $gallery->id . '\')"></span>
            <a href="' . asset('gallery/' . $gallery->picture) . '" data-fancybox="gallery">
              <div class="single-content">
                <div class="content-img-p">
                  <div class="content-img">
                    <img src="' . asset('gallery/re_' . $gallery->picture) . '" alt="img">
                  </div>
                </div>
              </div>
            </a>
          </div>';
        }
        return $data;
    }
    public function galleryRemover(Request $request)
    {
        $gallery = Gallery::find($request->id);
        $path = 'gallery/' . $gallery->picture;
        $path2 = 'gallery/re_' . $gallery->picture;
        if ($gallery->picture) {
            if (file_exists($path)) {
                unlink($path);
            }
            if (file_exists($path2)) {
                unlink($path2);
            }
        }
        $gallery->delete();
        $gallery = Gallery::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        Session::put('pgallery_total', $gallery->count());
        $limit_c = Session::get('pgallery_limit');
        if ($limit_c < $gallery->count()) {
            $limit = $limit_c;
            $data['s_m'] = 1;
        } else {
            $limit = $gallery->count();
            $data['s_m'] = 0;
        }
        Session::put('pgallery_limit', $limit);
        $gallery = $gallery->take($limit);
        $data['html'] = '';
        foreach ($gallery as $gallery) {
            $data['html'] .= '<div class="col-6 pad-0">
            <span class="fa fa-times remveImg" onclick="removeImg(event, \'' . $gallery->id . '\')"></span>
            <a href="' . asset('gallery/' . $gallery->picture) . '" data-fancybox="gallery">
              <div class="single-content">
                <div class="content-img-p">
                  <div class="content-img">
                    <img src="' . asset('gallery/re_' . $gallery->picture) . '" alt="img">
                  </div>
                </div>
              </div>
            </a>
          </div>';
        }
        return $data;
    }

    public function setProfilePic(Request $request)
    {
        $file = $request->file('avatar');
        if (Session::has('user_profile_img')) {
            $name = Session::get('user_profile_img');
        } else {
            $name = 'profile_pic' . time() . '.jpg';
        }
        Session::put('user_profile_img', $name);
        $file->move('userImage/tmpPic/' . Auth::user()->id, $name);
        $image = Session::get('user_profile_img');
        $html = '';
        $html .= '<span class="pip">';
        $html .= '<img class="imageThumb" src="' . asset('userImage/tmpPic/' . Auth::user()->id . '/' . $image) . '">';
        $html .= '<br/>';
        $html .= '<span class="remove" id="' . $image . '">✖</span>';
        return $html;
    }

    public function removeProfilePic(Request $request)
    {
        $image_name = $request->img_name;
        Session::forget('user_profile_img');
        $path = $path = 'userImage/tmpPic/' . Auth::user()->id . '/' . $image_name;
        unlink($path);
    }

    public function upPrPic(Request $request)
    {
        $user_pic = User::find(Auth::user()->id)->profile_image;
        if ($user_pic) {
            $path =  'userImage/fixPic/' . $user_pic;
            if (file_exists($path)) {
                unlink($path);
            }
        }
        if (Session::has('user_profile_img')) {
            $img = Session::get('user_profile_img');
        } else {
            $img = '';
        }
        Session::forget('user_profile_img');
        if ($img) {
            $path =  'userImage/tmpPic/' . Auth::user()->id . '/' . $img;
            $npath = 'userImage/fixPic/' . $img;
            copy($path, $npath);
            unlink($path);
        }

        User::where('id', Auth::user()->id)->update([
            'profile_image' => $img
        ]);
        return back();
    }
    public function coverImgUpload(Request $req)
    {
        $file = $req->file('avatar');
        $name = 'cover_img' . time() . '.jpg';
        $file->move('userImage/coverPic', $name);

        $user_cover = new UserCover;
        $user_cover->user_id = Auth::user()->id;
        $user_cover->img = $name;
        $user_cover->save();

        $html = '<div class="col-6 col-sm-3 pad-l-0">
        <span  class="rmv-p" onclick="deleteCoverPic(event, ' . $user_cover->id . ')">&#10006;</span>
        <label class="image-checkbox">
        <img class="cover" src="' . asset('userImage/coverPic/' . $name) . '" />
        <input type="checkbox" name="cover_img_d[]" value="' . $user_cover->id . '" />
        <i class="fa fa-check d-none"></i>
        </label>
        </div>';
        return $html;
    }
    public function coverImgUpdate(Request $req)
    {
        // dd($req->all());
        $user_cover = UserCover::where('status', 1)
            ->update([
                'status' => 0
            ]);
        $user_cover_u = UserCover::whereIn('id', $req->cover_img_d)
            ->update([
                'status' => 1
            ]);
        return back();
    }
    public function userCoverRemover(Request $request)
    {
        $user_cover = UserCover::find($request->id);
        $path = 'userImage/coverPic/' . $user_cover->img;
        if ($user_cover->img) {
            if (file_exists($path)) {
                unlink($path);
            }
        }
        $user_cover->delete();
    }
    public function getProfileInfo(Request $req)
    {
        $userD = User::find(Auth::user()->id);
        return $userD;
    }
    public function insertProfileInfo(Request $req)
    {
        // return $req->all();
        $user = User::find(Auth::user()->id);
        $user->name = $req->name;
        $user->surname = $req->surname;
        $user->nickname = $req->nickname;
        $user->email = $req->email;
        $user->phone = $req->phone;
        $user->address = $req->address;
        $user->facebook = $req->facebook;
        $user->twitter = $req->twitter;
        $user->gmail = $req->gmail;
        $user->linkedin = $req->linkedin;
        $user->pinterest = $req->pinterest;
        $user->user_lat = $req->lat;
        $user->user_lng = $req->long;
        $user->user_city = trim($req->user_city);
        $user->bio = $req->bio;
        $user->show_name = json_encode($req->show_name, true);
        $user->account_type = $req->account_type;
        $user->save();
        return $user;
    }

    public function addLiveZone(Request $request)
    {
        $liveZone = new LiveZone();
        $liveZone->user_id = Auth()->user()->id;
        $liveZone->title = $request->live_zone_title;
        $liveZone->description = $request->live_zone_des;
        $status = $request->live_zone_status;

        if ($status == 1) {
            if ($request->hasFile('live_zone_img')) {
                $file = $request->file('live_zone_img');
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $file->move('liveZone', $file_name);
                $liveZone->image = $file_name;
                $liveZone->status = 1;
            }
        } else if ($status == 2) {
            $liveZone->image = $request->live_zone_vido_link;
            $liveZone->status = 2;
        } else {
            $liveZone->status = 0;
        }
        $liveZone->save();
        $liveZone = LiveZone::find($liveZone->id);
        return view('pages.live-zone-data', compact('liveZone'));
    }

    public function editLiveZone(Request $request)
    {
        $liveZone = LiveZone::find($request->id);
        return $liveZone;
    }
    public function liveZoneDelete(Request $request)
    {
        $live_zone = LiveZone::find($request->id);
        if ($live_zone->status == 1) {
            \File::delete('liveZone/' . $live_zone->image);
        }
        $live_zone->delete();
        return 'success';
    }

    public function liveZoneUpdate(Request $request)
    {
        $liveZone = LiveZone::find($request->u_livezone_id);
        if ($request->u_des) {
            $liveZone->description = $request->des1;
        }
        $status = $request->u_livezone_status;

        if ($status == 1) {
            if ($request->hasFile('u_image')) {
                $file = $request->file('u_image');
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $file->move('liveZone', $file_name);
                $liveZone->image = $file_name;
                $liveZone->status = 1;
            }
        } else if ($status == 2) {
            if ($request->u_y_link) {
                $exp = explode('=', $request->u_y_link);
                $video_link = $exp[1];
                $liveZone->image = $video_link;
                $liveZone->status = 2;
            }
        }
        $liveZone->save();
        $livezone = LiveZone::find($request->u_livezone_id);


        $l_des = $livezone->description;

        $l_img = '';
        if ($livezone->status == 1) {
            $img = url('liveZone/' . $livezone->image);
            $l_img .= ' <div class="content-img-v-cnt ">
                   <div class="content-img-p ">
                    <div class="content-img">
                      <img id="lz-img-1" src="' . $img . '" onclick="openLiveeZonePopUp(' . $livezone->id . ')">
                    </div>
                  </div>
                </div>';
        }
        if ($livezone->status == 2) {
            $img = 'https://www.youtube.com/embed/' . $livezone->image;
            $l_img .= '<div class="content-img-v-cnt">
                  <div class="content-img-p video ">
                    <div class="content-img">
                      <iframe id="lz-video-1" src="' . $img . '">
                      </iframe>
                    </div>
                  </div>
                </div>';
        }
        return response()->json(['des' => $l_des, 'img' => $l_img, 'id' => $livezone->id]);
    }
    public function bookingList()
    {
        $data['booking'] = Booking::where('user_id', Auth::user()->id)->orderBy('id',  'DESC')->get();
        // dd($data['booking']->toArray());
        $service['cities'] = District::all();
        return view('pages.booking-list', $data, compact('service'));
    }
    public function cancledBooking(Service $service, Type $type, Booking $booking)
    {
        $booking->cancle = 1;
        $booking->confirm = 0;
        $booking->save();
        $bookingAll = $type->booking()->with(['user', 'worker'])->get();
        return redirect()->back();
    }
    public function cancledBook(Request $request)
    {
        $booking = Booking::find($request->id);
        $booking->cancle = 1;
        $booking->confirm = 0;
        $booking->save();
        $data['booking'] = Booking::where('user_id', $request->user_id)->orderBy('id',  'DESC')->get();
        return view('pages.booking-list-data', $data)->render();
    }
    public function updateBusinessHour(Request $request)
    {
        if (!BusinessHour::where('user_id', Auth::user()->id)->exists()) {
            $bh = BusinessHour::create([
                'user_id' => Auth::user()->id
            ]);
        } else {
            $bh = BusinessHour::where('user_id', Auth::user()->id)->first();
        }
        foreach ($request->business_hour as $key => $value) {
            foreach ($value as $k1 => $v1) {
                if ($v1[0] == '0') {
                    $start = null;
                } else {
                    $start = $v1[0];
                }
                if ($v1[1] == '0') {
                    $end = null;
                } else {
                    $end = $v1[1];
                }
                if ($k1 == 0) {
                    // return $request->business_hour;
                    if (RunningHour::where('business_hour_id', $bh->id)->where('day_no', $key)->exists()) {
                        RunningHour::where('business_hour_id', $bh->id)
                            ->where('day_no', $key)
                            ->update([
                                'start' => $start,
                                'end' => $end,
                            ]);
                    } else {
                        RunningHour::create([
                            'business_hour_id' => $bh->id,
                            'day_no' => $key,
                            'start' => $start,
                            'end' => $end,
                        ]);
                    }
                } else {
                    if (BreakHour::where('business_hour_id', $bh->id)->where('day_no', $key)->exists()) {
                        BreakHour::where('business_hour_id', $bh->id)
                            ->where('day_no', $key)
                            ->update([
                                'start' => $start,
                                'end' => $end,
                            ]);
                    } else {
                        BreakHour::create([
                            'business_hour_id' => $bh->id,
                            'day_no' => $key,
                            'start' => $start,
                            'end' => $end,
                        ]);
                    }
                }
            }
        }
        $html = '<div class="pro-m-time1 pro-t">';
        foreach (Auth::user()->businessHours()->first()->runningHours()->orderBy('day_no', 'asc')->get() as $hour) {
            $html .= '<div><img src="' . asset('assets/img/services/cal.png') . '" alt="img">' . (($hour->start) ? Carbon::parse($hour->start)->format('H:i') : '') . ' - ' . (($hour->end) ? Carbon::parse($hour->end)->format('H:i') : '') . '</div>';
        }
        $html .= '</div>' .
            '<div class="pro-m-time2 pro-t">';
        foreach (Auth::user()->businessHours()->first()->breakHours()->orderBy('day_no', 'asc')->get() as $hour) {
            $html .= '<div><img src="' . asset('assets/img/services/break-i.png') . '" alt="img">' . (($hour->start) ? Carbon::parse($hour->start)->format('H:i') : '') . ' - ' . (($hour->end) ? Carbon::parse($hour->end)->format('H:i') : '') . '</div>';
        }
        $html .= '</div>';
        return $html;
    }
    public function sendUserVcode(Request $request)
    {
        $digits = 5;
        // $email_code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $email_code = 12345;
        $data['name'] = auth()->user()->nickname;
        $data['email'] = $request->email;
        $data['code'] = $email_code;
        Session::put('e_v_info', $data);
        //Mail::send(new EmailVerification($data));
    }

    public function verifyUserEmail(Request $request)
    {
        $data = Session::get('e_v_info');
        if ($request->code != $data['code']) {
            return response()->json(['error' => 'Verification code not match']);
        }
        $user = User::find(auth()->user()->id);
        $user->email = $data['email'];
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->save();
        return url('user/profile');
    }
}
