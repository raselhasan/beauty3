<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubComment extends Model
{
    public function commenter(){

    	return $this->HasOne('App\User','id','commenter_id');
    }
}
