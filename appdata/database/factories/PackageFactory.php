<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\package;
use Faker\Generator as Faker;

$factory->define(package::class, function (Faker $faker) {
    return [
        'title' => $faker->word(rand(2, 3)),
        'description' => $faker->paragraph(rand(2,5)),
        'type' => rand(1,3),
    ];
});
