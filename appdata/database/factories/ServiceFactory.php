<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Service;
use App\Category;
use App\SubCategory;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
	$cat = Category::get()->random(1)->first();
    if ($cat->subCategories->count()) {
        $total = $cat->subCategories->count();
        $total = $total > 5 ? 5 : $total;
	   $sub = SubCategory::where('category_id', $cat->id)->get()->random(rand(0, $total))->pluck('id')->toJson();
    } else {
        $sub = null;
    }
    return [
        'title' => $faker->word(rand(2,5)),
        'description' => $faker->paragraph(rand(2,5)),
        'image' => 'demo.jpg',
        'address' => $faker->address,
        'sub_category' => $sub,
        'category_id' => $cat->id,

    ];
});
