<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Type;
use Faker\Generator as Faker;

$factory->define(Type::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'price' => $faker->randomElement([10.00, 15.00, 20.00, 25.00, 30.00, 35.00, 40.00, 45.00, 50.00]),
        'time' => $faker->randomELement([30, 60, 90, 120]),
    ];
});
