<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\WorkingDay;
use Faker\Generator as Faker;

$factory->define(WorkingDay::class, function (Faker $faker) {
    return [
        'day' => $faker->randomElement(['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri']),
        'repeat' => $faker->randomElement([0, 1]),
        'end_repeat' => $faker->randomElement([ date('Y-m-d'), null]),
    ];
});
