<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\OffDay;
use Faker\Generator as Faker;

$factory->define(OffDay::class, function (Faker $faker) {
    return [
        'date' => $faker->date('Y-m-d', 'now'),
        'start' => $faker->randomElement([date('H:i:s', rand(1, 26000)), null]),
        'end' => $faker->randomElement([date('H:i:s', rand(1, 26000)), null]),
    ];
});
