<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ServiceGallery;
use Faker\Generator as Faker;

$factory->define(ServiceGallery::class, function (Faker $faker) {
    return [
        'img' => 'demo.jpg',
    ];
});
