<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Route::get('localization/{locale}', 'LocalizationController@index');


Route::get('/service/{page_title?}/{id}', 'IndexController@aboutM')->name('aboutM');

Route::get('/user-profile', 'IndexController@userProfile')->name('userProfile');

//Rasel Route


Route::post('set-visitor-location', 'IndexController@setVisitorLocation');
Route::post('reset-visitor-location', 'IndexController@resetVisitorLocation');
Route::post('set-search-history', 'IndexController@setSearchHistory');
Route::get('reset-session-data', 'IndexController@resetSession');

Route::post('search-service-title', 'IndexController@searchServiceTitle');
Route::get('/select-plan/{id}', 'IndexController@selectPlan')->name('selectPlan');
Route::post('/select-plan/amount-calculation', 'PaymentController@amountCalculation');
Route::post('/select-plan/amount-with-star', 'PaymentController@starAmount');
Route::post('/select-plan/payment', 'PaymentController@payment');
Route::get('/stripe/success', 'PaymentController@paymentSuccess');
Route::get('/stripe/cancel', 'PaymentController@stripeCancel');
Route::get('/paysera/success', 'PaymentController@paymentSuccess');
Route::get('/paysera/cancel', 'PaymentController@payseraCancel');
Route::get('/paysera/callback', 'PaymentController@payseraCallback');
Route::get('payment-list', 'PaymentListController@index');
Route::get('payment-history', 'PaymentListController@paymentHistory');
Route::get('payment-download', 'PaymentListController@paymentDownload');
Route::get('download-service-payment', 'PaymentListController@downloadServicePayment');
Route::get('download-profile-payment/{id}', 'PaymentListController@downloadProfilePayment');
Route::get('download-addcontroll-payment/{id}', 'PaymentListController@downloadAddControllPayment');

Route::get('add-control-payment', 'PaymentController@addControlPayment');
Route::post('add-control-payment/get-price', 'PaymentController@addControlPaymentPrice');

Route::post('add-control-payment/make', 'PaymentController@addControlMakePayment');
Route::get('add-control-payment/success', 'PaymentController@addControllPaymentSuccess');
Route::get('add-control-payment/cancel', 'PaymentController@addControllPaymentCancel');

Route::get('/services/{cat?}/{sub_cat?}/{inner_cat?}/', 'SearchServiceController@index')->name('blog');
Route::post('/services/subcategory', 'SearchServiceController@getSubCategory');
Route::get('/services/index-data', 'SearchServiceController@indexData');
Route::get('/services/pagination-data', 'SearchServiceController@paginationData');
Route::post('/vip-profile-amount', 'PaymentController@getProfileAmount')->name('getProfileAmount');
Route::post('/vip-profile-payment', 'PaymentController@profilePayment')->name('profilePayment');

Route::get('profile-payment/success', 'PaymentController@profilePaymentSuccess');
Route::get('profile-payment/cancel', 'PaymentController@profilePaymentCancel');
Route::get('index-vipdata', 'SearchServiceController@indexVipData');

Route::get('index-vip-services', 'SearchServiceController@indexVipServices');

Route::get('public-profile/{name}/{id}', 'IndexController@publicProfile')->name('publicProfile');
Route::get('public-profile/{id}/service/{title}/{s_id}', 'IndexController@publicService')->name('publicService');
Route::get('get-user-city', 'SearchServiceController@getUserCity');
Route::get('profile-list', 'SearchServiceController@profileList');
Route::post('profile-search', 'SearchServiceController@searchProfile');
Route::post('get-profile-and-service', 'SearchServiceController@profileAndService');
Route::post('active-service', 'PaymentController@activeService');

Route::post('favarite', 'IndexController@favarite');
Route::get('favarite', 'IndexController@getFavarite');

Route::get('in', 'SearchServiceController@innerCategory');

Route::post('recently_view', 'IndexController@recentlyView');
Route::get('live-zone', 'LiveZoneController@index');
Route::get('terms-and-conditions', 'IndexController@termsConditions');
Route::get('privacy-policy', 'IndexController@privacyPolicy');

Route::post('verify-sms', 'Auth\RegisterController@verifyUserSms');
Route::post('verify-email', 'Auth\RegisterController@verifyUserEmail');


// Route::get('sms-verification/{name}/{email}/{password}', 'Auth\RegisterController@vefifySms');
// Route::post('sms-verification', 'Auth\RegisterController@vefifySendedSms');
// Route::get('mail-verification/{email}/{password}', 'Auth\RegisterController@vefifyEmail');
// Route::get('send-sms', 'Auth\RegisterController@sendSms');

Route::post('user/live-zone/upload-image', 'LiveZoneController@UploadImage');
Route::post('user/live-zone/upload-youtube-video', 'LiveZoneController@UploadYourtubeVideo');
Route::post('user/live-zone/save-livezone', 'LiveZoneController@saveLiveZone');
Route::post('user/live-zone/firstlike', 'LiveZoneController@saveFirstLike');
Route::post('user/live-zone/first-comment', 'LiveZoneController@saveFirstComment');
Route::post('user/live-zone/first-comment-popup', 'LiveZoneController@saveFirstCommentPopup');
Route::post('user/live-zone/comment-like', 'LiveZoneController@commentLike');
Route::post('user/live-zone/sub-comment-like', 'LiveZoneController@subCommentLike');
Route::post('user/live-zone/sub-comment', 'LiveZoneController@saveSubComment');
Route::post('user/live-zone/sub-comment-popup', 'LiveZoneController@saveSubCommentPopup');
Route::post('user/live-zone/sub-comment-reply', 'LiveZoneController@saveSubCommentReply');
Route::post('user/live-zone/sub-comment-reply-popup', 'LiveZoneController@saveSubCommentReplyPopup');
Route::post('user/live-zone/see-more-comment', 'LiveZoneController@seeMoreComment');
Route::post('user/live-zone/see-more-comment-popup', 'LiveZoneController@seeMoreCommentPopup');
Route::post('user/live-zone/see-more-sub-comment', 'LiveZoneController@seeMoreSubComment');
Route::post('user/live-zone/see-more-sub-comment-popup', 'LiveZoneController@seeMoreSubCommentPopup');
Route::post('user/service/save-service-comment', 'LiveZoneController@saveServiceComment');
Route::post('user/service/save-service-comment-replay', 'LiveZoneController@saveServiceCommentReplay');
Route::post('user/service/load-more-service-comment', 'LiveZoneController@loadMoreServiceComment');
Route::post('user/live-zone/open-popup', 'LiveZoneController@openPopUp');
Route::get('user/live-zone/pagination', 'LiveZoneController@getPagiantionData');
Route::post('see-more-gallery', 'IndexController@seeMoreGallery');

Route::get('test-sms', 'Auth\RegisterController@testSms');


//end rasel route




Route::namespace('Auth')->group(function () {
	Route::post('registration', 'RegisterController@registration')->name('registration');
});
Route::group(['prefix' => 'user'], function () {

	Route::name('user.')->group(function () {

		Route::post('get-service-gallery-tab', 'IndexController@getServiceGlryTab')->name('getServiceGlryTab');
		Route::namespace('Auth')->group(function () {

			// Route::get('login', 'LoginController@showLoginForm')->name('login');	
			Route::post('login', 'LoginController@login')->name('login.submit');
			Route::post('logout', 'LoginController@logout')->name('logout');
		});
		Route::namespace('User')->group(function () {
			// profile
			Route::post('add-live-zone', 'ProfileController@addLiveZone');
			Route::post('edit-live-zone', 'ProfileController@editLiveZone');
			Route::post('update-live-zone', 'ProfileController@liveZoneUpdate');
			Route::post('delete-live-zone', 'ProfileController@liveZoneDelete');
			Route::post('add-comment', 'CommentController@addComment');
			Route::post('main-comment-replay', 'CommentController@replayMainComment');
			Route::post('send-email-after-login', 'ProfileController@sendUserVcode');
			Route::post('verify-email-after-login', 'ProfileController@verifyUserEmail');


			Route::get('profile', 'ProfileController@index')->name('profile');
			Route::get('booking-list', 'ProfileController@bookingList')->name('bookingList');
			Route::get('services/{service}/types/{type}/bookings/{booking}/cancle', 'ProfileController@cancledBooking');
			Route::post('cancle-booking', 'ProfileController@cancledBook');
			Route::get('calendar/create', 'CalendarController@index')->name('calendar');
			Route::post('calendar/getsession', 'CalendarController@getSession')->name('getSession');
			Route::post('gallery-file-upload', 'ProfileController@galleryFileUp')->name('galleryFileUp');
			Route::post('gallery-file-remove', 'ProfileController@galleryRemover')->name('galleryRemover');
			Route::post('update-user-picture', 'ProfileController@upPrPic');
			Route::post('process-single-tmp-image', 'ProfileController@setProfilePic');
			Route::post('single-tmp-img-remove', 'ProfileController@removeProfilePic');
			Route::post('update-business-hour', 'ProfileController@updateBusinessHour');

			Route::post('cover-image-upload', 'ProfileController@coverImgUpload');
			Route::post('update-cover-img', 'ProfileController@coverImgUpdate');
			Route::post('user-cover-remove', 'ProfileController@userCoverRemover')->name('userCoverRemover');

			Route::post('get-profile-information', 'ProfileController@getProfileInfo');
			Route::post('insert-profile-information', 'ProfileController@insertProfileInfo');
			Route::post('service-address', 'ServicesController@saveServiceAddress')->name('saveServiceAddress');
			Route::get('my-service/{title}/{id}', 'ServicesController@index')->name('myService');
			Route::get('add-my-service', 'ServicesController@add')->name('addMyService');
			Route::post('change-service-details', 'ServicesController@changeServiceDetails')->name('changeServiceDetails');
			Route::post('service-tmp-img', 'ServicesController@serviceTmpImg')->name('serviceTmpImg');
			Route::post('update-service-image', 'ServicesController@updateServiceImg')->name('updateServiceImg');
			Route::post('service-tmp-img-remove', 'ServicesController@serviceTmpImgRemove')->name('serviceTmpImgRemove');
			Route::post('change-service-sub-category', 'ServicesController@changeServiceSubCategory')->name('changeServiceSubCategory');
			Route::post('change-service-inner-category', 'ServicesController@changeServiceInnerCategory')->name('changeServiceInnerCategory');
			Route::post('save-service-category', 'ServicesController@saveServiceCategory')->name('saveServiceCategory');
			Route::post('save-service-gallery-tab', 'ServicesController@saveServiceGlryTab')->name('saveServiceGlryTab');
			Route::post('del-service-gallery-tab', 'ServicesController@delServiceGlryTab')->name('delServiceGlryTab');
			Route::post('update-service-gallery-tab', 'ServicesController@updateServiceGlryTab')->name('updateServiceGlryTab');
			Route::post('add-service-gallery-image', 'ServicesController@addServiceGalleryImg')->name('addServiceGalleryImg');
			Route::post('service-gallery-image-upload', 'ServicesController@serviceGalleryImgUpload')->name('serviceGalleryImgUpload');
			Route::post('service-gallery-image-delete', 'ServicesController@serviceGalleryImgDelete')->name('serviceGalleryImgDelete');
			Route::post('service-gallery-tmp-image-remove', 'ServicesController@serviceGalleryTmpImgRemove')->name('serviceGalleryTmpImgRemove');

			// calendar
			// Route::get('calendar-list', 'CalendarController@calnedarList')->name('calnedarList');
		});
	});
});
Route::get('404-not-found', 'IndexController@errors')->name('404');
Route::get('/user/calendar/{any}', 'User\CalendarController@index')->where('any', '.*');
